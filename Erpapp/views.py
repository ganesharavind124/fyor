from django.shortcuts import render, redirect
from django.http import HttpResponse
from rest_framework.generics import ListAPIView
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.renderers import JSONRenderer
from django.db.models import Q
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.db.models import Sum
from django.http import JsonResponse
# For Mail
from collections import defaultdict
import smtplib, ssl
import imaplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from validate_email import validate_email
from datetime import datetime , timedelta

# csv file uploading
import io, csv

#Base64 importing

import base64
from django.core.files.base import ContentFile

#for distance calculation
# from django.contrib.gis.geos import Point
# from django.contrib.gis.geos import *
from geopy.geocoders import Nominatim
from .utils import haversine_distance
from django.contrib.gis.measure import D

#Mail settings
 
from Staging import settings 
from django.core.mail import send_mail 


# Password Hashing and Validating Password
from django.contrib.auth.hashers import make_password, check_password
from django.contrib.auth.password_validation import validate_password

# Create your views here.
# Importing JWT
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.permissions import DjangoModelPermissions, IsAuthenticated

from Erpapp.models import *
from Erpapp.serializer import *
from django.contrib.auth.models import User,Group,Permission
from django.contrib.contenttypes.models import ContentType
import json

from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.generics import ListAPIView
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.renderers import JSONRenderer
from Staging.settings import REST_FRAMEWORK
from django.db.models import Q
import json
from datetime import date

Base_Url ='http://185.190.140.163:3000'

Backend_Url = 'http://185.190.140.163:8000'


#Base64 importing

import base64
from django.core.files.base import ContentFile

# Password Hashing and Validating Password
from django.contrib.auth.hashers import make_password, check_password
from django.contrib.auth.password_validation import validate_password

#Mail settings
 
from Staging import settings  
from django.core.mail import send_mail 



# newly addedd
from reportlab.pdfgen import canvas
from django.core.mail import EmailMessage
from io import BytesIO
import calendar
from itertools import groupby



# Create your views here.
# Importing JWT
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.permissions import DjangoModelPermissions, IsAuthenticated

from Erpapp.models import (AttandanceManagement, Cadre, Customer, CustomerAddress, Department, Designation, EmployeeAddress, EmployeeAnnualCTC, EmployeeDeduction, EmployeeESI,    
                            EmployeeEarning, EmployeeExperience, EmployeeMaster, EmployeeProfessionalTax, EmployeeProvidentfund, EmployeeQualification, ItemMaster,Company, 
                            Leave, Leavetype, Services,Templates,Chartofaccount, TimeManagement,Voucherdetail)
from Erpapp.serializer import (AttandanceManagementSerialzer, AttandanceManagementSerialzers, CadreSerialzer, CadreSerialzers, ChartofaccountSerialzers, CustomerAddressSerialzer,
                                 CustomerSerialzer, CustomerSerialzers, DepartmentSerialzer, DepartmentSerialzers, DesignationSerialzer, DesignationSerialzers, EmployeeAddressSerialzer, 
                                EmployeeAnnualCTCSerialzer, EmployeeAnnualCTCSerialzers, EmployeeDeductionSerialzer, EmployeeDeductionSerialzers, EmployeeESISerialzer, EmployeeESISerialzers,
                                 EmployeeEarningSerialzer, EmployeeEarningSerialzers, EmployeeExperienceSerialzer, EmployeeMasterSerialzer, EmployeeMasterSerialzers, 
                                EmployeeProfessionalTaxSerialzer, EmployeeProfessionalTaxSerialzers, EmployeeProvidentfundSerialzer, EmployeeProvidentfundSerialzers,
                                 EmployeeQualificationSerialzer, ItemMasterSerialzer, LeaveSerialzer, LeaveSerialzers, LeavetypeSerialzer, LeavetypeSerialzers, ServicesSerialzer,GroupSerialzer,GroupSerialzers, 
                                TimeManagementSerialzer, TimeManagementSerialzers, UserSerializer,CompanyeSerialzer,
                                CompanyeSerialzers,TemplatesSerialzer,ChartofaccountSerialzer,VoucherdetailSerialzer,VoucherdetailSerialzers)
from django.contrib.auth.models import User,Group,Permission




def requestpost(serializer, data):
    try:
        serializer = serializer(data=data)
        # print(serializer.is_valid())
        if serializer.is_valid():
            serializer.save()
            return HttpResponse(JSONRenderer().render(serializer.data), content_type='application/json')
        # print(serializer.errors)
        return HttpResponse(JSONRenderer().render(serializer.errors), status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        # print(e)
        return HttpResponse(JSONRenderer().render({"Error": str(e)}), content_type='application/json',
                            status=status.HTTP_400_BAD_REQUEST)


def requestget(model, serializer, data, page, Operator) -> object:
    try:
        print('requestget')
        startval = (int(page) - 1) * REST_FRAMEWORK['PAGE_SIZE']
        endval = int(page) * REST_FRAMEWORK['PAGE_SIZE']
        if Operator == "and":
            count = model.objects.filter(**data).count()
            models = model.objects.filter(**data)[startval:endval]
        elif Operator == "or":
            count = model.objects.filter(data).count()
            models = model.objects.filter(data)[startval:endval]
        else:
            return HttpResponse(JSONRenderer().render({"Error": "Please send perfect Operator"}),
                                content_type='applicatiom/json',
                                status=status.HTTP_400_BAD_REQUEST)
        results = json.loads(JSONRenderer().render(serializer(models, many=True).data))
        return HttpResponse(JSONRenderer().render({"count": count, "page": page, "results": results}),
                            content_type='application/json')
    except Exception as e:
        return HttpResponse(JSONRenderer().render({"Error": str(e)}), content_type='applicatiom/json',
                            status=status.HTTP_400_BAD_REQUEST)


def requestput(model, serializers, data, filterdata):
    try:
        # print(filterdata)
        models = model.objects.get(**filterdata)
        # print(models)
        serializer = serializers(models, data=data)
        if serializer.is_valid():
            serializer.save()
            return HttpResponse(JSONRenderer().render(serializer.data), content_type='application/json')
        # print(serializer.errors)
        return HttpResponse(JSONRenderer().render(serializer.errors), status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        return HttpResponse(JSONRenderer().render({"Error": str(e)}), content_type='application/json',
                            status=status.HTTP_400_BAD_REQUEST)


def requestpatch(model, serializers, data, filterdata):
    try:
        models = model.objects.get(**filterdata)
        # print(models)
        serializer = serializers(models, data=data, partial=True)
        # print(serializer.is_valid())
        if serializer.is_valid():
            serializer.save()
            return HttpResponse(JSONRenderer().render(serializer.data), content_type='application/json')
        # print(serializer.errors)
        return HttpResponse(JSONRenderer().render(serializer.errors), status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        return HttpResponse(JSONRenderer().render({"Error": str(e)}), content_type='application/json',
                            status=status.HTTP_400_BAD_REQUEST)



#Permission list for backend purpose

class Permissionlist(ListAPIView):
    queryset = Permission.objects.all()
    serializer_class = PermissionSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]
    def get(self,request,*args,**kwargs):
        module = Permission.objects.all()
        serialize = PermissionSerialzer(module,many=True)
        output = JSONRenderer().render(serialize.data)
        return HttpResponse(output,content_type = 'application/json')


#Item master Crud operations
class ItemCRUD(ListAPIView):
    queryset = ItemMaster.objects.all()
    serializer_class = ItemMasterSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            if data=={}:
                raise TypeError
            decoded_data = base64.b64decode(data['Upload_Image'])
            data['Upload_Image'] = ContentFile(decoded_data, name=data['User_Photo_Name'])
            del data['User_Photo_Name']
            serialize = ItemMasterSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['Item_Id']
            module = ItemMaster.objects.get(Item_Id=id)
            serialize = ItemMasterSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            id=data['Item_Id']
            module=ItemMaster.objects.get(Item_Id=id)
            serialize = ItemMasterSerialzer(instance=module,data=data)
            decoded_data = base64.b64decode((data['Upload_Image']))
            data['Upload_Image'] = ContentFile(decoded_data, name=data['User_Photo_Name'])
            del data['User_Photo_Name']
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
            return HttpResponse(str(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    # def delete(self,request,*args,**kwargs):
    #     try:
    #         data=request.query_params
    #         id=data['Item_Id']
    #         module=ItemMaster.objects.filter(Item_Id=id).update(Is_Deleted=True)
    #         # serialize = ItemMasterSerialzer(module,{"Is_Deleted":True})
    #         # if serialize.is_valid():
    #         #     serialize.save()
    #         output = JSONRenderer().render({"message": "Item " + data['Item_Id'] + " is deleted"})
    #         return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
    #     except Exception as e:
    #         output=JSONRenderer().render({"Error":str(e)})
    #         return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
    def delete(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['Item_Id']
            module=ItemMaster.objects.get(Item_Id=id)
            serialize = ItemMasterSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
            
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)




class Itemlist(ListAPIView):
    queryset = ItemMaster.objects.all()
    serializer_class = ItemMasterSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

   # for Getting List Api
    def get_queryset(self, **kwargs):
        
        try:
            Is_Deleted = self.request.GET.get('Is_Deleted')
                # data=False
            
                # Is_Deleted = self.request.GET.get('Is_Deleted')
            module=ItemMaster.objects.filter(Is_Deleted=Is_Deleted).order_by('-id')
            return module
        
        except:
            return ItemMaster.objects.none()
        
                    # data=request
                    # if data =={}:
                    
                # except Exception as e:
                #     output=JSONRenderer().render({"Error":str(e)})
                #     return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
    
class Itemsearch(ListAPIView):
    queryset = ItemMaster.objects.all()
    serializer_class = ItemMasterSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data = self.request.GET.get('search')
        # data = request
        module = ItemMaster.objects.filter(
                Q(Brand__icontains=data) | Q(Item_Name__icontains=data) | Q(Item_GroupName__icontains=data) | Q(categragy__icontains=data) | Q(Unit_Price__icontains=data) | Q(Is_Deleted=False))
        return module


# For Filter APi
class Itemfilter(ListAPIView):
    queryset = ItemMaster.objects.all()
    serializer_class = ItemMasterSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]
    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            # data = request
            data1 = {"Brand":data['Brand'],"Item_Name":data['Item_Name'],"Item_GroupName":data['Item_GroupName'],"categragy":data['categragy'],"Is_Batch":data['Is_Batch'],"Is_Deleted":False,}
            return requestget(model=ItemMaster,serializer=ItemMasterSerialzers,data=data1,page=data['page'],Operator='and')
        except Exception as e:
            return HttpResponse(JSONRenderer().render({"Error": str(e)}), content_type='application/json',
                                status=status.HTTP_417_EXPECTATION_FAILED)

class ItemMyListAdditional(ListAPIView):
    queryset = ItemMaster.objects.all()
    serializer_class = ItemMasterSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        try:
            Company_Id = self.request.GET.get('Company_Id')
            Created_By = self.request.GET.get('Created_By')
            return ItemMaster.objects.filter(Q(Company_Id=Company_Id) & Q(Created_By=Created_By) &
                                           Q(Is_Deleted__exact=False)).order_by('-id')
        except:
            return ItemMaster.objects.none()

class Itemlistwithoutpagination(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = ItemMaster.objects.filter(Q(Company_Id=data['Company_Id']) & Q(Partner_Id=data['Partner_Id']) & Q(Is_Deleted=False)).order_by('-id')
            serialize = ItemMasterSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

# for front end purpose
# class Itemlistunderthestore(APIView):
#     def get(self,request,*args,**kwargs):
#         try:
#             data=request.query_params
#             module = Storemaster.objects.filter()


class ItemCategoryCRUD(ListAPIView):
    queryset = ItemCategory.objects.all()
    serializer_class = ItemCategorySerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            serialize = ItemCategorySerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['Item_Category_Id']
            module = ItemCategory.objects.get(Item_Category_Id=id)
            serialize = ItemCategorySerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            id=data['Item_Category_Id']
            module=ItemCategory.objects.get(Item_Category_Id=id)
            serialize = ItemCategorySerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
            return HttpResponse(str(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['Item_Category_Id']
            module=ItemCategory.objects.get(Item_Category_Id=id)
            serialize = ItemCategorySerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
            
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class ItemCategorylist(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = ItemCategory.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Is_Deleted=False))
            serialize = ItemCategorySerialzers(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)


# class ItemGrouplist(APIView):
#         def get(self,request,*args,**kwargs):
#         try:
#             data=request.query_params
#             module = ItemGroup.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Is_Deleted=False))
#             serialize = ItemCategorySerialzer(module,many=True)
#             output = JSONRenderer().render(serialize.data)
#             return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
#         except Exception as e:
#             output = JSONRenderer().render({"Error":str(e)})
#             return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)

class RoleCRUD(ListAPIView):
    queryset = Group.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data = request.data
            items = Group.objects.filter(Q(name__exact=data['name']))
            for item in items:
                print(item)
                return HttpResponse(JSONRenderer().render({"Error": data['name'] + " is already exists"}), content_type='application/json', status=status.HTTP_400_BAD_REQUEST)
            serialize = GroupSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_201_CREATED)
        except Exception as e:
            output = JSONRenderer().render({"error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Role_Id']
            module = Group.objects.get(Role_Id=id)
            serialize = GroupSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Role_Id']
            module=Group.objects.get(Role_Id=id)
            serialize = GroupSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Role_Id']
            module=Group.objects.get(Role_Id=id)
            serialize = GroupSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class RoleAdditionalCRUD(ListAPIView):
    queryset = Group.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]
    # filter api
    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            module = Group.objects.filter(
                Q(name__icontains=data['name']), Q(Is_Deleted=False))
            serialize = GroupSerialzer(module, many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)
    # search api
    def put(self, request, *args, **kwargs):
        try:
            data = request.data
            module = Group.objects.filter(Q(name__icontains=data['Search']) | Q(
                Role_Id__icontains=data['Search']), Q(Is_Deleted=False))
            serialize = GroupSerialzer(module, many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)


class Rolelist(ListAPIView):
    queryset = Group.objects.all()
    serializer_class = GroupSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        module=Group.objects.filter(Is_Deleted=False)
        return module

class Inviteuser(APIView):

    def post(self,request,*args,**kwargs):
        data=request.data  
        subject = data['subject'] 
        msg     = data['msg']
        to      = data['to']
        res     = send_mail(subject, msg, settings.EMAIL_HOST_USER, [to])
        if(res == 1):  
            msg = "Mail Sent Successfuly"  
        else:  
            msg = "Mail could not sent"  
        return HttpResponse(msg)


class Usersignup(ListAPIView):
    queryset = User.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            decoded_data = base64.b64decode((data['User_Photo']))
            data['User_Photo'] = ContentFile(decoded_data, name=data['User_Photo_Name'])
            print(type(data['User_Photo']))
            del data['User_Photo_Name']
            validate_password(password=data['password'], user=User)
            data['password'] = make_password(data['password'])
            serialize = UserSerializer(data=data)
            if serialize.is_valid():
                serialize.save()
            output=JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['username']
            module=User.objects.get(username=id)
            serialize = UserSerializer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)      


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['id']
            module=User.objects.get(id=id)
            # if data['User_Photo'] != '':
            #     decoded_data = base64.b64decode((data['User_Photo']))
            #     data['User_Photo'] = ContentFile(decoded_data, name=data['User_Photo_Name'])
            #     del data['User_Photo_Name']
            # validate_password(password=data['password'], user=User)
            # data['password'] = make_password(data['password'])
            serialize = UserSerializer(instance=module,data=data,partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['User_Id']
            module = User.objects.get(User_Id=id)
            serialize=UserSerializer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class UsersignupAdditional(ListAPIView):
    queryset = User.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    # filter api
    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            module = User.objects.filter(
                Q(Business_Unit__icontains=data['Business_Unit']) , Q(Is_Deleted=False))
            serialize = UserSerializer(module, many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)

    # search api
    def put(self, request, *args, **kwargs):
        try:
            data = request.data
            module = User.objects.filter( Q(
                Business_Unit__icontains=data['Search']) | Q(email__icontains=data['Search']) | Q(username__exact=data['Search']), Q(Is_Deleted=False))
            serialize = UserSerializer(module, many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)


class Userlist(ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data=self.request.data
        module=User.objects.filter(Is_Deleted=False).order_by('User_Id')
        return module

# Settings Module API's

class CompanyCRUD(ListAPIView):
    queryset = Company.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]
    def post(self,request,*args,**kwargs):
        try:
            data = request.data
            decoded_data = base64.b64decode(data['CompanyLogo'])
            data['CompanyLogo'] = ContentFile(decoded_data, name=data['CompanyLogo_Name'])
            del data['CompanyLogo_Name']
            # Serialize Communication_Address
            Comserialize = Communication_AddressSerialzer(data=data['CompanyAddress'])
            if Comserialize.is_valid():
                Comserialize.save()  # Save and get the instance
            Address=Comserialize.data
            print(Address)
            data['CompanyAddress']=Address['id']
            # Serialize Business_Hours
            Busserialize = Business_HoursSerialzer(data=data['Business_Hour'])
            if Busserialize.is_valid():
                Busserialize.save()  # Save and get the instance
            Hour = Busserialize.data
            data['Business_Hour']=Hour['id']
            # Serialize Financial_Calendar_Details
            Finserialize = Financial_Calendar_DetailsSerialzer(data=data['Fiscal_Year'])
            if Finserialize.is_valid():
                Finserialize.save()  # Save and get the instance
            Finance = Finserialize.data
            data['Fiscal_Year']=Finance['id']
            # Serialize Company
            serialize = CompanySerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            return HttpResponse({"error": str(e)}, status=status.HTTP_400_BAD_REQUEST)


    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Created_By']
            module = Company.objects.get(Created_By=id)
            serialize = CompanySerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data = request.data
            id = data['id']
            CompanyAddress = data['CompanyAddress']
            Businesshours_Id = data['Business_Hour']
            Calendar_Id = data['Fiscal_Year']
            module = Communication_Address.objects.get(id=CompanyAddress['id'])
            serialize = Communication_AddressSerialzer(instance=module, data=data['CompanyAddress'])
            if serialize.is_valid():
                serialize.save()
            out1=serialize.data
            module1 = Business_Hours.objects.get(id=Businesshours_Id['id'])
            serialize1 = Business_HoursSerialzer(instance=module1, data=data['Business_Hour'])
            if serialize1.is_valid():
                serialize1.save()
            out2 = serialize1.data
            module2 = Financial_Calendar_Details.objects.get(id=Calendar_Id['id'])
            serialize2 = Financial_Calendar_DetailsSerialzer(instance=module2, data=Calendar_Id)
            if serialize2.is_valid():
                serialize2.save()
            out = serialize2.data
            data['CompanyAddress'] = out1['id']
            data['Fiscal_Year'] = out['id']
            data['Business_Hour'] =out2['id']
            module3 = Company.objects.get(id=id)
            if data['CompanyLogo'] !='':
                decoded_data = base64.b64decode(data['CompanyLogo'])
                data['CompanyLogo'] = ContentFile(decoded_data, name=data['CompanyLogo_Name'])
                del data['CompanyLogo_Name']
            serialize3 = CompanySerialzer(instance=module3, data=data)
            if serialize3.is_valid():
                serialize3.save()
            output = JSONRenderer().render(serialize3.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)


class Companylist(APIView):
    def get(self,request,*args,**kwargs):
        try:
            module = Company.objects.filter(Is_Deleted=False).order_by('-id')
            serialize = CompanyeSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class Companygetwithouttoken(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Created_By']
            module = Company.objects.get(Created_By=id)
            serialize = CompanySerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class TemplatesCRUD(ListAPIView):
    queryset = Templates.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = TemplatesSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Template_Id']
            module = Templates.objects.get(Template_Id=id)
            serialize = TemplatesSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Template_Id']
            module=Templates.objects.get(Template_Id=id)
            serialize = TemplatesSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Template_Id']
            module=Templates.objects.get(Template_Id=id)
            serialize = TemplatesSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Templatelist(ListAPIView):
    queryset = Templates.objects.all()
    serializer_class = TemplatesSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        module=Templates.objects.filter(Is_Deleted=False)
        return module


class WorkflowCRUD(ListAPIView):
    queryset = Workflow.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = WorkflowSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Workflow_Id']
            module = Workflow.objects.get(Workflow_Id=id)
            serialize = WorkflowSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Workflow_Id']
            module=Workflow.objects.get(Workflow_Id=id)
            serialize = WorkflowSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Workflow_Id']
            module=Workflow.objects.get(Workflow_Id=id)
            serialize = WorkflowSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Workflowlist(ListAPIView):
    queryset = Workflow.objects.all()
    serializer_class = WorkflowSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        module=Workflow.objects.filter(Is_Deleted=False)
        return module

class WorkflowRuleCRUD(ListAPIView):
    queryset = WorkflowRule.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = WorkflowRuleSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['WorkflowRule_Id']
            module = WorkflowRule.objects.get(WorkflowRule_Id=id)
            serialize = WorkflowRuleSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['WorkflowRule_Id']
            module=WorkflowRule.objects.get(WorkflowRule_Id=id)
            serialize = WorkflowRuleSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['WorkflowRule_Id']
            module=WorkflowRule.objects.get(WorkflowRule_Id=id)
            serialize = WorkflowRuleSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class WorkflowRulelist(ListAPIView):
    queryset = WorkflowRule.objects.all()
    serializer_class = WorkflowRuleSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        module=WorkflowRule.objects.filter(Is_Deleted=False)
        return module


# front end purpose we created this api.
class Rulefilter(ListAPIView):
    queryset = WorkflowRule.objects.all()
    serializer_class = WorkflowRuleSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        data=request.query_params
        Rule_Name=data['Rule_Name']
        module = WorkflowRule.objects.filter(Q(Rule_Name=Rule_Name) & Q(Is_Deleted=False))
        serialize = WorkflowRuleSerialzer(module,many=True)
        output = JSONRenderer().render(serialize.data)
        return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)



class AssignworkflowCRUD(ListAPIView):
    queryset = Assignworkflow.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = AssignworkflowSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Assignworkflow_Id']
            module = Assignworkflow.objects.get(Assignworkflow_Id=id)
            serialize = AssignworkflowSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Assignworkflow_Id']
            module=Assignworkflow.objects.get(Assignworkflow_Id=id)
            serialize = AssignworkflowSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Assignworkflow_Id']
            module=Assignworkflow.objects.get(Assignworkflow_Id=id)
            serialize = AssignworkflowSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Assignworkflowlist(ListAPIView):
    queryset = Assignworkflow.objects.all()
    serializer_class = AssignworkflowSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        module=Assignworkflow.objects.filter(Is_Deleted=False)
        return module

#for front end purpose

class Assignworkflowdropdwonlist(ListAPIView):
    queryset = Assignworkflow.objects.all()
    serializer_class = AssignworkflowSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        module=Assignworkflow.objects.filter(Is_Deleted=False)
        serialize = AssignworkflowSerialzer(module,many=True)
        output = JSONRenderer().render(serialize.data)
        return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)

class WorkflowNameCRUD(ListAPIView):
    queryset = WorkflowName.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = WorkflowNameSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['WorkflowName_Id']
            module = WorkflowName.objects.get(WorkflowName_Id=id)
            serialize = WorkflowNameSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['WorkflowName_Id']
            module=WorkflowName.objects.get(WorkflowName_Id=id)
            serialize = WorkflowNameSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['WorkflowName_Id']
            module=WorkflowName.objects.get(WorkflowName_Id=id)
            serialize = WorkflowNameSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class WorkflowNamelist(ListAPIView):
    queryset = WorkflowName.objects.all()
    serializer_class = WorkflowNameSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        module=WorkflowName.objects.filter(Is_Deleted=False)
        return module

# for front end purpose
class WorkflowNamefilter(ListAPIView):
    queryset = WorkflowName.objects.all()
    serializer_class = WorkflowNameSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        data=request.query_params
        Workflow_Name=data['Workflow_Name']
        module=WorkflowName.objects.filter(Workflow_Name=Workflow_Name,Is_Deleted=False)
        serialize = WorkflowNameSerialzer(module,many=True)
        output = JSONRenderer().render(serialize.data)
        return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)

# for front end purpose
class Assignworkflowfilter(ListAPIView):
    queryset = Assignworkflow.objects.all()
    serializer_class = AssignworkflowSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        data=request.query_params
        Document_Name=data['Document_Name']
        module=Assignworkflow.objects.filter(Document_Name=Document_Name,Is_Deleted=False)
        serialize = AssignworkflowSerialzer(module,many=True)
        output = JSONRenderer().render(serialize.data)
        return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)

class workflowfilter(ListAPIView):
    queryset = Workflow.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Document_Name=data['Document_Name']
            module = Workflow.objects.filter(Document_Name=Document_Name)
            serialize = WorkflowSerialzers(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_404_NOT_FOUND)


class WorkfloworiginCRUD(ListAPIView):
    queryset = Workfloworigin.objects.all()
    serializer_class = WorkfloworiginSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = WorkfloworiginSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Workfloworigin_Id']
            module = Workfloworigin.objects.get(Workfloworigin_Id=id)
            serialize = WorkfloworiginSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Workfloworigin_Id']
            module=Workfloworigin.objects.get(Workfloworigin_Id=id)
            serialize = WorkfloworiginSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Workfloworigin_Id']
            module=Workfloworigin.objects.get(Workfloworigin_Id=id)
            serialize = WorkfloworiginSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Workfloworiginlist(ListAPIView):
    queryset = Workfloworigin.objects.all()
    serializer_class = WorkfloworiginSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        module=Workfloworigin.objects.filter(Is_Deleted=False)
        return module

class Workfloworiginfilter(ListAPIView):
    queryset = Workfloworigin.objects.all()
    serializer_class = WorkfloworiginSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        data=request.query_params
        Form_Name=data['Form_Name']
        module = Workfloworigin.objects.filter(Form_Name=Form_Name)
        serialize = WorkfloworiginSerialzer(module,many=True)
        output = JSONRenderer().render(serialize.data)
        return HttpResponse(output,content_type='application/json')



class Formmasterfilter(ListAPIView):
    queryset = FormPermission.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            FormName=data['FormName']
            module = FormPermission.objects.filter(FormName=FormName)
            serialize = FormPermissionSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class SlottimeCRUD(ListAPIView):
    queryset = Services.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = ServicesSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Service_Id']
            module = Services.objects.get(Service_Id=id)
            serialize = ServicesSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Service_Id']
            module=Services.objects.get(Service_Id=id)
            serialize = ServicesSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Service_Id']
            module=Services.objects.get(Service_Id=id)
            serialize = ServicesSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


# Transactionapprovalpreference API's

class TransactionapprovalCRUD(ListAPIView):
    queryset = Transactionapprovalpreference.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = TransactionapprovalpreferenceSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Created_By']
            module = Transactionapprovalpreference.objects.get(Created_By=id)
            serialize = TransactionapprovalpreferenceSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            Transactionapprovalpreference_Id=data['Transactionapprovalpreference_Id']
            module=Transactionapprovalpreference.objects.get(Transactionapprovalpreference_Id=Transactionapprovalpreference_Id)
            serialize = TransactionapprovalpreferenceSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


# Item Preferences API's

class ItempreferenceCRUD(ListAPIView):
    queryset = Itempreference.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = ItempreferenceSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Created_By=data['Created_By']
            module = Itempreference.objects.get(Created_By=Created_By)
            serialize = ItempreferenceSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Itempreference_Id']
            module=Itempreference.objects.get(Itempreference_Id=id)
            serialize = ItempreferenceSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


# def SendMail1(subject, From, To,Host, Port, Password):
#     mail = MIMEMultipart('mixed')
#     mail['subject'] = subject
#     mail['From'] = From
#     mail['To'] = To
#     mail['Disposition-Notification-To'] = From
#     mail['Return-Receipt-To'] = From
#     context = ssl.create_default_context()
#     with smtplib.SMTP_SSL(Host, Port, context=context) as server:
#         server.login(From, Password)
#         status = server.sendmail(From, To, mail.as_string())
#         server.close()
#     if status == {}:
#         return True
#     else:
#         return False


class Itemmailnotification(APIView):
    def post(self,request,*args,**kwargs):
        try:
            data = request.data
            Created_By=data['Created_By']
            usermod=MailConfiguration.objects.get(Created_By=Created_By)
            serialize = MailConfigurationSerializer(usermod)
            usermod1 = serialize.data
            Fromemail = usermod1['MailId']
            module = Itempreference.objects.get(Created_By=Created_By)
            serialize = ItempreferenceSerialzer(module)
            module1 = serialize.data
            Tomail = module1['Notifyto']
            RecevierMailIdVaild = validate_email(Tomail, verify=False)
            if RecevierMailIdVaild:
                response = SendMail(subject=data['mailsubject'], From=Fromemail, To=Tomail,
                                        Body=data['body'],Host=usermod1['SMTPHost'], Port=usermod1['SMTPPort'],
                                    Password=data['MailPassword'])
                if response:
                    return HttpResponse(JSONRenderer().render({"Message": "Mail sent Successfully"}),
                                        content_type='application/json', status=status.HTTP_200_OK)
                else:
                    return HttpResponse(JSONRenderer().render({"Message": "User Mail id is not valid"}),
                                        content_type='application/json', status=status.HTTP_400_BAD_REQUEST)
            else:
                return HttpResponse(JSONRenderer().render({"Message": "User Mail id is not valid"}),
                                    content_type='application/json', status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            if str(e) == "MailConfiguration matching query does not exist.":
                return HttpResponse(JSONRenderer().render({"Message": "Please Setup Mail First"}),
                                    content_type='application/json', status=status.HTTP_400_BAD_REQUEST)
            return HttpResponse(JSONRenderer().render({"Error": str(e)}), content_type='application/json',
                                status=status.HTTP_417_EXPECTATION_FAILED)


class AdditionalItempreferenceList(ListAPIView):
    queryset = Itempreference.objects.all()
    serializer_class = ItempreferenceSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]
    def get_queryset(self):
        try:
            module = Itempreference.objects.filter(Is_Deleted=False)
            return module
        except:
            module = Itempreference.objects.none()
            return module


# Salesorderpreference API's
class SalesorderpreferenceCRUD(ListAPIView):
    queryset = Salesorderpreference.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = SalesorderpreferenceSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Created_By=data['Created_By']
            module = Salesorderpreference.objects.get(Created_By=Created_By)
            serialize = SalesorderpreferenceSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Salesorderpreference_Id']
            module=Salesorderpreference.objects.get(Salesorderpreference_Id=id)
            serialize = SalesorderpreferenceSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


#Invoicepreference API's

class InvoicepreferenceCRUD(ListAPIView):
    queryset = Invoicepreference.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = InvoicepreferenceSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Created_By=data['Created_By']
            module = Invoicepreference.objects.get(Created_By=Created_By)
            serialize = InvoicepreferenceSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Invoicepreference_Id']
            module=Invoicepreference.objects.get(Invoicepreference_Id=id)
            serialize = InvoicepreferenceSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Invoicemailnotification(APIView):
    def post(self,request,*args,**kwargs):
        try:
            data = request.data
            Created_By=data['Created_By']
            usermod=MailConfiguration.objects.get(Created_By=Created_By)
            serialize = MailConfigurationSerializer(usermod)
            usermod1 = serialize.data
            Fromemail = usermod1['MailId']
            module = Invoicepreference.objects.get(Created_By=Created_By)
            serialize = InvoicepreferenceSerialzer(module)
            module1 = serialize.data
            Tomail = module1['Email']
            RecevierMailIdVaild = validate_email(Tomail, verify=False)
            if RecevierMailIdVaild:
                response = SendMail(subject=data['mailsubject'], From=Fromemail, To=Tomail,
                                    Body=data['body'], Host=usermod1['SMTPHost'], Port=usermod1['SMTPPort'],
                                    Password=data['MailPassword'])
                if response:
                    return HttpResponse(JSONRenderer().render({"Message": "Mail sent Successfully"}),
                                        content_type='application/json', status=status.HTTP_200_OK)
                else:
                    return HttpResponse(JSONRenderer().render({"Message": "User Mail id is not valid"}),
                                        content_type='application/json', status=status.HTTP_400_BAD_REQUEST)
            else:
                return HttpResponse(JSONRenderer().render({"Message": "User Mail id is not valid"}),
                                    content_type='application/json', status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            if str(e) == "MailConfiguration matching query does not exist.":
                return HttpResponse(JSONRenderer().render({"Message": "Please Setup Mail First"}),
                                    content_type='application/json', status=status.HTTP_400_BAD_REQUEST)
            return HttpResponse(JSONRenderer().render({"Error": str(e)}), content_type='application/json',
                                status=status.HTTP_417_EXPECTATION_FAILED)


# Purchasepreference API's

class PurchasepreferenceCRUD(ListAPIView):
    queryset = Purchasepreference.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = PurchasepreferenceSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Created_By=data['Created_By']
            module = Purchasepreference.objects.get(Created_By=Created_By)
            serialize = PurchasepreferenceSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Purchasepreference_Id']
            module=Purchasepreference.objects.get(Purchasepreference_Id=id)
            serialize = PurchasepreferenceSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Purchasemailnotification(APIView):
    def post(self,request,*args,**kwargs):
        try:
            data = request.data
            Created_By=data['Created_By']
            usermod=MailConfiguration.objects.get(Created_By=Created_By)
            serialize = MailConfigurationSerializer(usermod)
            usermod1 = serialize.data
            Fromemail = usermod1['MailId']
            module = Purchasepreference.objects.get(Created_By=Created_By)
            serialize = PurchasepreferenceSerialzer(module)
            module1 = serialize.data
            Tomail = module1['Notifyto']
            RecevierMailIdVaild = validate_email(Tomail, verify=False)
            if RecevierMailIdVaild:
                response = SendMail(subject=data['mailsubject'], From=Fromemail, To=Tomail,
                                    Body=data['body'], Host=usermod1['SMTPHost'], Port=usermod1['SMTPPort'],
                                    Password=data['MailPassword'])
                if response:
                    return HttpResponse(JSONRenderer().render({"Message": "Mail sent Successfully"}),
                                        content_type='application/json', status=status.HTTP_200_OK)
                else:
                    return HttpResponse(JSONRenderer().render({"Message": "User Mail id is not valid"}),
                                        content_type='application/json', status=status.HTTP_400_BAD_REQUEST)
            else:
                return HttpResponse(JSONRenderer().render({"Message": "User Mail id is not valid"}),
                                    content_type='application/json', status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            if str(e) == "MailConfiguration matching query does not exist.":
                return HttpResponse(JSONRenderer().render({"Message": "Please Setup Mail First"}),
                                    content_type='application/json', status=status.HTTP_400_BAD_REQUEST)
            return HttpResponse(JSONRenderer().render({"Error": str(e)}), content_type='application/json',
                                status=status.HTTP_417_EXPECTATION_FAILED)





# Budgetpreference API's

class BudgetpreferenceCRUD(ListAPIView):
    queryset = Budgetpreference.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = BudgetpreferenceSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Created_By=data['Created_By']
            module = Budgetpreference.objects.get(Created_By=Created_By)
            serialize = BudgetpreferenceSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Budgetpreference_Id']
            module=Budgetpreference.objects.get(Budgetpreference_Id=id)
            serialize = BudgetpreferenceSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


# Inventorypreference API's

class InventorypreferenceCRUD(ListAPIView):
    queryset = Inventorypreference.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = InventorypreferenceSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Created_By=data['Created_By']
            module = Inventorypreference.objects.get(Created_By=Created_By)
            serialize = InventorypreferenceSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Inventorypreference_Id']
            module=Inventorypreference.objects.get(Inventorypreference_Id=id)
            serialize = InventorypreferenceSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



# Finance Module API's
 
class ChartofaccountCRUD(ListAPIView):
    queryset = Chartofaccount.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = ChartofaccountSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Account_Id']
            module = Chartofaccount.objects.get(Account_Id=id)
            serialize = ChartofaccountSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Account_Id']
            module=Chartofaccount.objects.get(Account_Id=id)
            serialize = ChartofaccountSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Account_Id']
            module=Chartofaccount.objects.get(Account_Id=id)
            serialize = ChartofaccountSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class Chartofaccountmylist(ListAPIView):
    queryset = Chartofaccount.objects.all()
    serializer_class = ChartofaccountSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        id=self.request.GET.get('Created_By')
        module=Chartofaccount.objects.filter(Q(Created_By=id) & Q(Is_Deleted=False)).order_by('-id')
        return module


class chartofaccountlist(ListAPIView):
    queryset=Chartofaccount
    serializer_class=ChartofaccountSerialzers
    authentication_classes=[JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        module = Chartofaccount.objects.filter(Is_Deleted=False).order_by('-id')
        return module


class Partnerchartofaccount(ListAPIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Chartofaccount.objects.filter(Q(Partner_ID=data['Partner_Id']) & Q(Is_Deleted=False))
            serialize = ChartofaccountSerialzers(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST) 




class Chartaccountfilter(ListAPIView):
    queryset = Chartofaccount.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            data1 = {"Account_Name__icontains":data['Account_Name'],"Account_Type__icontains":data['Account_Type'],"Account_Group__icontains":data['Account_Group'],
                "Ledger_Type__icontains":data['Ledger_Type'],"Created_Date__range":data['Created_Date'],"Is_Deleted":False}
            return requestget(model=Chartofaccount,serializer=ChartofaccountSerialzers,data=data1,page=data['page'],Operator='and')
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class chartaccountsearch(ListAPIView):
    queryset = Chartofaccount.objects.all()
    serializer_class = ChartofaccountSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        # try:
        data=self.request.GET.get('search')
        module=Chartofaccount.objects.filter(Account_Name__icontains=data,Is_Deleted=False).order_by('-id')
        return module
        # except Exception as e:
        #     output=JSONRenderer().render({"Error":str(e)})
        #     return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class BankCRUD(ListAPIView):
    queryset = Bank.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize=BankSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Bank_Id=data['Bank_Id']
            module = Bank.objects.get(Bank_Id=Bank_Id)
            serialize = BankSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            Bank_Id=data['Bank_Id']
            module = Bank.objects.get(Bank_Id=Bank_Id)
            serialize = BankSerialzer(module,data=request.data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)       


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Bank_Id=data['Bank_Id']
            module = Bank.objects.get(Bank_Id=Bank_Id)
            serialize = BankSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class Banklist(ListAPIView):
    queryset = Bank.objects.all()
    serializer_class=BankSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get_queryset(self):
        data=self.request.GET.get('Company_Id')
        module = Bank.objects.filter(Q(Company_Id=data) & Q(Is_Deleted=False)).order_by('-id')
        return module


class Bankmylist(ListAPIView):
    queryset = Bank.objects.all()
    serializer_class=BankSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get_queryset(self):
        Company_Id=self.request.GET.get('Company_Id')
        Created_By = self.request.GET.get('Created_By')
        module = Bank.objects.filter(Q(Company_Id=Company_Id) & Q(Created_By=Created_By) & Q(Is_Deleted=False)).order_by('-id')
        return module


#This API used for front end purpose.
class Chartofaccountid(ListAPIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Account_Name']
            module = Chartofaccount.objects.filter(Account_Name=id)
            serialize = ChartofaccountSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class AcctgroupCRUD(ListAPIView):
    queryset = Acctgroup.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = AcctgroupSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['AcctgroupId']
            module = Acctgroup.objects.get(AcctgroupId=id)
            serialize = AcctgroupSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['AcctgroupId']
            module=Acctgroup.objects.get(AcctgroupId=id)
            serialize = AcctgroupSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class Acctgrouplist(ListAPIView):
    queryset = Acctgroup.objects.all()
    serializer_class=AcctgroupSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]   

    def get_queryset(self):
        module = Acctgroup.objects.filter(Is_Deleted = False)
        return module


      
def Journal_I(Company_Id):
    Company_Id=Company_Id
    module = Transactionalseries.objects.filter(Company_Id=Company_Id)
    serialize = TransactionalseriesSerialzer(module,many=True)
    prefix = serialize.data
    Organizatio=Journal.objects.all().order_by('Journal_Id').last()
    if not Organizatio:
        return prefix['Journal_vouchar'] + "000000001"
    Organizati = Organizatio.Journal_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = prefix['Journal_vouchar'] + str(Organiza).zfill(9)
    return Configur


class JournalCRUD(ListAPIView):
    queryset = Journal.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Company_Id=data['Company_Id']
            module = Transactionalseries.objects.get(Company_Id=Company_Id)
            serialize = TransactionalseriesSerialzer(module)
            prefix=JSONRenderer().render(serialize.data)
            pre=json.loads(prefix)
            out=pre['Journal_vouchar']
            Organizatio=Journal.objects.all().order_by('Journal_Id').last()
            if Organizatio is None:
                Journals=out + "000000001"
                data['Journal_Id']=Journals
            else:
                Organizati = Organizatio.Journal_Id
                Organizat=int(Organizati[4:])
                Organiza=Organizat + 1
                Configur = out + str(Organiza).zfill(9)
                data['Journal_Id']=Configur
            serialize = JournalSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Journal_Id']
            module = Journal.objects.get(Journal_Id=id)
            serialize = JournalSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Journal_Id']
            module=Journal.objects.get(Journal_Id=id)
            serialize = JournalSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Journal_Id']
            module=Journal.objects.get(Journal_Id=id)
            serialize = JournalSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class Journalmylist(ListAPIView):
    queryset = Journal.objects.all()
    serializer_class = JournalSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        id=self.request.GET.get('Created_By')
        module=Journal.objects.filter(Q(Created_By=id) & Q(Is_Deleted=False)).order_by('-id')
        return module


class Journallist(ListAPIView):
    queryset = Journal.objects.all()
    serializer_class = JournalSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        module=Journal.objects.filter(Is_Deleted=False).order_by('-id')
        return module


class Journalfilter(ListAPIView):
    queryset = Journal.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        data=request.data
        data1 = {"Journal_Id__icontains":data['Journal_Id'],
            "Created_Date__range":data['Created_Date'],"Is_Deleted":False}
        return requestget (model=Journal,serializer=JournalSerialzers,data=data1,page=data['page'],Operator='and')

class Journalsearch(ListAPIView):
    queryset = Journal.objects.all()
    serializer_class = JournalSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data=self.request.GET.get('search')
        module=Journal.objects.filter(Q(Account__Account_Name__icontains=data) | 
                Q(Journal_Name__icontains=data),Is_Deleted=False).order_by('-id')
        return module




class VoucherdetailCRUD(ListAPIView):
    queryset = Voucherdetail.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Company_Id=data['Company_Id']
            module = Transactionalseries.objects.get(Company_Id=Company_Id)
            serialize = TransactionalseriesSerialzer(module)
            prefix=JSONRenderer().render(serialize.data)
            pre=json.loads(prefix)
            out=pre['Cash_Vouchar']
            Organizatio=Voucherdetail.objects.all().order_by('Voucher_Id').last()
            if Organizatio is None:
                Journals=out + "000000001"
                data['Voucher_Id']=Journals
            else:
                Organizati = Organizatio.Voucher_Id
                Organizat=int(Organizati[4:])
                Organiza=Organizat + 1
                Configur = out + str(Organiza).zfill(9)
                data['Voucher_Id']=Configur
            serialize = VoucherdetailSerialzer(data=data)
            # Account=data['Account']
            # Account1=[]
            # TotalAmount=[]
            # for i in Account:
            #     data1={"Account_Name":i['Account_Name'],"Account_Id":i['Account_Id'],"Debit":i['Amount']}
            #     data2={"Account_Name"}
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Voucher_Id']
            module = Voucherdetail.objects.get(Voucher_Id=id)
            serialize = VoucherdetailSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Voucher_Id']
            module=Voucherdetail.objects.get(Voucher_Id=id)
            serialize = VoucherdetailSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Voucher_Id']
            module=Voucherdetail.objects.get(Voucher_Id=id)
            serialize = VoucherdetailSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class Voucherdetailmylist(ListAPIView):
    queryset = Voucherdetail.objects.all()
    serializer_class = VoucherdetailSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        id=self.request.GET.get('Created_By')
        module=Voucherdetail.objects.filter(Q(Created_By=id) & Q(Is_Deleted=False)).order_by('-id')
        return module


class Voucherdetaillist(ListAPIView):
    queryset=Voucherdetail
    serializer_class=VoucherdetailSerialzer
    authentication_classes=[JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        module = Voucherdetail.objects.filter(Is_Deleted=False).order_by('-id')
        return module


class Voucherdetailfilter(ListAPIView):
    queryset = Voucherdetail.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            data1 = {"Bank_Or_Cash":data['Bank_Or_Cash'],"Bank_Name__icontains":data['Bank_Name'],
                "Created_Date__range":data['Created_Date'],"Is_Deleted":False}
            return requestget (model=Voucherdetail,serializer=VoucherdetailSerialzers,data=data1,page=data['page'],Operator='and')
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class Voucherdetailsearch(ListAPIView):
    queryset = Voucherdetail.objects.all()
    serializer_class = VoucherdetailSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data=self.request.GET.get('search')
        module=Voucherdetail.objects.filter(Q(Voucher_Type__icontains=data) | 
                Q(Bank_Name__icontains=data),Is_Deleted=False).order_by('-id')
        return module


class Voucherdetailbankname(ListAPIView):
    queryset = Voucherdetail.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]
    def post(self,request,*args,**kwargs):
        try:
           data=request.data
           module = Voucherdetail.objects.filter(Q(Bank_Or_Cash=True) & Q(Voucher_Date__range=data['Voucher_Date']) & Q(Bank_Name=data['Bank_Name']) & Q(Is_Deleted=False))
           serialize = VoucherdetailSerialzer(module,many=True)
           vouchar = serialize.data
           module1 = Contravoucher.objects.filter(Q(Bank_Or_Cash=True) & Q(Voucher_Date__range=data['Voucher_Date']) & Q(Bank_Name=data['Bank_Name']) & Q(Is_Deleted=False))
           serialize1 = ContravoucherSerialzer(module1,many=True)
           contra = serialize1.data
           out={"Vouchar":vouchar,"Contra":contra}
           output = JSONRenderer().render(out)
           return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)


class Voucherdetailcashbook(ListAPIView):
    queryset = Voucherdetail.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]
    def post(self,request,*args,**kwargs):
        try:
           data=request.data
           module = Voucherdetail.objects.filter(Q(Cash_Or_Bank=True) & Q(Voucher_Date__range=data['Voucher_Date']) & Q(Is_Deleted=False))
           serialize = VoucherdetailSerialzer(module,many=True)
           vouchar = serialize.data
           module1 = Contravoucher.objects.filter(Q(Cash_Or_Bank=True) & Q(Voucher_Date__range=data['Voucher_Date']) & Q(Is_Deleted=False))
           serialize1 = ContravoucherSerialzer(module1,many=True)
           contra = serialize1.data
           out={"Vouchar":vouchar,"Contra":contra}
           output = JSONRenderer().render(out)
           output = JSONRenderer().render(serialize.data)
           return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)



class ContravoucherCRUD(ListAPIView):
    queryset = Contravoucher.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = ContravoucherSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Contravoucher_Id']
            module = Contravoucher.objects.get(Contravoucher_Id=id)
            serialize = ContravoucherSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Contravoucher_Id']
            module=Contravoucher.objects.get(Contravoucher_Id=id)
            serialize = ContravoucherSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Contravoucher_Id']
            module=Contravoucher.objects.get(Contravoucher_Id=id)
            serialize = ContravoucherSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class Contravouchermylist(ListAPIView):
    queryset = Contravoucher.objects.all()
    serializer_class = ContravoucherSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        id=self.request.GET.get('Created_By')
        module=Contravoucher.objects.filter(Q(Created_By=id) & Q(Is_Deleted=False)).order_by('-id')
        return module


class Contravoucherlist(ListAPIView):
    queryset=Contravoucher
    serializer_class=ContravoucherSerialzer
    authentication_classes=[JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        module = Contravoucher.objects.filter(Is_Deleted=False).order_by('-id')
        return module


class Contravoucherfilter(ListAPIView):
    queryset = Contravoucher.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            data1 = {"Bank_Name__icontains":data['Bank_Name'],
                "Created_Date__range":data['Created_Date'],"Is_Deleted":False}
            return requestget (model=Contravoucher,serializer=ContravoucherSerialzers,data=data1,page=data['page'],Operator='and')
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class Contravouchersearch(ListAPIView):
    queryset = Contravoucher.objects.all()
    serializer_class = ContravoucherSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data=self.request.GET.get('search')
        module=Contravoucher.objects.filter(Q(Voucher_Type__icontains=data) | 
                Q(Bank_Name__icontains=data),Is_Deleted=False).order_by('-id')
        return module


class SupplierinvoicedetailCRUD(ListAPIView):
    queryset = Supplierinvoicedetail.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data = request.data
            serialize = SupplierinvoicedetailSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output, content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST) 


    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Supplierinvoicedetail.objects.get(Supplierinvoicedetail_Id=data['Supplierinvoicedetail_Id'])
            serialize = SupplierinvoicedetailSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type = 'application/json',status = status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output, content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            module = Supplierinvoicedetail.objects.get(Supplierinvoicedetail_Id=data['Supplierinvoicedetail_Id'])
            serialize = SupplierinvoicedetailSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output , content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Supplierinvoicedetail.objects.get(Supplierinvoicedetail_Id=data['Supplierinvoicedetail_Id'])
            serialize = SupplierinvoicedetailSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json' , status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output, content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)


class Supplierinvoicelist(ListAPIView):
    queryset = Supplierinvoicedetail.objects.all()
    serializer_class = SupplierinvoicedetailSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get_queryset(self):
       Company_Id=self.request.GET.get('Company_Id')
       module = Supplierinvoicedetail.objects.filter(Q(Company_Id=Company_Id) & Q(Is_Deleted = False)).order_by('-id')
       return module    

class Supplierinvoicemylist(ListAPIView):
    queryset = Supplierinvoicedetail.objects.all()
    serializer_class = SupplierinvoicedetailSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get_queryset(self):
       Company_Id=self.request.GET.get('Company_Id')
       Created_By = self.request.GET.get('Created_By')
       module = Supplierinvoicedetail.objects.filter(Q(Company_Id=Company_Id) & Q(Created_By=Created_By) & Q(Is_Deleted = False)).order_by('-id')
       return module 


class Supplierinvoicesearch(ListAPIView):
    queryset = Supplierinvoicedetail.objects.all()
    serializer_class = SupplierinvoicedetailSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data=self.request.GET.get('search')
        module = Supplierinvoicedetail.objects.filter(Q(Supplier_Name=data) | Q(Supplierinvoicedetail_Id__icontains=data) | Q(GIN0__icontains=data),Is_Deleted = False)
        return module   


class Supplierinvoicefilter(ListAPIView):
    queryset = Supplierinvoicedetail.objects.all()
    serializer_class = SupplierinvoicedetailSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        data = request.data
        data1 = {"Supplier_Name__icontains":data['Supplier_Name'],"GIN0":data['GRNO'],"Created_Date__range":data['Created_Date'],"Is_Deleted":False}
        return requestget(model=Supplierinvoicedetail,serializer=SupplierinvoicedetailSerialzer,data=data1,page=data['page'],Operator='and')
        

# front end purpose without token

class Supplierinvoicewithouttoken(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Supplierinvoicedetail.objects.get(id=data['id'])
            serialize = SupplierinvoicedetailSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type = 'application/json',status = status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output, content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            module = Supplierinvoicedetail.objects.get(id=data['id'])
            serialize = SupplierinvoicedetailSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output , content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Billspayable(ListAPIView):
    queryset = Supplierinvoicedetail.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            module = Supplierinvoicedetail.objects.filter(Q(Company_Id=data['Company_Id']) & Q(Created_Date__range=data['Date']) & Q(Is_Deleted=False))
            serialize = SupplierinvoicedetailSerialzers(module,many=True)
            suplier=serialize.data
            billspayable=[]
            outstandingamount=[]
            for i in suplier:
                module = PODetails.objects.get(PO_Id=i['PONo'])
                amount = module.Total_POAmount
                outstanding=amount - i['Invoice_Amount']
                start_date = datetime.strptime(i['Supplier_BillDt'], "%Y-%m-%d").date()
                end_date = datetime.strptime(i['Created_Date'], "%Y-%m-%d").date()
                data1={"Date":i['Created_Date'],"Invoicenumber":i['Supplierinvoicedetail_Id'],"Suppliername":i['Supplier_Id']['ContactPerson_FirstName'],
                    "outstanding":outstanding,"DueDate":i['Supplier_BillDt'],"Overduedays":(start_date -end_date).days}
                billspayable.append(data1)
                outstandingamount.append(outstanding)
            Total=sum(outstandingamount)
            billspayable.append({"Total":Total})
            output = JSONRenderer().render(billspayable)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class Billsreceivable(ListAPIView):
    queryset = Supplierinvoicedetail.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            module = Supplierinvoicedetail.objects.filter(Q(Company_Id=data['Company_Id']) & Q(Created_Date__range=data['Date']) & Q(Is_Deleted=False))
            serialize = SupplierinvoicedetailSerialzers(module,many=True)
            suplier=serialize.data
            billspayable=[]
            outstandingamount=[]
            for i in suplier:
                module = PODetails.objects.get(PO_Id=i['PONo'])
                amount = module.Total_POAmount
                outstanding=amount - i['Invoice_Amount']
                start_date = datetime.strptime(i['Supplier_BillDt'], "%Y-%m-%d").date()
                end_date = datetime.strptime(i['Created_Date'], "%Y-%m-%d").date()
                data1={"Date":i['Created_Date'],"Invoicenumber":i['Supplierinvoicedetail_Id'],"Suppliername":i['Supplier_Id']['ContactPerson_FirstName'],
                    "outstanding":outstanding,"DueDate":i['Supplier_BillDt'],"Overduedays":(start_date -end_date).days}
                billspayable.append(data1)
                outstandingamount.append(outstanding)
            Total=sum(outstandingamount)
            billspayable.append({"Total":Total})
            output = JSONRenderer().render(billspayable)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Salesvoucherregister(ListAPIView):
    queryset = Voucherdetail.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            module = Voucherdetail.objects.filter(Q(Company_Id=data['Company_Id']) & Q(Voucher_Type=data['Voucher_Type']) & Q(Voucher_Date__range=data['Voucher_Date']) & Q(Is_Deleted= False))
            serialize = VoucherdetailSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Journalvoucherregister(ListAPIView):
    queryset = Journal.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            module = Journal.objects.filter(Q(Company_Id=data['Company_Id']) &  Q(Journal_Date__range=data['Journal_Date']) & Q(Is_Deleted= False))
            serialize = JournalSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class Cashvoucherregister(ListAPIView):
    queryset = Voucherdetail.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            module = Voucherdetail.objects.filter(Q(Company_Id=data['Company_Id']) &  Q(Voucher_Date__range=data['Voucher_Date']) & Q(Cash_Or_Bank=True) & Q(Is_Deleted= False))
            serialize = VoucherdetailSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


# HR Module API's

class EmployeeCRUD(ListAPIView):
    queryset = EmployeeMaster.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            EmployeeQualification_Id=data['EmployeeQualification_Id']
            EmployeeExperience_Id=data['EmployeeExperience_Id']
            EmployeeAddress_Id=data['EmployeeAddress_Id']
            AnnualCTC_Id = data['AnnualCTC_Id']
            serialize=EmployeeQualificationSerialzer(data=EmployeeQualification_Id,many=True)
            if serialize.is_valid():
                serialize.save()
            qualification=serialize.data
            qua=[]
            for i in qualification:
                qualifi=i['id']
                qua.append(qualifi)
            data['EmployeeQualification_Id']=qua
            serialize = EmployeeExperienceSerialzer(data=EmployeeExperience_Id,many=True)
            if serialize.is_valid():
                serialize.save()
            experience = serialize.data
            exp=[]
            for i in experience:
                experi=i['id']
                exp.append(experi)
            data['EmployeeExperience_Id']=exp
            serialize=EmployeeAddressSerialzer(data=EmployeeAddress_Id)
            if serialize.is_valid():
                serialize.save()
            address=serialize.data
            data['EmployeeAddress_Id']=address['id']
            Anualserialize = EmployeeAnnualCTCSerialzer(data=AnnualCTC_Id)
            if Anualserialize.is_valid():
                Anualserialize.save()
            Anualid = Anualserialize.data
            data['AnnualCTC_Id']=Anualid['id']
            serialize = EmployeeMasterSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Employee_Id']
            module = EmployeeMaster.objects.get(Employee_Id=id)
            serialize = EmployeeMasterSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Employee_Id']
            add=data['EmployeeAddress_Id']
            Anualid =data['AnnualCTC_Id']
            # print(add)
            experience=data['EmployeeExperience_Id']
            qualification=data['EmployeeQualification_Id']
            module=EmployeeAddress.objects.get(EmployeeAddress_Id=add['EmployeeAddress_Id'])
            serialize=EmployeeAddressSerialzer(instance=module,data=add)
            if serialize.is_valid():
                serialize.save()
            for i in experience:
                module=EmployeeExperience.objects.get(EmployeeExperience_Id=i['EmployeeExperience_Id'])
                serialize=EmployeeExperienceSerialzer(instance=module,data=i)
                if serialize.is_valid():
                    serialize.save()
            # print(experience)
            for i in qualification:
                # print(i['EmployeeQualification_Id'])
                module=EmployeeQualification.objects.get(EmployeeQualification_Id=i['EmployeeQualification_Id'])
                # print(module)
                serialize=EmployeeQualificationSerialzer(instance=module,data=i)
                # print(serialize.is_valid())
                if serialize.is_valid():
                    serialize.save()
                # ser=serialize.data
                # print(ser)
            Anualmodule = EmployeeAnnualCTC.objects.get(AnnualCTC_Id=Anualid['AnnualCTC_Id'])
            Anualserialize = EmployeeAnnualCTCSerialzer(instance=Anualmodule,data=Anualid)
            if Anualserialize.is_valid():
                Anualserialize.save()
            module=EmployeeMaster.objects.get(Employee_Id=id)
            serialize = EmployeeMasterSerialzers(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Employee_Id']
            module=EmployeeMaster.objects.get(Employee_Id=id)
            serialize = EmployeeMasterSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class EmployeeCRUDwithouttoken(APIView):

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['id']
            module = EmployeeMaster.objects.get(id=id)
            serialize = EmployeeMasterSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['id']
            add=data['EmployeeAddress_Id']
            Anualid =data['AnnualCTC_Id']
            # print(add)
            experience=data['EmployeeExperience_Id']
            qualification=data['EmployeeQualification_Id']
            module=EmployeeAddress.objects.get(EmployeeAddress_Id=add['EmployeeAddress_Id'])
            serialize=EmployeeAddressSerialzer(instance=module,data=add)
            if serialize.is_valid():
                serialize.save()
            for i in experience:
                module=EmployeeExperience.objects.get(EmployeeExperience_Id=i['EmployeeExperience_Id'])
                serialize=EmployeeExperienceSerialzer(instance=module,data=i)
                if serialize.is_valid():
                    serialize.save()
            # print(experience)
            for i in qualification:
                # print(i['EmployeeQualification_Id'])
                module=EmployeeQualification.objects.get(EmployeeQualification_Id=i['EmployeeQualification_Id'])
                # print(module)
                serialize=EmployeeQualificationSerialzer(instance=module,data=i)
                # print(serialize.is_valid())
                if serialize.is_valid():
                    serialize.save()
                # ser=serialize.data
                # print(ser)
            Anualmodule = EmployeeAnnualCTC.objects.get(AnnualCTC_Id=Anualid['AnnualCTC_Id'])
            Anualserialize = EmployeeAnnualCTCSerialzer(instance=Anualmodule,data=Anualid)
            if Anualserialize.is_valid():
                Anualserialize.save()
            module=EmployeeMaster.objects.get(id=id)
            serialize = EmployeeMasterSerialzers(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class Employeelist(ListAPIView):
    queryset = EmployeeMaster.objects.all()
    serializer_class = EmployeeMasterSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        Partner_Id=self.request.GET.get('Partner_Id')
        module=EmployeeMaster.objects.filter(Q(Partner_Id=Partner_Id) & Q(Is_Deleted=False)).order_by('-id')
        return module



class Employeefilter(ListAPIView):
    queryset = EmployeeMaster.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            data1 = {"Employee_FirstName__icontains":data['Employee_FirstName'],"Email_Id__icontains":data['Email_Id'],
                "DateOf_Joining__range":data['DateOf_Joining'],"Is_Deleted":False}
            return requestget(model=EmployeeMaster,serializer=EmployeeMasterSerialzers,data=data1,page=data['page'],Operator='and')
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Employeesearch(ListAPIView):
    queryset = EmployeeMaster.objects.all()
    serializer_class = EmployeeMasterSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data=self.request.GET.get('search')
        module=EmployeeMaster.objects.filter(Q(Employee_FirstName__icontains=data) | 
                Q(Email_Id__icontains=data),Is_Deleted=False).order_by('-id')
        return module



class DepartmentCRUD(ListAPIView):
    queryset = Department.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = DepartmentSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Department_Id']
            module = Department.objects.get(Department_Id=id)
            serialize = DepartmentSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Department_Id']
            module=Department.objects.get(Department_Id=id)
            serialize = DepartmentSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Department_Id']
            module=Department.objects.get(Department_Id=id)
            serialize = DepartmentSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class DepartmentAdditionalCRUD(ListAPIView):
    queryset = Department.objects.all()
    serializer_class = DepartmentSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]
    
    def get_queryset(self):
        try:
            data = self.request.query_params
            module = Department.objects.filter(Company_Id=data['Company_Id'], Created_By=data['Created_By'],
                                               Is_Deleted=False).order_by('-id')
            return module
        except:
            module = Department.objects.none()
            return module


class Departmentsearch(ListAPIView):
    queryset = Department.objects.all()
    serializer_class = DepartmentSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data=self.request.GET.get('search')
        module = Department.objects.filter(Q(Department_Name__icontains=data) | Q(Department_Head__icontains=data) | Q(Parent_Department__icontains=data),Is_Deleted=False)
        return module


class Departmentfilter(ListAPIView):
    queryset = Department.objects.all()
    serializer_class = DepartmentSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        data=request.data
        data1={"Department_Name__icontains":data['Department_Name'],"Department_Head__icontains":data['Department_Head'],"Parent_Department__icontains":data['Parent_Department'],"Created_Date__range":data['Created_Date'],"Is_Deleted":False}
        return requestget(model=Department,serializer=DepartmentSerialzers,data=data1,page=data['page'],Operator='and')



class Departmentdropdownlist(APIView):
    def get(self,request,*args,**kwargs):
        module = Department.objects.filter(Is_Deleted=False)
        serialize = DepartmentSerialzer(module,many=True)
        output = JSONRenderer().render(serialize.data)
        return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)

class DesignationCRUD(ListAPIView):
    queryset = Designation.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = DesignationSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Designation_Id']
            module = Designation.objects.get(Designation_Id=id)
            serialize = DesignationSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Designation_Id']
            module=Designation.objects.get(Designation_Id=id)
            serialize = DesignationSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Designation_Id']
            module=Designation.objects.get(Designation_Id=id)
            serialize = DesignationSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Designationlist(ListAPIView):
    queryset = Designation.objects.all()
    serializer_class = DesignationSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        module = Designation.objects.filter(Is_Deleted=False)
        return module


class Designationsearch(ListAPIView):
    queryset = Designation.objects.all()
    serializer_class = DesignationSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data=self.request.GET.get('search')
        module = Designation.objects.filter(Q(Designation_Name__icontains=data) | Q(Cadre_Name__icontains=data),Is_Deleted=False)
        return module


class Designationfilter(ListAPIView):
    queryset = Designation.objects.all()
    serializer_class = DesignationSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        data=request.data
        data1={"Designation_Name__icontains":data['Designation_Name'],"Cadre_Name__icontains":data['Cadre_Name'],"Created_Date__range":data['Created_Date'],"Is_Deleted":False}
        return requestget(model=Designation,serializer=DesignationSerialzers,data=data1,page=data['page'],Operator='and')




class Designationropdownlist(APIView):
    def get(self,request,*args,**kwargs):
        module = Designation.objects.filter(Is_Deleted=False)
        serialize = DesignationSerialzer(module,many=True)
        output = JSONRenderer().render(serialize.data)
        return HttpResponse(output,content_type='application/json')


class TerritoryManagementCRUD(ListAPIView):
    queryset = Territory.objects.all()
    serializer_class = TerritorySerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args, **kwargs):
        try:
            data = request.data
            serialize = TerritorySerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_201_CREATED)
        except Exception as e:
            output = JSONRenderer().render({"error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, *args, **kwargs):
        try:
            data = request.query_params
            id = data['id']
            module = Territory.objects.get(id=id)
            serialize = TerritorySerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)

    def put(self,request,*args, **kwargs):
        try:
            data = request.data
            module = Territory.objects.get(Territory_Id=data['Territory_Id'])
            serialize = TerritorySerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_201_CREATED)
        except Exception as e:
            output = JSONRenderer().render({"error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args, **kwargs):
        try:
            data = request.query_params
            module = Territory.objects.get(id=data['id'])
            serialize = TerritorySerialzer(module,{"Is_Deleted":True}, partial = True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_201_CREATED)
        except Exception as e:
            output = JSONRenderer().render({"error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)


class MyTerritoryLists(ListAPIView):
    queryset = Territory.objects.all()
    serializer_class = TerritorySerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    # my territory list api
    def get_queryset(self):
        try:
            Company_Id = self.request.GET.get('Company_Id')
            Created_By = self.request.GET.get('Created_By')
            module = Territory.objects.filter(Q(
                Company_Id=Company_Id) & Q( Created_By=Created_By) & Q(Is_Deleted=False)).order_by('-id')
            return module
        except:
            module = Territory.objects.none()
            return module


class AllTerritoryLists(ListAPIView):
    queryset = Territory.objects.all()
    serializer_class = TerritorySerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]
    # my territory list api

    def get_queryset(self):
        try:
            Company_Id = self.request.GET.get('Company_Id')
            module = Territory.objects.filter(Q(
                Company_Id=Company_Id) & Q(Is_Deleted=False)).order_by('-id')
            return module
        except:
            module = Territory.objects.none()
            return module


class Teritorysearch(ListAPIView):
    queryset = Territory.objects.all()
    serializer_class = TerritorySerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data=self.request.GET.get('search')
        module=Territory.objects.filter(Q(Territory_Name__icontains=data) | Q(Parent_Teritory__icontains=data) | Q(Territorry_Manager__icontains=data),Is_Deleted=False)
        return module


class Teritoryfilter(ListAPIView):
    queryset = Territory.objects.all()
    serializer_class = TerritorySerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        data=request.data
        data1={"Territory_Name__icontains":data['Territory_Name'],"Parent_Teritory__icontains":data['Parent_Teritory'],
                "Territorry_Manager__icontains":data["Territorry_Manager"],"Created_Date__range":data['Created_Date'],"Is_Deleted":False}
        return requestget(model=Territory,serializer=TerritorySerialzers,data=data1,page=data['page'],Operator='and')


class CadreCRUD(ListAPIView):
    queryset = Cadre.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = CadreSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Cadre_Id']
            module = Cadre.objects.get(Cadre_Id=id)
            serialize = CadreSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Cadre_Id']
            module=Cadre.objects.get(Cadre_Id=id)
            serialize = CadreSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Cadre_Id']
            module=Cadre.objects.get(Cadre_Id=id)
            serialize = CadreSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class Cadrelist(ListAPIView):
    queryset = Cadre.objects.all()
    serializer_class =CadreSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]  

    def get_queryset(self):
        module = Cadre.objects.filter(Is_Deleted=False)
        return module


class Cadresearch(ListAPIView):
    queryset = Cadre.objects.all()
    serializer_class = CadreSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data=self.request.GET.get('search')
        module = Cadre.objects.filter(Q(Cadre_Name__icontains=data) | Q(Payscale_From__icontains=data),Is_Deleted=False)
        return module


class Cadrefilter(ListAPIView):
    queryset = Cadre.objects.all()
    serializer_class = CadreSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        data=request.data
        data1={"Cadre_Name__icontains":data['Cadre_Name'],"Payscale_From__range":data['Payscale_From'],"Created_Date__range":data['Created_Date'],"Is_Deleted":False}
        return requestget(model=Cadre,serializer=CadreSerialzers,data=data1,page=data['page'],Operator='and')



class Cadredropdownlist(APIView):
    def get(self,request,*args,**kwargs):
        module = Cadre.objects.filter(Is_Deleted=False)
        serialize = CadreSerialzer(module,many=True)
        output = JSONRenderer().render(serialize.data)
        return HttpResponse(output,content_type='application/json')



class EmployeeAnnualCTCCRUD(ListAPIView):
    queryset = EmployeeAnnualCTC.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = EmployeeAnnualCTCSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['AnnualCTC_Id']
            module = EmployeeAnnualCTC.objects.get(AnnualCTC_Id=id)
            serialize = EmployeeAnnualCTCSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['AnnualCTC_Id']
            module=EmployeeAnnualCTC.objects.get(AnnualCTC_Id=id)
            serialize = EmployeeAnnualCTCSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['AnnualCTC_Id']
            module=EmployeeAnnualCTC.objects.get(AnnualCTC_Id=id)
            serialize = EmployeeAnnualCTCSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class LeavetypeCRUD(ListAPIView):
    queryset = Leavetype.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = LeavetypeSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Leavetype_Id']
            module = Leavetype.objects.get(Leavetype_Id=id)
            serialize = LeavetypeSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Leavetype_Id']
            module=Leavetype.objects.get(Leavetype_Id=id)
            serialize = LeavetypeSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Leavetype_Id']
            module=Leavetype.objects.get(Leavetype_Id=id)
            serialize = LeavetypeSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Leavetypelist(ListAPIView):
    queryset = Leavetype.objects.all()
    serializer_class = LeavetypeSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions] 

    def get_queryset(self):
        data=self.request.GET.get('Partner_Id')
        module = Leavetype.objects.filter(Partner_Id=data,Is_Deleted=False).order_by('-id')
        return module


class Leavetypesearch(ListAPIView):
    queryset = Leavetype.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]  

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Leavetype.objects.filter(Q(Name__icontains=data['search']) | Q(Leave_Type__icontains=data['search']),Is_Deleted=False).order_by('-id')
            serialize = LeavetypeSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output , content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)

class Leavetypefilter(ListAPIView):
    queryset = Leavetype.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        data=request.data
        data1 = {"Name__icontains":data['Name'],"Leave_Type__icontains":data['Leave_Type'],"Validity_From__range":data['Validity_From'],"Created_Date__range":data['Created_Date'],"Is_Deleted":False}
        return requestget(model=Leavetype,serializer=LeavetypeSerialzers,data=data1,page=data['page'],Operator='and')

class LeaveopeningbalanceCRUD(ListAPIView):
    queryset = Leaveopeningbalance.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = LeaveopeningbalanceSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['id']
            module = Leaveopeningbalance.objects.get(id=id)
            serialize = LeaveopeningbalanceSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['id']
            module=Leaveopeningbalance.objects.get(id=id)
            serialize = LeaveopeningbalanceSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['id']
            module=Leaveopeningbalance.objects.get(id=id)
            serialize = LeaveopeningbalanceSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Leaveopeningbalancelist(ListAPIView):
    queryset = Leaveopeningbalance.objects.all()
    serializer_class = LeaveopeningbalanceSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions] 

    def get_queryset(self):
        data=self.request.GET.get('Partner_Id')
        module = Leaveopeningbalance.objects.filter(Partner_Id=data,Is_Deleted=False).order_by('-id')
        return module


class Leaveopeningbalancesearch(ListAPIView):
    queryset = Leaveopeningbalance.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]  

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Leaveopeningbalance.objects.filter(Leavetype__icontains=data['search'],Is_Deleted=False).order_by('-id')
            serialize = LeaveopeningbalanceSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output , content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)

class Leaveopeningbalancefilter(ListAPIView):
    queryset = Leaveopeningbalance.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        data=request.data
        data1 = {"Leavetype__icontains":data['Leavetype'],"Year":data['Year'],"Is_Deleted":False}
        return requestget(model=Leaveopeningbalance,serializer=LeaveopeningbalanceSerialzers,data=data1,page=data['page'],Operator='and')




class Leavetypelistdropdown(APIView):
    def get(self,request,*args,**kwargs):
        data=request.query_params
        module = Leavetype.objects.filter(Partner_Id=data['Partner_Id'],Is_Deleted=False)
        serialize = LeavetypeSerialzer(module,many=True)
        output = JSONRenderer().render(serialize.data)
        return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)



class LeaveCRUD(ListAPIView):
    queryset = Leave.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = LeaveSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Application_Id']
            module = Leave.objects.get(Application_Id=id)
            serialize = LeaveSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Application_Id']
            module=Leave.objects.get(Application_Id=id)
            serialize = LeaveSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Application_Id']
            module=Leave.objects.get(Application_Id=id)
            serialize = LeaveSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)




class Leavelist(ListAPIView):
    queryset = Leave.objects.all()
    serializer_class = LeaveSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data=self.request.GET.get('Employee_Id')
        module = Leave.objects.filter(Employee_Id=data,Is_Deleted=False).order_by('-id')
        return module


class Leavefilter(ListAPIView):
    queryset = Leave.objects.all()
    serializer_class = LeaveSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]  

    def post(self,request,*args,**kwargs):
        data=request.data
        data1 = {"From_Date__range":data['From_Date']}
        return requestget(model=Leave,serializer=LeaveSerialzers,data=data1,page=data['page'],Operator='and')


class HolidaylistCRUD(ListAPIView):
    queryset = Holidaylist.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = HolidaylistSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Holidaylist_Id']
            module = Holidaylist.objects.get(Holidaylist_Id=id)
            serialize = HolidaylistSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Holidaylist_Id']
            module=Holidaylist.objects.get(Holidaylist_Id=id)
            serialize = HolidaylistSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Holidaylist_Id']
            module=Holidaylist.objects.get(Holidaylist_Id=id)
            serialize = HolidaylistSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)




class Holidaylistlist(ListAPIView):
    queryset = Holidaylist.objects.all()
    serializer_class = HolidaylistSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data=self.request.GET.get('Partner_Id')
        module = Holidaylist.objects.filter(Partner_Id=data,Is_Deleted=False).order_by('-id')
        return module


class Holidaylistfilter(ListAPIView):
    queryset = Holidaylist.objects.all()
    serializer_class = HolidaylistSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]  

    def post(self,request,*args,**kwargs):
        data=request.data
        data1 = {"Holiday_Name__icontains":data['Holiday_Name'],"Year":data['Year'],"Is_Deleted":False}
        return requestget(model=Holidaylist,serializer=HolidaylistSerialzers,data=data1,page=data['page'],Operator='and')


class Holidaylistsearch(ListAPIView):
    queryset = Holidaylist.objects.all()
    serializer_class = HolidaylistSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions] 

    def get_queryset(self):
        data=self.request.GET.get('search')
        module = Holidaylist.objects.filter(Q(Holiday_Name__icontains=data) | Q(Year=data),Is_Deleted=False).order_by('-id')
        return module


class Leavecalculation(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Leave.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Employee_Id=data['Employee_Id'])& Q(Year=data['Year']) & Q(Approved_Flag=True))
            serialize = LeaveSerialzer(module,many=True)
            leaveoutput = serialize.data
            output = []
            for i in leaveoutput:
                module1=Leave.objects.get(Application_Id=i['Application_Id'])
                serialize1 = LeaveSerialzer(module1)
                out=serialize1.data
                fromdate = datetime.strptime(out['From_Date'], "%Y-%m-%d")
                todate = datetime.strptime(out['To_Date'], "%Y-%m-%d")
                count = (todate - fromdate).days
                output.append({"Name":out['Leavetype'],"Count":count})
            counts = {}
            for item in output:
                name = item["Name"]
                count = item["Count"]
                if name in counts:
                    counts[name] += count
                else:
                    counts[name] = count
            output3 = JSONRenderer().render(counts)
            return HttpResponse(output3,content_type='application/json')
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

# This api used for front end purpose

class Getleave(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module=Leave.objects.get(id=data['id'])
            serialize = LeaveSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Leavelistwithoutpagination(APIView):
    def get(self,request,*args,**kwargs):
        data=request.query_params
        module = Leave.objects.filter(Employee_Id=data['Employee_Id'],Is_Deleted=False).order_by('-id')
        serialize = LeaveSerialzer(module,many=True)
        output = JSONRenderer().render(serialize.data)
        return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)


#for front end purpose:
class updateleave(APIView):
    
    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Application_Id']
            module=Leave.objects.get(Application_Id=id)
            serialize = LeaveSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)




class LeavecancellationCRUD(ListAPIView):
    queryset = Leavecancellation.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = LeavecancellationSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Cancellation_Id']
            module = Leavecancellation.objects.get(Cancellation_Id=id)
            serialize = LeavecancellationSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Cancellation_Id']
            module=Leavecancellation.objects.get(Cancellation_Id=id)
            serialize = LeavecancellationSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Cancellation_Id']
            module=Leavecancellation.objects.get(Cancellation_Id=id)
            serialize = LeavecancellationSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class LeavecancellationwithouttokenCRUD(APIView):  

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['id']
            module = Leavecancellation.objects.get(id=id)
            serialize = LeavecancellationSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['id']
            module=Leavecancellation.objects.get(id=id)
            serialize = LeavecancellationSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)




class Leavecancellationlist(ListAPIView):
    queryset = Leavecancellation.objects.all()
    serializer_class = LeavecancellationSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data=self.request.GET.get('Created_By')
        module = Leavecancellation.objects.filter(Q(Created_By=data) & Q(Is_Deleted=False)).order_by('-id')
        return module


class Allleavecancellationlist(ListAPIView):
    queryset = Leavecancellation.objects.all()
    serializer_class = LeavecancellationSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        module = Leavecancellation.objects.filter(Is_Deleted=False).order_by('-id')
        return module


class Leavecancellationfilter(ListAPIView):
    queryset = Leavecancellation.objects.all()
    serializer_class = LeavecancellationSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]  

    def post(self,request,*args,**kwargs):
        data=request.data
        data1 = {"From_Date__range":data['From_Date']}
        return requestget(model=Leavecancellation,serializer=LeavecancellationSerialzers,data=data1,page=data['page'],Operator='and')


class EmployeecheckinCRUD(APIView):
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            data['Year'] = data['Date'].strftime("%Y")
            data['Month'] = data['Date'].strftime("%B")
            serialize = EmployeecheckinSerializer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Employeecheckin_Id']
            module = Employeecheckin.objects.get(Employeecheckin_Id=id)
            serialize = EmployeecheckinSerializers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Employeecheckin_Id']
            module=Employeecheckin.objects.get(Employeecheckin_Id=id)
            serialize = EmployeecheckinSerializer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Employeecheckin_Id']
            module=Employeecheckin.objects.get(Employeecheckin_Id=id)
            serialize = EmployeecheckinSerializer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

class Daywisecheckinlist(APIView):    
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Employeecheckin.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Date=data['Date']) & Q(Employee_Id=data['Employee_Id']))
            serialize = EmployeecheckinSerializer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        


class Mycheckinlist(APIView):
    queryset = Employeecheckin.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]
    
    def get_queryset(self):    
        Employee_Id = self.request.GET.get('Employee_Id')
        Partner_Id = self.request.GET.get('Partner_Id')
        module = Employeecheckin(Q(Employee_Id=Employee_Id)& Q(Partner_Id=Partner_Id) & Q(Is_Deleted=False)).order_by('-id')
        return module


class TimeSheetCRUD(ListAPIView):
    queryset = TimeSheet.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            checkinmodule = Employeecheckin.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Date=data['Date']) & Q(Employee_Id=data['Employee_Id']))
            checkinserialize = EmployeecheckinSerializer(checkinmodule,many=True)
            checkindata=checkinserialize.data
            checkinda=checkindata[0]
            checkindat=checkindata[-1]
            data['FromDate']=checkinda['Date']
            your_date = datetime.strptime(checkinda['Date'], "%Y-%m-%d")
            data['Month'] = your_date.strftime("%B")
            data['Weekno'] = your_date.strftime("%W")
            data['Year'] = your_date.strftime("%Y")
            data['Timemanagement']={"Checkin_Time":checkinda['Checkin_Time'],"Checkout_Time":checkindat['Checkout_Time'],"Working_Hours":checkindat['Working_Hours']}
            data['Partner_Id']=checkinda['Partner_Id']
            data['Employee_Id']=checkinda['Employee_Id']
            serialize = TimeSheetSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        


    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['TimeSheet_Id']
            module = TimeSheet.objects.get(TimeSheet_Id=id)
            serialize = TimeSheetSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['TimeSheet_Id']
            module=TimeSheet.objects.get(TimeSheet_Id=id)
            serialize = TimeSheetSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['TimeSheet_Id']
            module=TimeSheet.objects.get(TimeSheet_Id=id)
            serialize = TimeSheetSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class TimeSheetwithoutCRUD(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['id']
            module = TimeSheet.objects.get(id=id)
            serialize = TimeSheetSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['id']
            module=TimeSheet.objects.get(id=id)
            serialize = TimeSheetSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)




class Mytimesheetlist(ListAPIView):
    queryset = TimeSheet.objects.all()
    serializer_class = TimeSheetSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        id=self.request.GET.get('Created_By')
        module=TimeSheet.objects.filter(Q(Created_By=id) & Q(Is_Deleted=False)).order_by('-id')
        return module


class Timesheetlist(ListAPIView):
    queryset = TimeSheet.objects.all()
    serializer_class = TimeSheetSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        module=TimeSheet.objects.filter(Is_Deleted=False).order_by('-id')
        return module


class mobiletimesheetfilter(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = TimeSheet.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Employee_Id=data['Employee_Id']) & Q(Weekno=data['Weekno']) & Q (Is_Deleted=False)).order_by('-id')
            serialize = TimeSheetSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/jaon',status=status.HTTP_400_BAD_REQUEST)



class Timesheetsearch(ListAPIView):
    queryset = TimeSheet.objects.all()
    serializer_class = TimeSheetSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data=self.request.GET.get('search')
        module= TimeSheet.objects.filter(Q(Timesheet_Name__icontains=data),Is_Deleted=False).order_by('-id')
        return module


class Timesheetfilter(ListAPIView):
    queryset = TimeSheet.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        data=request.data
        data1={"Timesheet_Name__icontains":data['Timesheet_Name'],"FromDate__range":data['FromDate'],"Created_Date":data['Created_Date']}
        return requestget(model=TimeSheet,serializer=TimeSheetSerialzers,data=data1,page=data['page'],Operator='and')

class TimeManagementCRUD(ListAPIView):
    queryset = TimeManagement.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = TimeManagementSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Time_Id']
            module = TimeManagement.objects.get(Time_Id=id)
            serialize = TimeManagementSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Time_Id']
            module=TimeManagement.objects.get(Time_Id=id)
            serialize = TimeManagementSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Time_Id']
            module=TimeManagement.objects.get(Time_Id=id)
            serialize = TimeManagementSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class MytimeManagementlist(ListAPIView):
    queryset = TimeManagement.objects.all()
    serializer_class = TimeManagementSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        id=self.request.GET.get('Created_By')
        module=TimeManagement.objects.filter(Q(Created_By=id) & Q(Is_Deleted=False)).order_by('-id')
        return module


class TimeManagementlist(ListAPIView):
    queryset = TimeManagement.objects.all()
    serializer_class = TimeManagementSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        module=TimeManagement.objects.filter(Is_Deleted=False).order_by('-id')
        return module


class TimeManagementsearch(ListAPIView):
    queryset = TimeManagement.objects.all()
    serializer_class = TimeManagementSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data=self.request.GET.get('search')
        module= TimeManagement.objects.filter(Q(Timesheet_Name__icontains=data),Is_Deleted=False).order_by('-id')
        return module


class TimeManagementfilter(ListAPIView):
    queryset = TimeManagement.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        data=request.data
        data1={"Timesheet_Name__icontains":data['Timesheet_Name'],"FromDate__range":data['FromDate'],"Created_Date":data['Created_Date']}
        return requestget(model=TimeManagement,serializer=TimeManagementSerialzers,data=data1,page=data['page'],Operator='and')


class AttandanceManagementCRUD(ListAPIView):
    queryset = AttandanceManagement.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = AttandanceManagementSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Attandance_Id']
            module = AttandanceManagement.objects.get(Attandance_Id=id)
            serialize = AttandanceManagementSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Attandance_Id']
            module=AttandanceManagement.objects.get(Attandance_Id=id)
            serialize = AttandanceManagementSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Attandance_Id']
            module=AttandanceManagement.objects.get(Attandance_Id=id)
            serialize = AttandanceManagementSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class EmployeeProvidentfundCRUD(ListAPIView):
    queryset = EmployeeProvidentfund.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = EmployeeProvidentfundSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Employeepf_Id']
            module = EmployeeProvidentfund.objects.get(Employeepf_Id=id)
            serialize = EmployeeProvidentfundSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Employeepf_Id']
            module=EmployeeProvidentfund.objects.get(Employeepf_Id=id)
            serialize = EmployeeProvidentfundSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Employeepf_Id']
            module=EmployeeProvidentfundCRUD.objects.get(Employeepf_Id=id)
            serialize = EmployeeProvidentfundSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class EmployeeESICRUD(ListAPIView):
    queryset = EmployeeESI.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = EmployeeESISerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['EmployeeESI_Id']
            module = EmployeeESI.objects.get(EmployeeESI_Id=id)
            serialize = EmployeeESISerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['EmployeeESI_Id']
            module=EmployeeESI.objects.get(EmployeeESI_Id=id)
            serialize = EmployeeESISerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['EmployeeESI_Id']
            module=EmployeeESI.objects.get(EmployeeESI_Id=id)
            serialize = EmployeeESISerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class EmployeeProfessionalTaxCRUD(ListAPIView):
    queryset = EmployeeProfessionalTax.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = EmployeeProfessionalTaxSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['EmployeePT_Id']
            module = EmployeeProfessionalTax.objects.get(EmployeePT_Id=id)
            serialize = EmployeeProfessionalTaxSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['EmployeePT_Id']
            module=EmployeeProfessionalTax.objects.get(EmployeePT_Id=id)
            serialize = EmployeeProfessionalTaxSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['EmployeePT_Id']
            module=EmployeeProfessionalTax.objects.get(EmployeePT_Id=id)
            serialize = EmployeeProfessionalTaxSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class EmployeeEarningCRUD(ListAPIView):
    queryset = EmployeeEarning.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = EmployeeEarningSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Earning_Id']
            module = EmployeeEarning.objects.get(Earning_Id=id)
            serialize = EmployeeEarningSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Earning_Id']
            module=EmployeeEarning.objects.get(Earning_Id=id)
            serialize = EmployeeEarningSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Earning_Id']
            module=EmployeeEarning.objects.get(Earning_Id=id)
            serialize = EmployeeEarningSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class EmployeeEarningmylist(ListAPIView):
    queryset = EmployeeEarning.objects.all()
    serializer_class=EmployeeEarningSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]    

    def get_queryset(self):
        id=self.request.GET.get('Created_By')
        module = EmployeeEarning.objects.filter(Q(Created_By=id) & Q(Is_Deleted=False)).order_by('-id')
        return module
        

class EmployeeEarninglist(ListAPIView):
    queryset = EmployeeEarning.objects.all()
    serializer_class=EmployeeEarningSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]    

    def get_queryset(self):
        module = EmployeeEarning.objects.filter(Is_Deleted=False).order_by('-id')
        return module


class Earningfilter(ListAPIView):
    queryset = EmployeeEarning.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):

        data=request.data
        data1={"Earning_Name__icontains":data['Earning_Name'],"Earning_Type":data['Earning_Type'],"Created_Date__range":data['Created_Date'],"Is_Deleted":False}
        return requestget(model=EmployeeEarning,serializer=EmployeeEarningSerialzers,data=data1,page=data['page'],Operator='and')


class Earningsearch(ListAPIView):
    queryset = EmployeeEarning.objects.all()
    serializer_class = EmployeeEarningSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data=self.request.GET.get('search')
        module=EmployeeEarning.objects.filter(Q(Earning_Name__icontains=data) | Q(Earning_Type=data),Is_Deleted=False).order_by('-id')
        return module



class EmployeeDeductionCRUD(ListAPIView):
    queryset = EmployeeDeduction.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = EmployeeDeductionSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Deduction_Id']
            module = EmployeeDeduction.objects.get(Deduction_Id=id)
            serialize = EmployeeDeductionSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Deduction_Id']
            module=EmployeeDeduction.objects.get(Deduction_Id=id)
            serialize = EmployeeDeductionSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Deduction_Id']
            module=EmployeeDeduction.objects.get(Deduction_Id=id)
            serialize = EmployeeDeductionSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class EmployeeDeductionmylist(ListAPIView):
    queryset = EmployeeDeduction.objects.all()
    serializer_class=EmployeeDeductionSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        id=self.request.GET.get('Created_By')
        module = EmployeeDeduction.objects.filter(Q(Created_By=id) & Q(Is_Deleted=False))
        return module

class EmployeeDeductionlist(ListAPIView):
    queryset = EmployeeDeduction.objects.all()
    serializer_class=EmployeeDeductionSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        module = EmployeeDeduction.objects.filter(Is_Deleted=False)
        return module


class Deductionfilter(ListAPIView):
    queryset = EmployeeDeduction.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        data=request.data
        data1={"Deduction_Name__icontains":data['Deduction_Name'],"Deduction_Type":data['Deduction_Type'],"Created_Date__range":data['Created_Date'],"Is_Deleted":False}
        return requestget(model=EmployeeDeduction,serializer=EmployeeDeductionSerialzers,data=data1,page=data['page'],Operator='and')

class Dedcutionsearch(ListAPIView):
    queryset = EmployeeDeduction.objects.all()
    serializer_class = EmployeeDeductionSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get_queryset(self):
        data=self.request.GET.get('search')
        module=EmployeeDeduction.objects.filter(Q(Deduction_Name__icontains=data) | Q(Deduction_Type__icontains=data),Is_Deleted=False).order_by('-id')
        return module


#Employee Salary

class Totalattandancecalculation(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Partnermodule=Partner.objects.filter(Is_Deleted=False)
            Partnerserilaize=PartnerSerialzer(Partnermodule,many=True)
            partneroutput=Partnerserilaize.data
            result = []
            for i in partneroutput:
                employeemodule1=EmployeeMaster.objects.filter(Q(Partner_Id=i['id']) & Q(Is_Deleted=False)).count()
                employeemodule=EmployeeMaster.objects.filter(Q(Partner_Id=i['id']) & Q(Is_Deleted=False))
                Emplist=[]
                for y in employeemodule:
                    Timemodule = TimeSheet.objects.filter(Q(Partner_Id=i['id']) & Q(Year = data['Year']) & Q(Month = data['Month']) & Q(Approved_Flag=True))
                    Timesheetserialize = Timesheetserialize(Timemodule,many=True)
                    if Timesheetserialize==[]:
                        Emplist.append(y['id'])
                result.append({"PartnerName":i['Partner_Name'],"Partner_Id":i['Partner_Id'],"Total_Employee":employeemodule1,
                    "Total_attandance":Timemodule,"Balance":sum(Emplist)})
            output = JSONRenderer().render(result)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class Timesheetcalculation(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = TimeSheet.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Employee_Id = data['Employee_Id']) & Q(Year = data['Year']) & Q(Month = data['Month']) & Q(Approved_Flag=True))
            serialize = TimeSheetSerialzer(module,many=True)
            output = serialize.data
            Noofdays=[]
            for i in output:
                fromdate = datetime.strptime(i['To_Date'], "%Y-%m-%d")
                todate = datetime.strptime(i['From_Date'], "%Y-%m-%d")
                daycount=(todate - fromdate).days
                Noofdays.append(daycount)
            output = JSONRenderer().render({"Totalattandance":sum(Noofdays)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class Runattandance(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params   
            module = Leave.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Employee_Id = data['Employee_Id']) & Q(Year = data['Year']) & Q(Month = data['Month']) & Q(Approve_Flg=True))
            serialize=LeaveSerialzer(module,many=True)
            leave_calc= serialize.data
            paidleave=[]
            unpaidleave=[]
            # leavecount=[]
            for i in leave_calc:
                if i['Leavetype__Leave_Type']=='Paid':
                    fromdate = datetime.strptime(i['To_Date'], "%Y-%m-%d")
                    todate = datetime.strptime(i['From_Date'], "%Y-%m-%d")
                    leaves = (todate - fromdate).days
                    paidleave.append(leaves)
                else:
                    fromdate = datetime.strptime(i['To_Date'], "%Y-%m-%d")
                    todate = datetime.strptime(i['From_Date'], "%Y-%m-%d")
                    leaves = (todate - fromdate).days
                    unpaidleave.append(leaves)
            totalleave = sum(paidleave) + sum(unpaidleave)
            output = JSONRenderer().render("totalleave")
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Attandancecalculation(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            partnermodule=EmployeeMaster.objects.filter(Partner_Id=data['Partner_Id'])
            partnerserialize = EmployeeMasterSerialzer(partnermodule,many=True)
            employeelist=partnerserialize.data
            result = []
            for j in employeelist:
                Employeemodule = EmployeeMaster.objects.get(Employee_Id=j['Employee_Id'])
                Employeeserialize=EmployeeMasterSerialzer(Employeemodule)
                Empsalary = Employeeserialize.data
                if Empsalary['Cadre'] == 'Level2':
                    Timemodule = TimeSheet.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Employee_Id = j['id']) & Q(Year = data['Year']) & Q(Month = data['Month']) & Q(Approved_Flag=True)).count()
                    leavemodule = Leave.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Employee_Id = j['id']) & Q(Year = data['Year']) & Q(Month = data['Month']) & Q(Approved_Flag=True))
                    # Timeserialize = TimeSheetSerialzer(Timemodule,many=True)
                    leaveserialize=LeaveSerialzer(leavemodule,many=True)
                    leave_calc= leaveserialize.data
                    paidleave=[]
                    paideleavename=[]
                    unpaidleave=[]
                    unpaidleavename=[]
                # leavecount=[]
                    for i in leave_calc:
                        leavetype=Leavetype.objects.get(id=i['Leavetype_Id'])
                        leserialize=LeavetypeSerialzer(leavetype)
                        getleavetype=leserialize.data
                        if getleavetype['Leave_Type']=='Paid leave':
                            fromdate = datetime.strptime(i['To_Date'], "%Y-%m-%d")
                            todate = datetime.strptime(i['From_Date'], "%Y-%m-%d")
                            leaves = (fromdate - todate ).days
                            paidleave.append(leaves)
                            paideleavename.append({"Leavename":getleavetype['Name']})
                        else:
                            fromdate = datetime.strptime(i['To_Date'], "%Y-%m-%d")
                            todate = datetime.strptime(i['From_Date'], "%Y-%m-%d")
                            leaves = (fromdate - todate).days
                            unpaidleave.append(leaves)
                            unpaidleavename.append({"Leavename":getleavetype['Name']})
                    totalleave = sum(paidleave) + sum(unpaidleave)
                    Employeemodule = EmployeeMaster.objects.get(Employee_Id=j['Employee_Id'])
                    Employeeserialize=EmployeeMasterSerialzer(Employeemodule)
                    Empsalary = Employeeserialize.data
                    AnnualCTC_Id=Empsalary['AnnualCTC_Id']
                    Anualmodule = EmployeeAnnualCTC.objects.get(id=AnnualCTC_Id)
                    Annualserialize = EmployeeAnnualCTCSerialzer(Anualmodule)
                    Annualsalary=Annualserialize.data
                    Present = Timemodule - sum(unpaidleave)
                    onedaysalary=Annualsalary['monthly'] // 30
                    Deduct = sum(unpaidleave) *onedaysalary
                    Thismnthsalary = onedaysalary * Present
                    Totalsalary=(Annualsalary['monthly']) - Deduct
                    data1={"EmployeeName":j['Employee_FirstName'],"Designation":j['Designation'],"Department":j['Department'],
                        "Employee_Id":j['Employee_Id'],"Nofodays_Present":Present,"Month":data['Month'],"Onedaysalary":onedaysalary,
                        "NoofDays_Leave":totalleave,"Paid_Leave":sum(paidleave),"UnPaid_Leave":sum(unpaidleave),
                        "Monthly_Salary":Annualsalary['monthly'],"This_Month_Salary":Thismnthsalary,"Year":int(data['Year']),
                        "Other_Details":Annualsalary['Salarycomponents'],'Partner_Id':int(data['Partner_Id']),"PaidLeavename":paideleavename,"Unpaidleavename":unpaidleavename}
                    result.append(data1)
                    module = AttandancemasterSerialzer(data=data1)
                    if module.is_valid():
                        module.save()
                elif Empsalary['Cadre'] == 'Level3':
                    Checkinmodule = Employeecheckin.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Employee_Id = j['id']) & Q(Year = data['Year']) & Q(Month = data['Month']))
                    Checkinmodule1 = Checkinmodule.values('Date').distinct().count()
                    Jobcard=Jobtimecard.objects.filter(Q(Partner_Id=data['Partner_Id'])& Q(Month = data['Month']) & Q(Year=data['Year'])& Q(Is_Deleted=False))
                    Jobserialize = JobtimecardSerialzer(Jobcard,many=True)
                    jobout = Jobserialize.data
                    jobsalary=[]
                    for i in jobout:
                        servicepricemodule = Serviceprice.objects.get(id=i['service_Id'])
                        servicepriceserialize = ServicepriceSerialzer(servicepricemodule)
                        pricedata=servicepriceserialize.data
                        perserviceprice=(pricedata['Price'] // 43) * 100
                        jobsalary.append(perserviceprice)
                    leavemodule = Leave.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Employee_Id = j['id']) & Q(Year = data['Year']) & Q(Month = data['Month']) & Q(Approved_Flag=True))
                    # Timeserialize = TimeSheetSerialzer(Timemodule,many=True)
                    leaveserialize=LeaveSerialzer(leavemodule,many=True)
                    leave_calc= leaveserialize.data
                    paidleave=[]
                    paideleavename=[]
                    unpaidleave=[]
                    unpaidleavename=[]
                # leavecount=[]
                    for i in leave_calc:
                        leavetype=Leavetype.objects.get(id=i['Leavetype_Id'])
                        leserialize=LeavetypeSerialzer(leavetype)
                        getleavetype=leserialize.data
                        if getleavetype['Leave_Type']=='Paid leave':
                            fromdate = datetime.strptime(i['To_Date'], "%Y-%m-%d")
                            todate = datetime.strptime(i['From_Date'], "%Y-%m-%d")
                            leaves = (fromdate - todate ).days
                            paidleave.append(leaves)
                            paideleavename.append({"Leavename":getleavetype['Name']})
                        else:
                            fromdate = datetime.strptime(i['To_Date'], "%Y-%m-%d")
                            todate = datetime.strptime(i['From_Date'], "%Y-%m-%d")
                            leaves = (fromdate - todate).days
                            unpaidleave.append(leaves)
                            unpaidleavename.append({"Leavename":getleavetype['Name']})
                    totalleave = sum(paidleave) + sum(unpaidleave)
                    Employeemodule = EmployeeMaster.objects.get(Employee_Id=j['Employee_Id'])
                    Employeeserialize=EmployeeMasterSerialzer(Employeemodule)
                    Empsalary = Employeeserialize.data
                    AnnualCTC_Id=Empsalary['AnnualCTC_Id']
                    Anualmodule = EmployeeAnnualCTC.objects.get(id=AnnualCTC_Id)
                    Annualserialize = EmployeeAnnualCTCSerialzer(Anualmodule)
                    Annualsalary=Annualserialize.data
                    Present = Checkinmodule1 - sum(unpaidleave)
                    onedaysalary=Annualsalary['monthly'] // 30
                    Deduct = sum(unpaidleave) *onedaysalary
                    Thismnthsalary = sum(jobsalary)
                    Totalsalary=(Annualsalary['monthly']) - Deduct
                    data1={"EmployeeName":j['Employee_FirstName'],"Designation":j['Designation'],"Department":j['Department'],
                        "Employee_Id":j['Employee_Id'],"Nofodays_Present":Present,"Month":data['Month'],"Onedaysalary":onedaysalary,
                        "NoofDays_Leave":totalleave,"Paid_Leave":sum(paidleave),"UnPaid_Leave":sum(unpaidleave),
                        "Monthly_Salary":Annualsalary['monthly'],"This_Month_Salary":Thismnthsalary,"Year":int(data['Year']),
                        "Other_Details":Annualsalary['Salarycomponents'],'Partner_Id':int(data['Partner_Id']),"PaidLeavename":paideleavename,"Unpaidleavename":unpaidleavename}
                    result.append(data1)
                    module = AttandancemasterSerialzer(data=data1)
                    if module.is_valid():
                        module.save()
                elif Empsalary['Cadre'] == 'Level1':
                    print(Empsalary['Cadre'])
                    Checkinmodule = Employeecheckin.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Employee_Id = j['id']) & Q(Year = data['Year']) & Q(Month = data['Month']))
                    Checkinmodule1 = Checkinmodule.values('Date').distinct().count()
                    leavemodule = Leave.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Employee_Id = j['id']) & Q(Year = data['Year']) & Q(Month = data['Month']) & Q(Approved_Flag=True))
                    # Timeserialize = TimeSheetSerialzer(Timemodule,many=True)
                    leaveserialize=LeaveSerialzer(leavemodule,many=True)
                    leave_calc= leaveserialize.data
                    paidleave=[]
                    paideleavename=[]
                    unpaidleave=[]
                    unpaidleavename=[]
                # leavecount=[]
                    for i in leave_calc:
                        leavetype=Leavetype.objects.get(id=i['Leavetype_Id'])
                        leserialize=LeavetypeSerialzer(leavetype)
                        getleavetype=leserialize.data
                        if getleavetype['Leave_Type']=='Paid leave':
                            fromdate = datetime.strptime(i['To_Date'], "%Y-%m-%d")
                            todate = datetime.strptime(i['From_Date'], "%Y-%m-%d")
                            leaves = (fromdate - todate ).days
                            paidleave.append(leaves)
                            paideleavename.append({"Leavename":getleavetype['Name']})
                        else:
                            fromdate = datetime.strptime(i['To_Date'], "%Y-%m-%d")
                            todate = datetime.strptime(i['From_Date'], "%Y-%m-%d")
                            leaves = (fromdate - todate).days
                            unpaidleave.append(leaves)
                            unpaidleavename.append({"Leavename":getleavetype['Name']})
                    totalleave = sum(paidleave) + sum(unpaidleave)
                    Employeemodule = EmployeeMaster.objects.get(Employee_Id=j['Employee_Id'])
                    Employeeserialize=EmployeeMasterSerialzer(Employeemodule)
                    Empsalary = Employeeserialize.data
                    AnnualCTC_Id=Empsalary['AnnualCTC_Id']
                    Anualmodule = EmployeeAnnualCTC.objects.get(id=AnnualCTC_Id)
                    Annualserialize = EmployeeAnnualCTCSerialzer(Anualmodule)
                    Annualsalary=Annualserialize.data
                    Present = Checkinmodule1 - sum(unpaidleave)
                    onedaysalary=Annualsalary['monthly'] // 30
                    Deduct = sum(unpaidleave) *onedaysalary
                    Thismnthsalary = onedaysalary * Present
                    Totalsalary=(Annualsalary['monthly']) - Deduct
                    data1={"EmployeeName":j['Employee_FirstName'],"Designation":j['Designation'],"Department":j['Department'],
                        "Employee_Id":j['Employee_Id'],"Nofodays_Present":Present,"Month":data['Month'],"Onedaysalary":onedaysalary,
                        "NoofDays_Leave":totalleave,"Paid_Leave":sum(paidleave),"UnPaid_Leave":sum(unpaidleave),
                        "Monthly_Salary":Annualsalary['monthly'],"This_Month_Salary":Thismnthsalary,"Year":int(data['Year']),
                        "Other_Details":Annualsalary['Salarycomponents'],'Partner_Id':int(data['Partner_Id']),"PaidLeavename":paideleavename,"Unpaidleavename":unpaidleavename}
                    result.append(data1)
                    module = AttandancemasterSerialzer(data=data1)
                    if module.is_valid():
                        module.save()
            output1 = JSONRenderer().render(result)
            return HttpResponse(output1,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class AttandancemasterCRUD(APIView):
    
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = AttandancemasterSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['id']
            module = Attandancemaster.objects.get(id=id)
            serialize = AttandancemasterSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['id']
            module=Attandancemaster.objects.get(id=id)
            serialize = AttandancemasterSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['id']
            module=Attandancemaster.objects.get(id=id)
            serialize = AttandancemasterSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Processattandance(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data = request.query_params
            module = TimeSheet.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Month=data['Month']) & Q(Employee_Id=data['Employee_Id'])).count()
            module1 = TimeSheet.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Month=data['Month']) & Q(Employee_Id=data['Employee_Id']) & Q(Approved_Flag=True)).count()
            module2 = TimeSheet.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Month=data['Month']) & Q(Employee_Id=data['Employee_Id']) & Q(Approved_Flag=False)& Q(Draftflg=False)).count()
            module3 = TimeSheet.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Month=data['Month']) & Q(Employee_Id=data['Employee_Id']) & Q(Approved_Flag=False) & Q(Draftflg=True)).count()
            data1={"TimsheetSubmitted":module,"ApprovedTimeSheet":module1,"TimeSheetNotApproved":module2,"AttandanceProcessing":module3}
            output = JSONRenderer().render(data1)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class Timesheetapprovedwithtindatelist(APIView):
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            module = TimeSheet.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Employee_Id=data['Employee_Id'])& Q(FromDate__range=data['FromDate']) & Q(Approved_Flag=True)).order_by('-id')
            serialize = TimeSheetSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)

class PayslipCRUD(APIView):
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            module=Attandancemaster.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Month=data['Month']) & Q(Year=data['Year']) & Q(Approved_Flag=True))
            serialize = AttandancemasterSerialzer(module,many=True)
            Attandance = serialize.data
            print(Attandance)
            Employeepayslip = []
            if Attandance != []:
                for i in Attandance:
                    onedaysalary=i['Monthly_Salary'] // 30
                    Deductions=onedaysalary * i['UnPaid_Leave']
                    data1={"Partner_Id":data['Partner_Id'],"Year":data['Year'],"Month":data['Month'],"Company_Id":data['Company_Id'],
                        "Employee_Id":i['Employee_Id'],"Employee_Name":i['EmployeeName'],"Department":i['Department'],
                        "Noof_DaysPresent":i['Nofodays_Present'],"Deduction":{"Monthly_Salary":i['Monthly_Salary'],
                        "This_Month_Salary":i['This_Month_Salary'],"Other_Details":i['Other_Details']}}
                    Payslipserialize = PayslipHeaderSerialzer(data=data1)
                    if Payslipserialize.is_valid():
                        Payslipserialize.save()
                    output = JSONRenderer().render(Payslipserialize.data)
                    Employeepayslip.append(output)
                return HttpResponse(Employeepayslip,content_type='application/json',status=status.HTTP_200_OK)
            else:
                return HttpResponse(JSONRenderer().render(Attandance),content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)


    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = PayslipHeader.objects.get(Payslip_Id=data['Payslip_Id'])
            serialize = PayslipHeaderSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)


class PayslipwithoutCRUD(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = PayslipHeader.objects.get(id=data['id'])
            serialize = PayslipHeaderSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)

    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            module = PayslipHeader.objects.get(id=data['id'])
            serialize = PayslipHeaderSerialzer(instance=module,data=request.data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)


class PayrollProcessing(APIView):
   def get(self, request, *args, **kwargs):
        try:
            data = request.query_params
            month = data['Month']
            year = data['Year']
            attandance = Attandancemaster.objects.filter(Month=month, Year=year, Approved_Flag=True)
            employees = EmployeeMaster.objects.all()
            total_attendance = len(attandance)
            Employeepayslip = []
            for atten in attandance:
                if atten.Month == month and atten.Year == int(year):
                    for emp in employees:
                        company_id = emp.Company_Id
                        employees1 = EmployeeMaster.objects.filter(Company_Id=company_id.id)
                        total_employee_count = len(employees1)
                        company = Company.objects.get(id=company_id.id)
                        key = f"{atten.Month}"
                        print(key)
                        if key not in Employeepayslip:
                            id = len(Employeepayslip)
                            payslip_data = {
                                "id": id,
                                "Month": key,
                                "Year": atten.Year,
                                "Organization_Name": company.Company_Name,
                                "Total_Employees_Count": total_employee_count,
                                "Total_Attendance": total_attendance,
                                "Balance": total_employee_count - total_attendance,
                                "Check_Flag": False,
                                "Approved_Flag": False,
                                "Status_Flag": False
                            }
                            Employeepayslip.append(payslip_data)
                        else:
                            payslip_data = {
                            }
                            Employeepayslip.append(payslip_data)
                else:
                    print("Month and Year don't match")
            response_data = json.dumps(Employeepayslip)
            return HttpResponse(response_data, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = json.dumps({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)

class HRDashboard(ListAPIView):
    queryset = EmployeeMaster.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Partner.objects.filter(Company_Id=data['Company_Id'])
            serialize = PartnerSerialzer(module,many=True)
            partnerlist= serialize.data
            output1=[]
            for i in partnerlist:
                employeemodule = EmployeeMaster.objects.filter(Partner_Id=i['id']).count()
                data1={"Busnessunit_Name":i['Partner_Name'],"TotalEmployee":employeemodule}
                output1.append(data1)
            output = JSONRenderer().render(output1)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



# ponkumar modified this function on 17-11-2023
class ViewAttendance(APIView):
    def get(self, request, *args, **kwargs):
        try:
            data = request.query_params
            leave_query = Q(
                Partner_Id=data['Partner_Id'],
                Month=data['Month'],
                Approved_Flag=True,
                Is_Deleted=False
            )
            leave_records = Leave.objects.filter(leave_query)
            timesheet_records = TimeSheet.objects.filter(leave_query)
            employee_data = EmployeeMaster.objects.all()
            cadrelevels = Cadre.objects.filter(Is_Deleted = False)
            paycalendar = Paycalendar.objects.get()
            days_in_month = paycalendar.Workingdays if paycalendar.Actualdays != "Actual days in a month" else 31
            month_name_to_number = {
                'January': 1,
                'February': 2,
                'March': 3,
                'April': 4,
                'May': 5,
                'June': 6,
                'July': 7,
                'August': 8,
                'September': 9,
                'October': 10,
                'November': 11,
                'December': 12
            }
            result_data = []
            for emp in employee_data:
                name = emp.Employee_FirstName
                leave_type_map = {}
                for leave in leave_records:
                    from_date = leave.From_Date
                    to_date = leave.To_Date
                    leavetype_name = leave.Leavetype_Id.Name
                    num_days = (to_date - from_date).days + 1
                    for day in range(num_days):
                        current_date = from_date + timedelta(days=day)
                        date_str = current_date.strftime('%Y-%m-%d')
                        if emp.id == leave.Employee_Id.id:
                            leave_type_map[date_str] = leavetype_name
                timesheet_map = {}
                for timesheet in timesheet_records:
                    id = 'id'
                    from_date = timesheet.FromDate
                    to_date = timesheet.ToDate
                    num_days = (to_date - from_date).days + 1
                    for day in range(num_days):
                        current_date = from_date + timedelta(days=day)
                        date_str = current_date.strftime('%Y-%m-%d')
                        if emp.id == timesheet.Employee_Id.id:
                            timesheet_map[date_str] = 'P'
                leaves = sum(1 for leave_date, leave_type in leave_type_map.items() if leave_type != 'None')
                leave_info = []
                for day in range(1, days_in_month + 1):
                    month_number = month_name_to_number.get(data['Month'])
                    date_str = f"{data['Year']}-{month_number:02}-{day:02}"
                    leave_status = leave_type_map.get(date_str, 'None')
                    present_status = timesheet_map.get(date_str, None)
                    attendance = ''
                    presents = 0
                    absents = 0
                    for cadrelevel in cadrelevels:
                        print("cadrelevel",emp.Employee_FirstName,cadrelevel.Cadre_Name,emp.Cadre)
                        if emp.Designation == "Cleaner" and emp.Cadre != cadrelevel.Cadre_Name:
                            presents = 0
                            if leave_status != 'None':
                                attendance = 'L'
                            else:
                                attendance = 'A'
                        elif emp.Designation != "Cleaner" and emp.Cadre == cadrelevel.Cadre_Name:
                            presents = days_in_month - absents
                            attendance = 'P'
                            if leave_status != 'None':
                                attendance = 'L'
                        elif emp.Designation != "Cleaner" and emp.Cadre != cadrelevel.Cadre_Name:
                            presents = sum(1 for date, present_data in timesheet_map.items() if present_data == 'P')
                            if leave_status != 'None':
                                attendance = 'L'
                            elif present_status == 'P':
                                attendance = present_status
                            else:
                                attendance = 'A'
                    absents = days_in_month - leaves - presents
                    print(presents,leaves,absents)
                    leave_info.append({
                        'Date': date_str,
                        'Leave_Type': leave_status,
                        'Attendance': attendance,
                    })
                result_data.append({
                    "Name": name,
                    "leave": leave_info,
                    'Present_Days': presents,
                    'Leave_Days': leaves,
                    'Absent_Days': absents
                })
            output = JSONRenderer().render(result_data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)


class Sendpayslip(APIView):
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            module=PayslipHeader.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Month=data['Month']) & Q(Year=data['Year']) & Q(Is_Deleted=False))
            serialize = PayslipHeaderSerialzer(module,many=True)
            Attandance = serialize.data
            for i in Attandance:
                employeemodule = EmployeeMaster.objects.get(Employee_Id=i['Employee_Id'])
                email=employeemodule.Email_Id
                html = data['mailhtml'].replace('{{url}}',f'{Base_Url}/hr/payroll-payslip-pdf/?Payslip_Id='+str(i['Payslip_Id']))
                body=html
                SendMail(To=email,subject=data['subject'],From=settings.EMAIL_HOST_USER,Body=html,Host=settings.EMAIL_HOST,Port=settings.EMAIL_PORT,Password=settings.EMAIL_HOST_PASSWORD)
            output=JSONRenderer().render({"Message":"Mail sent Successfully"})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)


class HRDashboarddepartmentwiseemployee(ListAPIView):
    queryset = EmployeeMaster.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = EmployeeMaster.objects.filter(Q(Company_Id=data['Company_Id']) & Q(Partner_Id=data['Partner_Id']) & Q(Is_Deleted=False))
            serialize = EmployeeMasterSerialzer(module,many=True)
            Employeeserialize = serialize.data
            output1=[]
            for i in Employeeserialize:
                    data1={"Department":i['Department'],"Count":1}
                    output1.append(data1)
            output2={}
            for item in output1:
                Department=item['Department']
                Count=item['Count']
                if Department in output2:
                    output2[Department] +=Count
                else:
                    output2[Department] =Count
            output = JSONRenderer().render(output2)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class HRDashboardemployeename(ListAPIView):
    queryset = EmployeeMaster.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = EmployeeMaster.objects.filter(Q(Company_Id=data['Company_Id']) & Q(Partner_Id=data['Partner_Id']) & Q(Department=data['Department'])& Q(Is_Deleted=False))
            serialize = EmployeeMasterSerialzer(module,many=True)
            Employeeserialize = serialize.data
            output1=[]
            for i in Employeeserialize:
                data1={"Employee_Name":i['Employee_FirstName'],"Mobile_Number":i['Mobile_No']}
                output1.append(data1)
            output = JSONRenderer().render(output1)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class Departmentwiseemployee(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Empmodule=EmployeeMaster.objects.filter(Q(Company_Id=data['Company_Id']) & Q(Is_Deleted=False)).count()
            Depmodule=Department.objects.filter(Q(Company_Id=data['Company_Id']) & Q(Is_Deleted=False))
            depserialize = DepartmentSerialzer(Depmodule,many=True)
            deplist=depserialize.data
            output1=[]
            for i in deplist:
                employeemodule = EmployeeMaster.objects.filter(Q(Company_Id=data['Company_Id']) & Q(Department=i['Department_Name']) & Q(Is_Deleted=False)).count()
                avg=(employeemodule/Empmodule) * 100
                data1={"Department_Name":i['Department_Name'],"Employee_Count":employeemodule,"Percentage":avg}
                output1.append(data1)
            output = JSONRenderer().render(output1)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)



class Agewiseemployee(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Empmodule=EmployeeMaster.objects.filter(Q(Company_Id=data['Company_Id']) & Q(Is_Deleted=False)).count()
            Agemodule=EmployeeMaster.objects.filter(Q(Company_Id=data['Company_Id']) & Q(Is_Deleted=False))
            depserialize = EmployeeMasterSerialzer(Agemodule,many=True)
            deplist=depserialize.data
            age18=0
            age30=0
            age40=0
            age50=0
            for i in deplist:
                if i['Age'] >= 18 and i['Age'] <= 30:
                    age18=age18+ 1
                elif i['Age'] >= 31 and i['Age'] <= 40:
                    age30=age30+ 1
                elif i['Age'] >= 41 and i['Age'] <= 50:
                    age40=age40+ 1
                elif i['Age'] >= 51:
                    age50=age50+ 1
            data1=[{"Age":"18 to 30","Count":age18,"Avg":(age18/Empmodule)*100},
                {"Age":"30 to 40","Count":age30,"Avg":(age30/Empmodule)*100},
                {"Age":"40 to 50","Count":age40,"Avg":(age40/Empmodule)*100},
                {"Age":"50 to 100","Count":age50,"Avg":(age50/Empmodule)*100}]
            output = JSONRenderer().render(data1)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)



class Experiencewiseemployee(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Empmodule=EmployeeMaster.objects.filter(Q(Company_Id=data['Company_Id']) & Q(Is_Deleted=False)).count()
            Agemodule=EmployeeMaster.objects.filter(Q(Company_Id=data['Company_Id']) & Q(Is_Deleted=False))
            depserialize = EmployeeMasterSerialzer(Agemodule,many=True)
            deplist=depserialize.data
            exp1=0
            exp2=0
            exp3=0
            exp4=0
            exp5=0
            exp7=0
            exp9=0
            exp11=0
            for i in deplist:
                if i['Experience'] ==0 and i['Experience'] <1:
                    exp1=exp1+ 1
                elif i['Experience'] ==1 and i['Experience'] <2:
                    exp2=exp2+ 1
                elif i['Experience'] ==2 and i['Experience'] <3:
                    exp3=exp3+ 1
                elif i['Experience'] ==3 and i['Experience'] <4:
                    exp4=exp4+ 1
                elif i['Experience'] ==4 and i['Experience'] <5:
                    exp5=exp5+ 1
                elif i['Experience'] ==5 and i['Experience'] <6:
                    exp7=exp7+ 1
                elif i['Experience'] ==6 or i['Experience'] <8:
                    exp9=exp9+ 1
                elif i['Experience'] ==8 or i['Experience'] <10:
                    exp11=exp11+ 1
            data1=[{"Exp":"0 to 1","Count":exp1,"Avg":(exp1/Empmodule)*100},
                {"Exp":"1 to 2","Count":exp2,"Avg":(exp2/Empmodule)*100},
                {"Exp":"2 to 3","Count":exp3,"Avg":(exp3/Empmodule)*100},
                {"Exp":"3 to 4","Count":exp4,"Avg":(exp4/Empmodule)*100},
                {"Exp":"4 to 5","Count":exp5,"Avg":(exp5/Empmodule)*100},
                {"Exp":"5 to 6","Count":exp7,"Avg":(exp7/Empmodule)*100},
                {"Exp":"6 to 8","Count":exp9,"Avg":(exp9/Empmodule)*100},
                {"Exp":"8 to 10","Count":exp11,"Avg":(exp11/Empmodule)*100}]
            output = JSONRenderer().render(data1)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)


class Genderwiseemployee(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Empmodule=EmployeeMaster.objects.filter(Q(Company_Id=data['Company_Id']) & Q(Is_Deleted=False)).count()
            Agemodule=EmployeeMaster.objects.filter(Q(Company_Id=data['Company_Id']) & Q(Is_Deleted=False))
            depserialize = EmployeeMasterSerialzer(Agemodule,many=True)
            deplist=depserialize.data
            male=0
            female=0
            others=0
            for i in deplist:
                if i['Gender'] =="Male":
                    male=male+1
                elif i['Gender'] =="Female":
                    female=female+ 1
                elif i['Gender'] =="Others":
                    others=others+ 1
            data1=[{"Gender":"Male","Count":male,"Avg":(male/Empmodule)*100},
                {"Gender":"Female","Count":female,"Avg":(female/Empmodule)*100},
                {"Gender":"Others","Count":others,"Avg":(others/Empmodule)*100}]
            output = JSONRenderer().render(data1)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)


class Employeeaddition(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = EmployeeMaster.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Is_Deleted=False))
            serialize= EmployeeMasterSerialzer(module, many=True)
            empser=serialize.data
            monthlist=[]
            finallist={}
            for i in empser:
                datestring = i['Created_Date']
                date_object = datetime.strptime(datestring, "%Y-%m-%d")
                month_name = date_object.strftime("%B")
                data1={"Month":month_name,"Count":1}
                monthlist.append(data1)
            for item in monthlist:
                Month=item["Month"]
                Count=item["Count"]
                if Month in finallist:
                    finallist[Month]+=Count
                else:
                    finallist[Month]=Count
            output = JSONRenderer().render(finallist)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Employeeattrition(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = EmployeeMaster.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Resigned_Flg=True))
            serialize= EmployeeMasterSerialzer(module, many=True)
            empser=serialize.data
            monthlist=[]
            finallist={}
            for i in empser:
                datestring = i['Created_Date']
                date_object = datetime.strptime(datestring, "%Y-%m-%d")
                month_name = date_object.strftime("%B")
                data1={"Month":month_name,"Count":1}
                monthlist.append(data1)
            for item in monthlist:
                Month=item["Month"]
                Count=item["Count"]
                if Month in finallist:
                    finallist[Month]+=Count
                else:
                    finallist[Month]=Count
            output = JSONRenderer().render(finallist)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Employeeorganization(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            usermodule = User.objects.filter(UserType='Corporate')
            userserialize = UserSerializer(usermodule,many=True)
            userhead=userserialize.data
            empdata=[]
            for i in userhead:
                module = EmployeeMaster.objects.filter(Reportingto=i['first_name'])
                serialize = EmployeeMasterSerialzer(module , many=True)
                emporg=serialize.data
                for y in emporg:
                    data1={"Employee_FirstName":y['Employee_FirstName'],"Department":y['Department'],"Mobile_No":y['Mobile_No']}
                    empdata.append({"Head":i,"Employee":data1})
            output = JSONRenderer().render(empdata)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Employeeorganizationreporting(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = EmployeeMaster.objects.filter(Reportingto=data['Reportingto'])
            serialize = EmployeeMasterSerialzer(module , many=True)
            empdata=[]
            emporg=serialize.data
            for i in emporg:
                data1={"Employee_FirstName":i['Employee_FirstName'],"Department":i['Department'],"Mobile_No":i['Mobile_No']}
                empdata.append(data1)
            output = JSONRenderer().render(empdata)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class HRApprovalsystem(APIView):
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            # Workflow_Id=data['Workflow_Id']
            Employee_Id=data['Employee_Id']
            # print(Depname['Department_Head'])
            # html =f'{Base_Url}/inventory/material-request/document-approval'
            Created_By=data['Created_By']
            first_name=data['approver_name']
            Approverid=data['Approverid']
            module=User.objects.get(id=Approverid)
            serialize = UserSerializer(module)
            userdata=serialize.data
            # use=userdata[0]
            To=userdata['email']
            module1 = MailConfiguration.objects.get(Created_By=Created_By)
            serialize = MailConfigurationSerializer(module1)
            com=serialize.data
            From=com['MailId']
            subject=data['mailsubject']
            # Body=html
            Host=com['SMTPHost']
            Port = com['SMTPPort']
            Password=com['MailPassword']
            if Employee_Id != "":
                module = EmployeeMaster.objects.get(Employee_Id=Employee_Id)
                serialize = EmployeeMasterSerialzer(module)
                emp=serialize.data
                Dep_id=emp['Department_Id']
                module = Department.objects.get(id=Dep_id)
                serialize = DepartmentSerialzer(module)
                Depname=serialize.data
                module = User.objects.get(id=Depname['Updated_By'])
                serialize = UserSerializer(module)
                head=serialize.data
                To=head['email']
                html = data['mailhtml'].replace('{{url}}',f'{Base_Url}/hr/DocumentApprovalForHr/?Document_Type='+str(data['Document_Type'])+"&id="+str(data['id'])+"&Initiated_By=" +data['Initiated_By']+"&Url=" +data['Url']+"&Comments=" +data['Comments']+"&index="+str(data['index'])+"&documentapprovalid="+data['documentapprovalid'] +"&Designation="+head['Designation']+"&Headname="+head['first_name'])
                Body = html
                output = {"Headname":head['first_name'],"rolename":head['Designation'],"Message": "Mail sent Successfully","id":head['id']}
                response = SendMail(subject, From, To,Body, Host, Port,Password)
                return HttpResponse(JSONRenderer().render(output),
                                            content_type='application/json', status=status.HTTP_200_OK)
            else:
                html = data['mailhtml'].replace('{{url}}',f'{Base_Url}/hr/DocumentApprovalForHr/?Document_Type='+str(data['Document_Type'])+"&id="+str(data['id'])+"&Initiated_By=" +data['Initiated_By']+"&Url=" +data['Url']+"&Comments=" +data['Comments']+"&index="+str(data['index'])+"&documentapprovalid="+data['documentapprovalid']+"&Designation="+data['Designation']+"&Headname="+data['Headname'])
                Body = html
                To=userdata['email']
                response = SendMail(subject, From, To,Body, Host, Port,Password)
                return HttpResponse(JSONRenderer().render({"Message": "Mail sent Successfully"}),
                                            content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            return HttpResponse(JSONRenderer().render({"Message": "Mail Not sent"}),
                                        content_type='application/json', status=status.HTTP_200_OK)



# Sales Module API's
class CustomerCRUD(ListAPIView):
    queryset = Customer.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Add = data['CustAddress_Id']
            Addd=[]
            for i in Add:
                serialize= CustomerAddressSerialzer(data=i)
                if serialize.is_valid():
                    serialize.save()
                Ad = serialize.data
                Addd.append(Ad['id'])
            Cars=[]
            car=data['Customer_Car']
            for y in car:
                serialize= CarSerialzer(data=y)
                if serialize.is_valid():
                    serialize.save()
                CA = serialize.data
                Cars.append(CA['id'])
            data['CustAddress_Id']=Addd
            data['Customer_Car'] = Cars
            serialize = CustomerSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
            return HttpResponse(JSONRenderer().render(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Customer_Id']
            module = Customer.objects.get(Customer_Id=id)
            serialize = CustomerSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Customer_Id']
            Add=data['CustAddress_Id']
            Cars=data['Customer_Car']
            for i in Add:
                module = CustomerAddress.objects.get(CustAddress_Id=i['CustAddress_Id'])
                serialize = CustomerAddressSerialzer(instance=module,data=i)
                if serialize.is_valid():
                    serialize.save()
            for y in Cars:
                module = Car.objects.get(id=y['id'])
                serialize = CarSerialzer(instance=module,data=y)
                if serialize.is_valid():
                    serialize.save()
            module=Customer.objects.get(Customer_Id=id)
            serialize = CustomerSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
            return HttpResponse(JSONRenderer().render(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Customer_Id']
            module=Customer.objects.get(Customer_Id=id)
            serialize = CustomerSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Customerlist(ListAPIView):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        Partner_Id=self.request.GET.get('Partner_Id')
        module = Customer.objects.filter(Q(Partner_Id=Partner_Id) & Q(Is_Deleted=False)).order_by('-id')
        return module

class companycustomerlist(APIView):
    def get(self,request,*args,**kwargs):
        try:
            module = Customer.objects.filter(Is_Deleted=False)
            serialize = CustomerSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Customerlistwithoutpagination(ListAPIView):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]   


    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Customer.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Is_Deleted=False)).order_by('-id')
            serialize = CustomerSerialzers(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)

    # def get_queryset(self):
    #     Partner_Id=self.request.GET.get('Partner_Id')
    #     module = Customer.objects.filter(Q(Partner_Id=Partner_Id) & Q(Is_Deleted=False)).order_by('-id')
    #     return module


class Customersearch(ListAPIView):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data=self.request.GET.get('search')
        module = Customer.objects.filter(Q(Cus_FirstName__icontains=data) | Q(Email_Id__icontains=data) | Q(Location__icontains=data),Is_Deleted=False).order_by('-id')
        return module


class Customerfilter(ListAPIView):
    queryset = Customer.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        data=request.data
        data1={"Cus_FirstName__icontains":data['Cus_FirstName'],"Location":data['Location'],"Created_Date__range":data['Created_Date'],"Is_Deleted":False} 
        return requestget(model=Customer,serializer=CustomerSerialzers,data=data1,page=data['page'],Operator='and')   



class CustomerrefundsCRUD(ListAPIView):
    queryset = Customerrefunds.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            SalesOrder_Id=data['ServiceOrder_Id']
            module = Invoice.objects.filter(SalesOrder_Id=SalesOrder_Id)
            serialize = InvoiceSerialzer(module,many=True)
            invoice=serialize.data
            data['Invoice_Id']=invoice[0]
            serialize = CustomerrefundsSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Refund_Id']
            module = Customerrefunds.objects.get(Refund_Id=id)
            serialize = CustomerrefundsSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            module = Customerrefunds.objects.get(Refund_Id=data['Refund_Id'])
            serialize = CustomerrefundsSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Refund_Id']
            module=Customerrefunds.objects.get(Refund_Id=id)
            serialize = CustomerrefundsSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Customerrefundslist(ListAPIView):
    queryset = Customerrefunds.objects.all()
    serializer_class = CustomerrefundsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]   


    def get_queryset(self):
        Partner_Id = self.request.GET.get('Partner_Id')
        module = Customerrefunds.objects.filter(Q(Partner_Id=Partner_Id) & Q(Is_Deleted=False)).order_by('-id')
        return module


class Customerrefundsearch(ListAPIView):
    queryset = Customerrefunds.objects.all()
    serializer_class = CustomerrefundsSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data=self.request.GET.get('search')
        module = Customerrefunds.objects.filter((Q(Customer_Name__icontains=data)| Q(ServiceOrder_Id__icontains=data) | Q(Service_Name__icontains=data) | Q(Refund_Id__icontains=data) |
             Q(Cancellation_Id__Cancellation_Id__icontains=data)|Q(Paid_Amount__icontains=data))& Q(Is_Deleted=False)).order_by('-id')
        return module


class Customerrefundfilter(ListAPIView):
    queryset = Customerrefunds.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        data=request.data
        data1={"Customer_Name__icontains":data['Customer_Name'],"ServiceOrder_Id__icontains":data['ServiceOrder_Id'],"Service_Name__icontains":data['Service_Name'],"Refund_Date__range":data['Refund_Date'],"Refund_Id__icontains":data['Refund_Id'],"Is_Deleted":False} 
        return requestget(model=Customerrefunds,serializer=CustomerrefundsSerialzers,data=data1,page=data['page'],Operator='and') 


class Customerrefundlistwithperiod(ListAPIView):
    queryset = Customerrefunds.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        data=request.data
        data1={"Partner_Id":data['Partner_Id'],"Refund_Date__range":data['Refund_Date'],"Is_Deleted":False}
        return requestget(model=Customerrefunds,serializer=CustomerrefundsSerialzers,data=data1,page=data['page'],Operator='and')


class Customerrefundwithouttoken(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['id']
            module = Customerrefunds.objects.get(id=id)
            serialize = CustomerrefundsSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            module = Customerrefunds.objects.get(id=data['id'])
            serialize = CustomerrefundsSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class salesordertransaction(ListAPIView):
    queryset = SalesOrder.objects.all()
    serializer_class = SalesOrderSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        data=request.data
        module = SalesOrder.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Order_Cancelled=data['Order_Cancelled'])& Q(Is_Deleted=False))
        serialize = SalesOrderSerialzers(module,many=True)
        ser=serialize.data
        monthlist=[]
        for i in ser:
            datestring = i['SalesOrderDate']
            date_object = datetime.strptime(datestring, "%Y-%m-%d")
            month_name = date_object.strftime("%B")
            monthlist.append({"Month":month_name,"orderlist":i})
        group={}
        for entry in monthlist:
            month = entry["Month"]
            data1 = entry["orderlist"]
    
            # Check if the month is already a key in the dictionary
            if month in group:
                group[month].append(data1)
            else:
        # Create a new entry for the month if it doesn't exist
                group[month] = [data1]
# Convert the grouped data dictionary to the desired list format
        result_list = [{"month": month, "data1": data_list} for month, data_list in group.items()]
        total_amount=0.0
        for entry in result_list:
            for item in entry["data1"]:
                total_amount += item["Amount_Paid"]
        result_list.append({"Total_Amount":total_amount})
        output = JSONRenderer().render(result_list)
        return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)       

#     def post(self,request,*args,**kwargs):
#         data=request.data
#         module = SalesOrder.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Order_Cancelled=data['Order_Cancelled'])& Q(Is_Deleted=False))
#         serialize = SalesOrderSerialzer(module,many=True)
#         ser=serialize.data
#         monthlist=[]
#         for i in ser:
#             datestring = i['SalesOrderDate']
#             date_object = datetime.strptime(datestring, "%Y-%m-%d")
#             month_name = date_object.strftime("%B")
#             month_number = datetime.strptime(month_name, '%B').month
#             total_amount_paid = SalesOrder.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Order_Cancelled=data['Order_Cancelled'])& Q(Is_Deleted=False) & Q(SalesOrderDate__month=month_number)).aggregate(Sum('Amount_Paid'))['Amount_Paid__sum']
#             monthlist.append({"Month":month_name,"orderlist":i,"month_amount":total_amount_paid})
#         group={}
#         for entry in monthlist:
#             month = entry["Month"]
#             data1 = entry["orderlist"]
#             month_amount = entry['month_amount']
    
#             # Check if the month is already a key in the dictionary
#             if month in group:
#                 group[month].append({"data1": data1, "month_amount": month_amount})
#             else:
#         # Create a new entry for the month if it doesn't exist
#                 group[month] = [{"data1": data1, "month_amount": month_amount}]
#     # Convert the grouped data dictionary to the desired list format
#         amount_paid_values = {}

#         for month, month_data in group.items():
#     # Use a list comprehension to extract Amount_Paid values from the month_data
#             amount_paid_values[month] = [item['data1']['Amount_Paid'] for item in month_data]
            

#     # Now, amount_paid_values will be a dictionary where the keys are months, and the values are lists of Amount_Paid values for each month
#         total_amounts = {}

# # Iterate through the dictionary and calculate the sum for each month
#         for month, amounts in amount_paid_values.items():
#             total_amount = sum(amounts)
#             total_amounts[month] = total_amount
#         total_sum = sum(total_amounts.values())
#         out=[]
#         out.append({"Data":group,"Total_Amount":total_sum})

#     # Now, total_sum will contain the sum of all the values
#         print(total_sum)
#         # result_list = [{"month": month,"data1": data_list} for month,data_list in group.items()]
#         # amount_paid_list = [item['orderlist']['Amount_Paid'] for item in group]
#         # print(amount_paid_list)
#         # total_amount=0.0
#         # print(group)
#         # for entry in result_list:
#         #     for item in entry["data1"]:
#         #         total_amount += item["month_amount"]
#         # result_list.append({"Total_Amount":total_amount})
        
#         output = JSONRenderer().render(out)
#         return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)       


class salesordertransactionwithperiod(ListAPIView):
    queryset = SalesOrder.objects.all()
    serializer_class = SalesOrderSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        data=request.data
        module = SalesOrder.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Order_Cancelled=data['Order_Cancelled'])& Q(Is_Deleted=False) & Q(SalesOrderDate__range=data['SalesOrderDate']))
        serialize = SalesOrderSerialzers(module,many=True)
        ser=serialize.data
        monthlist=[]
        for i in ser:
            datestring = i['SalesOrderDate']
            date_object = datetime.strptime(datestring, "%Y-%m-%d")
            month_name = date_object.strftime("%B")
            monthlist.append({"Month":month_name,"orderlist":i})
        group={}
        for entry in monthlist:
            month = entry["Month"]
            data1 = entry["orderlist"]
    
            # Check if the month is already a key in the dictionary
            if month in group:
                group[month].append(data1)
            else:
        # Create a new entry for the month if it doesn't exist
                group[month] = [data1]
# Convert the grouped data dictionary to the desired list format
        result_list = [{"month": month, "data1": data_list} for month, data_list in group.items()]
        total_amount=0.0
        for entry in result_list:
            for item in entry["data1"]:
                total_amount += item["Amount_Paid"]
        result_list.append({"Total_Amount":total_amount})
        output = JSONRenderer().render(result_list)
        return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)       


class salesordercancelation(ListAPIView):
    queryset = Servicecancellations.objects.all()
    serializer_class = ServicecancellationsSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        data=request.data
        module = Servicecancellations.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Cancel_Status=True)& Q(Is_Deleted=False))
        serialize = ServicecancellationsSerialzers(module,many=True)
        ser=serialize.data
        monthlist=[]
        for i in ser:
            datestring = i['Created_Date']
            date_object = datetime.strptime(datestring, "%Y-%m-%d")
            month_name = date_object.strftime("%B")
            monthlist.append({"Month":month_name,"orderlist":i})
        group={}
        for entry in monthlist:
            month = entry["Month"]
            data1 = entry["orderlist"]
    
            # Check if the month is already a key in the dictionary
            if month in group:
                group[month].append(data1)
            else:
        # Create a new entry for the month if it doesn't exist
                group[month] = [data1]
# Convert the grouped data dictionary to the desired list format
        result_list = [{"month": month, "data1": data_list} for month, data_list in group.items()]
        total_amount=0.0
        for entry in result_list:
            for item in entry["data1"]:
                total_amount += item["Cancel_Amount"]
        result_list.append({"Total_Amount":total_amount})
        output = JSONRenderer().render(result_list)
        return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)       
       


class salesordercancelationwithperiod(ListAPIView):
    queryset = Servicecancellations.objects.all()
    serializer_class = ServicecancellationsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        data=request.data
        module = Servicecancellations.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Cancel_Status=True)& Q(Is_Deleted=False)& Q(Created_Date__range=data['Created_Date']))
        serialize = ServicecancellationsSerialzers(module,many=True)
        ser=serialize.data
        monthlist=[]
        for i in ser:
            datestring = i['Created_Date']
            date_object = datetime.strptime(datestring, "%Y-%m-%d")
            month_name = date_object.strftime("%B")
            monthlist.append({"Month":month_name,"orderlist":i})
        group={}
        for entry in monthlist:
            month = entry["Month"]
            data1 = entry["orderlist"]
    
            # Check if the month is already a key in the dictionary
            if month in group:
                group[month].append(data1)
            else:
        # Create a new entry for the month if it doesn't exist
                group[month] = [data1]
# Convert the grouped data dictionary to the desired list format
        result_list = [{"month": month, "data1": data_list} for month, data_list in group.items()]
        total_amount=0.0
        for entry in result_list:
            for item in entry["data1"]:
                total_amount += item["Cancel_Amount"]
        result_list.append({"Total_Amount":total_amount})
        output = JSONRenderer().render(result_list)
        return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)       


class Salesordertransactionsearch(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = SalesOrder.objects.filter((Q(SalesOrder_Id__icontains=data['search'])|Q(Customer_Id__Cus_FirstName__icontains=data['search']) 
                |Q(SalesOrder_Type__icontains=data['search']) | Q(Amount_Paid__icontains=data['search']) | Q(Assignedservice_Personal__icontains=data['search']))
                &Q(Partner_Id=data['Partner_Id']) & Q(Order_Cancelled=data['Order_Cancelled'])& Q(Is_Deleted=False)).order_by('-id')
            serialize = SalesOrderSerialzers(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Salesinvoice(ListAPIView):
    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]   

    def post(self,request,*args,**kwargs):
        data=request.data
        module = Invoice.objects.filter(Q(Partner_Id=data['Partner_Id'])& Q(Is_Deleted=False))
        serialize = InvoiceSerialzers(module,many=True)
        ser=serialize.data
        monthlist=[]
        for i in ser:
            datestring = i['Created_Date']
            date_object = datetime.strptime(datestring, "%Y-%m-%d")
            month_name = date_object.strftime("%B")
            monthlist.append({"Month":month_name,"orderlist":i})
        group={}
        for entry in monthlist:
            month = entry["Month"]
            data1 = entry["orderlist"]
    
            # Check if the month is already a key in the dictionary
            if month in group:
                group[month].append(data1)
            else:
        # Create a new entry for the month if it doesn't exist
                group[month] = [data1]
# Convert the grouped data dictionary to the desired list format
        result_list = [{"month": month, "data1": data_list} for month, data_list in group.items()]
        total_amount=0.0
        for entry in result_list:
            for item in entry["data1"]:
                total_amount += item["Net_Amt"]
        result_list.append({"Total_Amount":total_amount})
        output = JSONRenderer().render(result_list)
        return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)       


class Salesinvoicewithperiod(ListAPIView):
    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]   

    def post(self,request,*args,**kwargs):
        data=request.data
        module = Invoice.objects.filter(Q(Partner_Id=data['Partner_Id'])& Q(Created_Date__range=data['Created_Date'])& Q(Is_Deleted=False))
        serialize = InvoiceSerialzers(module,many=True)
        ser=serialize.data
        monthlist=[]
        for i in ser:
            datestring = i['Created_Date']
            date_object = datetime.strptime(datestring, "%Y-%m-%d")
            month_name = date_object.strftime("%B")
            monthlist.append({"Month":month_name,"orderlist":i})
        group={}
        for entry in monthlist:
            month = entry["Month"]
            data1 = entry["orderlist"]
    
            # Check if the month is already a key in the dictionary
            if month in group:
                group[month].append(data1)
            else:
        # Create a new entry for the month if it doesn't exist
                group[month] = [data1]
# Convert the grouped data dictionary to the desired list format
        result_list = [{"month": month, "data1": data_list} for month, data_list in group.items()]
        total_amount=0.0
        for entry in result_list:
            for item in entry["data1"]:
                total_amount += item["Net_Amt"]
        result_list.append({"Total_Amount":total_amount})
        output = JSONRenderer().render(result_list)
        return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)       


class Salesinvoicesearch(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Invoice.objects.filter((Q(Invoice_Id__icontains=data['search'])|Q(Customer_Id__Cus_FirstName__icontains=data['search']) 
                |Q(SalesPerson__icontains=data['search']) | Q(Amount__icontains=data['search']) | Q(SalesOrder_Id__SalesOrder_Id__icontains=data['search'])
                |Q(Service_Id__Service_Name__icontains=data['search']))&Q(Partner_Id=data['Partner_Id'])& Q(Is_Deleted=False)).order_by('-id')
            serialize = InvoiceSerialzers(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class salesorderrefund(ListAPIView):
    queryset = SalesOrder.objects.all()
    serializer_class = SalesOrderSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        data=request.data
        module = SalesOrder.objects.filter(Q(Partner_Id=data['Partner_Id'])& Q(TotalAmt_Refunded=data['TotalAmt_Refunded']) & Q(Order_Cancelled=['Order_Cancelled']) & Q(Is_Deleted=False))
        serialize = SalesOrderSerialzers(module,many=True)
        ser=serialize.data
        monthlist=[]
        for i in ser:
            datestring = i['SalesOrderDate']
            date_object = datetime.strptime(datestring, "%Y-%m-%d")
            month_name = date_object.strftime("%B")
            monthlist.append({"Month":month_name,"orderlist":i})
        group={}
        for entry in monthlist:
            month = entry["Month"]
            data1 = entry["orderlist"]
    
            # Check if the month is already a key in the dictionary
            if month in group:
                group[month].append(data1)
            else:
        # Create a new entry for the month if it doesn't exist
                group[month] = [data1]
# Convert the grouped data dictionary to the desired list format
        result_list = [{"month": month, "data1": data_list} for month, data_list in group.items()]
        total_amount=0.0
        for entry in result_list:
            for item in entry["data1"]:
                total_amount += item["Amount_Paid"]
        result_list.append({"Total_Amount":total_amount})
        output = JSONRenderer().render(result_list)
        return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)       


class salesorderrefundwithperiod(ListAPIView):
    queryset = SalesOrder.objects.all()
    serializer_class = SalesOrderSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        data=request.data
        module = SalesOrder.objects.filter(Q(Partner_Id=data['Partner_Id'])& Q(TotalAmt_Refunded=data['TotalAmt_Refunded']) & Q(Order_Cancelled=data['Order_Cancelled'])& Q(SalesOrderDate__range=data['SalesOrderDate']) & Q(Is_Deleted=False))
        serialize = SalesOrderSerialzers(module,many=True)
        ser=serialize.data
        monthlist=[]
        for i in ser:
            datestring = i['SalesOrderDate']
            date_object = datetime.strptime(datestring, "%Y-%m-%d")
            month_name = date_object.strftime("%B")
            monthlist.append({"Month":month_name,"orderlist":i})
        group={}
        for entry in monthlist:
            month = entry["Month"]
            data1 = entry["orderlist"]
    
            # Check if the month is already a key in the dictionary
            if month in group:
                group[month].append(data1)
            else:
        # Create a new entry for the month if it doesn't exist
                group[month] = [data1]
# Convert the grouped data dictionary to the desired list format
        result_list = [{"month": month, "data1": data_list} for month, data_list in group.items()]
        total_amount=0.0
        for entry in result_list:
            for item in entry["data1"]:
                total_amount += item["Amount_Paid"]
        result_list.append({"Total_Amount":total_amount})
        output = JSONRenderer().render(result_list)
        return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)


class servicewisetransaction(ListAPIView):
    queryset = SalesOrder.objects.all()
    serializer_class = SalesOrderSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        data=request.data
        module = SalesOrder.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Service_Id=data['Service_Id']) & Q(Order_Cancelled=data['Order_Cancelled'])& Q(Is_Deleted=False))
        serialize = SalesOrderSerialzers(module,many=True)
        ser=serialize.data
        print(ser)
        monthlist=[]
        for i in ser:
            datestring = i['SalesOrderDate']
            date_object = datetime.strptime(datestring, "%Y-%m-%d")
            month_name = date_object.strftime("%B")
            monthlist.append({"Month":month_name,"orderlist":i})
        group={}
        for entry in monthlist:
            month = entry["Month"]
            data1 = entry["orderlist"]
    
            # Check if the month is already a key in the dictionary
            if month in group:
                group[month].append(data1)
            else:
        # Create a new entry for the month if it doesn't exist
                group[month] = [data1]
# Convert the grouped data dictionary to the desired list format
        result_list = [{"month": month, "data1": data_list} for month, data_list in group.items()]
        total_amount=0.0
        print(result_list)
        for entry in result_list:
            for item in entry["data1"]:
                total_amount += item["Amount_Paid"]
        result_list.append({"Total_Amount":total_amount})
        output = JSONRenderer().render(result_list)
        return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)       

class servicewisetransactionwithperiod(ListAPIView):
    queryset = SalesOrder.objects.all()
    serializer_class = SalesOrderSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        data=request.data
        module = SalesOrder.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Service_Id=data['Service_Id']) & Q(SalesOrderDate__range=data['SalesOrderDate']) & Q(Order_Cancelled=data['Order_Cancelled'])& Q(Is_Deleted=False))
        serialize = SalesOrderSerialzers(module,many=True)
        ser=serialize.data
        monthlist=[]
        for i in ser:
            datestring = i['SalesOrderDate']
            date_object = datetime.strptime(datestring, "%Y-%m-%d")
            month_name = date_object.strftime("%B")
            monthlist.append({"Month":month_name,"orderlist":i})
        group={}
        for entry in monthlist:
            month = entry["Month"]
            data1 = entry["orderlist"]
    
            # Check if the month is already a key in the dictionary
            if month in group:
                group[month].append(data1)
            else:
        # Create a new entry for the month if it doesn't exist
                group[month] = [data1]
# Convert the grouped data dictionary to the desired list format
        result_list = [{"month": month, "data1": data_list} for month, data_list in group.items()]
        total_amount=0.0
        for entry in result_list:
            for item in entry["data1"]:
                total_amount += item["Amount_Paid"]
        result_list.append({"Total_Amount":total_amount})
        output = JSONRenderer().render(result_list)
        return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)


class Salesservicesearch(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = SalesOrder.objects.filter((Q(SalesOrder_Id__icontains=data['search'])|Q(Customer_Id__Cus_FirstName__icontains=data['search']) 
                |Q(SalesOrder_Type__icontains=data['search']) | Q(Amount_Paid__icontains=data['search']) | Q(Assignedservice_Personal__icontains=data['search']))
                &Q(Partner_Id=data['Partner_Id']) & Q(Service_Id=data['Service_Id'])& Q(Order_Cancelled=data['Order_Cancelled'])& Q(Is_Deleted=False)).order_by('-id')
            serialize = SalesOrderSerialzers(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class customerwisetransaction(ListAPIView):
    queryset = SalesOrder.objects.all()
    serializer_class = SalesOrderSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        data=request.data
        module = SalesOrder.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Customer_Id=data['Customer_Id']) & Q(Order_Cancelled=data['Order_Cancelled'])& Q(Is_Deleted=False))
        serialize = SalesOrderSerialzers(module,many=True)
        ser=serialize.data
        monthlist=[]
        for i in ser:
            datestring = i['SalesOrderDate']
            date_object = datetime.strptime(datestring, "%Y-%m-%d")
            month_name = date_object.strftime("%B")
            monthlist.append({"Month":month_name,"orderlist":i})
        group={}
        for entry in monthlist:
            month = entry["Month"]
            data1 = entry["orderlist"]
    
            # Check if the month is already a key in the dictionary
            if month in group:
                group[month].append(data1)
            else:
        # Create a new entry for the month if it doesn't exist
                group[month] = [data1]
# Convert the grouped data dictionary to the desired list format
        result_list = [{"month": month, "data1": data_list} for month, data_list in group.items()]
        total_amount=0.0
        for entry in result_list:
            for item in entry["data1"]:
                total_amount += item["Amount_Paid"]
        result_list.append({"Total_Amount":total_amount})
        output = JSONRenderer().render(result_list)
        return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)       

class customerwisetransactionwithperiod(ListAPIView):
    queryset = SalesOrder.objects.all()
    serializer_class = SalesOrderSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        data=request.data
        module = SalesOrder.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Customer_Id=data['Customer_Id']) & Q(SalesOrderDate__range=data['SalesOrderDate']) & Q(Order_Cancelled=data['Order_Cancelled'])& Q(Is_Deleted=False))
        serialize = SalesOrderSerialzers(module,many=True)
        ser=serialize.data
        monthlist=[]
        for i in ser:
            datestring = i['SalesOrderDate']
            date_object = datetime.strptime(datestring, "%Y-%m-%d")
            month_name = date_object.strftime("%B")
            monthlist.append({"Month":month_name,"orderlist":i})
        group={}
        for entry in monthlist:
            month = entry["Month"]
            data1 = entry["orderlist"]
    
            # Check if the month is already a key in the dictionary
            if month in group:
                group[month].append(data1)
            else:
        # Create a new entry for the month if it doesn't exist
                group[month] = [data1]
# Convert the grouped data dictionary to the desired list format
        result_list = [{"month": month, "data1": data_list} for month, data_list in group.items()]
        total_amount=0.0
        for entry in result_list:
            for item in entry["data1"]:
                total_amount += item["Amount_Paid"]
        result_list.append({"Total_Amount":total_amount})
        output = JSONRenderer().render(result_list)
        return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)


class Salescustomersearch(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = SalesOrder.objects.filter((Q(SalesOrder_Id__icontains=data['search'])|Q(Customer_Id__Cus_FirstName__icontains=data['search']) 
                |Q(SalesOrder_Type__icontains=data['search']) | Q(Amount_Paid__icontains=data['search']) | Q(Assignedservice_Personal__icontains=data['search']))
                &Q(Partner_Id=data['Partner_Id']) & Q(Customer_Id=data['Customer_Id'])& Q(Order_Cancelled=data['Order_Cancelled'])& Q(Is_Deleted=False)).order_by('-id')
            serialize = SalesOrderSerialzers(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)




class organizationwisetransaction(ListAPIView):
    queryset = SalesOrder.objects.all()
    serializer_class = SalesOrderSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        data=request.data
        module = SalesOrder.objects.filter(Q(Company_Id=data['Company_Id']) & Q(Order_Cancelled=data['Order_Cancelled'])& Q(Is_Deleted=False))
        serialize = SalesOrderSerialzers(module,many=True)
        ser=serialize.data
        monthlist=[]
        for i in ser:
            datestring = i['SalesOrderDate']
            date_object = datetime.strptime(datestring, "%Y-%m-%d")
            month_name = date_object.strftime("%B")
            monthlist.append({"Month":month_name,"orderlist":i})
        group={}
        for entry in monthlist:
            month = entry["Month"]
            data1 = entry["orderlist"]
    
            # Check if the month is already a key in the dictionary
            if month in group:
                group[month].append(data1)
            else:
        # Create a new entry for the month if it doesn't exist
                group[month] = [data1]
# Convert the grouped data dictionary to the desired list format
        result_list = [{"month": month, "data1": data_list} for month, data_list in group.items()]
        total_amount=0.0
        for entry in result_list:
            for item in entry["data1"]:
                total_amount += item["Amount_Paid"]
        result_list.append({"Total_Amount":total_amount})
        output = JSONRenderer().render(result_list)
        return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)       

class organizationtransactionwithperiod(ListAPIView):
    queryset = SalesOrder.objects.all()
    serializer_class = SalesOrderSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        data=request.data
        module = SalesOrder.objects.filter(Q(Company_Id=data['Company_Id']) & Q(SalesOrderDate__range=data['SalesOrderDate']) & Q(Order_Cancelled=data['Order_Cancelled'])& Q(Is_Deleted=False))
        serialize = SalesOrderSerialzers(module,many=True)
        ser=serialize.data
        monthlist=[]
        for i in ser:
            datestring = i['SalesOrderDate']
            date_object = datetime.strptime(datestring, "%Y-%m-%d")
            month_name = date_object.strftime("%B")
            monthlist.append({"Month":month_name,"orderlist":i})
        group={}
        for entry in monthlist:
            month = entry["Month"]
            data1 = entry["orderlist"]
    
            # Check if the month is already a key in the dictionary
            if month in group:
                group[month].append(data1)
            else:
        # Create a new entry for the month if it doesn't exist
                group[month] = [data1]
# Convert the grouped data dictionary to the desired list format
        result_list = [{"month": month, "data1": data_list} for month, data_list in group.items()]
        total_amount=0.0
        for entry in result_list:
            for item in entry["data1"]:
                total_amount += item["Amount_Paid"]
        result_list.append({"Total_Amount":total_amount})
        output = JSONRenderer().render(result_list)
        return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)


class Salesorganizationsearch(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = SalesOrder.objects.filter((Q(SalesOrder_Id__icontains=data['search'])|Q(Customer_Id__Cus_FirstName__icontains=data['search']) 
                |Q(SalesOrder_Type__icontains=data['search']) | Q(Amount_Paid__icontains=data['search']) | Q(Assignedservice_Personal__icontains=data['search']))
                & Q(Company_Id=data['Company_Id']) & Q(Order_Cancelled=data['Order_Cancelled'])& Q(Is_Deleted=False)).order_by('-id')
            serialize = SalesOrderSerialzers(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


# Customer Payments details API

class BUCustomerinvoicedetail(ListAPIView):
    queryset = Customerinvoicedetail.objects.all()
    serializer_class = CustomerinvoicedetailSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]
    
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            data1 = {"Partner_Id":data['Partner_Id'],"Created_Date__range":data['Created_Date'],"Is_Deleted":False}
            return requestget(model=Customerinvoicedetail,serializer=CustomerinvoicedetailSerialzer,data=data1,page=data['page'],Operator='and')
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



#ponkumar
class MailConfigurationCRUD(APIView):
    # queryset = MailConfiguration.objects.all()
    # serializer_class = MailConfigurationSerializer
    # authentication_classes = [JSONWebTokenAuthentication]
    # permission_classes = [DjangoModelPermissions]

    def get(self, request, *args, **kwargs):
        try:
            Created_By = request.GET.get('Created_By')
            # print(UserId)
            mail = MailConfiguration.objects.get( Created_By= Created_By)
            # serializer = MailConfigurationSerializer(mail)
            # print(mail)
            return HttpResponse(JSONRenderer().render(MailConfigurationSerializer(mail).data), content_type='application/json')
        except Exception as e:
            return HttpResponse(JSONRenderer().render({"Error": str(e)}), status=status.HTTP_417_EXPECTATION_FAILED)

    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            try:
                with smtplib.SMTP_SSL(data['SMTPHost'], data['SMTPPort'],
                                      context=ssl.create_default_context()) as server:
                    server.login(data['MailId'], data['MailPassword'])
                    server.quit()
                if data['IMAPHost'] != "":
                    imp = imaplib.IMAP4_SSL(data['IMAPHost'])
                    imp.login(data['MailId'], data['MailPassword'])
                    imp.logout()
            except Exception as e:
                print(str(e))
                raise ConnectionAbortedError("Please check the Mail Configration")
            data['MailPassword'] = data['MailPassword']
            serializer = MailConfigurationSerializer(data=data)
            if serializer.is_valid():
                serializer.save()
                return HttpResponse(JSONRenderer().render(serializer.data), content_type='application/json',
                                    status=status.HTTP_200_OK)
            return HttpResponse(JSONRenderer().render(serializer.errors), status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return HttpResponse(JSONRenderer().render({"Error": str(e)}), content_type='application/json',
                                status=status.HTTP_417_EXPECTATION_FAILED)

    def put(self, request, *args, **kwargs):
        try:
            data = request.data
            try:
                with smtplib.SMTP_SSL(data['SMTPHost'], data['SMTPPort'],
                                      context=ssl.create_default_context()) as server:
                    server.login(data['MailId'], data['MailPassword'])
                    server.quit()
                imp = imaplib.IMAP4_SSL(data['IMAPHost'])
                imp.login(data['MailId'], data['MailPassword'])
                imp.logout()
            except:
                raise ConnectionAbortedError("Please check the Mail Configration")
            data['MailPassword'] = data['MailPassword']
            model = MailConfiguration.objects.get(id=data['id'])
            serializer = MailConfigurationSerializer(model, data=data)
            if serializer.is_valid():
                serializer.save()
                return HttpResponse(JSONRenderer().render(serializer.data), content_type='application/json',
                                    status=status.HTTP_200_OK)
            return HttpResponse(JSONRenderer().render(serializer.errors), status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return HttpResponse(JSONRenderer().render({"Error": str(e)}), content_type='application/json',
                                status=status.HTTP_417_EXPECTATION_FAILED)


#ponkumar
class SignupUsers(APIView):
    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            Company.objects.get(id=data['Company_Id'])
            html = data['mailhtml'].replace('{{url}}',
                                            f'{Backend_Url}/Erpapp/UserReg/?Company_Id=' + str(
                                                data['Company_Id']) + "&email=" + data['email'] + "&BusinessUnit=" + data['Business_Unit'] + "&Role=" + data['Designation'] + "&UserType=" +
                                            data['UserType'] + "&Url=" + data['Url'] + "&groups=" + str(
                                                data['groups']) + "&Created_by=" + str(data['Created_by']))
            mail = MailConfiguration.objects.get(User_Id=data['Created_by'])
            mailserializer = eval(JSONRenderer().render(MailConfigurationSerializer(mail).data))
            # if not check_password(data['MailPassword'], mail.MailPassword):
            #     return HttpResponse(JSONRenderer().render({"Error": "Please give the valid Mail Password"}),
            #                         content_type='application/json', status=status.HTTP_400_BAD_REQUEST)
            RecevierMailIdVaild = validate_email(data['email'], verify=False)
            if RecevierMailIdVaild:
                response = SendMail(subject=data['mailsubject'], From=mailserializer['MailId'], To=data['email'],
                                    Body=html, Host=mailserializer['SMTPHost'], Port=mailserializer['SMTPPort'],
                                    Password=data['MailPassword'])
                if response:
                    mailmaster = MailMasterSerializer(data=data)
                    return HttpResponse(JSONRenderer().render({"Message": "User is Create Successfully"}),
                                        content_type='application/json', status=status.HTTP_200_OK)
                else:
                    return HttpResponse(JSONRenderer().render({"Message": "User Mail id is not valid"}),
                                        content_type='application/json', status=status.HTTP_400_BAD_REQUEST)
            else:
                return HttpResponse(JSONRenderer().render({"Message": "User Mail id is not valid"}),
                                    content_type='application/json', status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            if str(e) == "MailConfiguration matching query does not exist.":
                return HttpResponse(JSONRenderer().render({"Message": "Please Setup Mail First"}),
                                    content_type='application/json', status=status.HTTP_400_BAD_REQUEST)
            return HttpResponse(JSONRenderer().render({"Error": str(e)}), content_type='application/json',
                                status=status.HTTP_417_EXPECTATION_FAILED)



def SendMail(subject, From, To, Body, Host, Port, Password):
    mail = MIMEMultipart('mixed')
    mail['subject'] = subject
    mail['From'] = From
    mail['To'] = To
    # mail['To'] = ', '.join(To)  # Join multiple recipients with comma and space
    mail['Disposition-Notification-To'] = From
    mail['Return-Receipt-To'] = From
    body = MIMEText(Body, 'html')
    mail.attach(body)
    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(Host, Port, context=context) as server:
        server.login(From, Password)
        status = server.sendmail(From, To, mail.as_string())
        server.close()
    if status == {}:
        return True
    else:
        return False


def SendMail1(subject, From, To, Body, Host, Port, Password):
    mail = MIMEMultipart('mixed')
    mail['subject'] = subject
    mail['From'] = From
    # mail['To'] = To
    mail['To'] = ', '.join(To)  # Join multiple recipients with comma and space
    mail['Disposition-Notification-To'] = From
    mail['Return-Receipt-To'] = From
    body = MIMEText(Body, 'html')
    mail.attach(body)
    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(Host, Port, context=context) as server:
        server.login(From, Password)
        status = server.sendmail(From, To, mail.as_string())
        server.close()
    if status == {}:
        return True
    else:
        return False



# def SendMail(subject, From, To, Body, Host, Port, Password,Attachment):
#     mail = MIMEMultipart('mixed')
#     mail['subject'] = subject
#     mail['From'] = From
#     mail['To'] = To
#     mail['Disposition-Notification-To'] = From
#     mail['Return-Receipt-To'] = From
#     body = MIMEText(Body, 'html')
#     mail.attach(body)
#     context = ssl.create_default_context()
#     with smtplib.SMTP_SSL(Host, Port, context=context) as server:
#         server.login(From, Password)
#         status = server.sendmail(From, [To], mail.as_string())
#         server.close()
#     if status == {}:
#         return True
#     else:
#         return False

def userreg(request):
    user = {"Company_Id": request.GET.get('Company_Id'),
            "email": request.GET.get('email'),
            "UserType": request.GET.get('UserType'),
            "Url": request.GET.get('Url'),
            "BusinessUnit": request.GET.get('BusinessUnit'),
            "Role": request.GET.get('Role'),
            "groups": request.GET.get('groups'), "Created_by": request.GET.get('Created_by')}
    return render(request, 'RegistrationForm.html', {'user': user})



# @csrf_exempt
@method_decorator(csrf_exempt, name='dispatch')
def userupdate(request):
    try:
        user = {}
        user['Company_Id'] = request.POST.get('Company_Id')
        user['UserType'] = request.POST.get('UserType')
        user['Url'] = request.POST.get('Url')
        groups = request.POST.get('groups')
        user['groups'] = eval(groups)
        user['username'] = request.POST.get('username')
        user['Created_by'] = request.POST.get('Created_by')
        user['first_name'] = request.POST.get('first_name')
        user['Business_Unit']=request.POST.get('BusinessUnit')
        user['last_name'] = request.POST.get('last_name')
        user['username'] = request.POST.get('username')
        user['Designation'] = request.POST.get('Role')
        user['email'] = request.POST.get('email')
        user['User_emailId'] = request.POST.get('email')
        user['PhoneNo'] = request.POST.get('phone')
        user['BuildingHuseNo'] = request.POST.get('HNo')
        user['Street'] = request.POST.get('Street')
        user['Area'] = request.POST.get('Area')
        user['City'] = request.POST.get('City')
        user['State'] = request.POST.get('State')
        user['Country'] = request.POST.get('Country')
        user['is_staff'] = False
        user['is_active'] = True
        user['User_Login_Status'] = False
        psw = request.POST.get('psw')
        pswrepeat = request.POST.get('pswrepeat')
        if psw == pswrepeat:
            validate_password(password=psw, user=User)
            user['password'] = make_password(psw)
        else:
            messages.warning(request, 'The Password and Re_type Password are not same')
            return render(request, 'RegistrationForm.html', {"user": user})
        email = User.objects.filter(email__exact=user['email'])
        if email:
            messages.warning(request, 'Already a user is created with this mail id')
            return render(request, 'RegistrationForm.html', {"user": user})
        serializer = UserSerializer(data=user)
        # print(serializer.is_valid())
        if serializer.is_valid():
            # print("save")
            serializer.save()
            # print(user.Url)
            return redirect(user['Url'])
        else:
            print("serializererror")
            for name, error in serializer.errors.items():
                messages.warning(request, error[0])
            return render(request, 'RegistrationForm.html', {"user": user})
    except Exception as e:
        print(str(e))
        errors = e.args[0]
        if type(errors) == type(''):
            messages.warning(request, errors)
            return render(request, 'RegistrationForm.html', {"user": user})
        for error in errors:
            messages.warning(request, error)
        return render(request, 'RegistrationForm.html', {"user": user})



#ponkumar
class ForgotPassword(APIView):

    # my forgot password api
    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            user = User.objects.filter(email = data['email'])
            html = data['mailhtml'].replace('{{url}}',
                                            f'{Backend_Url}/Erpapp/PasswordReg/')
            module = UserSerializer(user,many=True)
            mail1=module.data
            mail2=mail1[0]
            mail = MailConfiguration.objects.get(User_Id=mail2['id'])
            mailserializer = eval(JSONRenderer().render(MailConfigurationSerializer(mail).data))
            RecevierMailIdVaild = validate_email(data['email'], verify=False)
            if RecevierMailIdVaild:
                response = SendEmail(subject=data['mailsubject'], From=settings.EMAIL_HOST_USER, To=mailserializer['MailId'],
                                    Body=html, Host=settings.EMAIL_HOST, Port=settings.EMAIL_PORT, Password=settings.EMAIL_HOST_PASSWORD)
                print("ok")
                if response:
                    mailmaster = MailMasterSerializer(data=data)
                    return HttpResponse(JSONRenderer().render({"Message": "Password Reset Successfully"}),
                                        content_type='application/json', status=status.HTTP_200_OK)
                else:
                    return HttpResponse(JSONRenderer().render({"Message": "User Mail id is not valid"}),
                                        content_type='application/json', status=status.HTTP_400_BAD_REQUEST)
            else:
                return HttpResponse(JSONRenderer().render({"Message": "User Mail id is not valid"}),
                                    content_type='application/json', status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            if str(e) == "MailConfiguration matching query does not exist.":
                return HttpResponse(JSONRenderer().render({"Message": "Please Setup Mail First"}),
                                    content_type='application/json', status=status.HTTP_400_BAD_REQUEST)
            return HttpResponse(JSONRenderer().render({"Error": str(e)}), content_type='application/json',
                                status=status.HTTP_417_EXPECTATION_FAILED)
    # Updating the Password
    def patch(self, request, *args, **kwargs):
        try:
            data = request.data
            user = User.objects.get(username__exact = data['username'])
            if data['password'] == data['ReTypePassword']:
                validate_password(password=data['password'], user=User)
                data['password'] = make_password(data['password'])
                serialize = UserSerializer(instance=user,data=data, partial=True)
                if serialize.is_valid():
                    serialize.save()
                    output = JSONRenderer().render(serialize.data)
                    return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
                return HttpResponse(JSONRenderer().render(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
            return HttpResponse(JSONRenderer().render({"Error":"Password and Re-Type Password are not same"}),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        


def SendEmail(subject, From, To, Body, Host, Port, Password):
    mail = MIMEMultipart('mixed')
    mail['subject'] = subject
    mail['From'] = From
    mail['To'] = To
    mail['Disposition-Notification-To'] = From
    mail['Return-Receipt-To'] = From
    body = MIMEText(Body, 'html')
    mail.attach(body)
    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(Host, Port, context=context) as server:
        server.login(From, Password)
        status = server.sendmail(From, To, mail.as_string())
        server.close()
    if status == {}:
        return True
    else:
        return False

def passwordReg(request):
    user = {"Company_Id": request.GET.get('Company_Id'),
            "Url": request.GET.get('Url'),
            "groups": request.GET.get('groups'), "Created_by": request.GET.get('Created_by')}
    return render(request, 'templates/ForgotPassword.html', {'user': user})


# @csrf_exempt
@method_decorator(csrf_exempt, name='dispatch')
def passwordupdate(request):
    try:
        user = {}
        user['Company_Id'] = request.POST.get('Company_Id')
        user['Url'] = request.POST.get('Url')
        groups = request.POST.get('groups')
        user['groups'] = eval(groups)
        user['Created_by'] = request.POST.get('Created_by')
        psw = request.POST.get('psw')
        pswrepeat = request.POST.get('pswrepeat')
        if psw == pswrepeat:
            validate_password(password=psw, user=User)
            user['password'] = make_password(psw)
        else:
            messages.warning(request, 'The Password and Re_type Password are not same')
            return render(request, 'ForgotPassword.html', {"user": user})
        serializer = UserSerializer(data=user)
        if serializer.is_valid():
            serializer.save()
            # print(user.Url)
            return redirect(user['Url'])
        else:
            print("serializererror")
            for name, error in serializer.errors.items():
                messages.warning(request, error[0])
            return render(request, 'ForgotPassword.html', {"user": user})
    except Exception as e:
        print("exception")
        errors = e.args[0]
        if type(errors) == type(''):
            messages.warning(request, errors)
            return render(request, 'ForgotPassword.html', {"user": user})
        for error in errors:
            messages.warning(request, error)
        return render(request, 'ForgotPassword.html', {"user": user})




#ponkumar
class EntityDetailsCRUD(ListAPIView):
    queryset = Entity.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data = request.data
            items = Entity.objects.filter(Entity_Name__exact = data['Entity_Name'])
            for item in items:
                return HttpResponse(JSONRenderer().render({"Error":"Entity name is already exists"}),content_type='application/json',status=status.HTTP_400_BAD_REQUEST) 
            serialize = EntitySerializer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



    def get(self,request,*args,**kwargs):
        try:
            data = request.query_params
            id = data['id']
            module = Entity.objects.get(id=id)
            serialize = EntitySerializer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)  


    
class EntityLists(ListAPIView):
    queryset = Entity.objects.all()
    serializer_class = EntitySerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    # my list of entities
    def get_queryset(self):
        data = self.request.data
        module = Entity.objects.filter(Is_Deleted=False).order_by('-id')
        print("ss", module)
        return module



#ponkumar
class CountryDetailsCRUD(ListAPIView):
    queryset = Country.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data = request.data
            items = Country.objects.filter(Q(Entity_Id__id = data['Entity_Id']), Q(Country_Name__exact = data['Country_Name']))
            for item in items:
                return HttpResponse(JSONRenderer().render({"Error":"Country name is already exists"}),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
            serialize = CountrySerializer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



    def get(self,request,*args,**kwargs):
        try:
            data = request.query_params
            id = data['id']
            module = Country.objects.get(id=id)
            serialize = CountrySerializer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)  



class CountryLists(ListAPIView):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    # my list of countries
    def get_queryset(self):
        data = self.request.query_params
        module = Country.objects.filter(Is_Deleted=False).order_by('-id')
        print("ss", module)
        return module


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            module=Country.objects.filter(Entity_Id=data['Entity_Id'], Is_Deleted=False)
            serialize=CountrySerializer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)




#ponkumar
class RegionDetailsCRUD(ListAPIView):
    queryset = Region.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data = request.data
            items = Region.objects.filter(Q(Entity_Id__id = data['Entity_Id']), Q(Country_Id__id = data['Country_Id']), Q(Region_Name__exact = data['Region_Name']))
            for item in items:
                return HttpResponse(JSONRenderer().render({"Error":"Region name is already exists"}),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
            serialize = RegionSerializer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



    def get(self,request,*args,**kwargs):
        try:
            data = request.query_params
            id = data['id']
            module = Region.objects.get(id=id)
            serialize = RegionSerializer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST) 



class RegionLists(ListAPIView):
    queryset = Region.objects.all()
    serializer_class = RegionSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    # my list of regions
    def get_queryset(self):
        data = self.request.data
        module = Region.objects.filter(Is_Deleted=False).order_by('-id')
        print("ss", module)
        return module


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            module=Region.objects.filter(Country_Id=data['Country_Id'], Is_Deleted=False)
            serialize=RegionSerializer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


#ponkumar
class StateDetailsCRUD(ListAPIView):
    queryset = State.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        # try:
        #     data = request.data
        #     id = Country.objects.all()
        #     country=Country.objects.get(Country_Name__exact = id['Country_Name'])
        #     if country in id['Country_Name']:
        #         pass
        #     else:
        #         country_serialize=CountrySerializer(data=Country_Id)
        #         if country_serialize.is_valid():
        #             state=State.objects.get(State_Name__exact = data['State_Name'])
        #             if state != data['State_Name']:
        #                 serialize = StateSerializer(data=data)
        #                 if serialize.is_valid():
        #                     serialize.save()
        #                     output = JSONRenderer().render(serialize.data)
        #                     return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        #             else:
        #                 return HttpResponse(JSONRenderer().render({"Error":"State name already exist"}),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        # except Exception as e:
        #     output=JSONRenderer().render({"error":str(e)})
        #     return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

        try:
            data = request.data
            items = State.objects.filter( Q(Region_Id__id = data['Region_Id']) , Q(State_Name__exact = data['State_Name']))
            for item in items:
                return HttpResponse(JSONRenderer().render({"Error":"State name is already exists"}),content_type='application/json',status=status.HTTP_400_BAD_REQUEST) 
            serialize = StateSerializer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



    def get(self,request,*args,**kwargs):
        try:
            data = request.query_params
            id = data['id']
            module = State.objects.get(id=id)
            serialize = StateSerializer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)  



class StateLists(ListAPIView):
    queryset = State.objects.all()
    serializer_class = StateSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    # my list of regions
    def get_queryset(self):
        data = self.request.data
        module = State.objects.filter(Is_Deleted=False).order_by('-id')
        print("ss", module)
        return module


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            module=State.objects.filter(Region_Id=data['Region_Id'], Is_Deleted=False)
            serialize=StateSerializer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



#ponkumar
class CityDetailsCRUD(ListAPIView):
    queryset = City.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data = request.data
            items = City.objects.filter(Q(Entity_Id__id = data['Entity_Id']),  Q(Country_Id__id = data['Country_Id']), Q(Region_Id__id = data['Region_Id']), Q(State_Id__id = data['State_Id']), Q(City_Name__exact = data['City_Name']))
            for item in items:
                return HttpResponse(JSONRenderer().render({"Error":"City name is already exists"}),content_type='application/json',status=status.HTTP_400_BAD_REQUEST) 
            serialize = CitySerializer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



    def get(self,request,*args,**kwargs):
        try:
            data = request.query_params
            id = data['id']
            module = City.objects.get(id=id)
            serialize = CitySerializer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)      


class CityLists(ListAPIView):
    queryset = City.objects.all()
    serializer_class = CitySerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    # my list of regions
    def get_queryset(self):
        data = self.request.data
        module = City.objects.filter(Is_Deleted=False).order_by('-id')
        print("ss", module)
        return module

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            module=City.objects.filter(State_Id=data['State_Id'], Is_Deleted=False)
            serialize=CitySerializer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)




# ponkumar
class ZoneDetailsCRUD(ListAPIView):
    queryset = Zone.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]
    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            items = Zone.objects.filter(Q(City_Id__exact=data['City_Id']), Q(
                Zone_Name__exact=data['Zone_Name']), Q(Pincode__exact=data['Pincode']))
            for item in items:
                print(item)
                return HttpResponse(JSONRenderer().render({"Error": data['Pincode'] + " is already exists"}), content_type='application/json', status=status.HTTP_400_BAD_REQUEST)
            serialize = ZoneSerializer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_201_CREATED)
        except Exception as e:
            output = JSONRenderer().render({"error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)
    def get(self, request, *args, **kwargs):
        try:
            data = request.query_params
            id = data['id']
            module = Zone.objects.get(id=id)
            serialize = ZoneSerializer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)


class ZoneLists(ListAPIView):
    queryset = Zone.objects.all()
    serializer_class = ZoneSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]
    # my list of regions
    def get_queryset(self):
        module = Zone.objects.filter(Is_Deleted=False).order_by('-id')
        return module
    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            module = Zone.objects.filter(
                Q(City_Id=data['City_Id']), Is_Deleted=False)
            serialize = ZoneSerializer(module, many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)
            

# code for purchase request
# created by ponkumar

class PurchaseRequestCRUD(ListAPIView):
    queryset = PRDetails.objects.all()
    serializer_class = PRDetailsSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    # create a purchase request
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            # data=request
            # if data["UploadFile"] != "":
            Company_Id=data['Company_Id']
            module = Transactionalseries.objects.get(Company_Id=Company_Id)
            serialize = TransactionalseriesSerialzer(module)
            prefix=JSONRenderer().render(serialize.data)
            pre=json.loads(prefix)
            out=pre['Purchase_Request']
            Organizatio=PRDetails.objects.all().order_by('PR_Id').last()
            if Organizatio is None:
                Journals=out + "000000001"
                data['PR_Id']=Journals
            else:
                Organizati = Organizatio.PR_Id
                Organizat=int(Organizati[4:])
                Organiza=Organizat + 1
                Configur = out + str(Organiza).zfill(9)
                data['PR_Id']=Configur
            # decoded_data = base64.b64decode((data['UploadFile']))
            # data['UploadFile'] = ContentFile(decoded_data, name=data['User_File_Name'])
            # del data['User_File_Name']
            serialize = PRDetailsSerialzer(data=data)    
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
            print(serialize.errors)
            return HttpResponse(str(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print(e)
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    
    # view purchase request
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            # data=request
            id=data['id']
            module=PRDetails.objects.get(id=id)
            serialize = PRDetailsSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    # edit the purchase request
    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            #data=request
            # if data['UploadFile'] != '':
            #     decoded_data = base64.b64decode((data['UploadFile']))
            #     data['UploadFile'] = ContentFile(decoded_data, name=data['User_File_Name'])
            #     del data['User_File_Name']
            id=data['id']
            module=PRDetails.objects.get(id=id)
            serialize = PRDetailsSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    # partial update the purchase request
    def patch(self,request,*args,**kwargs):
        try:
            data=request.data
            #data=request
            id=data['id']
            module=PRDetails.objects.get(id=id)
            serialize = PRDetailsSerialzer(instance=module,data=data,partial = True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



    # delete the purchase request
    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            #data=request
            id=data['id']
            module=PRDetails.objects.get(id=id)
            serialize=PRDetailsSerialzer(module, {"Is_Deleted": True}, partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


# Additional filter methods for purchase request
class PurchaseRequestAdditional(ListAPIView):
    queryset = PRDetails.objects.all()
    serializer_class = PRDetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    # filter api
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            #data=request
            data1={"Department_Id__Department_Name__icontains":data['Department_Name'],"CostCentre_Id__Costcenter_Name__icontains":data['Costcenter_Name'],"PR_Date__range":data['PR_Date'],"RequestedBy__icontains":data['RequestedBy'],"Is_Deleted":False}
            return requestget(model=PRDetails,serializer=PRDetailsSerialzers,data=data1,page=data['page'],Operator='and')
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    # all pr list api
    def get_queryset(self):
        try:
            Partner_Id=self.request.GET.get('Partner_Id')
            module = PRDetails.objects.filter(Q(Is_Deleted=False) & Q(Partner_Id=Partner_Id)).order_by('-id')
            return module
        except:
            module = PRDetails.objects.none()
            return module



class PRsearch(ListAPIView):
    queryset = PRDetails.objects.all()
    serializer_class = PRDetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    # search api
    def get_queryset(self):
        data=self.request.GET.get('search')
        module=PRDetails.objects.filter(Q(PR_Id__icontains=data) |Q(Partner_Id__Partner_Name__icontains=data)|Q(Store_Name__icontains=data)|Q(TotalEstValue__icontains=data)| Q(RequestedBy__icontains=data) | Q(Department_Id__Department_Name__icontains=data) | Q(CostCentre_Id__Costcenter_Name__icontains=data),Is_Deleted=False)
        return module


class MyPRLists(ListAPIView):
    queryset = PRDetails.objects.all()
    serializer_class = PRDetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    # my pr list api
    def get_queryset(self):
        try:
            Partner_Id = self.request.GET.get('Partner_Id')
            Created_By= self.request.GET.get('Created_By')
            #data=self.request
            module = PRDetails.objects.filter(Q(Partner_Id=Partner_Id) & Q( Created_By=Created_By)
                                              & Q(Is_Deleted=False)).order_by('-id')
            return module
        except:
            module = PRDetails.objects.none()
            return module




# api for material request module
class MaterialRequestCRUD(ListAPIView):
    queryset = MaterialRequestDetail.objects.all()
    serializer_class = MaterialRequestDetailSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    # create a material request
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            # data=request
            if data == {}:
                raise TypeError
            # module = Storemaster.objects.get(id=data['Store_Id'])
            # out=StoremasterSerialzer(module)
            # out1=out.data
            # dict2 = {item['id']: item for item in data['Item']}
            # # Append items from list2 to list1 based on "id"
            # for item in out1['Items']:
            #     if item['id'] in dict2:
            #         item.update(dict2[item['id']])
            # out2=[]
            # for i in out1['Items']:
            #     if 'Stock_After' in i:
            #         data1={'id': i['id'], 'Item_Name': i['Item_Name'],'Opening_Stock':i['Stock_After']}
            #         out2.append(data1)
            #     else:
            #         data1={'id': i['id'], 'Item_Name': i['Item_Name'],'Opening_Stock':i['Opening_Stock']}
            #         out2.append(data1)
            # out1['Items'] = out2
            # # print(out2)
            # serialize1 = StoremasterSerialzer(instance=module,data=out1)
            # if serialize1.is_valid():
            #     serialize1.save()
            serialize = MaterialRequestDetailSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
            print(serialize.errors)
            return HttpResponse(str(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    # view the material request
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            #data=request
            id=data['id']
            module=MaterialRequestDetail.objects.get(id=id)
            serialize = MaterialRequestDetailSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    # edit the material request
    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            #data=request
            id=data['id']
            module=MaterialRequestDetail.objects.get(id=id)
            # module = Storemaster.objects.get(id=data['Store_Id'])
            # out=StoremasterSerialzer(module)
            # out1=out.data
            # dict2 = {item['id']: item for item in data['Item']}
            # # Append items from list2 to list1 based on "id"
            # for item in out1['Items']:
            #     if item['id'] in dict2:
            #         item.update(dict2[item['id']])
            # out2=[]
            # for i in out1['Items']:
            #     if 'Stock_After' in i:
            #         data1={'id': i['id'], 'Item_Name': i['Item_Name'],'Opening_Stock':i['Stock_After']}
            #         out2.append(data1)
            #     else:
            #         data1={'id': i['id'], 'Item_Name': i['Item_Name'],'Opening_Stock':i['Opening_Stock']}
            #         out2.append(data1)
            # out1['Items'] = out2
            # # print(out2)
            # serialize1 = StoremasterSerialzer(instance=module,data=out1)
            # if serialize1.is_valid():
            #     serialize1.save()
            serialize = MaterialRequestDetailSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    
    # partial edit the material request
    def patch(self,request,*args,**kwargs):
        try:
            data=request.data
            #data=request
            id=data['id']
            module=MaterialRequestDetail.objects.get(id=id)
            serialize = MaterialRequestDetailSerialzer(instance=module,data=data,partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    # delete the material request
    def delete(self, request, *args, **kwargs):
        try:
            data = request.query_params
            #data=request
            id = data['id']
            module = MaterialRequestDetail.objects.get(id=id)
            serialize = MaterialRequestDetailSerialzer(
                module, {"Is_Deleted": True}, partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)



class MaterialRequestAdditionalCURD(ListAPIView):
    queryset = MaterialRequestDetail.objects.all()
    serializer_class = MaterialRequestDetailSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    # filter api
    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            # data = request
            data1 = {"Department_Id__Department_Name__icontains":data['Department_Id'],"Partner_Id__Partner_Name__icontains":data["Partner_Id"],"CostCentre_Id__Costcenter_Name__icontains":data['CostCentre_Id']
                        ,"Requester_Id__first_name__icontains":data['Requester_Id'],"Request_Date__range":data['Request_Date'],"Is_Deleted":False}
            return requestget(model=MaterialRequestDetail,serializer=MaterialRequestDetailSerialzers,data=data1,page=data['page'],Operator='and')
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)



    # all material request list api
    def get_queryset(self):
        try:
            module = MaterialRequestDetail.objects.filter(Is_Deleted=False)
            return module
        except:
            module = MaterialRequestDetail.objects.none()
            return module

    # search api
class Materialrequestsearch(ListAPIView):
    queryset = MaterialRequestDetail.objects.all()
    serializer_class = MaterialRequestDetailSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data = self.request.GET.get('search')
        # data = request
        module = MaterialRequestDetail.objects.filter(Q(
                Requester_Id__first_name__icontains=data) | Q(Partner_Id__Partner_Name__icontains=data) |Q(CostCentre_Id__Costcenter_Name__icontains=data)| Q(
                Department_Id__Department_Name__icontains=data), Q(Is_Deleted=False))
        return module

class MaterialRequestMyLists(ListAPIView):
    queryset = MaterialRequestDetail.objects.all()
    serializer_class = MaterialRequestDetailSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    # my material request list api
    def get_queryset(self):
        try:
            data = self.request.query_params
            #data=self.request
            module = MaterialRequestDetail.objects.filter(Company_Id=data['Company_Id'], Created_By=data['Created_By'],
                                              Is_Deleted=False).order_by('-id')
            print("ss", module)
            return module
        except:
            module = MaterialRequestDetail.objects.none()
            return module


#Front end purpose for this api.
class MaterialRequestdropdown(ListAPIView):
    queryset = MaterialRequestDetail.objects.all()
    serializer_class = MaterialRequestDetailSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    # search api
    def put(self, request, *args, **kwargs):
        try:
            data = request.data
            # data = request
            module = MaterialRequestDetail.objects.filter(Q(
                Requester_Id__first_name__icontains=data['first_name']) & Q(
                Approve_Flag=True)& Q(Is_Deleted=False))
            serialize = MaterialRequestDetailSerialzers(module, many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)



class Meterialrequestandissuetransaction(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = MaterialIssueDetail.objects.filter(MatReq_Id=data['MatReq_Id']).order_by('-id')
            serialize = MaterialIssueDetailSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)



class Materialreceipttransaction(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = GoodsTrfReceiptDetails.objects.filter(PO_Id=data['PO_Id']).order_by('-id')
            serialize = GoodsTrfReceiptDetailsSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)



# api for goods receipt module
# ponkumar
class GoodsReceiptCRUD(ListAPIView):
    queryset = GoodsTrfReceiptDetails.objects.all()
    serializer_class = GoodsTrfReceiptDetailsSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    # create a goods receipt
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            # data=request
            if data == {}:
                raise TypeError
            module = Storemaster.objects.get(id=data['Store_Id'])
            out=StoremasterSerialzer(module)
            out1=out.data
            dict2 = {item['id']: item for item in data['Item']}
            # Append items from list2 to list1 based on "id"
            for item in out1['Items']:
                if item['id'] in dict2:
                    item.update(dict2[item['id']])
            out2=[]
            for i in out1['Items']:
                if 'Stock_After' in i:
                    data1={'id': i['id'], 'Item_Name': i['Item_Name'],'Opening_Stock':i['Stock_After']}
                    out2.append(data1)
                else:
                    data1={'id': i['id'], 'Item_Name': i['Item_Name'],'Opening_Stock':i['Opening_Stock']}
                    out2.append(data1)
            out1['Items'] = out2
            # print(out2)
            serialize1 = StoremasterSerialzer(instance=module,data=out1)
            if serialize1.is_valid():
                serialize1.save()
            serialize = GoodsTrfReceiptDetailsSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
            return HttpResponse(str(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    # view the goods receipt
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            # data=request
            id=data['id']
            module=GoodsTrfReceiptDetails.objects.get(id=id)
            serialize = GoodsTrfReceiptDetailsSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    # edit the goods receipt
    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            # data=request
            id=data['id']
            module=GoodsTrfReceiptDetails.objects.get(id=id)
            module = Storemaster.objects.get(id=data['Store_Id'])
            out=StoremasterSerialzer(module)
            out1=out.data
            dict2 = {item['id']: item for item in data['Item']}
            # Append items from list2 to list1 based on "id"
            for item in out1['Items']:
                if item['id'] in dict2:
                    item.update(dict2[item['id']])
            out2=[]
            for i in out1['Items']:
                if 'Stock_After' in i:
                    data1={'id': i['id'], 'Item_Name': i['Item_Name'],'Opening_Stock':i['Stock_After']}
                    out2.append(data1)
                else:
                    data1={'id': i['id'], 'Item_Name': i['Item_Name'],'Opening_Stock':i['Opening_Stock']}
                    out2.append(data1)
            out1['Items'] = out2
            # print(out2)
            serialize1 = StoremasterSerialzer(instance=module,data=out1)
            if serialize1.is_valid():
                serialize1.save()
            serialize = GoodsTrfReceiptDetailsSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save() 
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    
     # partial edit the goods receipt
    def patch(self,request,*args,**kwargs):
        try:
            data=request.data
            # data=request
            id=data['id']
            module=GoodsTrfReceiptDetails.objects.get(id=id)
            serialize = GoodsTrfReceiptDetailsSerialzer(instance=module,data=data,partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    # delete the goods receipt
    def delete(self, request, *args, **kwargs):
        try:
            data = request.query_params
            # data=request
            id = data['id']
            module = GoodsTrfReceiptDetails.objects.get(id=id)
            serialize = GoodsTrfReceiptDetailsSerialzer(
                module, {"Is_Deleted": True}, partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)



class GoodsReceiptAdditionalCRUD(ListAPIView):
    queryset = GoodsTrfReceiptDetails.objects.all()
    serializer_class = GoodsTrfReceiptDetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    # filter api
    def post(self, request, *args, **kwargs):
            data = request.data
            # data = request
            data1={"Supplier_Name__Company_Name__icontains":data["Company_Name"],"Created_Date__range":data['Created_Date'],"Trf_FromStore__icontains":data['Trf_FromStore'],"Received_By__first_name__icontains":data['Received_By'],"PO_Id__PO_Id__icontains":data['PO_Id'],"Is_Deleted":False}
            return requestget(model=GoodsTrfReceiptDetails,serializer=GoodsTrfReceiptDetailsSerialzers,data=data1,page=data['page'],Operator='and')


    # all goods receipt list api
    def get_queryset(self):
        try:
            module = GoodsTrfReceiptDetails.objects.filter(Is_Deleted=False)
            return module
        except:
            module = GoodsTrfReceiptDetails.objects.none()
            return module

class Meterialreceiptsearch(ListAPIView):
    # search api
    queryset = GoodsTrfReceiptDetails.objects.all()
    serializer_class = GoodsTrfReceiptDetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
            data = self.request.GET.get('search')
            # data = request
            module = GoodsTrfReceiptDetails.objects.filter(Q(Supplier_Name__Company_Name__icontains=data) |Q(PO_Id__PO_Id__icontains=data) |Q(Trf_FromStore__icontains=data)| Q(TrfIn_Id__icontains=data) | Q(Received_By__first_name__icontains=data), Is_Deleted=False)
            return module

class GoodsReceiptwithoutpagination(ListAPIView):

    # my goods receipt list api
    def get(self,request,*args,**kwargs):
        try:
            data = request.query_params
            # data=self.request
            module = GoodsTrfReceiptDetails.objects.filter(Q(Company_Id=data['Company_Id']) & Q( Created_By=data['Created_By']) & Q(Partner_Id=data['Partner_Id']) &
                                              Q(Is_Deleted=False)).order_by('-id')
            serialize = GoodsTrfReceiptDetailsSerialzers(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)



class GoodsReceiptMyLists(ListAPIView):
    queryset = GoodsTrfReceiptDetails.objects.all()
    serializer_class = GoodsTrfReceiptDetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    # my goods receipt list api
    def get_queryset(self):
        try:
            data = self.request.query_params
            # data=self.request
            module = GoodsTrfReceiptDetails.objects.filter(Company_Id=data['Company_Id'], Created_By=data['Created_By'],
                                              Is_Deleted=False).order_by('-id')
            print("ss", module)
            return module
        except:
            module = GoodsTrfReceiptDetails.objects.none()
            return module


# api for material issue module
# ponkumar
class MaterialIssueCRUD(ListAPIView):
    queryset = MaterialIssueDetail.objects.all()
    serializer_class = MaterialIssueDetailSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    # create a material issue
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            # data=request
            if data == {}:
                raise TypeError
            module = Storemaster.objects.get(id=data['Store_Id'])
            out=StoremasterSerialzer(module)
            out1=out.data
            dict2 = {item['id']: item for item in data['Item']}
            # Append items from list2 to list1 based on "id"
            for item in out1['Items']:
                if item['id'] in dict2:
                    item.update(dict2[item['id']])
            out2=[]
            for i in out1['Items']:
                if 'Stock_After' in i:
                    data1={'id': i['id'], 'Item_Name': i['Item_Name'],'Opening_Stock':i['Stock_After']}
                    out2.append(data1)
                else:
                    data1={'id': i['id'], 'Item_Name': i['Item_Name'],'Opening_Stock':i['Opening_Stock']}
                    out2.append(data1)
            out1['Items'] = out2
            # print(out2)
            serialize1 = StoremasterSerialzer(instance=module,data=out1)
            if serialize1.is_valid():
                serialize1.save()
            serialize = MaterialIssueDetailSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
            print(serialize.errors)
            return HttpResponse(str(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    # view the material issue
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            # data=request
            id=data['id']
            module=MaterialIssueDetail.objects.get(id=id)
            serialize = MaterialIssueDetailSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    # edit the material issue
    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            # data=request
            id=data['id']
            module=MaterialIssueDetail.objects.get(id=id)
            module = Storemaster.objects.get(id=data['Store_Id'])
            out=StoremasterSerialzer(module)
            out1=out.data
            dict2 = {item['id']: item for item in data['Item']}
            # Append items from list2 to list1 based on "id"
            for item in out1['Items']:
                if item['id'] in dict2:
                    item.update(dict2[item['id']])
            out2=[]
            for i in out1['Items']:
                if 'Stock_After' in i:
                    data1={'id': i['id'], 'Item_Name': i['Item_Name'],'Opening_Stock':i['Stock_After']}
                    out2.append(data1)
                else:
                    data1={'id': i['id'], 'Item_Name': i['Item_Name'],'Opening_Stock':i['Opening_Stock']}
                    out2.append(data1)
            out1['Items'] = out2
            # print(out2)
            serialize1 = StoremasterSerialzer(instance=module,data=out1)
            if serialize1.is_valid():
                serialize1.save()
            serialize = MaterialIssueDetailSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    
     # partial edit the material issue
    def patch(self,request,*args,**kwargs):
        try:
            data=request.data
            # data=request
            id=data['id']
            module=MaterialIssueDetail.objects.get(id=id)
            serialize = MaterialIssueDetailSerialzer(instance=module,data=data,partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    # delete the material issue
    def delete(self, request, *args, **kwargs):
        try:
            data = request.query_params
            # data=request
            id = data['id']
            module = MaterialIssueDetail.objects.get(id=id)
            serialize = MaterialIssueDetailSerialzer(
                module, {"Is_Deleted": True}, partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)



class MaterialIssueAdditionalCRUD(ListAPIView):
    queryset = MaterialIssueDetail.objects.all()
    serializer_class = MaterialIssueDetailSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    # filter api
    def post(self, request, *args, **kwargs):
        data=request.data
        data1={"Issued_By__first_name__icontains":data['Issued_By'],"Issue_Date__range":data['Issue_Date'],"Partner_Id__Partner_Name__icontains":data['Partner_Name'],"MatReq_Id__MatReq_Id__icontains":data['MatReq_Id'],"CostCentre_Id__Costcenter_Name__icontains":data['Costcenter_Name'],"Is_Deleted":False}
        return requestget(model=MaterialIssueDetail,serializer=MaterialIssueDetailSerialzers,data=data1,page=data['page'],Operator='and')


    # all material issue list api
    def get_queryset(self):
        try:
            module = MaterialIssueDetail.objects.filter(Is_Deleted=False)
            return module
        except:
            module = MaterialIssueDetail.objects.none()
            return module


class MaterialIssuesearch(ListAPIView):
    queryset = MaterialIssueDetail.objects.all()
    serializer_class = MaterialIssueDetailSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]
    # search api
    def get_queryset(self):
        data=self.request.GET.get('search')
            # data = request
        module = MaterialIssueDetail.objects.filter(Q(MatIsu_Id__icontains=data) | Q(Issued_By__first_name__icontains=data) |Q(Partner_Id__Partner_Name__icontains=data)| Q(
                MatReq_Id__MatReq_Id__icontains=data) | Q(CostCentre_Id__CostCenter_Id__icontains=data), Q(Is_Deleted=False))
        return module

class MaterialIssueMyLists(ListAPIView):
    queryset = MaterialIssueDetail.objects.all()
    serializer_class = MaterialIssueDetailSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    # my material issue list api
    def get_queryset(self):
        try:
            data = self.request.query_params
            #data=self.request
            module = MaterialIssueDetail.objects.filter(Company_Id=data['Company_Id'], Created_By=data['Created_By'],
                                              Is_Deleted=False).order_by('-id')
            return module
        except:
            module = MaterialIssueDetail.objects.none()
            return module


# api for material return module
# ponkumar
class MaterialReturnCRUD(ListAPIView):
    queryset = Materialreturndetails.objects.all()
    serializer_class = MaterialreturndetailsSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    # create a material return
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            #data=request
            if data=={}:
                raise TypeError
            module = Storemaster.objects.get(id=data['Store_Id'])
            out=StoremasterSerialzer(module)
            out1=out.data
            dict2 = {item['id']: item for item in data['Item']}
            # Append items from list2 to list1 based on "id"
            for item in out1['Items']:
                if item['id'] in dict2:
                    item.update(dict2[item['id']])
            out2=[]
            for i in out1['Items']:
                if 'Stock_After' in i:
                    data1={'id': i['id'], 'Item_Name': i['Item_Name'],'Opening_Stock':i['Stock_After']}
                    out2.append(data1)
                else:
                    data1={'id': i['id'], 'Item_Name': i['Item_Name'],'Opening_Stock':i['Opening_Stock']}
                    out2.append(data1)
            out1['Items'] = out2
            # print(out2)
            serialize1 = StoremasterSerialzer(instance=module,data=out1)
            if serialize1.is_valid():
                serialize1.save()
            serialize = MaterialreturndetailsSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
            print(serialize.errors)
            return HttpResponse(str(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    # view the material return
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            #data=request
            id=data['id']
            module=Materialreturndetails.objects.get(id=id)
            serialize = MaterialreturndetailsSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    # edit the material return
    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            #data=request
            id=data['id']
            module=Materialreturndetails.objects.get(id=id)
            module = Storemaster.objects.get(id=data['Store_Id'])
            out=StoremasterSerialzer(module)
            out1=out.data
            dict2 = {item['id']: item for item in data['Item']}
            # Append items from list2 to list1 based on "id"
            for item in out1['Items']:
                if item['id'] in dict2:
                    item.update(dict2[item['id']])
            out2=[]
            for i in out1['Items']:
                if 'Stock_After' in i:
                    data1={'id': i['id'], 'Item_Name': i['Item_Name'],'Opening_Stock':i['Stock_After']}
                    out2.append(data1)
                else:
                    data1={'id': i['id'], 'Item_Name': i['Item_Name'],'Opening_Stock':i['Opening_Stock']}
                    out2.append(data1)
            out1['Items'] = out2
            # print(out2)
            serialize1 = StoremasterSerialzer(instance=module,data=out1)
            if serialize1.is_valid():
                serialize1.save()
            serialize = MaterialreturndetailsSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    
    # partial edit the material return
    def patch(self,request,*args,**kwargs):
        try:
            data=request.data
            #data=request
            id=data['id']
            module=Materialreturndetails.objects.get(id=id)
            serialize = MaterialreturndetailsSerialzer(instance=module,data=data,partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    # delete the material return
    def delete(self, request, *args, **kwargs):
        try:
            data = request.query_params
            #data=request
            id = data['id']
            module = Materialreturndetails.objects.get(id=id)
            serialize = MaterialreturndetailsSerialzer(
                module, {"Is_Deleted": True}, partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)



class MaterialReturnAdditionalCRUD(ListAPIView):
    queryset = Materialreturndetails.objects.all()
    serializer_class = MaterialreturndetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    # filter api
    def post(self, request, *args, **kwargs):
        data = request.data
        data1={"Return_By__first_name__icontains":data['Return_By'],"Created_Date__range":data['Created_Date'],"Item":[{"Item":data['Item_Name']}],"Is_Deleted":False}
        return requestget(model=Materialreturndetails,serializer=MaterialreturndetailsSerialzers,data=data1,page=data['page'],Operator='and')


    # all material return list api
    def get_queryset(self):
        try:
            module = Materialreturndetails.objects.filter(Is_Deleted=False)
            return module
        except:
            module = Materialreturndetails.objects.none()
            return module

class Materialreturnsearch(ListAPIView):
    queryset = Materialreturndetails.objects.all()
    serializer_class = MaterialreturndetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]
    # search api
    def get_queryset(self):
        data = self.request.GET.get('search')
        # data = request
        module = Materialreturndetails.objects.filter( Q(MatRet_Id__icontains=data) | Q(
                Department_Name__Department_Name__icontains=data) |Q(Return_By__first_name__icontains=data) | Q(Received_By__icontains=data)| Q(Store_Id__Store_Name__icontains=data) , Q(Is_Deleted=False))
        return module

class MaterialReturnMyList(ListAPIView):
    queryset = Materialreturndetails.objects.all()
    serializer_class = MaterialreturndetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    # my material return list api
    def get_queryset(self):
        try:
            data = self.request.query_params
            #data=self.request
            module = Materialreturndetails.objects.filter(Company_Id=data['Company_Id'], Created_By=data['Created_By'],
                                              Is_Deleted=False).order_by('-id')
            return module
        except:
            module = Materialreturndetails.objects.none()
            return module


# api for material transfer module
# ponkumar
class MaterialTransferCRUD(ListAPIView):
    queryset = MaterialTransferDetails.objects.all()
    serializer_class = MaterialTransferDetailsSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    # create method of material transfer
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            #data=request
            module = Storemaster.objects.get(id=data['Store_Id'])
            out=StoremasterSerialzer(module)
            out1=out.data
            dict2 = {item['id']: item for item in data['Item']}
            # Append items from list2 to list1 based on "id"
            for item in out1['Items']:
                if item['id'] in dict2:
                    item.update(dict2[item['id']])
            out2=[]
            for i in out1['Items']:
                if 'Stock_After' in i:
                    data1={'id': i['id'], 'Item_Name': i['Item_Name'],'Opening_Stock':i['Stock_After']}
                    out2.append(data1)
                else:
                    data1={'id': i['id'], 'Item_Name': i['Item_Name'],'Opening_Stock':i['Opening_Stock']}
                    out2.append(data1)
            out1['Items'] = out2
            # print(out2)
            serialize1 = StoremasterSerialzer(instance=module,data=out1)
            if serialize1.is_valid():
                serialize1.save()
            serialize=MaterialTransferDetailsSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
                output=JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
            print(serialize.errors)
            return HttpResponse(str(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    
    # view method of material transfer
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            #data=request
            id=data['id']
            module=MaterialTransferDetails.objects.get(id=id)
            serialize=MaterialTransferDetailsSerialzer(module)
            output= JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    # edit the material transfer
    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            #data=request
            id=data['id']
            module=MaterialTransferDetails.objects.get(id=id)
            module = Storemaster.objects.get(id=data['Store_Id'])
            out=StoremasterSerialzer(module)
            out1=out.data
            dict2 = {item['id']: item for item in data['Item']}
            # Append items from list2 to list1 based on "id"
            for item in out1['Items']:
                if item['id'] in dict2:
                    item.update(dict2[item['id']])
            out2=[]
            for i in out1['Items']:
                if 'Stock_After' in i:
                    data1={'id': i['id'], 'Item_Name': i['Item_Name'],'Opening_Stock':i['Stock_After']}
                    out2.append(data1)
                else:
                    data1={'id': i['id'], 'Item_Name': i['Item_Name'],'Opening_Stock':i['Opening_Stock']}
                    out2.append(data1)
            out1['Items'] = out2
            # print(out2)
            serialize1 = StoremasterSerialzer(instance=module,data=out1)
            if serialize1.is_valid():
                serialize1.save()
            serialize=MaterialTransferDetailsSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    # partial edit the material transfer
    def patch(self,request,*args,**kwargs):
        try:
            data=request.data
            #data=request
            id=data['id']
            module=MaterialTransferDetails.objects.get(id=id)
            serialize=MaterialTransferDetailsSerialzer(instance=module,data=data,partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    # delete the material transfer
    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            #data=request
            id=data['id']
            module=MaterialTransferDetails.objects.get(id=id)
            serialize=MaterialTransferDetailsSerialzer(module,{"Is_Deleted": True}, partial=True)
            if serialize.is_valid():
                serialize.save()
            output=JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)



class MaterialTransferAdditionalCRUD(ListAPIView):
    queryset = MaterialTransferDetails.objects.all()
    serializer_class = MaterialTransferDetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    # filter api
    def post(self, request, *args, **kwargs):
        data=request.data
        data1={"Partner_Id__Partner_Name__icontains":data['Partner_Id'],"Item__icontains":[{"Item":data['Item_Name']}],"MatTransf_Date__range":data['MatTransf_Date'],"Transfer_Store_Name__icontains":data['Transfer_Store_Name'],"Is_deleted":False}
        return requestget(model=MaterialTransferDetails,serializer=MaterialTransferDetailsSerialzers,data=data1,page=data['page'],Operator='and')


    # all material return list api
    def get_queryset(self):
        try:
            module = MaterialTransferDetails.objects.filter(Is_Deleted=False)
            return module
        except:
            module = MaterialTransferDetails.objects.none()
            return module


class Meterialtransfersearch(ListAPIView):
    queryset = MaterialTransferDetails.objects.all()
    serializer_class = MaterialTransferDetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]
# search api
    def get_queryset(self):
        data = self.request.GET.get('search')
            # data = request
        module = MaterialTransferDetails.objects.filter( Q(
                Partner_Id__Partner_Name__icontains=data) | Q(From_Store_Name__icontains=data)| Q(CostCenter_Id__CostCenter_Id__icontains=data) |Q(Transfer_Store_Name__icontains=data) | Q(Issued_By__icontains=data) |Q(Requester_Id__first_name__icontains=data) , Q(Is_Deleted=False))
        return module

class MaterialTransferMyList(ListAPIView):
    queryset = MaterialTransferDetails.objects.all()
    serializer_class = MaterialTransferDetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    # my material transfer list api
    def get_queryset(self):
        try:
            data = self.request.query_params
            # data=self.request
            module = MaterialTransferDetails.objects.filter(Company_Id=data['Company_Id'], Created_By=data['Created_By'],
                                              Is_Deleted=False).order_by('-id')
            print("ss", module)
            return module
        except:
            module = MaterialTransferDetails.objects.none()
            return module






class BatchtableCRUD(ListAPIView):
    queryset = Batchtable.objects.all()
    serializer_class = BatchtableSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    # create method of item group
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            #data=request
            serialize=BatchtableSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output=JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
            #return HttpResponse(str(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)




    # view method of item group
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            #data=request
            id=data['id']
            module=Batchtable.objects.get(id=id)
            serialize=BatchtableSerialzer(module)
            output= JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    
    # edit method of item group
    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            #data=request
            id=data['id']
            module=Batchtable.objects.get(id=id)
            serialize=BatchtableSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    # partial edit method of item group
    def patch(self,request,*args,**kwargs):
        try:
            data=request.data
            #data=request
            id=data['id']
            module=Batchtable.objects.get(id=id)
            serialize=BatchtableSerialzer(instance=module,data=data,partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    # delete method of item group
    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            #data=request
            id=data['id']
            module=Batchtable.objects.get(id=id)
            serialize=BatchtableSerialzer(module,{"Is_Deleted": True}, partial=True)
            if serialize.is_valid():
                serialize.save()
            output=JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)

class Batchtablelist(APIView):
    def get(swelf,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Batchtable.objects.filter(Partner_Id=data['Partner_Id'])
            serialize = BatchtableSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

# api for item group module
# ponkumar
class ItemGroupCRUD(ListAPIView):
    queryset = ItemGroup.objects.all()
    serializer_class = ItemGroupSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    # create method of item group
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            #data=request
            serialize=ItemGroupSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output=JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
            #return HttpResponse(str(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)




    # view method of item group
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            #data=request
            id=data['id']
            module=ItemGroup.objects.get(id=id)
            serialize=ItemGroupSerialzer(module)
            output= JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    
    # edit method of item group
    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            #data=request
            id=data['id']
            module=ItemGroup.objects.get(id=id)
            serialize=ItemGroupSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    # partial edit method of item group
    def patch(self,request,*args,**kwargs):
        try:
            data=request.data
            #data=request
            id=data['id']
            module=ItemGroup.objects.get(id=id)
            serialize=ItemGroupSerialzer(instance=module,data=data,partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    # delete method of item group
    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            #data=request
            id=data['id']
            Item_Id=data['Item_Id']
            for i in Item_Id:
                module1=ItemMaster.objects.get(Item_Id=i)
                serialize=ItemMasterSerialzer(module1,{"Is_Deleted":True}, partial = True)
                if serialize.is_valid():
                    serialize.save()
            module=ItemGroup.objects.get(id=id)
            serialize=ItemGroupSerialzer(module,{"Is_Deleted": True}, partial=True)
            if serialize.is_valid():
                serialize.save()
            output=JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)



#Front end purpose we are created this api

class MaterialTransferwithouttoken(APIView):
    # create method of material transfer
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            #data=request
            serialize=MaterialTransferDetailsSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
                output=JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
            print(serialize.errors)
            return HttpResponse(str(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    
    # view method of material transfer
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            #data=request
            id=data['id']
            module=MaterialTransferDetails.objects.get(id=id)
            serialize=MaterialTransferDetailsSerialzers(module)
            output= JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    # edit the material transfer
    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            #data=request
            id=data['id']
            module=MaterialTransferDetails.objects.get(id=id)
            serialize=MaterialTransferDetailsSerialzers(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    # partial edit the material transfer
    def patch(self,request,*args,**kwargs):
        try:
            data=request.data
            #data=request
            id=data['id']
            module=MaterialTransferDetails.objects.get(id=id)
            serialize=MaterialTransferDetailsSerialzers(instance=module,data=data,partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    # delete the material transfer
    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            #data=request
            id=data['id']
            module=MaterialTransferDetails.objects.get(id=id)
            serialize=MaterialTransferDetailsSerialzer(module,{"Is_Deleted": True}, partial=True)
            if serialize.is_valid():
                serialize.save()
            output=JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)



class ItemGroupisactive(ListAPIView):
    queryset = ItemGroup.objects.all()
    serializer_class = ItemGroupSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            #data=request
            id=data['id']
            module=ItemGroup.objects.get(id=id)
            serialize=ItemGroupSerialzer(module,{"Is_Active": False}, partial=True)
            if serialize.is_valid():
                serialize.save()
            output=JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)


class ItemGroupisreactive(ListAPIView):
    queryset = ItemGroup.objects.all()
    serializer_class = ItemGroupSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            #data=request
            id=data['id']
            module=ItemGroup.objects.get(id=id)
            serialize=ItemGroupSerialzer(module,{"Is_Active": True}, partial=True)
            if serialize.is_valid():
                serialize.save()
            output=JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)


class ItemGroupAdditionalCRUD(ListAPIView):
    queryset = ItemGroup.objects.all()
    serializer_class = ItemGroupSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    # all item group list api
    def get_queryset(self):
        try:
            module = ItemGroup.objects.filter(Is_Deleted=False)
            return module
        except:
            module = ItemGroup.objects.none()
            return module


class ItemGroupisactivelist(ListAPIView):
    queryset = ItemGroup.objects.all()
    serializer_class = ItemGroupSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    # all item group list api
    def post(self,request,*args,**kwargs):
        data=request.data
        data1={"Is_Active":data['Is_Active'],"Is_Deleted":False}
        return requestget(model=ItemGroup,serializer=ItemGroupSerialzers,data=data1,page=data['page'],Operator='and')


class MyItemGroup(ListAPIView):
    queryset = ItemGroup.objects.all()
    serializer_class = ItemGroupSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    # my item group list api
    def get_queryset(self):
        try:
            data = self.request.data
            # data=self.request
            module = ItemGroup.objects.filter(Company_Id=data['Company_Id'], Created_By=data['Created_By'],
                                              Is_Deleted=False).order_by('-id')
            print("ss", module)
            return module
        except:
            module = ItemGroup.objects.none()
            return module

# Ravikumar API's

# REQ CRUD operations

class RFQCRUD(ListAPIView):
    queryset = RFQdetails.objects.all()
    serializer_class = RFQdetailsSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            # data=request
            if data=={}:
                raise TypeError
            
            serialize = RFQdetailsSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
            return HttpResponse(str(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['RFQ_Id']
            module = RFQdetails.objects.get(RFQ_Id=id)
            serialize = RFQdetailsSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            if data=={}:
                raise TypeError
          
            id=data['RFQ_Id']
            module=RFQdetails.objects.get(RFQ_Id=id)
            serialize = RFQdetailsSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
            return HttpResponse(str(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['RFQ_Id']
            module=RFQdetails.objects.get(RFQ_Id=id)
            serialize = RFQdetailsSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
            
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class RFQAdditional(ListAPIView):
    queryset = RFQdetails.objects.all()
    serializer_class = RFQdetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        try:
            
            data = self.request.GET.get('Partner_Id')
            # data=request
            return RFQdetails.objects.filter(Q(Partner_Id=data)&Q(Is_Deleted=False)).order_by('-id')
        except:
            return RFQdetails.objects.none()


    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            # data = request
            data1={"Quotation_Name__icontains":data['Quotation_Name'],"RFQ_Suppliers__Company_Name__icontains":data['Company_Name'],"Item_code__Item_Name__icontains":data['Item_Name'],"Created_Date__range":data['Created_Date'],"Is_Deleted":False}
            return requestget(model=RFQdetails,serializer=RFQdetailsSerialzers,data=data1,page=data['page'],Operator='and')
        except Exception as e:
            return HttpResponse(JSONRenderer().render({"Error": str(e)}), content_type='application/json',
                                status=status.HTTP_417_EXPECTATION_FAILED)



class RFQsearch(ListAPIView):
    queryset = RFQdetails.objects.all()
    serializer_class = RFQdetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get_queryset(self):
        data = self.request.GET.get('search')
        # data = request
        package = RFQdetails.objects.filter(Q(Quotation_Name__icontains=data)| Q(RFQ_Id__icontains=data) | Q(RFQ_Suppliers__Company_Name__icontains=data),Is_Deleted=False)
        return package


class RFQMyAdditional(ListAPIView):
    queryset = RFQdetails.objects.all()
    serializer_class = RFQdetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        try:
            Partner_Id = self.request.GET.get('Partner_Id')
            Created_By = self.request.GET.get('Created_By')
            module = RFQdetails.objects.filter(Q(Partner_Id=Partner_Id) & Q(Created_By=Created_By)
                                           &Q(Is_Deleted=False)).order_by('-id')
            return module
        except:
            return RFQdetails.objects.none()


# Front end purpose we are created

class RFQwithouttoken(APIView):

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            # data=request
            if data=={}:
                raise TypeError
            
            serialize = RFQdetailsSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
            return HttpResponse(str(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['id']
            module = RFQdetails.objects.get(id=id)
            serialize = RFQdetailsSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            if data=={}:
                raise TypeError
          
            id=data['RFQ_Id']
            module=RFQdetails.objects.get(RFQ_Id=id)
            serialize = RFQdetailsSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
            return HttpResponse(str(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['RFQ_Id']
            module=RFQdetails.objects.get(RFQ_Id=id)
            serialize = RFQdetailsSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
            
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)




# Supplier Quatation CRUD operations

class QuoteDetailsCRUD(ListAPIView):
    queryset = QuoteDetails.objects.all()
    serializer_class = QuoteDetailsSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]
    def post(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            serialize = QuoteDetailsSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
            return HttpResponse(str(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['id']
            module = QuoteDetails.objects.get(id=id)
            serialize = QuoteDetailsSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            # if data=={}:
            #     raise TypeError
            id=data['id']
            module=QuoteDetails.objects.get(id=id)
            serialize = QuoteDetailsSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
            return HttpResponse(str(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['id']
            module=QuoteDetails.objects.get(id=id)
            serialize = QuoteDetailsSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
            
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class QuoteDetailsAdditional(ListAPIView):
    queryset = QuoteDetails.objects.all()
    serializer_class = QuoteDetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        try:
            
            data = self.request.GET.get('Is_Deleted')
            # data=request
            return QuoteDetails.objects.filter(Q(Is_Deleted__exact=data) & Q(Draft_Flg=False)).order_by('-id')
        except:
            return QuoteDetails.objects.none()


    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            # data = request
            data1 = {"Quotation_Name__icontains":data['Quotation_Name'],"Supplier_Id__Company_Name__icontains":data['Company_Name'],"Created_Date__range":data['Created_Date'],"Is_Deleted":False}
            return requestget(model=QuoteDetails,serializer=QuoteDetailsSerialzers,data=data1,page=data['page'],Operator='and')
        except Exception as e:
            return HttpResponse(JSONRenderer().render({"Error": str(e)}), content_type='application/json',
                                status=status.HTTP_417_EXPECTATION_FAILED)

class Quotedetailssearch(ListAPIView):
    queryset = QuoteDetails.objects.all()
    serializer_class = QuoteDetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data = self.request.GET.get('search')
        # data = request
        package = QuoteDetails.objects.filter(Q(Quotation_Name__icontains=data)| Q(Quotation_Id__icontains=data)| Q(RFQNumber__icontains=data)| Q(Catalogue_Name__icontains=data) |Q(Catalogue_Number__icontains=data)| Q(Supplier_Id__Company_Name__icontains=data),Is_Deleted=False)
        return package


class QuoteDetailsMyListAdditional(ListAPIView):
    queryset = QuoteDetails.objects.all()
    serializer_class = QuoteDetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        try:
            Company_Id = self.request.GET.get('Company_Id')
            Created_By = self.request.GET.get('Created_By')
            Is_Deleted = self.request.GET.get('Is_Deleted')
            return QuoteDetails.objects.filter(Q(Company_Id=Company_Id) & Q( Created_By=Created_By) & Q(Is_Deleted=False) & Q(Draft_Flg=False)).order_by('-id')
        except:
            return QuoteDetails.objects.none()

    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            # data = request
            package = QuoteDetails.objects.filter(SupplierItems__items__exact=data['items'])
            return HttpResponse(JSONRenderer().render(QuoteDetailsSerialzer(package, many=True).data),
                                content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            return HttpResponse(JSONRenderer().render({"Error": str(e)}), content_type='application/json',
                                status=status.HTTP_417_EXPECTATION_FAILED)

class QuoteDetailsdraftCRUD(ListAPIView):
    queryset = QuoteDetails.objects.all()
    serializer_class = QuoteDetailsSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]
    def post(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            data['Draft_Flg']=True
            if data=={}:
                raise TypeError
            serialize = QuoteDetailsSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
            return HttpResponse(str(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['id']
            module = QuoteDetails.objects.get(id=id)
            serialize = QuoteDetailsSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            # if data=={}:
            #     raise TypeError
           
            id=data['id']
            module=QuoteDetails.objects.get(id=id)
            serialize = QuoteDetailsSerialzer(module,{"Draft_Flg":False},partial=True)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
            return HttpResponse(str(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['id']
            module=QuoteDetails.objects.get(id=id)
            serialize = QuoteDetailsSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
            
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)  


class QuoteDetailsdraftlist(ListAPIView):
    queryset = QuoteDetails.objects.all()
    serializer_class = QuoteDetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        Company_Id = self.request.GET.get('Company_Id')
        Created_By = self.request.GET.get('Created_By')
        Is_Deleted = self.request.GET.get('Is_Deleted')
        return QuoteDetails.objects.filter(Q(Company_Id=Company_Id) & Q( Created_By=Created_By) & Q(Is_Deleted=False) & Q(Draft_Flg=True)).order_by('-id')
        


class AgainstRFQdetails(ListAPIView):
    queryset = QuoteDetails.objects.all()
    serializer_class = QuoteDetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        Company_Id = self.request.GET.get('Company_Id')
        Created_By = self.request.GET.get('Created_By')
        Is_Deleted = self.request.GET.get('Is_Deleted')
        return QuoteDetails.objects.filter(Q(Company_Id=Company_Id) & Q( Created_By=Created_By) & Q(Is_Deleted=False) & Q(AgainstRFQ=False)).order_by('-id')


class WithoutRFQdetails(ListAPIView):
    queryset = QuoteDetails.objects.all()
    serializer_class = QuoteDetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        Company_Id = self.request.GET.get('Company_Id')
        Created_By = self.request.GET.get('Created_By')
        Is_Deleted = self.request.GET.get('Is_Deleted')
        return QuoteDetails.objects.filter(Q(Company_Id=Company_Id) & Q( Created_By=Created_By) & Q(Is_Deleted=False) & Q(AgainstRFQ=True)).order_by('-id')



class RFQandSupplierquotationtransaction(ListAPIView):
    queryset = QuoteDetails.objects.all()
    serializer_class = QuoteDetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        try:
            data = request.query_params
            module = QuoteDetails.objects.filter(Q(Company_Id=data['Company_Id']) & Q(RFQNumber=data['RFQNumber']) & Q(Is_Deleted=False)& Q(Draft_Flg=False))
            serialize = QuoteDetailsSerialzer(module, many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_100_CONTINUE)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)

# List for the Quatation Price Comparison

class QuoteItemDetails(ListAPIView):
    queryset = QuotationItem.objects.all()
    serializer_class = QuotationItemSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):

        try:
            # Company_Id = self.request.GET.get('Company_Id')
            # Created_By = self.request.GET.get('Created_By')
            # Items = self.request.GET.get('Is_Deleted')
            item = self.request.GET.get('items')
            return QuotationItem.objects.filter(items=item)
                                          
        except:
            return QuotationItem.objects.none()

# Purchase Order

class PODetailsCRUD(ListAPIView):
    queryset = PODetails.objects.all()
    serializer_class = PODetailsSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]
    def post(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            if data=={}:
                raise TypeError
            Company_Id=data['Company_Id']
            module = Transactionalseries.objects.get(Company_Id=Company_Id)
            serialize = TransactionalseriesSerialzer(module)
            prefix=JSONRenderer().render(serialize.data)
            pre=json.loads(prefix)
            out=pre['Purchase_Order']
            Organizatio=PODetails.objects.all().order_by('PO_Id').last()
            if Organizatio is None:
                Journals=out + "000000001"
                data['PO_Id']=Journals
            else:
                Organizati = Organizatio.PO_Id
                Organizat=int(Organizati[4:])
                Organiza=Organizat + 1
                Configur = out + str(Organiza).zfill(9)
                data['PO_Id']=Configur
            serialize = PODetailsSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
            return HttpResponse(str(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['id']
            module = PODetails.objects.get(id=id)
            serialize = PODetailsSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            # if data=={}:
            #     raise TypeError
           
            id=data['id']
            module=PODetails.objects.get(id=id)
            serialize = PODetailsSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
            return HttpResponse(str(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['id']
            module=PODetails.objects.get(id=id)
            serialize = PODetailsSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
            
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class PODetailsMyListAdditional(ListAPIView):
    queryset =PODetails.objects.all()
    serializer_class = PODetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        try:
            Partner_Id = self.request.GET.get('Partner_Id')
            Created_By = self.request.GET.get('Created_By')
            Is_Deleted = self.request.GET.get('Is_Deleted')
            return PODetails.objects.filter(Q(Partner_Id=Partner_Id) & Q(Created_By=Created_By)& Q(Is_Deleted=False)).order_by('-id')
        except:
            return PODetails.objects.none()


class PODetailsMyListwithoutpagination(ListAPIView):
    queryset =PODetails.objects.all()
    serializer_class = PODetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        try:
            data = request.query_params
            module = PODetails.objects.filter(Q(Company_Id=data["Company_Id"]) & Q(Partner_Id=data['Partner_Id']) & Q(Created_By=data["Created_By"]) & Q(Is_Deleted=False)).order_by('-id') 
            serialize = PODetailsSerialzers(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)



class PODetailsAdditional(ListAPIView):
    queryset = PODetails.objects.all()
    serializer_class =  PODetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        try:
            
            Partner_Id = self.request.GET.get('Partner_Id')
            # data=request
            return PODetails.objects.filter(Q(Is_Deleted=False) & Q(Partner_Id=Partner_Id)).order_by('-id')
        except:
            return PODetails.objects.none()
    


    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            # data = request
            data1 = {"Buyer_Name__icontains":data['Buyer_Name'],"Supplier_Id__Company_Name__icontains":data['Company_Name'],"Delivery_Date__range":data['Delivery_Date'],"Total_POAmount__range":data['Total_POAmount'],"Is_Deleted":False}
            return requestget(model=PODetails,serializer=PODetailsSerialzers,data=data1,page=data['page'],Operator='and')
        except Exception as e:
            return HttpResponse(JSONRenderer().render({"Error": str(e)}), content_type='application/json',
                                status=status.HTTP_417_EXPECTATION_FAILED)


class PODetailsearch(ListAPIView):
    queryset = PODetails.objects.all()
    serializer_class =  PODetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]
    def get_queryset(self):
        data = self.request.GET.get('search')
        # data = request
        package =PODetails.objects.filter(Q(PO_Id__icontains=data) | Q(PR_Id__PR_Id__icontains=data)| Q(Buyer_Name__icontains=data)| Q(Supplier_Id__Company_Name__icontains=data)| Q(Total_POAmount__icontains=data),Is_Deleted=False)
        return package


class POAmendDetailsCRUD(ListAPIView):
    queryset = POAmendDetails.objects.all()
    serializer_class = POAmendDetailsSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]
    def post(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            if data=={}:
                raise TypeError
            
            serialize = POAmendDetailsSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
            return HttpResponse(str(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['id']
            module = POAmendDetails.objects.get(id=id)
            serialize = POAmendDetailsSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            # if data=={}:
            #     raise TypeError
           
            id=data['id']
            module=POAmendDetails.objects.get(id=id)
            serialize = POAmendDetailsSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
            return HttpResponse(str(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            data=request
            data=request.query_params
            id=data['id']
            module=POAmendDetails.objects.get(id=id)
            serialize = POAmendDetailsSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
            
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class POAmendMyListAdditional(ListAPIView):
    queryset =POAmendDetails.objects.all()
    serializer_class = POAmendDetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        try:
            Company_Id = self.request.GET.get('Company_Id')
            Created_By = self.request.GET.get('Created_By')
            Is_Deleted = self.request.GET.get('Is_Deleted')
            return POAmendDetails.objects.filter(Company_Id=Company_Id, Created_By=Created_By,
                                           Is_Deleted__exact=Is_Deleted).order_by('-id')
        except:
            return POAmendDetails.objects.none()
        

        
class POAmendDetailsAdditional(ListAPIView):
    queryset = POAmendDetails.objects.all()
    serializer_class =  POAmendDetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        try:
            
            data = self.request.GET.get('Partner_Id')
            # data=request
            return POAmendDetails.objects.filter(Q(Partner_Id=data)& Q(Is_Deleted=False)).order_by('-id')
        except:
            return POAmendDetails.objects.none()
    
    def put(self, request, *args, **kwargs):
        try:
            data = request.data
            # data = request
            package =POAmendDetails.objects.filter(
               ( Q(PO_No__icontains=data['search']) | Q(POAmend_Id__icontains=data['search']) | Q(Supplier_Name__icontains=data['search'])
                 | Q(PO_Amount__icontains=data['search'])) | Q(supplier_reference__exact=data['search'])
                ,Is_Deleted__exact=False)
            return HttpResponse(JSONRenderer().render(POAmendDetailsSerialzers(package, many=True).data),
                                content_type='application/json', status=status.HTTP_200_OK)
            # d = Q()
            # d |= Q(**{"Name__icontains": data['search']})
            # d |= Q(**{"Currency_code__icontains": data['search']})
            # d &= Q(**{"Is_Deleted__exact": data['Is_Deleted']})
            # return requestget(model=Addons, serializer=Addonserializer, data=d, page=data['page'], Operator='or')
        except Exception as e:
            return HttpResponse(JSONRenderer().render({"Error": str(e)}), content_type='application/json',
                                status=status.HTTP_417_EXPECTATION_FAILED)

    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            # data = request
            data1 = {"Supplier_Name__icontains":data['Supplier_Name'],"PO_Amount__exact":data['PO_Amount'],"Created_Date__range":data['Created_Date'],
                    "PO_No__icontains":data['PO_No'],"supplier_reference__icontains":data['supplier_reference'],
                    "Is_Deleted__exact":data['Is_Deleted']}
            return requestget(model=POAmendDetails,serializer=POAmendDetailsSerialzers,data=data1,page=data['page'],Operator='and')
        except Exception as e:
            return HttpResponse(JSONRenderer().render({"Error": str(e)}), content_type='application/json',
                                status=status.HTTP_417_EXPECTATION_FAILED)

        
class POCancelDetailsCRUD(ListAPIView):
    queryset = POCancelDetails.objects.all()
    serializer_class =POCancelDetailsSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]
    def post(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            if data=={}:
                raise TypeError
            
            serialize = POCancelDetailsSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
            return HttpResponse(str(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['id']
            module = POCancelDetails.objects.get(id=id)
            serialize = POCancelDetailsSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            # if data=={}:
            #     raise TypeError
           
            id=data['id']
            module=POCancelDetails.objects.get(id=id)
            serialize = POCancelDetailsSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
            return HttpResponse(str(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['id']
            module=POCancelDetails.objects.get(id=id)
            serialize = POCancelDetailsSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
            
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

class POCancelDetailsMyList(ListAPIView):
    queryset =POCancelDetails.objects.all()
    serializer_class = POCancelDetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        try:
            Partner_Id = self.request.GET.get('Partner_Id')
            Created_By = self.request.GET.get('Created_By')
            return POCancelDetails.objects.filter(Q(Partner_Id=Partner_Id) & Q(Created_By=Created_By)&
                                           Q(Is_Deleted=False)).order_by('-id')
        except:
            return POCancelDetails.objects.none()

class POCancelDetailsAdditional(ListAPIView):

    queryset = POCancelDetails.objects.all()
    serializer_class =  POCancelDetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        try:
            
            data = self.request.GET.get('Partner_Id')
            # data=request
            return POCancelDetails.objects.filter(Q(Partner_Id=data) & Q(Is_Deleted=False)).order_by('-id')
        except:
            return POCancelDetails.objects.none()
    


    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            # data = request
            data1 = {"Supplier_Name__icontains":data['Supplier_Name'],"Cancellation_Reason__icontains":data['Cancellation_Reason'],"Created_Date__range":data['Created_Date'],"PO_Amount__range":data['PO_Amount'],"Is_Deleted":False}
            return requestget(model=POCancelDetails,serializer=POCancelDetailsSerialzers,data=data1,page=data['page'],Operator='and')
        except Exception as e:
            return HttpResponse(JSONRenderer().render({"Error": str(e)}), content_type='application/json',
                                status=status.HTTP_417_EXPECTATION_FAILED)


class POcancelsearch(ListAPIView):
    queryset = POCancelDetails.objects.all()
    serializer_class =  POCancelDetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data = self.request.GET.get('search')
        # data = request
        package =POCancelDetails.objects.filter(Q(Supplier_Name__icontains=data) | Q(PO_Amount__icontains=data) | Q(PO_No__icontains=data)| Q(Cancellation_Reason__icontains=data) | Q(POCancel_Id__icontains=data),Is_Deleted=False)
        return package

class Pricecomparison(ListAPIView):
    queryset = Catalogue.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module=Catalogue.objects.filter(Q(Item__contains=[{"Item_Name":data['Item_Name']}]) & Q(Is_Deleted=False))
            serialize = CatalogueSerialzer(module,many=True)
            response = serialize.data
            output1=[]
            for i in response:
                unitprice=i['Item']
                for y in unitprice:
                    if y['Item_Name']==data['Item_Name']:
                        data1={"Supplier_Name":i['Supplier_Name'],"Item_Name":data['Item_Name'],"Unit_Price":y['Unit_Price']}
                        output1.append(data1)
            output = JSONRenderer().render(output1)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


# Crud opration for Store Adjustment


class StoreAdjustmentCRUD(ListAPIView):

    queryset=StoreAdjustment.objects.all()
    serializer_class=StoreAdjustmentSerialzer
    authentication_classes=[JSONWebTokenAuthentication]
    permission_classes=[DjangoModelPermissions]

    def post(self,request,*args,**kwargs):

        try:
            # data=request
            data=request.data
            if data=={}:
                raise TypeError
            
            serialize=StoreAdjustmentSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
                output=JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
            return HttpResponse(str(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
            
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    
    def get(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['id']
            module = StoreAdjustment.objects.get(id=id)
            serialize = StoreAdjustmentSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            # if data=={}:
            #     raise TypeError
            id=data['id']
            module=StoreAdjustment.objects.get(id=id)
            serialize = StoreAdjustmentSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
            return HttpResponse(str(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['id']
            module=StoreAdjustment.objects.get(id=id)
            serialize = StoreAdjustmentSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
            
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        
#For front end purpose 
class StoreAdjustmentwithouttokenCRUD(APIView):
    
    def get(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['id']
            module = StoreAdjustment.objects.get(id=id)
            serialize = StoreAdjustmentSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            id=data['id']
            module=StoreAdjustment.objects.get(id=id)
            serialize = StoreAdjustmentSerialzers(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
            return HttpResponse(str(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class StoreAdjustmentMyList(ListAPIView):
    queryset =StoreAdjustment.objects.all()
    serializer_class = StoreAdjustmentSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        Company_Id = self.request.GET.get('Company_Id')
        Created_By = self.request.GET.get('Created_By')
        module = StoreAdjustment.objects.filter(Q(Company_Id=Company_Id) & Q(Created_By=Created_By) & Q(Is_Deleted=False)).order_by('-id')
        return module
        


class StoreAdjustmentAdditional(ListAPIView):
    queryset = StoreAdjustment.objects.all()
    serializer_class =  StoreAdjustmentSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        try:
            
            data = self.request.GET.get('Is_Deleted')
            # data=request
            return StoreAdjustment.objects.filter(Is_Deleted__exact=data)
        except:
            return StoreAdjustment.objects.none()
    
    def put(self, request, *args, **kwargs):
        try:
            data = request.data
            # data = request
            package =StoreAdjustment.objects.filter(
               (Q(StoreAdj_Id__icontains=data['search'])| Q(Hub_Name__icontains=data['search']) | Q(Adjusted_By__icontains=data['search'])),Is_Deleted__exact=False)
            return HttpResponse(JSONRenderer().render(StoreAdjustmentSerialzers(package, many=True).data),
                                content_type='application/json', status=status.HTTP_200_OK)
            # d = Q()
            # d |= Q(**{"Name__icontains": data['search']})
            # d |= Q(**{"Currency_code__icontains": data['search']})
            # d &= Q(**{"Is_Deleted__exact": data['Is_Deleted']})
            # return requestget(model=Addons, serializer=Addonserializer, data=d, page=data['page'], Operator='or')
        except Exception as e:
            return HttpResponse(JSONRenderer().render({"Error": str(e)}), content_type='application/json',
                                status=status.HTTP_417_EXPECTATION_FAILED)



class StoreAdjustmentfilter(ListAPIView):
    queryset = StoreAdjustment.objects.all()
    serializer_class =  StoreAdjustmentSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self, request, *args, **kwargs):
        data = request.data
            # data = request
        data1={"Hub_Name__icontains":data['Hub_Name'],"Adjusted_By__icontains":data['Adjusted_By'],"Created_Date__range":data['Created_Date'],"Is_Deleted":False}
        return requestget(model=StoreAdjustment,serializer=StoreAdjustmentSerialzers,data=data1,page=data['page'],Operator='and')

        
#Suplier Table

class SupplierCRUD(ListAPIView):
    queryset=Supplier.objects.all()
    authentication_classes=[JSONWebTokenAuthentication]
    permission_classes=[DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Supplieraddress=SupplieraddressSerialzer(data=data['Supplieraddress_Id'])
            if Supplieraddress.is_valid():
                Supplieraddress.save()
            Supplieraddress_Id=Supplieraddress.data
            data['Supplieraddress_Id']=Supplieraddress_Id['id']
            print(data['Supplieraddress_Id'])
            module =SupplierSerialzer(data=data)
            if module.is_valid():
                module.save()
            output = JSONRenderer().render(module.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Supplier_Id=data['Supplier_Id']
            module = Supplier.objects.get(Supplier_Id=Supplier_Id)
            serialize = SupplierSerialzers(module)
            output=JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            Supplier_Id=data['Supplier_Id']
            Supplieraddress_Id=data['Supplieraddress_Id']
            Address = Supplieraddress.objects.get(Supplieraddress_Id=Supplieraddress_Id['Supplieraddress_Id'])
            addserialize = SupplieraddressSerialzer(instance=Address,data=data['Supplieraddress_Id'])
            if addserialize.is_valid():
                addserialize.save()
            add=addserialize.data
            data['Supplieraddress_Id']=add['id']
            module = Supplier.objects.get(Supplier_Id=Supplier_Id)
            serialize = SupplierSerialzer(instance=module,data=request.data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Supplier_Id=data['Supplier_Id']
            module=Supplier.objects.get(Supplier_Id=Supplier_Id)
            serialize = SupplierSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Supplierlist(ListAPIView):
    queryset = Supplier.objects.all()
    serializer_class =  SupplierSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data=self.request.GET.get('Partner_Id')
        module = Supplier.objects.filter(Q(Partner_Id=data) & Q(Is_Deleted=False)).order_by("Supplier_Id")
        return module

class Suplierfilter(ListAPIView):
    queryset = Supplier.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        data=request.data
        data1={"Company_Name__icontains":data['Company_Name'],"ContactPerson_FirstName__icontains":data['ContactPerson_FirstName'],"Created_Date__range":data['Created_Date'],"Is_Deleted":False}
        return requestget(model=Supplier,serializer=SupplierSerialzers,data=data1,page=data['page'],Operator="and")


class Supliersearch(ListAPIView):
    queryset = Supplier.objects.all()
    serializer_class =  SupplierSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data=self.request.GET.get('search')
        module= Supplier.objects.filter(Q(Company_Name__icontains=data) | Q(ContactPerson_FirstName__icontains=data) |
            Q(Mobile_No__icontains=data) | Q(Supplier_Id__icontains=data)| Q(Supplieraddress_Id__Building_City__contains=data),Is_Deleted=False).order_by("Supplier_Id")
        return module

class Formpermissioncrud(ListAPIView):
    queryset = FormPermission.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            serialize = FormPermissionSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_201_CREATED)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, *args, **kwargs):
        try:
            data = request.query_params
            id = data['id']
            module = FormPermission.objects.get(id=id)
            serialize = FormPermissionSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, *args, **kwargs):
        try:
            data = request.data
            id = data['id']
            module = FormPermission.objects.get(id=id)
            serialize = FormPermissionSerialzer(instance=module, data=request.data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)

class Formpermisionlist(ListAPIView):
    queryset = FormPermission.objects.all()
    serializer_class = FormPermissionSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        module = FormPermission.objects.all()
        return module


class Formpermisionlists(ListAPIView):
    queryset = FormPermission.objects.all()
    serializer_class = FormPermissionSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        module = FormPermission.objects.all()
        serialize = FormPermissionSerialzer(module,many=True)
        output = JSONRenderer().render(serialize.data)
        return HttpResponse(output,content_type='application/json')


class Formpermissionfilter(ListAPIView):
    queryset = FormPermission.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]
    # filter api
    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            module = FormPermission.objects.filter(
                Q(ModuleName__icontains=data['ModuleName']))
            serialize = FormPermissionSerialzer(module, many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)



class Formpermastercrud(ListAPIView):
    queryset = FormMaster.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            serialize = FormMasterSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_201_CREATED)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, *args, **kwargs):
        try:
            data = request.query_params
            id = data['FormmasterId']
            module = FormMaster.objects.get(FormmasterId=id)
            serialize = FormMasterSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            da=json.loads(output)
            data1=da['FormId']
            permision=[]
            for i in data1:
                permision.append(i['ModulePermission'])
            per = []
            for i in permision:
                for y in i:
                    per.append(y)
            output1= JSONRenderer().render(per)
            return HttpResponse(output1,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, *args, **kwargs):
        try:
            data = request.data
            id = data['FormmasterId']
            module = FormMaster.objects.get(FormmasterId=id)
            serialize = FormMasterSerialzer(instance=module, data=request.data, partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)

class Formmasterlist(ListAPIView):
    queryset = FormMaster.objects.all()
    serializer_class = FormMasterSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        module = FormMaster.objects.all()
        serialize = FormMasterSerialzers(module,many=True)
        output = JSONRenderer().render(serialize.data)
        return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)

    # queryset = FormMaster.objects.all()
    # serializer_class = FormMasterSerialzer
    # authentication_classes = [JSONWebTokenAuthentication]
    # permission_classes = [DjangoModelPermissions]

    # def get_queryset(self):
    #     module = FormMaster.objects.filter(Is_Deleted=False)
    #     return module


class Allpermission(ListAPIView):
    queryset = Permission.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        module = Permission.objects.all()
        serialize = PermissionSerialzer(module,many=True)
        output = JSONRenderer().render(serialize.data)
        data=json.loads(output)
        per=[]
        for i in data:
            per.append(i['id'])
        output=JSONRenderer().render(per)
        return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)


class Cataloguecrud(ListAPIView):
    queryset = Catalogue.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            serialize = CatalogueSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_201_CREATED)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, *args, **kwargs):
        try:
            data = request.query_params
            Cat_Id = data['Cat_Id']
            module = Catalogue.objects.get(Cat_Id=Cat_Id)
            serialize = CatalogueSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, *args, **kwargs):
        try:
            data = request.data
            Cat_Id = data['Cat_Id']
            module = Catalogue.objects.get(Cat_Id=Cat_Id)
            serialize = CatalogueSerialzer(instance=module, data=request.data, partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data = request.query_params
            Cat_Id =data['Cat_Id']
            module = Catalogue.objects.get(Cat_Id=Cat_Id)
            serialize = CatalogueSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)



class Cataloguelist(ListAPIView):
    queryset = Catalogue.objects.all()
    serializer_class = CatalogueSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        module = Catalogue.objects.filter(Q(Is_Deleted=False)&Q(Expiry_Flg=False)).order_by('Cat_Id')
        return module

class CatalogueExpirylist(ListAPIView):
    queryset = Catalogue.objects.all()
    serializer_class = CatalogueSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        module = Catalogue.objects.filter(Q(Is_Deleted=False)&Q(Expiry_Flg=True)).order_by('Cat_Id')
        return module

class Expiryflgchange(ListAPIView):
    queryset = Catalogue.objects.all()
    serializer_class = CatalogueSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Catalogue.objects.get(Cat_Id=data['Cat_Id'])
            serialize = CatalogueSerialzer(module,{"Expiry_Flg":data['Expiry_Flg']},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)


class Cataloguesearch(ListAPIView):
    queryset = Catalogue.objects.all()
    serializer_class = CatalogueSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get_queryset(self):
        data=self.request.GET.get('search')
        module = Catalogue.objects.filter(Q(Catalogue_Name__icontains=data)|Q(Cat_Id__icontains=data) | Q(Quotation_Id__icontains=data)| Q(Supplier_Name__icontains=data),Is_Deleted=False).order_by('Cat_Id')
        return module

class Cataloguefilter(ListAPIView):
    queryset = Catalogue.objects.all()
    serializer_class = CatalogueSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        data=request.data
        data1 = {"Catalogue_Name__icontains":data['Catalogue_Name'],"Supplier_Name__icontains":data['Supplier_Name'],"Created_Date__range":data["Created_Date"],"Is_Deleted":False}
        return requestget(model=Catalogue,serializer=CatalogueSerialzers,data=data1,page=data['page'],Operator='and')
     

class PincodeCRUD(ListAPIView):
    queryset = Pincodetable.objects.all()
    serializer_class = PincodetableSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['pincode']
            # print(id)
            pincode = Pincodetable.objects.get(Pincode__exact=id)
            serialize = PincodetableSerialzer(pincode)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render(str(e))
            return HttpResponse(output,content_type='application/json',status=status.HTTP_404_NOT_FOUND)
        # serialize = PincodetableSerialzer(pincode,many=True)
        # output = JSONRenderer().render(serialize.data)
        # string = json.loads(output)
        # for i in string:
        #     if i==id:
        #         print("True")
        #     else:
        #         print("False")


# Service API's
class ServicepriceCRUD(ListAPIView):
    queryset = Serviceprice.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            serialize = ServicepriceSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['Serviceprice_Id']
            module = Serviceprice.objects.get(Serviceprice_Id=id)
            serialize = ServicepriceSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            id=data['Serviceprice_Id']
            module=Serviceprice.objects.get(Serviceprice_Id=id)
            serialize = ServicepriceSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['Serviceprice_Id']
            module=Serviceprice.objects.get(Serviceprice_Id=id)
            serialize = ServicepriceSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)    
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Servicepricelist(ListAPIView):
    queryset = Serviceprice.objects.all()
    serializer_class = ServicepriceSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        Created_By=self.request.GET.get('Created_By')
        Partner_Id = self.request.GET.get('Partner_Id')
        module = Serviceprice.objects.filter(Q(Partner_Id=Partner_Id) & Q(Created_By=Created_By) & Q(Is_Deleted=False)).order_by('-id')
        return module


class Citywiseservicepricelist(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Serviceprice.objects.filter(Q(City_Name=data['City_Name']) & Q(Is_Deleted=False)).order_by('-id')
            serialize = ServicepriceSerialzers(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)



class ServiceCategoryCRUD(ListAPIView):
    queryset = Servicecategory.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            serialize = ServicecategorySerialzer(data=data)
            decoded_data = base64.b64decode(data['Category_Image'])
            data['Category_Image'] = ContentFile(decoded_data, name=data['Category_Image_Name'])
            del data['Category_Image_Name']
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['Category_Id']
            module = Servicecategory.objects.get(Category_Id=id)
            serialize = ServicecategorySerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            id=data['Category_Id']
            module=Servicecategory.objects.get(Category_Id=id)
            serialize = ServicecategorySerialzer(instance=module,data=data)
            decoded_data = base64.b64decode((data['Category_Image']))
            data['Category_Image'] = ContentFile(decoded_data, name=data['Category_Image_Name'])
            del data['Category_Image_Name']
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['Category_Id']
            module=Servicecategory.objects.get(Category_Id=id)
            serialize = ServicecategorySerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)    
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Myservicecategorylist(ListAPIView):
    queryset = Servicecategory.objects.all()
    serializer_class = ServicecategorySerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        Created_By=self.request.GET.get('Created_By')
        module = Servicecategory.objects.filter(Q(Created_By=Created_By) & Q(Is_Deleted=False)).order_by('-id')
        return module



class Servicecategorylist(ListAPIView):
    queryset = Servicecategory.objects.all()
    serializer_class = ServicecategorySerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        module = Servicecategory.objects.filter(Is_Deleted=False).order_by('-id')
        return module

class Servicecategoryfilter(ListAPIView):
    queryset = Servicecategory.objects.all()
    serializer_class =ServicecategorySerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
            data=request.data
            data1={"Category_Name__icontains":data['Category_Name'],"Category_Id__icontains":data['Category_Id'],"Created_Date__range":data['Created_Date'],"Is_Deleted":False}
            return requestget(model=Servicecategory,serializer=ServicecategorySerialzers,data=data1,page=data['page'],Operator='and')

class Servicecategorysearch(ListAPIView):
    queryset = Servicecategory.objects.all()
    serializer_class = ServicecategorySerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data=self.request.GET.get('search')
        module = Servicecategory.objects.filter(Q(Category_Name__icontains=data)|Q(Category_Id__icontains=data),Is_Deleted=False).order_by('-id')
        return module



class ServiceCRUD(ListAPIView):
    queryset = Services.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            serialize = ServicesSerialzer(data=data)
            decoded_data = base64.b64decode(data['Service_Image'])
            data['Service_Image'] = ContentFile(decoded_data, name=data['Service_Image_Name'])
            del data['Service_Image_Name']
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['Service_Id']
            module = Services.objects.get(Service_Id=id)
            serialize = ServicesSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            id=data['Service_Id']
            module=Services.objects.get(Service_Id=id)
            serialize = ServicesSerialzer(instance=module,data=data)
            decoded_data = base64.b64decode((data['Service_Image']))
            data['Service_Image'] = ContentFile(decoded_data, name=data['Service_Image_Name'])
            del data['Service_Image_Name']
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['Service_Id']
            module=Services.objects.get(Service_Id=id)
            serialize = ServicesSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)    
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Myservicelist(ListAPIView):
    queryset = Services.objects.all()
    serializer_class = ServicesSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        Created_By=self.request.GET.get('Created_By')
        module = Services.objects.filter(Q(Created_By=Created_By) & Q(Is_Deleted=False)).order_by('-id')
        return module



class Servicelist(ListAPIView):
    queryset = Services.objects.all()
    serializer_class = ServicesSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        module = Services.objects.filter(Is_Deleted=False).order_by('-id')
        return module

class Servicefilter(ListAPIView):
    queryset = Services.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
            data=request.data
            data1={"Service_Id__icontains":data['Service_Id'],"Service_Name__icontains":data['Service_Name'],"Category_Name__icontains":data['Category_Name'],"Created_Date__range":data['Created_Date'],"Is_Deleted":False}
            return requestget(model=Services,serializer=ServicesSerialzers,data=data1,page=data['page'],Operator='and')

class Servicesearch(ListAPIView):
    queryset = Services.objects.all()
    serializer_class = ServicesSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get_queryset(self):
        data=self.request.GET.get('search')
        module = Services.objects.filter(Q(Service_Name__icontains=data)| Q(Service_Id__icontains=data)|Q(Category_Name__icontains=data) | Q(Partner_Id__Partner_Name__icontains=data),Is_Deleted=False).order_by('-id')
        return module



class PlanCRUD(ListAPIView):
    queryset = Plan.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            serialize = PlanSerialzer(data=data)
            decoded_data = base64.b64decode(data['PlanImage'])
            data['PlanImage'] = ContentFile(decoded_data, name=data['PlanImage_Name'])
            del data['PlanImage_Name']
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['Plan_Id']
            module = Plan.objects.get(Plan_Id=id)
            serialize = PlanSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            id=data['Plan_Id']
            module=Plan.objects.get(Plan_Id=id)
            serialize = PlanSerialzer(instance=module,data=data)
            decoded_data = base64.b64decode((data['PlanImage']))
            data['PlanImage'] = ContentFile(decoded_data, name=data['PlanImage_Name'])
            del data['PlanImage_Name']
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['Plan_Id']
            module=Plan.objects.get(Plan_Id=id)
            serialize = PlanSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)    
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Myplanlist(ListAPIView):
    queryset = Plan.objects.all()
    serializer_class = PlanSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        Created_By=self.request.GET.get('Created_By')
        module = Plan.objects.filter(Q(Created_By=Created_By) & Q(Is_Deleted=False)).order_by('-id')
        return module



class Planlist(ListAPIView):
    queryset = Plan.objects.all()
    serializer_class = PlanSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        module = Plan.objects.filter(Is_Deleted=False).order_by('-id')
        return module

class Planfilter(ListAPIView):
    queryset = Plan.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
            data=request.data
            data1={"PlanName__icontains":data['PlanName'],"Service_Name__icontains":data['Service_Name'],"Category_Name__icontains":data['Category_Name'],"Created_Date__range":data['Created_Date'],"Is_Deleted":False}
            return requestget(model=Plan,serializer=PlanSerialzers,data=data1,page=data['page'],Operator='and')

class Plansearch(ListAPIView):
    queryset = Plan.objects.all()
    serializer_class = PlanSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get_queryset(self):
        data=self.request.GET.get('search')
        module = Plan.objects.filter(Q(PlanName__icontains=data) | Q(Plan_Id__icontains=data)|Q(Service_Name__icontains=data) | Q(Category_Name__icontains=data) | Q(Price__icontains=data),Is_Deleted=False).order_by('-id')
        return module


class PlanwithouttokenCRUD(APIView):

    def post(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            serialize = PlanSerialzer(data=data)
            decoded_data = base64.b64decode(data['PlanImage'])
            data['PlanImage'] = ContentFile(decoded_data, name=data['PlanImage_Name'])
            del data['PlanImage_Name']
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['id']
            module = Plan.objects.get(id=id)
            serialize = PlanSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            id=data['id']
            module=Plan.objects.get(id=id)
            serialize = PlanSerialzer(instance=module,data=data)
            decoded_data = base64.b64decode((data['PlanImage']))
            data['PlanImage'] = ContentFile(decoded_data, name=data['PlanImage_Name'])
            del data['PlanImage_Name']
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['Plan_Id']
            module=Plan.objects.get(Plan_Id=id)
            serialize = PlanSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)    
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Planapprovalsystem(APIView):
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            # Workflow_Id=data['Workflow_Id']
            Employee_Id=data['Employee_Id']
            # print(Depname['Department_Head'])
            # html =f'{Base_Url}/inventory/material-request/document-approval'
            Created_By=data['Created_By']
            first_name=data['approver_name']
            Approverid=data['Approverid']
            module=User.objects.get(id=Approverid)
            serialize = UserSerializer(module)
            userdata=serialize.data
            # use=userdata[0]
            To=userdata['email']
            module1 = MailConfiguration.objects.get(Created_By=Created_By)
            serialize = MailConfigurationSerializer(module1)
            com=serialize.data
            From=com['MailId']
            subject=data['mailsubject']
            # Body=html
            Host=com['SMTPHost']
            Port = com['SMTPPort']
            Password=com['MailPassword']
            if Employee_Id != "":
                module = EmployeeMaster.objects.get(Employee_Id=Employee_Id)
                serialize = EmployeeMasterSerialzer(module)
                emp=serialize.data
                Dep_id=emp['Department_Id']
                module = Department.objects.get(id=Dep_id)
                serialize = DepartmentSerialzer(module)
                Depname=serialize.data
                module = User.objects.get(id=Depname['Updated_By'])
                serialize = UserSerializer(module)
                head=serialize.data
                To=head['email']
                html = data['mailhtml'].replace('{{url}}',f'{Base_Url}/sales/DocumentApprovalForRefund/?Document_Type='+str(data['Document_Type'])+"&id="+str(data['id'])+"&Initiated_By=" +data['Initiated_By']+"&Url=" +data['Url']+"&Comments=" +data['Comments']+"&index="+str(data['index'])+"&documentapprovalid="+data['documentapprovalid'] +"&Designation="+head['Designation']+"&Headname="+head['first_name'])
                Body = html
                output = {"Headname":head['first_name'],"rolename":head['Designation'],"Message": "Mail sent Successfully","id":head['id']}
                response = SendMail(subject, From, To,Body, Host, Port,Password)
                return HttpResponse(JSONRenderer().render(output),
                                            content_type='application/json', status=status.HTTP_200_OK)
            else:
                html = data['mailhtml'].replace('{{url}}',f'{Base_Url}/sales/DocumentApprovalForRefund/?Document_Type='+str(data['Document_Type'])+"&id="+str(data['id'])+"&Initiated_By=" +data['Initiated_By']+"&Url=" +data['Url']+"&Comments=" +data['Comments']+"&index="+str(data['index'])+"&documentapprovalid="+data['documentapprovalid']+"&Designation="+data['Designation']+"&Headname="+data['Headname'])
                Body = html
                To=userdata['email']
                response = SendMail(subject, From, To,Body, Host, Port,Password)
                return HttpResponse(JSONRenderer().render({"Message": "Mail sent Successfully"}),content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            return HttpResponse(JSONRenderer().render({"Message": "Mail Not sent"}),content_type='application/json', status=status.HTTP_200_OK)




class SubscriptionCRUD(ListAPIView):
    queryset = Subscription.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            serialize = SubscriptionSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['Subscription_Id']
            module = Subscription.objects.get(Subscription_Id=id)
            serialize = SubscriptionSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            id=data['Subscription_Id']
            module=Subscription.objects.get(Subscription_Id=id)
            serialize = SubscriptionSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['Subscription_Id']
            module=Subscription.objects.get(Subscription_Id=id)
            serialize = SubscriptionSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)    
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Mysubscriptionlist(ListAPIView):
    queryset = Subscription.objects.all()
    serializer_class = SubscriptionSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        Created_By=self.request.GET.get('Created_By')
        module = Subscription.objects.filter(Q(Created_By=Created_By) & Q(Is_Deleted=False)).order_by('-id')
        return module



class Subscriptionlist(ListAPIView):
    queryset = Subscription.objects.all()
    serializer_class = SubscriptionSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        module = Subscription.objects.filter(Is_Deleted=False).order_by('-id')
        return module

class Subscriptionfilter(ListAPIView):
    queryset = Subscription.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
            data=request.data
            data1={"Service_Name__icontains":data['Service_Name'],"PlanName__icontains":data['PlanName'],"Subscriber_Name__icontains":data['Subscriber_Name'],"Category_Name__icontains":data['Category_Name'],"Created_Date__range":data['Created_Date'],"Is_Deleted":False}
            return requestget(model=Subscription,serializer=SubscriptionSerialzers,data=data1,page=data['page'],Operator='and')

class Subscriptionsearch(ListAPIView):
    queryset = Subscription.objects.all()
    serializer_class = SubscriptionSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get_queryset(self):
        data=self.request.GET.get('search')
        module = Subscription.objects.filter(Q(Service_Name__icontains=data)|Q(Subscription_Id__icontains=data) |Q(Car_Type__icontains=data)|Q(Car_Brand__icontains=data)|Q(Subscriber_Name__icontains=data) |Q(PlanName__icontains=data)| Q(Category_Name__icontains=data),Is_Deleted=False).order_by('-id')
        return module


class SlotCRUD(ListAPIView):
    queryset = Slot.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            serialize = SlotSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            Created_By=data['Created_By']
            module = Slot.objects.get(Created_By=Created_By)
            serialize = SlotSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            Created_By=data['Created_By']
            module=Slot.objects.get(Created_By=Created_By)
            serialize = SlotSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['Slot_Id']
            module=Slot.objects.get(Slot_Id=id)
            serialize = SlotSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)    
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Myslotlist(ListAPIView):
    queryset = Slot.objects.all()
    serializer_class = SlotSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        Created_By=self.request.GET.get('Created_By')
        module = Slot.objects.filter(Q(Created_By=Created_By) & Q(Is_Deleted=False)).order_by('-id')
        return module



class Slotlist(ListAPIView):
    queryset = Slot.objects.all()
    serializer_class = SlotSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        module = Slot.objects.filter(Is_Deleted=False).order_by('-id')
        return module

class Slotfilter(ListAPIView):
    queryset = Slot.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
            data=request.data
            data1={"Slot_Name__icontains":data['Slot_Name'],"Service_Name__icontains":data['Service_Name'],"Category_Name__icontains":data['Category_Name'],"Created_Date__range":data['Created_Date'],"Is_Deleted":False}
            return requestget(model=Slot,serializer=SlotSerialzers,data=data1,page=data['page'],Operator='and')

class Slotsearch(ListAPIView):
    queryset = Slot.objects.all()
    serializer_class = SlotSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get_queryset(self):
        data=self.request.GET.get('search')
        module = Slot.objects.filter(Q(Slot_Name__icontains=data)|Q(Service_Name__icontains=data)| Q(Slot_Id__icontains=data) | Q(Category_Name__icontains=data),Is_Deleted=False).order_by('-id')
        return module




class AlocationCRUD(ListAPIView):
    queryset = JobAlocationMaster.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            serialize = JobAlocationMasterSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['JobAlocationMaster_Id']
            module = JobAlocationMaster.objects.get(JobAlocationMaster_Id=id)
            serialize = JobAlocationMasterSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            id=data['JobAlocationMaster_Id']
            module=JobAlocationMaster.objects.get(JobAlocationMaster_Id=id)
            serialize = JobAlocationMasterSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['JobAlocationMaster_Id']
            module=JobAlocationMaster.objects.get(JobAlocationMaster_Id=id)
            serialize = JobAlocationMasterSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)    
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class Slotalocation(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Service_Name = data['Service_Name']
            module = Slot.objects.filter(Service_Name=Service_Name)
            serialize = SlotSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json')
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json')


class MyAlocationlist(ListAPIView):
    queryset = JobAlocationMaster.objects.all()
    serializer_class = JobAlocationMasterSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        Created_By=self.request.GET.get('Created_By')
        module = JobAlocationMaster.objects.filter(Q(Created_By=Created_By) & Q(Is_Deleted=False)).order_by('-id')
        return module



class Alocationlist(ListAPIView):
    queryset = JobAlocationMaster.objects.all()
    serializer_class = JobAlocationMasterSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        module = JobAlocationMaster.objects.filter(Is_Deleted=False).order_by('-id')
        return module

class Alocationfilter(ListAPIView):
    queryset = JobAlocationMaster.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
            data=request.data
            data1={"Employee_Name__icontains":data['Employee_Name'],"Day":data['Day'],"Created_Date__range":data['Created_Date'],"Is_Deleted":False}
            return requestget(model=JobAlocationMaster,serializer=JobAlocationMasterSerialzers,data=data1,page=data['page'],Operator='and')

class Alocationsearch(ListAPIView):
    queryset = JobAlocationMaster.objects.all()
    serializer_class = JobAlocationMasterSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get_queryset(self):
        data=self.request.GET.get('search')
        module = JobAlocationMaster.objects.filter(Q(Employee_Name__icontains=data)| Q(Day__icontains=data),Is_Deleted=False).order_by('-id')
        return module


# this is used for roster
# class Rostering(ListAPIView):
#     queryset = JobAlocationMaster.objects.all()
#     authentication_classes = [JSONWebTokenAuthentication]
#     permission_classes = [DjangoModelPermissions]
    
#     def post(self,request,*args,**kwargs):
#         try:
#             data=request.data
#             module = JobAlocationMaster.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Service_Name=data['Service_Name']))
#             serialize = JobAlocationMasterSerialzer(module,many=True)
#             ser=serialize.data
#             output = list(ser)
#             for i in output:
#                 leavemodule=Leave.objects.filter(Q(Employee_Id=i['Employee_Id']) & Q(WeekNo=data['WeekNo']) & Q(Day__icontains=i['Day']))
#                 leaveserialize = LeaveSerialzer(leavemodule,many=True)
#                 lea = leaveserialize.data
#                 if lea !=[]:
#                     leaveoutput =lea[0]
#                     Da=leaveoutput['Day']
#                     if i['Day'] in Da:
#                         if i['Employee_Id']==leaveoutput['Employee_Id'] and i['WeekNo'] == leaveoutput['WeekNo']:
#                             i['leave']="Leave"
#                             Rostermodule = RoasterMasterSerialzer(data=i)
#                             if Rostermodule.is_valid():
#                                 Rostermodule.save()
#                 else:
#                     Rostermodule = RoasterMasterSerialzer(data=i)
#                     i['WeekNo']=data['WeekNo']
#                     if Rostermodule.is_valid():
#                         Rostermodule.save()
#             Rostermodule = RoasterMaster.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Service_Name=data['Service_Name']) & Q(WeekNo=data['WeekNo']))
#             Rosterserialize = RoasterMasterSerialzer(Rostermodule,many=True)
#             output = JSONRenderer().render(Rosterserialize.data)
#             return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
#         except Exception as e:
#             output = JSONRenderer().render({"Error":str(e)})
#             return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Rostering(APIView):    
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            customermodule = subscriptioncustomer.objects.filter(Q(WeekNo=data['WeekNo']) & Q(Partner_Id=data['Partner_Id']))
            subcustomerserialize = subscriptioncustomerSerialzer(customermodule,many=True)
            subserialize = subcustomerserialize.data
            # print(subserialize)
            distinct_locations = set()
            for dat in subserialize:
                location = dat.get('Location')
                if location:
                    distinct_locations.add(location)
            distinct_location_list = list(distinct_locations)
            # print(distinct_location_list)
            slotmodule = Slot.objects.get(id=data['Slot_Id'])
            slotserialize = SlotSerialzer(slotmodule)
            slotdetail=slotserialize.data
            slotlength=len(slotdetail['Slots'])
            slots = {}
            for i in range(1, slotlength + 1):
                slot_name = f"slot{i}"
                slots[slot_name] = []
            # print(slots)
            slot_index = 1
            for i in distinct_location_list:
                # print(i)
                module = subscriptioncustomer.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Location=i) & Q(WeekNo=data['WeekNo'])& Q(Is_Deleted=False))
                # print(module)
                serialize = subscriptioncustomerSerialzer(module,many=True)
                ser=serialize.data
                # print("ok")
                # print(ser)
                empnoleave=[]
                for x in ser:
                    geolocator=Nominatim(user_agent='Erpapp')
                    employee_location = geolocator.geocode(x['Location'])
                    # print(employee_location.latitude)
                    # print(employee_location.longitude)
                    emplist=[]
                    for employee in EmployeeMaster.objects.all():
                        emplong=employee.Longitude
                        emplot=employee.Lotitude
                        distance=haversine_distance(employee_location.latitude,employee_location.longitude,emplong ,emplot)
                        # print(distance)
                        if distance <= data['Radius']:
                            emplist.append(employee)
                        empserialize = EmployeeMasterSerialzer(emplist,many=True)
                    empser = empserialize.data
                    for y in empser:
                        leavemodule=Leave.objects.filter(Q(Employee_Id=y['id']) & Q(WeekNo=data['WeekNo']))
                        leaveserialize = LeaveSerialzer(leavemodule,many=True)
                        lea = leaveserialize.data
                        if lea == []:
                            empnoleave.append(y)
                # print(ser,empnoleave)
                days_values = []
                for record in ser:
                    days = record.get('Days', [])
                    days_values.extend(days)
                # print(days_values)
                day_wise_data = {}
                # Iterate through the records and organize them by day
                for record in days_values:
                    day = record['day']    
                # Check if the day already has a list, if not, create an empty list
                    if day not in day_wise_data:
                        day_wise_data[day] = []
                        # Add the record to the respective day's list
                    day_wise_data[day].append(record)
                    # Convert the day-wise data into a list
                day_wise_list = list(day_wise_data.values())
                # print(day_wise_list)
                finalslot = []
                # allocated_employee_ids = defaultdict(lambda: defaultdict(set))
                day_wise_slots = {}
                for x in day_wise_list:
                    # day=x['day']
                    for n,m in zip(x,empnoleave):
                        day = n['day']
                        data2={"Day":n['day'],"MobileNo":n['MobileNo'],"Customer_Id":n['Customer_Id'],"Customer_Name":n['Customer_Name'],"E.id":m['id'],"Employee_Id":m["Employee_Id"],"Longitude":m["Longitude"],
                            "Lotitude":m["Lotitude"],"Employee_FirstName":m["Employee_FirstName"],"DateOf_Joining":m["DateOf_Joining"],
                            "Designation": "Software Developer","Seating_Location": m['Seating_Location'],"Department":m['Department'],
                            "Email_Id": m['Email_Id'],"Employee_Type":m['Employee_Type'],"Persional_Mail":m['Persional_Mail'],"Employee_Mobile_No": m['Mobile_No'],
                            "Alternative_Contact_No": m['Alternative_Contact_No'],"Blood_Group": m['Blood_Group'],"DateOf_Birth": m['DateOf_Birth'],
                            "Employee_Age": m['Age'],"Reporting_City": m['Reporting_City'],"Employee_Worklocation": m['Worklocation'],
                            "Mother_Tongue": m['Mother_Tongue'],"Employee_Type1": m['Employee_Type1'],"Nationality": m['Nationality'],
                            "Gender": m['Gender'],"Role": m['Role'],"Employee_Partner_Id": m['Partner_Id']}
                        if day not in day_wise_slots:
                            day_wise_slots[day] = {f'slot{i}': [] for i in range(1, slotlength + 1)}
                        slot_index = 1
                        while slot_index <= slotlength:
                            slot_key = f'slot{slot_index}'
                            # Check if Employee_Id is not already in the slot
                            if data2['Employee_Id'] not in [item['Employee_Id'] for item in day_wise_slots[day][slot_key]]:
                                day_wise_slots[day][slot_key].append(data2)
                                break  # Exit the loop if data is added to a slot
                            slot_index += 1
            data['Allocations']=day_wise_slots
            today = datetime.today()
            print(today)
            print(data['WeekNo'])
            current_year = today.year
            first_day_of_week = today - timedelta(days=(today.weekday() - data['WeekNo'] * 7))
            # Extract the month name
            month_name = first_day_of_week.strftime("%B")
            print(month_name)
            data['Month'] = month_name
            # Rostermodule = RoasterMaster.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Service_Name=data['Service_Name']) & Q(WeekNo=data['WeekNo']))
            serialize = RoasterMasterSerialzer(data=request.data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def get(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            module = RoasterMaster.objects.filter(Q(RoasterMaster_Id=data['RoasterMaster_Id'])&Q(WeekNo=data['WeekNo']) & Q(Partner_Id=data['Partner_Id']))
            serialize = RoasterMasterSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            id=data['id']
            module=RoasterMaster.objects.get(id=id)
            serialize = RoasterMasterSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['id']
            module=RoasterMaster.objects.get(id=id)
            serialize = RoasterMasterSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)    
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Rostermassedit(APIView):
    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            Start_Date=date.fromisoformat(data['Start_Date'])
            End_Date = date.fromisoformat(data['End_Date'])
            dates_for_days = [Start_Date + timedelta(days=i) for i in range(7)]
            module = RoasterMaster.objects.filter(Partner_Id=data['Partner_Id'],WeekNo=data['WeekNo'],Start_Date__lte=End_Date,
                    End_Date__gte=Start_Date).values('Allocations')
            print(module)
            day_wise_allocations = defaultdict(list)
            for roaster in module:
                allocations = roaster['Allocations']
                for day, slots in allocations.items():
                    # Convert 'day' to integer if it's not already
                    day = int(day)
                    if day in range(7) and dates_for_days[day] >= Start_Date and dates_for_days[day] <= End_Date:
                        for slot_name, slot_values in slots.items():
                        #     day_wise_allocations[day] = []
                            day_wise_allocations[day].extend(slot_values)
            day_wise_allocations = dict(day_wise_allocations)
            output = JSONRenderer().render(day_wise_allocations)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)



class Masseditroster(APIView):
    def put(self, request, *args, **kwargs):
        try:
            data = request.data
            Start_Date = date.fromisoformat(data['Start_Date'])
            End_Date = date.fromisoformat(data['End_Date'])
            Partner_Id = data['Partner_Id']
            WeekNo = data['WeekNo']  # Get the specific RoasterMaster record within the date range
            roaster = RoasterMaster.objects.filter(
                Partner_Id=Partner_Id,
                WeekNo=WeekNo,
                Start_Date__lte=End_Date,
                End_Date__gte=Start_Date
            ).first()
            if roaster:
                allocations = roaster.Allocations or {}
                # Iterate over the specified date range
                for day in range(7):
                    current_date = Start_Date + timedelta(days=day)
                    # Check if the current date falls within the specified range
                    if Start_Date <= current_date <= End_Date:
                        # Define the data to be updated for the current day
                        day_str = str(day)
                        employee_id = data['Employee_Id']  # Replace with your desired Employee_Id
                        employee_name = data['Employee_FirstName']  # Replace with your desired Employee_Name
                        new_entry = {
                            "Day": day,
                            "E.id": employee_id,
                            "Employee_FirstName": employee_name,
                        }
                        if day_str in allocations:
                            # If 'slot1' exists for the day, check if the new entry already exists
                            if "slot1" in allocations[day_str]:
                                existing_data = allocations[day_str]["slot1"]
                                data4=[]
                                for z in existing_data:
                                    if z['Employee_Id'] == data['Employee_Id1']:
                                        rep={'Day': z['Day'], 'E.id':z['E.id'], 'Role': z['Role'],
                                         'Gender': z['Gender'], 'Email_Id': z['Email_Id'], 
                                         'Lotitude': z['Lotitude'], 'MobileNo': z['MobileNo'], 'Longitude': z['Longitude'], 
                                         'Department':z['Department'], 'Blood_Group': z['Blood_Group'], 'Customer_Id': z['Customer_Id'], 
                                         'Designation': z['Designation'],"Employee_Id": new_entry['E.id'],
                                         'Nationality': z['Nationality'], 'DateOf_Birth': z['DateOf_Birth'], 'Employee_Age': z['Employee_Age'], 
                                         'Customer_Name':z['Customer_Name'], 'Employee_Type': z['Employee_Type'], 'Mother_Tongue':z['Mother_Tongue'],
                                          'DateOf_Joining': z['DateOf_Joining'], 'Employee_Type1': z['Employee_Type1'], 
                                          'Persional_Mail':z['Persional_Mail'], 'Reporting_City': z['Reporting_City'], 
                                          'Seating_Location':z['Seating_Location'], 'Employee_FirstName':new_entry['Employee_FirstName'], 
                                          'Employee_Mobile_No':z['Employee_Mobile_No'], 'Employee_Partner_Id': z['Employee_Partner_Id'], 
                                          'Employee_Worklocation': z['Employee_Worklocation'], 'Alternative_Contact_No':z['Alternative_Contact_No']}
                                        data4.append(rep)
                                    else:
                                        data4.append(z)
                                # Check if the new entry is not already in the existing data
                                # if new_entry not in existing_data:
                                #     existing_data.append(new_entry)  # Append the new entry
                                allocations[day_str]["slot1"] = data4
                            else:
                                allocations[day_str]["slot1"] = [data4]
                        else:
                            # If the day doesn't exist in allocations, create it with the updated slot1
                            allocations[day_str] = {"slot1": [data4]}
                # Only update the Allocations field, leaving other fields untouched
                roaster.Allocations = allocations
                roaster.save()
                # Return the modified RoasterMaster instance as JSON
                response_data = {
                    "id": roaster.id,
                    "RoasterMaster_Id": roaster.RoasterMaster_Id,
                    "Partner_Name": roaster.Partner_Name,
                    "WeekNo": roaster.WeekNo,
                    "Start_Date": str(roaster.Start_Date),
                    "End_Date": str(roaster.End_Date),
                    "Roster_Name": roaster.Roster_Name,
                    "Radius": roaster.Radius,
                    "Day": roaster.Day,
                    "Location": roaster.Location,
                    "Service_Name": roaster.Service_Name,
                    "Allocations": allocations,
                    "leave": roaster.leave,
                    "Is_Deleted": roaster.Is_Deleted,
                    "Created_Date": str(roaster.Created_Date),
                    "Updated_Date": str(roaster.Updated_Date),
                    "Partner_Id": data['Partner_Id']
                }
                output = JSONRenderer().render(response_data)
                return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
            else:
                return HttpResponse("Roaster entry not found", content_type='text/plain', status=status.HTTP_404_NOT_FOUND)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)



class RostermasterCRUD(ListAPIView):
    queryset = RoasterMaster.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]  

    def post(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            serialize = RoasterMaster(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['id']
            module = RoasterMaster.objects.get(id=id)
            serialize = RoasterMasterSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            id=data['id']
            module=RoasterMaster.objects.get(id=id)
            serialize = RoasterMasterSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['id']
            module=RoasterMaster.objects.get(id=id)
            serialize = RoasterMasterSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)    
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class MyRoasterMasterlist(ListAPIView):
    queryset = RoasterMaster.objects.all()
    serializer_class = RoasterMasterSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        Created_By=self.request.GET.get('Created_By')
        Partner_Id=self.request.GET.get('Partner_Id')
        module = RoasterMaster.objects.filter(Q(Created_By=Created_By) & Q(Is_Deleted=False) & Q(Partner_Id=Partner_Id)).order_by('-id')
        return module



class RoasterMasterlist(ListAPIView):
    queryset = RoasterMaster.objects.all()
    serializer_class = RoasterMasterSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        Partner_Id=self.request.GET.get('Partner_Id')
        module = RoasterMaster.objects.filter(Q(Partner_Id=Partner_Id)& Q(Is_Deleted=False)).order_by('-id')
        return module


class RoasterMasterfilter(ListAPIView):
    queryset = RoasterMaster.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
            data=request.data
            data1={"Partner_Name__icontains":data['Partner_Name'],"Service_Name":data['Service_Name'],"Start_Date__range":data['Start_Date'],"Is_Deleted":False}
            return requestget(model=RoasterMaster,serializer=RoasterMasterSerialzers,data=data1,page=data['page'],Operator='and')


class Roasterfilter(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = RoasterMaster.objects.filter(Q(WeekNo=data['WeekNo']) & Q(Day=data['Day']))
            serialize = RoasterMasterSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)


class RoasterMastersearch(ListAPIView):
    queryset = RoasterMaster.objects.all()
    serializer_class = RoasterMasterSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get_queryset(self):
        data=self.request.GET.get('search')
        module = RoasterMaster.objects.filter(Q(Partner_Name__icontains=data)| Q(Service_Name__icontains=data) | Q(Roster_Name__icontains=data) | Q(RoasterMaster_Id__icontains=data) | Q(WeekNo__icontains=data),Is_Deleted=False).order_by('-id')
        return module    

class JobtimecardCRUD(ListAPIView):
    queryset = Jobtimecard.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            data['Year'] = data['Date'].strftime("%Y")
            data['Month'] = data['Date'].strftime("%B")
            data['Jobcompleted_Flg']=True
            serialize = JobtimecardSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['Jobtimecard_Id']
            module = Jobtimecard.objects.get(Jobtimecard_Id=id)
            serialize = JobtimecardSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            id=data['Jobtimecard_Id']
            module=Jobtimecard.objects.get(Jobtimecard_Id=id)
            serialize = JobtimecardSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['Jobtimecard_Id']
            module=Jobtimecard.objects.get(Jobtimecard_Id=id)
            serialize = JobtimecardSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)    
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Myjobtimecardlist(ListAPIView):
    queryset = Jobtimecard.objects.all()
    serializer_class = JobtimecardSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        Created_By=self.request.GET.get('Created_By')
        module = Jobtimecard.objects.filter(Q(Created_By=Created_By) & Q(Is_Deleted=False)).order_by('-id')
        return module



class Jobtimecardlist(ListAPIView):
    queryset = Jobtimecard.objects.all()
    serializer_class = JobtimecardSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        module = Jobtimecard.objects.filter(Is_Deleted=False).order_by('-id')
        return module

class Jobtimecardfilter(ListAPIView):
    queryset = Jobtimecard.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
            data=request.data
            data1={"Employee_Name__icontains":data['Employee_Name'],"Day":data['Day'],"Created_Date__range":data['Created_Date'],"Is_Deleted":False}
            return requestget(model=Jobtimecard,serializer=JobtimecardSerialzers,data=data1,page=data['page'],Operator='and')

class Jobtimecardsearch(ListAPIView):
    queryset = Jobtimecard.objects.all()
    serializer_class = JobtimecardSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get_queryset(self):
        data=self.request.GET.get('search')
        module = Jobtimecard.objects.filter(Q(Employee_Name__icontains=data)| Q(Day__icontains=data),Is_Deleted=False).order_by('-id')
        return module




class DropdowntableCRUD(ListAPIView):
    queryset = Dropdowntable.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            serialize = DropdowntableSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['Dropdown_Id']
            module = Dropdowntable.objects.get(Dropdown_Id=id)
            serialize = DropdowntableSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            id=data['Dropdown_Id']
            module=Dropdowntable.objects.get(Dropdown_Id=id)
            serialize = DropdowntableSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['Dropdown_Id']
            module=Dropdowntable.objects.get(Dropdown_Id=id)
            serialize = DropdowntableSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)    
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Dropdwontablemylist(ListAPIView):
    queryset = Dropdowntable.objects.all()
    serializer_class = DropdowntableSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get_queryset(self):
        Created_By = self.request.GET.get('Created_By')
        module = Dropdowntable.objects.filter(Q(Created_By=Created_By) & Q(Is_Deleted=False)).order_by('-id')
        return module


class Dropdwontablelist(ListAPIView):
    queryset = Dropdowntable.objects.all()
    serializer_class = DropdowntableSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get_queryset(self):
        module = Dropdowntable.objects.filter(Is_Deleted=False).order_by('-id')
        return module


class Dropdwontablesearch(ListAPIView):
    queryset = Dropdowntable.objects.all()
    serializer_class = DropdowntableSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

# filter api
    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            module = Dropdowntable.objects.filter(
                Lookupname__icontains=data['Lookupname'], Is_Deleted=False)
            serialize = DropdowntableSerialzer(module, many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)
            

    # search api
    def put(self, request, *args, **kwargs):
        try:
            data = request.data
            module = Dropdowntable.objects.filter(Q(Lookupname__icontains=data['Search']) | Q(
                Dropdown_Id__icontains=data['Search']) | Q(
                value__icontains=data['Search']), Q(Is_Deleted=False))
            serialize = DropdowntableSerialzer(module, many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)

class StoremasterCRUD(ListAPIView):
    queryset = Storemaster.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            serialize = StoremasterSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['Store_Id']
            module = Storemaster.objects.get(Store_Id=id)
            serialize = StoremasterSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            id=data['Store_Id']
            module=Storemaster.objects.get(Store_Id=id)
            serialize = StoremasterSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['Store_Id']
            module=Storemaster.objects.get(Store_Id=id)
            serialize = StoremasterSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)    
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class Storelist(ListAPIView):
    queryset = Storemaster.objects.all()
    serializer_class = StoremasterSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get_queryset(self):
        module = Storemaster.objects.filter(Is_Deleted=False).order_by('-id')
        return module

# search api for backend purpose

    def put(self, request, *args, **kwargs):
        try:
            data = request.data
            module = Storemaster.objects.filter(Q(Store_Name__icontains=data['Search']) |Q(Partner_Id__Partner_Name__icontains=data['Search'])| Q(
                Own_Partner__icontains=data['Search'])|Q(Store_Id__icontains=data['Search'])|Q(Store_City__icontains=data['Search'])|Q(Store_Incharge__icontains=data['Search']), Q(Is_Deleted=False))
            serialize = StoremasterSerialzer(module, many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)


class Storelistfilter(ListAPIView):
    queryset = Storemaster.objects.all()
    serializer_class = StoremasterSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get_queryset(self):
        Store_City=self.request.GET.get('Store_City')
        module = Storemaster.objects.filter(Q(Store_City=Store_City) & Q(Is_Deleted=False)).order_by('-id')
        return module

class storeitemavailableqty(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data = request.query_params
            module = Storemaster.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Items__contains=[{"Item_Name":data['Item_Name']}]))
            serialize = StoremasterSerialzer(module,many=True)
            output = serialize.data
            print(output)
            output1 =[]
            for i in output:
                for y in i['Items']:
                    if y['Item_Name']==data['Item_Name']:
                        output1.append({"Item_Name":y['Item_Name'],"Quantity":y['Opening_Stock'],"Store_Name":i['Store_Name'],"Location":i['Store_City']})
            output2=JSONRenderer().render(output1)
            return HttpResponse(output2,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output, content_type = 'application/json', status=status.HTTP_400_BAD_REQUEST)



class CostcenterCRUD(ListAPIView):
    queryset = Costcenter.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            serialize = CostcenterSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['CostCenter_Id']
            module = Costcenter.objects.get(CostCenter_Id=id)
            serialize = CostcenterSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            id=data['CostCenter_Id']
            module=Costcenter.objects.get(CostCenter_Id=id)
            serialize = CostcenterSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['CostCenter_Id']
            module=Costcenter.objects.get(CostCenter_Id=id)
            serialize = CostcenterSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)    
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class Costcenterlist(ListAPIView):
    queryset = Costcenter.objects.all()
    serializer_class = CostcenterSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get_queryset(self):
        module = Costcenter.objects.filter(Is_Deleted=False).order_by('-id')
        return module

class Costcentersearch(ListAPIView):
    queryset = Costcenter.objects.all()
    serializer_class = CostcenterSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def put(self, request, *args, **kwargs):
        try:
            data = request.data
            module = Costcenter.objects.filter(
                Q(Costcenter_Name__icontains=data['Search']), Q(Is_Deleted=False))
            serialize = CostcenterSerialzer(module, many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)



#Sales Report API's

class Totalsales(ListAPIView):
    queryset = SalesOrder.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Company_Id=data['Company_Id']
            Recurring_FromDate=data['Recurring_FromDate']
            module = SalesOrder.objects.filter(Q(Company_Id=Company_Id) & Q(Recurring_FromDate__range=Recurring_FromDate) & Q(Order_Cancelled=False)& Q(Is_Deleted=False))
            Total=module.aggregate(Sum('Amount_Paid'))['Amount_Paid__sum']
            return HttpResponse(Total,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Onetimetotalsale(ListAPIView):
    queryset = SalesOrder.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Partner_Id=data['Partner_Id']
            Recurring_FromDate=data['Recurring_FromDate']
            Onetime = 0
            Subscription = 0
            module = SalesOrder.objects.filter(Q(Partner_Id=Partner_Id) & Q(Recurring_FromDate__range=Recurring_FromDate) & Q(Order_Cancelled=False)& Q(Is_Deleted=False))
            serialize = SalesOrderSerialzer(module,many=True)
            outputserialize = serialize.data
            for i in outputserialize:
                if i['SalesOrder_Type'] == 'OneTime':
                    Onetime=Onetime + i['Amount_Paid']
                else:
                    Subscription = Subscription + i['Amount_Paid']
            Total={"Onetime":Onetime,"Subscription":Subscription}
            output = JSONRenderer().render(Total)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class Salesreports(ListAPIView):
    queryset = SalesOrder.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Company_Id=data['Company_Id']
            Recurring_FromDate=data['Recurring_FromDate']
            SalesOrder_Type = data['SalesOrder_Type']
            module = SalesOrder.objects.filter(Q(Company_Id=Company_Id) & Q(Recurring_FromDate__range=Recurring_FromDate) & Q(SalesOrder_Type=SalesOrder_Type) & Q(Is_Deleted=False))
            Total=module.aggregate(Sum('Amount_Paid'))['Amount_Paid__sum']
            return HttpResponse(Total,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Salespersontarget(ListAPIView):
    queryset = Monthlyusertarget.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Monthlyusertarget.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Service_Type=data['Service_Type']) & Q(Is_Deleted=False))
            serialize = MonthlyusertargetSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_100_CONTINUE)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class salestargetcountry(ListAPIView):
    queryset = SalesOrder.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Company_Id=data['Company_Id']
            Recurring_FromDate=data['Recurring_FromDate']
            SalesOrder_Type = data['SalesOrder_Type']
            # Building_Country=data['Building_Country']
            module = SalesOrder.objects.filter(Q(Company_Id=Company_Id) & Q(Recurring_FromDate__range=Recurring_FromDate) & Q(SalesOrder_Type=SalesOrder_Type) & Q(Order_Cancelled=False)& Q(Is_Deleted=False))
            serialize = SalesOrderSerialzer(module,many=True)
            outputserialize = serialize.data
            result=[]
            output={}
            for i in outputserialize:
                partnermodule = Partner.objects.get(id=i['Partner_Id'])
                partnerserislize = partnermodule.BusinessUnit_Country
                data1={"Country":partnerserislize,"Amount":i['Amount_Paid']}
                result.append(data1)
            for item in result:
                country=item['Country']
                Amount=item['Amount']
                if country in output:
                    output[country] +=Amount
                else:
                    output[country]= Amount
            output1 = JSONRenderer().render(output)
            return HttpResponse(output1,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class salestargetstate(ListAPIView):
    queryset = SalesOrder.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Company_Id=data['Company_Id']
            Recurring_FromDate=data['Recurring_FromDate']
            SalesOrder_Type = data['SalesOrder_Type']
            Country = data['Country']
            module = SalesOrder.objects.filter(Q(Company_Id=Company_Id) & Q(Recurring_FromDate__range=Recurring_FromDate) & Q(SalesOrder_Type=SalesOrder_Type) & Q(Order_Cancelled=False)& Q(Is_Deleted=False))
            serialize = SalesOrderSerialzer(module,many=True)
            outputserialize = serialize.data
            result=[]
            output={}
            for i in outputserialize:
                partnermodule = Partner.objects.get(id=i['Partner_Id'])
                country = partnermodule.BusinessUnit_Country
                state = partnermodule.BusinessUnit_State
                if country == Country:
                    data1={"State":state,"Amount":i['Amount_Paid']}
                result.append(data1)
            for item in result:
                State=item['State']
                Amount=item['Amount']
                if State in output:
                    output[State] +=Amount
                else:
                    output[State]= Amount
            output1 = JSONRenderer().render(output)
            return HttpResponse(output1,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class salestargetcity(ListAPIView):
    queryset = SalesOrder.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Company_Id=data['Company_Id']
            Recurring_FromDate=data['Recurring_FromDate']
            SalesOrder_Type = data['SalesOrder_Type']
            Country = data['Country']
            State = data['State']
            module = SalesOrder.objects.filter(Q(Company_Id=Company_Id) & Q(Recurring_FromDate__range=Recurring_FromDate) & Q(SalesOrder_Type=SalesOrder_Type) & Q(Order_Cancelled=False)& Q(Is_Deleted=False))
            serialize = SalesOrderSerialzer(module,many=True)
            outputserialize = serialize.data
            result=[]
            output={}
            for i in outputserialize:
                partnermodule = Partner.objects.get(id=i['Partner_Id'])
                country = partnermodule.BusinessUnit_Country
                state = partnermodule.BusinessUnit_State
                city = partnermodule.BusinessUnit_City
                if country == Country and state == State:
                    data1={"City":city,"Amount":i['Amount_Paid']}
                    result.append(data1)
            for item in result:
                City=item['City']
                Amount=item['Amount']
                if City in output:
                    output[City] +=Amount
                else:
                    output[City]= Amount
            output1 = JSONRenderer().render(output)
            return HttpResponse(output1,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class salestargetzone(ListAPIView):
    queryset = SalesOrder.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Company_Id=data['Company_Id']
            Recurring_FromDate=data['Recurring_FromDate']
            SalesOrder_Type = data['SalesOrder_Type']
            Country = data['Country']
            State = data['State']
            City = data['City']
            module = SalesOrder.objects.filter(Q(Company_Id=Company_Id) & Q(Recurring_FromDate__range=Recurring_FromDate) & Q(SalesOrder_Type=SalesOrder_Type) & Q(Order_Cancelled=False)& Q(Is_Deleted=False))
            serialize = SalesOrderSerialzer(module,many=True)
            outputserialize = serialize.data
            result=[]
            output={}
            for i in outputserialize:
                partnermodule = Partner.objects.get(id=i['Partner_Id'])
                country = partnermodule.BusinessUnit_Country
                state = partnermodule.BusinessUnit_State
                city = partnermodule.BusinessUnit_City
                zone = partnermodule.BusinessUnit_Zone
                if country == Country and state == State and city == City:
                    data1={"Zone":zone,"Amount":i['Amount_Paid']}
                    result.append(data1)
            for item in result:
                Zone=item['Zone']
                Amount=item['Amount']
                if Zone in output:
                    output[Zone] +=Amount
                else:
                    output[Zone]= Amount
            output1 = JSONRenderer().render(output)
            return HttpResponse(output1,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class salestargetmonth(ListAPIView):
    queryset = SalesOrder.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Company_Id=data['Company_Id']
            Recurring_FromDate=data['Recurring_FromDate']
            SalesOrder_Type = data['SalesOrder_Type']
            Country = data['Country']
            State = data['State']
            City = data['City']
            Zone = data['Zone']
            module = SalesOrder.objects.filter(Q(Company_Id=Company_Id) & Q(Recurring_FromDate__range=Recurring_FromDate) & Q(SalesOrder_Type=SalesOrder_Type) & Q(Order_Cancelled=False)& Q(Is_Deleted=False))
            serialize = SalesOrderSerialzer(module,many=True)
            outputserialize = serialize.data
            result=[]
            output={}
            for i in outputserialize:
                partnermodule = Partner.objects.get(id=i['Partner_Id'])
                country = partnermodule.BusinessUnit_Country
                state = partnermodule.BusinessUnit_State
                city = partnermodule.BusinessUnit_City
                zone = partnermodule.BusinessUnit_Zone
                if country == Country and state == State and city == City and zone == Zone:
                    datestring = i['SalesOrderDate']
                    date_object = datetime.strptime(datestring, "%Y-%m-%d")
                    month_name = date_object.strftime("%B")
                    data1={"Month":month_name,"Amount":i['Amount_Paid']}
                    result.append(data1)
            for item in result:
                Month=item['Month']
                Amount=item['Amount']
                if Month in output:
                    output[Month] +=Amount
                else:
                    output[Month]= Amount
            output1 = JSONRenderer().render(output)
            return HttpResponse(output1,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class salestargetdate(ListAPIView):
    queryset = SalesOrder.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Company_Id=data['Company_Id']
            Recurring_FromDate=data['Recurring_FromDate']
            SalesOrder_Type = data['SalesOrder_Type']
            Country = data['Country']
            State = data['State']
            City = data['City']
            Zone = data['Zone']
            Month = data['Month']
            module = SalesOrder.objects.filter(Q(Company_Id=Company_Id) & Q(Recurring_FromDate__range=Recurring_FromDate) & Q(SalesOrder_Type=SalesOrder_Type) & Q(Order_Cancelled=False)& Q(Is_Deleted=False))
            serialize = SalesOrderSerialzer(module,many=True)
            outputserialize = serialize.data
            result=[]
            output={}
            for i in outputserialize:
                partnermodule = Partner.objects.get(id=i['Partner_Id'])
                country = partnermodule.BusinessUnit_Country
                state = partnermodule.BusinessUnit_State
                city = partnermodule.BusinessUnit_City
                zone = partnermodule.BusinessUnit_Zone
                datestring = i['SalesOrderDate']
                date_object = datetime.strptime(datestring, "%Y-%m-%d")
                month_name = date_object.strftime("%B")
                if country == Country and state == State and city == City and zone == Zone and month_name == Month:
                    data1={"Date":datestring,"Amount":i['Amount_Paid']}
                    result.append(data1)
            for item in result:
                Date=item['Date']
                Amount=item['Amount']
                if Date in output:
                    output[Date] +=Amount
                else:
                    output[Date]= Amount
            output1 = JSONRenderer().render(output)
            return HttpResponse(output1,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class salestargetcustomer(ListAPIView):
    queryset = SalesOrder.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Company_Id=data['Company_Id']
            Recurring_FromDate=data['Recurring_FromDate']
            SalesOrder_Type = data['SalesOrder_Type']
            Country = data['Country']
            State = data['State']
            City = data['City']
            Zone = data['Zone']
            Month = data['Month']
            Date = data['Date']
            module = SalesOrder.objects.filter(Q(Company_Id=Company_Id) & Q(Recurring_FromDate__range=Recurring_FromDate) & Q(SalesOrder_Type=SalesOrder_Type) & Q(Order_Cancelled=False)& Q(Is_Deleted=False))
            serialize = SalesOrderSerialzer(module,many=True)
            outputserialize = serialize.data
            result=[]
            output={}
            for i in outputserialize:
                partnermodule = Partner.objects.get(id=i['Partner_Id'])
                country = partnermodule.BusinessUnit_Country
                state = partnermodule.BusinessUnit_State
                city = partnermodule.BusinessUnit_City
                zone = partnermodule.BusinessUnit_Zone
                datestring = i['SalesOrderDate']
                date_object = datetime.strptime(datestring, "%Y-%m-%d")
                month_name = date_object.strftime("%B")
                if country == Country and state == State and city == City and zone == Zone and month_name == Month and datestring== Date:
                    customermodule=Customer.objects.get(id=i['Customer_Id'])
                    Customername = customermodule.Cus_FirstName
                    data1={"Customername":Customername,"Amount":i['Amount_Paid']}
                    result.append(data1)
            # for item in result:
            #     Date=item['Date']
            #     Amount=item['Amount']
            #     if Date in output:
            #         output[Date] +=Amount
            #     else:
            #         output[Date]= Amount
            output1 = JSONRenderer().render(result)
            return HttpResponse(output1,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class Totalpurchase(ListAPIView):
    queryset = PODetails.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Company_Id=data['Company_Id']
            Po_Date=data['Po_Date']
            module = PODetails.objects.filter(Q(Company_Id=Company_Id) & Q(Po_Date__range=Po_Date) & Q(POCancel_Flag=False)& Q(Is_Deleted=False))
            Total=module.aggregate(Sum('Total_POAmount'))['Total_POAmount__sum']
            return HttpResponse(Total,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class Totalpurchasesuplier(ListAPIView):
    queryset = PODetails.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Company_Id=data['Company_Id']
            Po_Date=data['Po_Date']
            module = PODetails.objects.filter(Q(Company_Id=Company_Id) & Q(Po_Date__range=Po_Date) & Q(POCancel_Flag=False)& Q(Is_Deleted=False))
            serialize = PODetailsSerialzer(module,many=True)
            Supliers=serialize.data
            result = []
            res = {}
            for i in Supliers:
                supliermodule = Supplier.objects.get(id=i['Supplier_Id'])
                Supliername = supliermodule.Company_Name
                data1={"SupplierName":Supliername,"POamount":i['Total_POAmount']}
                result.append(data1)
            for item in result:
                SupplierName=item['SupplierName']
                POamount = item['POamount']
                if SupplierName in res:
                    res[SupplierName] += POamount
                else:
                    res[SupplierName] = POamount
            Total=JSONRenderer().render(res)
            return HttpResponse(Total,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Totalpurchasemonth(ListAPIView):
    queryset = PODetails.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Company_Id=data['Company_Id']
            Po_Date=data['Po_Date']
            SupplierName = data['SupplierName']
            module = PODetails.objects.filter(Q(Company_Id=Company_Id) & Q(Po_Date__range=Po_Date) & Q(POCancel_Flag=False)& Q(Is_Deleted=False))
            serialize = PODetailsSerialzer(module,many=True)
            Supliers=serialize.data
            result=[]
            output={}
            for i in Supliers:
                supliermodule = Supplier.objects.get(id=i['Supplier_Id'])
                Supliername = supliermodule.Company_Name
                if SupplierName == Supliername:
                    datestring = i['Po_Date']
                    date_object = datetime.strptime(datestring, "%Y-%m-%d")
                    month_name = date_object.strftime("%B")
                    data1={"Month":month_name,"Amount":i['Total_POAmount']}
                    result.append(data1)
            for item in result:
                Month=item['Month']
                Amount=item['Amount']
                if Month in output:
                    output[Month] +=Amount
                else:
                    output[Month]= Amount
            output1 = JSONRenderer().render(output)
            return HttpResponse(output1,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Totalpurchasdate(ListAPIView):
    queryset = PODetails.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Company_Id=data['Company_Id']
            Po_Date=data['Po_Date']
            SupplierName = data['SupplierName']
            Month = data['Month']
            module = PODetails.objects.filter(Q(Company_Id=Company_Id) & Q(Po_Date__range=Po_Date) & Q(POCancel_Flag=False)& Q(Is_Deleted=False))
            serialize = PODetailsSerialzer(module,many=True)
            Supliers=serialize.data
            result=[]
            output={}
            for i in Supliers:
                supliermodule = Supplier.objects.get(id=i['Supplier_Id'])
                Supliername = supliermodule.Company_Name
                datestring = i['Po_Date']
                date_object = datetime.strptime(datestring, "%Y-%m-%d")
                month_name = date_object.strftime("%B")
                if SupplierName == Supliername and Month == month_name:
                    data1={"Date":datestring,"Amount":i['Total_POAmount']}
                    result.append(data1)
            for item in result:
                Date=item['Date']
                Amount=item['Amount']
                if Date in output:
                    output[Date] +=Amount
                else:
                    output[Date]= Amount
            output1 = JSONRenderer().render(output)
            return HttpResponse(output1,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class Totalpurchasponumber(ListAPIView):
    queryset = PODetails.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Company_Id=data['Company_Id']
            Po_Date=data['Po_Date']
            SupplierName = data['SupplierName']
            Month = data['Month']
            Date = data['Date']
            module = PODetails.objects.filter(Q(Company_Id=Company_Id) & Q(Po_Date__range=Po_Date) & Q(POCancel_Flag=False)& Q(Is_Deleted=False))
            serialize = PODetailsSerialzer(module,many=True)
            Supliers=serialize.data
            result=[]
            output={}
            for i in Supliers:
                supliermodule = Supplier.objects.get(id=i['Supplier_Id'])
                Supliername = supliermodule.Company_Name
                datestring = i['Po_Date']
                date_object = datetime.strptime(datestring, "%Y-%m-%d")
                month_name = date_object.strftime("%B")
                if SupplierName == Supliername and Month == month_name and Date == datestring:
                    data1={"PO_Id":i['PO_Id'],"Amount":i['Total_POAmount']}
                    result.append(data1)
            for item in result:
                PO_Id=item['PO_Id']
                Amount=item['Amount']
                if PO_Id in output:
                    output[PO_Id] +=Amount
                else:
                    output[PO_Id]= Amount
            output1 = JSONRenderer().render(output)
            return HttpResponse(output1,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class Totalinventoryvalue(APIView):
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Company_Id=data['Company_Id']
            Period = data['Period']
            module = MaterialIssueDetail.objects.filter(Q(Company_Id=Company_Id) & Q(Issue_Date__range=Period) & Q(Quantity_Issued=True) & Q(Is_Deleted=False))
            serialize = MaterialIssueDetailSerialzer(module,many=True)
            material = serialize.data
            materialissue = {}
            materialpurchase = {}
            result1 =[]
            for i in material:
                for y in i['Item']:
                    data1={"id":y['id'],"Qty":y['issue_quantity']}
                    result1.append(data1)
            for item in result1:
                id=item['id']
                qty=item['Qty']
                if id in materialissue:
                    materialissue[id]['id']=id
                    materialissue[id]['qty'] += qty
                else:
                    materialissue[id]={"qty":qty}
            response1=[]
            for id, values in materialissue.items():
                data3={
                    "id":id,"Quantity":values['qty']
                }
                response1.append(data3)
            purchasemodule = PODetails.objects.filter(Q(Company_Id=Company_Id) & Q(Po_Date__range=Period)& Q(POCancel_Flag=False) & Q(Is_Deleted=False))
            purchaseserialize = PODetailsSerialzer(purchasemodule,many=True)
            purchasemat = purchaseserialize.data
            result2=[]
            for i in purchasemat:
                for y in i['Items']:
                    data2={"id":y['id'],"Qty":y['quantity'],"Amount":y['Amount']}
                    result2.append(data2)
            for item in result2:
                id=item['id']
                qty = item['Qty']
                Amount = item['Amount']
                if id in materialpurchase:
                    materialpurchase[id]['id']=id
                    materialpurchase[id]['Qty'] +=qty
                    materialpurchase[id]['Amount'] +=Amount
                else:
                    materialpurchase[id]={"qty":qty}
                    materialpurchase[id]['Amount'] =Amount
            response2 = []
            for id, values in materialpurchase.items():
                data3={
                    "id":id,"Quantity":values['qty'],"Amount":values['Amount']
                }
                response2.append(data3)
            response3=[]
            for res1, res2 in zip(response1, response2):
                if res1['id'] == res2['id']:
                    quantity=res2['Quantity']- res1['Quantity']
                    data3={"id":res2['id'],"qty":quantity,"Amount":res2['Amount']}
                    response3.append(data3)
                else:
                    print("Values are not equal.")
            response4 = []
            response5 = []
            for i in response3:
                module = ItemMaster.objects.get(id=i['id'])
                serialize = ItemMasterSerialzer(module)
                itemoutput = serialize.data
                data4={"id":itemoutput['id'],"Opening_Stock":itemoutput['Opening_Stock'],"Openingprice":itemoutput['Opening_Stock']*itemoutput['Unit_Price']}
                response4.append(data4)
            for res3 , res4 in zip(response3 , response4):
                if res3['id']== res4['id']:
                    Price = res3['Amount'] + res4['Openingprice']
                    Quantity = res3['qty'] + res4['Opening_Stock']
                    War = Price // Quantity
                    data5={"id":res3['id'],"War":War}
                response5.append(data5)
            response6 = []
            for res5,res6 in zip(response3,response5):
                if res5['id'] == res6['id']:
                    inventry=res5['qty'] * res6['War']
                response6.append(inventry)
            Totalinventery={"Totalinventery":sum(response6)}
            output1 = JSONRenderer().render(Totalinventery)
            return HttpResponse(output1,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class Totalinventorystore(APIView):
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Company_Id=data['Company_Id']
            Period = data['Period']
            module = MaterialIssueDetail.objects.filter(Q(Company_Id=Company_Id) & Q(Issue_Date__range=Period) & Q(Quantity_Issued=True) & Q(Is_Deleted=False))
            serialize = MaterialIssueDetailSerialzer(module,many=True)
            material = serialize.data
            result1 =[]
            for i in material:
                for y in i['Item']:
                    data1={"id":y['id'],"Qty":y['issue_quantity'],"store":i['Store_Id'],"Amount":y['Amount']}
                    result1.append(data1)
            purchasemodule = GoodsTrfReceiptDetails.objects.filter(Q(Company_Id=Company_Id) & Q(Trfin_Date__range=Period)& Q(Is_Deleted=False))
            purchaseserialize = GoodsTrfReceiptDetailsSerialzer(purchasemodule,many=True)
            purchasemat = purchaseserialize.data
            result2=[]
            for i in purchasemat:
                for y in i['Item']:
                    data2={"id":y['id'],"Qty":y['Received_quantity'],"Amount":y['Amount'],"store":i['Store_Id']}
                    result2.append(data2)
            response3=[]
            for res1, res2 in zip(result1, result2):
                if  res1['store'] == res2['store'] and res1['id'] == res2['id']:
                    quantity=res2['Qty']- res1['Qty']
                    data3={"id":res2['id'],"Qty":quantity,"Amount":res2['Amount'],"store":res2['store']}
                    response3.append(data3)
                else:
                    response3.append(res2)
                    response3.append(res1)
            response4 = []
            response5 = []
            for i in response3:
                module = ItemMaster.objects.get(id=i['id'])
                serialize = ItemMasterSerialzer(module)
                itemoutput = serialize.data
                Openingprice=itemoutput['Opening_Stock']*itemoutput['Unit_Price']
                warvalue=i['Amount'] + Openingprice
                warquantity =itemoutput['Opening_Stock'] + i['Qty']
                War = warvalue // warquantity
                data4={"id":itemoutput['id'],"War":War,"store":i['store']}
                response4.append(data4)
            for res3 , res4 in zip(response3 , response4):
                if res3['id']== res4['id'] and res3['store'] ==res4['store']:
                    Price = res3['Qty'] * res4['War']
                    data5={"War":Price,"store":res3['store']}
                    response5.append(data5)
            response6={}
            response7=[]
            for item in response5:
                store =item['store']
                War = item['War']
                if store in response6:
                    response6[store]['store']=store
                    response6[store]['War'] +=War
                else:
                    response6[store]={"store":store,"War":War}
            for store,values in response6.items():
                data6 ={"Store":store,"Amount":values['War']}
                response7.append(data6)
            output1 = JSONRenderer().render(response7)
            return HttpResponse(output1,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Totalinventorymonth(APIView):
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Company_Id=data['Company_Id']
            Period = data['Period']
            Store_Id=data['Store_Id']
            module = MaterialIssueDetail.objects.filter(Q(Company_Id=Company_Id) & Q(Issue_Date__range=Period) & Q(Quantity_Issued=True) & Q(Is_Deleted=False))
            serialize = MaterialIssueDetailSerialzer(module,many=True)
            material = serialize.data
            result1 =[]
            for i in material:
                if i['Store_Id'] == Store_Id:
                    for y in i['Item']:
                        datestring = i['Issue_Date']
                        date_object = datetime.strptime(datestring, "%Y-%m-%d")
                        month_name = date_object.strftime("%B")
                        data1={"id":y['id'],"Qty":y['issue_quantity'],"Amount":y['Amount'],"Month":month_name}
                        result1.append(data1)
                else:
                    pass
            materialissue = {}
            for item in result1:
                Month=item['Month']
                Amount = item['Amount']
                if Month in materialissue:
                    materialissue[Month]['Month']=Month
                    materialissue[Month]["Amount"] +=Amount
                else:
                    materialissue[Month]={"Month":Month,"Amount":Amount}
            materialissue1=[]
            for item,values in materialissue.items():
                data3={"Month":values['Month'],"IssueAmount":values['Amount']}
                materialissue1.append(data3)
            purchasemodule = GoodsTrfReceiptDetails.objects.filter(Q(Company_Id=Company_Id) & Q(Trfin_Date__range=Period)& Q(Is_Deleted=False))
            purchaseserialize = GoodsTrfReceiptDetailsSerialzer(purchasemodule,many=True)
            purchasemat = purchaseserialize.data
            result2=[]
            for i in purchasemat:
                if i['Store_Id']==Store_Id:
                    for y in i['Item']:
                        datestring = i['Trfin_Date']
                        date_object = datetime.strptime(datestring, "%Y-%m-%d")
                        month_name = date_object.strftime("%B")
                        data2={"id":y['id'],"Qty":y['Received_quantity'],"Amount":y['Amount'],"Month":month_name}
                        result2.append(data2)
                else:
                    pass
            materialrecipt = {}
            for item in result2:
                Month=item['Month']
                Amount = item['Amount']
                if Month in materialrecipt:
                    materialrecipt[Month]['Month']=Month
                    materialrecipt[Month]["Amount"] +=Amount
                else:
                    materialrecipt[Month]={"Month":Month,"Amount":Amount}
            materialrecipt1=[]
            for item,values in materialrecipt.items():
                data4={"Month":values['Month'],"ReciptAmount":values['Amount']}
                materialrecipt1.append(data4)
            data5={"Issue":materialissue1,"Receipt":materialrecipt1}
            output1 = JSONRenderer().render(data5)
            return HttpResponse(output1,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Totalinventoryitemstock(APIView):
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Company_Id=data['Company_Id']
            Period=data['Period']
            # module = ItemMaster.objects.filter(Company_Id=Company_Id)
            # serialize = ItemMasterSerialzers(module,many=True)
            # allitem = serialize.data
            receiptmodule = GoodsTrfReceiptDetails.objects.filter(Q(Company_Id=Company_Id)& Q(Trfin_Date__range=Period) & Q(Is_Deleted=False))
            receiptserialize = GoodsTrfReceiptDetailsSerialzer(receiptmodule,many=True)
            receiptitem = receiptserialize.data
            issuemodule = MaterialIssueDetail.objects.filter(Q(Company_Id=Company_Id)& Q(Issue_Date__range=Period) & Q(Is_Deleted=False))
            issueserialize = MaterialIssueDetailSerialzer(issuemodule,many=True)
            issueitem = issueserialize.data
            receiptlist={}
            receiptlist1 = []
            for y in receiptitem:
                for item in y['Item']:
                    id=item['id']
                    uom=item['uom']
                    Received_quantity=item['Received_quantity']
                    if id in receiptlist:
                        receiptlist[id]['id']=id
                        receiptlist[id]['uom']=uom
                        receiptlist[id]['Received_quantity']+=Received_quantity
                    else:
                        receiptlist[id]={"id":id,"uom":uom,"Received_quantity":Received_quantity}
            for item,values in receiptlist.items():
                data1={"id":values['id'],"uom":values['uom'],"Received_quantity":values['Received_quantity']}                
                receiptlist1.append(data1)
            issuelist={}              
            for y in issueitem:
                for item in y['Item']:
                    id=item['id']
                    uom=item['uom']
                    issue_quantity=item['issue_quantity']
                    if id in issuelist:
                        issuelist[id]['id']=id
                        issuelist[id]['uom']=uom
                        issuelist[id]['issue_quantity']+=issue_quantity
                    else:
                        issuelist[id]={"id":id,"uom":uom,"issue_quantity":issue_quantity}
            for item,values in issuelist.items():
                data1={"id":values['id'],"uom":values['uom'],"issue_quantity":values['issue_quantity']}                
                receiptlist1.append(data1)
            unique_ids = set()
            # Create a dictionary to store the combined quantities for each ID
            combined_quantities = {}
            # Iterate over the data
            for item in receiptlist1:
                id = item['id']
                if id not in unique_ids:
                    unique_ids.add(id)
                    combined_quantities[id] = item
                else:
                    existing_item = combined_quantities[id]
                    for key, value in item.items():
                        if key != 'id':
                            existing_item[key] = existing_item.get(key, 0) + value
            # Get the list of items with unique IDs and combined quantities
            result = list(combined_quantities.values())
            finaloutput = []
            for i in result:
                itemmodule = ItemMaster.objects.get(id=i['id'])
                itemserialize = ItemMasterSerialzer(itemmodule)
                items = itemserialize.data
                closingstock=(items['Opening_Stock']+i['Received_quantity'])-i['issue_quantity']
                data2={'id':items['Item_Id'],"uom":items['UOM'],"Opening_Stock":items['Opening_Stock'],"Unit_Price":items['Unit_Price'],
                    "Received_quantity":i['Received_quantity'],"issue_quantity":i['issue_quantity'],"closingstock":closingstock}
                finaloutput.append(data2)
            output1 = JSONRenderer().render(finaloutput)
            return HttpResponse(output1,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Goodsreceiptregister(APIView):
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Company_Id=data['Company_Id']
            Period=data['Period']
            receiptmodule = GoodsTrfReceiptDetails.objects.filter(Q(Company_Id=Company_Id)& Q(Trfin_Date__range=Period) & Q(Is_Deleted=False))
            receiptserialize = GoodsTrfReceiptDetailsSerialzers(receiptmodule,many=True)
            output1 = JSONRenderer().render(receiptserialize.data)
            return HttpResponse(output1,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class Materialreturnreports(APIView):
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Company_Id=data['Company_Id']
            Period=data['Period']
            receiptmodule = Materialreturndetails.objects.filter(Q(Company_Id=Company_Id)& Q(Created_Date__range=Period) & Q(Is_Deleted=False))
            receiptserialize = MaterialreturndetailsSerialzers(receiptmodule,many=True)
            output1 = JSONRenderer().render(receiptserialize.data)
            return HttpResponse(output1,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Materialtransaferreports(APIView):
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Company_Id=data['Company_Id']
            Period=data['Period']
            receiptmodule = MaterialTransferDetails.objects.filter(Q(Company_Id=Company_Id)& Q(MatTransf_Date__range=Period) & Q(Is_Deleted=False))
            receiptserialize = MaterialTransferDetailsSerialzers(receiptmodule,many=True)
            output1 = JSONRenderer().render(receiptserialize.data)
            return HttpResponse(output1,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class checkstocks(APIView):
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Company_Id=data['Company_Id']
            Item_Id=data['Item_Id']
            receiptmodule = GoodsTrfReceiptDetails.objects.filter(Q(Company_Id=Company_Id)&Q(Is_Deleted=False))
            receiptserialize = GoodsTrfReceiptDetailsSerialzers(receiptmodule,many=True)
            allrecipts = receiptserialize.data
            receiptitem=[]
            for i in allrecipts:
                for y in i['Item']:
                    if y['id']==Item_Id:
                        data1={"ReceiptQty":y['Received_quantity'],"Store_Name":i['Store_Id']['Store_Name']}
                        receiptitem.append(data1)
            receiptitem1={}
            for item in receiptitem:
                ReceiptQty=item['ReceiptQty']
                Store_Name=item['Store_Name']
                if Store_Name in receiptitem1:
                   receiptitem1[Store_Name]['Store_Name']=Store_Name
                   receiptitem1[Store_Name]['ReceiptQty'] +=ReceiptQty
                else:
                    receiptitem1[Store_Name]={"Store_Name":Store_Name,"ReceiptQty":ReceiptQty}
            totelreceipt=[]
            for item,values in receiptitem1.items():
                data5={"Store_Name":values['Store_Name'],"ReceiptQty":values['ReceiptQty']}
                totelreceipt.append(data5)
            issuemodule = MaterialIssueDetail.objects.filter(Q(Company_Id=Company_Id)&Q(Is_Deleted=False))
            issueserialize = MaterialIssueDetailSerialzers(issuemodule,many=True)
            allissue = issueserialize.data
            issueitem=[]
            for i in allissue:
                for y in i['Item']:
                    if y['id']==Item_Id:
                        data2={"IssuetQty":y['issue_quantity'],"Store_Name":i['Store_Id']['Store_Name']}
                        issueitem.append(data2)
            issueitem1={}
            for item in issueitem:
                IssuetQty=item['IssuetQty']
                Store_Name=item['Store_Name']
                if Store_Name in issueitem1:
                   issueitem1[Store_Name]['Store_Name']=Store_Name
                   issueitem1[Store_Name]['IssuetQty'] +=IssuetQty
                else:
                    issueitem1[Store_Name]={"Store_Name":Store_Name,"IssuetQty":IssuetQty}
            totelissue=[]
            for item,values in issueitem1.items():
                data6={"Store_Name":values['Store_Name'],"IssuetQty":values['IssuetQty']}
                totelissue.append(data6)
            transfermodule = MaterialTransferDetails.objects.filter(Q(Company_Id=Company_Id)&Q(Is_Deleted=False))
            transferserialize = MaterialTransferDetailsSerialzer(transfermodule,many=True)
            alltransfer = transferserialize.data
            transferitem=[]
            for i in alltransfer:
                for y in i['Item']:
                    if y['id']==Item_Id:
                        data3={"Transfer_Store_Name":i['Transfer_Store_Name'],"From_Store_Name":i['From_Store_Name'],"Transfer_quantity":y['Transfer_quantity']}
                        transferitem.append(data3)
            allstock=[]
            for i in totelreceipt:
                for y in totelissue:
                    if i['Store_Name']==y['Store_Name']:
                        i['ReceiptQty']=i['ReceiptQty'] - y['IssuetQty']
                        data7 = {'Store_Name':i['Store_Name'],"ReceiptQty":i['ReceiptQty']}
                        allstock.append(data7)
                    else:
                        # data7={'ReceiptQty':i,"IssuetQty":y}
                        allstock.append(i)
            eachstock = []
            for i in allstock:
                for y in transferitem:
                    if i['Store_Name']==y['Transfer_Store_Name']:
                        i['ReceiptQty']=i['ReceiptQty'] + y['Transfer_quantity']
                        data8={'Store_Name':i['Store_Name'],"ReceiptQty":i['ReceiptQty']}
                        eachstock.append(data8)
                    elif i['Store_Name']==y['From_Store_Name']:
                        i['ReceiptQty']=i['ReceiptQty'] - y['Transfer_quantity']
                        data8={'Store_Name':i['Store_Name'],"ReceiptQty":i['ReceiptQty']}
                        eachstock.append(data8)
                    else:
                        eachstock.append(i)
            module = ItemMaster.objects.get(id=Item_Id)
            openingstock = module.Opening_Stock
            UOM=module.UOM
            Totalstock=[]
            storestock=[]
            for i in eachstock:
                i['ReceiptQty']=i['ReceiptQty'] + openingstock
                data9={"Store_Name":i['Store_Name'],"ReceiptQty":i['ReceiptQty'],"UOM":UOM}
                storestock.append(data9)
                Totalstock.append(i['ReceiptQty'])
            Total=sum(Totalstock)
            storestock.append({"Total":Total})
            output1 = JSONRenderer().render(storestock)
            return HttpResponse(output1,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class Materialrequestuserfilter(ListAPIView):
    queryset = MaterialRequestDetail.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['id']
            model = MaterialRequestDetail.objects.filter(Requester_Id=id)
            serialize = MaterialRequestfilter(model,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output, content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)


# Budget Module API's

class BudgetheadCRUD(ListAPIView):
    queryset = Budgethead.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Distributionbudget_Id=data['Distributionbudget_Id']
            serialize = DistributionbudgetSerialzer(data=Distributionbudget_Id,many=True)
            if serialize.is_valid():
                serialize.save()
            Distributionbudget=serialize.data
            Distributionlist=[]
            for i in Distributionbudget:
                Distributionlist.append(i['id'])
            data['Distributionbudget_Id']=Distributionlist
            serialize1 = BudgetheadSerialzer(data=request.data)
            if serialize1.is_valid():
                serialize1.save()
            output=JSONRenderer().render(serialize1.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def get(self,request,*args,**kwargs):
        try:
            data = request.query_params
            Created_By=data['Created_By']
            module = Budgethead.objects.get(Created_By=Created_By)
            serialize = BudgetheadSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



    def put(self,request,*args,**kwargs):
        try:
            data = request.data
            Budgethead_Id = data['Budgethead_Id']
            Add=data['Distributionbudget_Id']
            for i in Add:
                module = Distributionbudget.objects.get(Distributionbudget_Id=i['Distributionbudget_Id'])
                serialize = DistributionbudgetSerialzer(instance=module,data=i)
                if serialize.is_valid():
                    serialize.save()
            module = Budgethead.objects.get(Budgethead_Id=Budgethead_Id)
            serialize = BudgetheadSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Budgethead_Id=data['Budgethead_Id']
            module = Budgethead.objects.get(Budgethead_Id=Budgethead_Id)
            serialize = BudgetheadSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output, content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    


class Budgetheadlist(ListAPIView):
    queryset = Budgethead.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    serializer_class = BudgetheadSerialzers
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        module = Budgethead.objects.filter(Is_Deleted = False).order_by('-id')
        return module


class DistributionbudgetCRUD(ListAPIView):
    queryset = Distributionbudget.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]   


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            Distributionbudget_Id=data['Distributionbudget_Id']
            module = Distributionbudget.objects.get(Distributionbudget_Id=Distributionbudget_Id)
            serialize = DistributionbudgetSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Distributionbudget_Id=data['Distributionbudget_Id']
            module = Distributionbudget.objects.get(Distributionbudget_Id=Distributionbudget_Id)
            serialize = DistributionbudgetSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class BudgetCRUD(ListAPIView):
    queryset = Budget.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]     


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = BudgetSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            # Budgethead_Id=data['Budgethead_Id']
            # module=Budgethead.objects.get(id=Budgethead_Id)
            # serialize = BudgetheadSerialzer(module)
            # Distribut=serialize.data
            # Price=Distribut['Earning_Perservice']
            # # module =Services.objects.get(id=id)
            # # serialize=ServicesSerialzer(module)
            # # price=serialize.data
            # # if data['Service_Type']=='Subscription':
            # #     budgetprice=price['Subscription_Baseprice']
            # # else:
            # #     budgetprice=price['Ontime_Baseprice']
            # # print(budgetprice)
            # Distributionbud=Distribut['Distributionbudget_Id']
            # # costs={}
            # # Bugets = []
            # months = {'jan':0,'feb':0,'march':0,"april":0,"may":0,"june":0,"july":0,"aug":0,"sep":0,"oct":0,"nov":0,"dec":0}
            # keys = list(months.keys())
            # Budgets = []        
            # for x in Distributionbud:
            #     module = Distributionbudget.objects.get(id=x)
            #     serialize=DistributionbudgetSerialzer(module)
            #     dat=serialize.data
            #     Base_Price=dat['Unit_Price']*Price
            #     Totalprice=Base_Price//100
            #     for i in range(0,len(keys)):
            #         costs={}
            #         # print(dat['Unit_Price'])
            #         # print(data['First_Month'])
            #         if keys[i] == 'jan':
            #             out=Totalprice*data['NoOfUsers']
            #             months[keys[i]] = out
            #         else:
            #             out=(months[keys[i-1]]//data['Increment'])+months[keys[i-1]]
            #             months[keys[i]] = out
            #     # costs[dat['Heads']] = months                        
            #         # out=dat['Unit_Price']*data['First_Month']
            #         # jan.append(out)
            #         # per=(out//10)+out
            #         # jan1.append(per)
            #         Budgets.append(out)
            #     costs[dat['Heads']] = months
            # users = []
            # revenues=[]
            # for i in range(0,12):
            #     user=(data['NoOfUsers']*data['Increment'])//100
            #     use=user+data['NoOfUsers']
            #     revenue=Distribut['Base_Price']*data['NoOfUsers']
            #     data['NoOfUsers']=use
            #     users.append(use)
            #     revenues.append(revenue)
            # serialize = BudgetSerialzer(data=data)
            # if serialize.is_valid():
            #     serialize.save()
            # output = JSONRenderer().render(serialize.data)
            # data1={"Service_Cost":Budgets[0:12],"Material_Cost":Budgets[12:24],"Operational_Cost":Budgets[24:36],
            #         "Business_Development_Cost":Budgets[36:48],"Business_Salary":Budgets[48:60],"Central_IT_Cost":Budgets[60:72],
            #         "Central_Salary":Budgets[72:84],"Central_Salary":Budgets[84:96],"Central_Finance":Budgets[96:108],
            #         "Central_HR":Budgets[108:120],"Central_Bus_Development":Budgets[120:132],"Central_Procurment_Cost":Budgets[132:144],
            #         "Central_Operation_Cost":Budgets[144:156],"users":users,"revenues":revenues}
            output1 = JSONRenderer().render(serialize.data)
            return HttpResponse(output1,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Budget_Id=data['Budget_Id']
            module = Budget.objects.get(Budget_Id=Budget_Id)
            serialize = BudgetSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            Budget_Id=data['Budget_Id']
            module = Budget.objects.get(Budget_Id=Budget_Id)
            serialize = BudgetSerialzer(instance=module,data=request.data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Budget_Id=data['Budget_Id']
            module = Budget.objects.get(Budget_Id=Budget_Id)
            serialize = BudgetSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

# without tokan buget
class BudgetCRUDwithouttoken(APIView):

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['id']
            module = Budget.objects.get(id=id)
            serialize = BudgetSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['id']
            module = Budget.objects.get(id=id)
            serialize = BudgetSerialzers(instance=module,data=request.data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class Budgetlist(ListAPIView):
    queryset = Budget.objects.all()
    serializer_class=BudgetSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        module = Budget.objects.filter(Is_Deleted=False).order_by('-id')
        return module
        
class Budgetmylist(ListAPIView):
    queryset = Budget.objects.all()
    serializer_class=BudgetSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        Created_By=self.request.GET.get('Created_By')
        module = Budget.objects.filter(Created_By=Created_By,Is_Deleted=False).order_by('-id')
        return module


class Budgetfilter(ListAPIView):
    queryset = Budget.objects.all()
    serializer_class=BudgetSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]    

    def post(self,request,*args,**kwargs):
        data=request.data
        data1={"Partner_Name__icontains":data['Partner_Name'],"Budget_Methods__icontains":data['Budget_Methods'],"Created_Date__range":data['Created_Date']}
        return requestget(model=Budget,serializer=BudgetSerialzers,data=data1,page=data['page'],Operator='and')


class Budgetsearch(ListAPIView):
    queryset = Budget.objects.all()
    serializer_class=BudgetSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data=self.request.GET.get('search')
        module = Budget.objects.filter(Q(Partner_Name__icontains=data) |Q(Service_Type__icontains=data)|Q(Service__icontains=data)| Q(Budget_Id__icontains=data) |Q(Name__icontains=data) | Q(Budget_Methods=data)).order_by('-id')
        return module

class Partnerbudgetlist(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Budget.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Is_Deleted=False))
            serialize = BudgetSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Budgetapprovalsystem(APIView):
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            # Workflow_Id=data['Workflow_Id']
            Employee_Id=data['Employee_Id']
            # print(Depname['Department_Head'])
            # html =f'{Base_Url}/inventory/material-request/document-approval'
            Created_By=data['Created_By']
            first_name=data['approver_name']
            Approverid=data['Approverid']
            module=User.objects.get(id=Approverid)
            serialize = UserSerializer(module)
            userdata=serialize.data
            # use=userdata[0]
            To=userdata['email']
            module1 = MailConfiguration.objects.get(Created_By=Created_By)
            serialize = MailConfigurationSerializer(module1)
            com=serialize.data
            From=com['MailId']
            subject=data['mailsubject']
            # Body=html
            Host=com['SMTPHost']
            Port = com['SMTPPort']
            Password=com['MailPassword']
            if Employee_Id != "":
                module = EmployeeMaster.objects.get(Employee_Id=Employee_Id)
                serialize = EmployeeMasterSerialzer(module)
                emp=serialize.data
                Dep_id=emp['Department_Id']
                module = Department.objects.get(id=Dep_id)
                serialize = DepartmentSerialzer(module)
                Depname=serialize.data
                module = User.objects.get(id=Depname['Updated_By'])
                serialize = UserSerializer(module)
                head=serialize.data
                To=head['email']
                html = data['mailhtml'].replace('{{url}}',f'{Base_Url}/budget/BuBudgetApproval/?Document_Type='+str(data['Document_Type'])+"&id="+str(data['id'])+"&Initiated_By=" +data['Initiated_By']+"&Url=" +data['Url']+"&Comments=" +data['Comments']+"&index="+str(data['index'])+"&documentapprovalid="+data['documentapprovalid'] +"&Designation="+head['Designation']+"&Headname="+head['first_name'])
                Body = html
                output = {"Headname":head['first_name'],"rolename":head['Designation'],"Message": "Mail sent Successfully","id":head['id']}
                response = SendMail(subject, From, To,Body, Host, Port,Password)
                return HttpResponse(JSONRenderer().render(output),
                                            content_type='application/json', status=status.HTTP_200_OK)
            else:
                html = data['mailhtml'].replace('{{url}}',f'{Base_Url}/budget/BuBudgetApproval/?Document_Type='+str(data['Document_Type'])+"&id="+str(data['id'])+"&Initiated_By=" +data['Initiated_By']+"&Url=" +data['Url']+"&Comments=" +data['Comments']+"&index="+str(data['index'])+"&documentapprovalid="+data['documentapprovalid']+"&Designation="+data['Designation']+"&Headname="+data['Headname'])
                Body = html
                To=userdata['email']
                response = SendMail(subject, From, To,Body, Host, Port,Password)
                return HttpResponse(JSONRenderer().render({"Message": "Mail sent Successfully"}),
                                            content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            return HttpResponse(JSONRenderer().render({"Message": "Mail Not sent"}),
                                        content_type='application/json', status=status.HTTP_200_OK)



class ConsolidatedBudgetCRUD(ListAPIView):
    queryset = ConsolidatedBudget.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            serialize1 = ConsolidatedBudgetSerialzer(data=request.data)
            if serialize1.is_valid():
                serialize1.save()
            output=JSONRenderer().render(serialize1.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def get(self,request,*args,**kwargs):
        try:
            data = request.query_params
            id=data['id']
            module = ConsolidatedBudget.objects.get(id=id)
            serialize = ConsolidatedBudgetSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



    def put(self,request,*args,**kwargs):
        try:
            data = request.data
            id = data['id']
            module = ConsolidatedBudget.objects.get(id=id)
            serialize = ConsolidatedBudgetSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['id']
            module = ConsolidatedBudget.objects.get(id=id)
            serialize = ConsolidatedBudgetSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output, content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class ConsolidatedBudgetCRUDwithouttoken(APIView):


    def get(self,request,*args,**kwargs):
        try:
            data = request.query_params
            id=data['id']
            module = ConsolidatedBudget.objects.get(id=id)
            serialize = ConsolidatedBudgetSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



    def put(self,request,*args,**kwargs):
        try:
            data = request.data
            id = data['id']
            module = ConsolidatedBudget.objects.get(id=id)
            serialize = ConsolidatedBudgetSerialzers(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)


class ConsolidatedBudgetlist(ListAPIView):
    queryset = ConsolidatedBudget.objects.all()
    serializer_class=ConsolidatedBudgetSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        Company_Id=self.request.GET.get('Company_Id')
        module = ConsolidatedBudget.objects.filter(Q(Company_Id=Company_Id) & Q(Is_Deleted=False)).order_by('-id')
        return module

class CentralBudgetCRUD(ListAPIView):
    queryset = CentralBudget.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            serialize1 = CentralBudgetSerialzer(data=request.data)
            if serialize1.is_valid():
                serialize1.save()
            output=JSONRenderer().render(serialize1.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def get(self,request,*args,**kwargs):
        try:
            data = request.query_params
            id=data['id']
            module = CentralBudget.objects.get(id=id)
            serialize = CentralBudgetSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



    def put(self,request,*args,**kwargs):
        try:
            data = request.data
            id = data['id']
            module = CentralBudget.objects.get(id=id)
            serialize = CentralBudgetSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['id']
            module = CentralBudget.objects.get(id=id)
            serialize = CentralBudgetSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output, content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class CentralBudgetlist(ListAPIView):
    queryset = CentralBudget.objects.all()
    serializer_class=CentralBudgetSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        Company_Id=self.request.GET.get('Company_Id')
        module = CentralBudget.objects.filter(Q(Company_Id=Company_Id) & Q(Is_Deleted=False)).order_by('-id')
        return module


class SalesforecastCRUD(ListAPIView):
    queryset = Salesforecast.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            serialize1 = SalesforecastSerialzer(data=request.data)
            if serialize1.is_valid():
                serialize1.save()
            output=JSONRenderer().render(serialize1.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def get(self,request,*args,**kwargs):
        try:
            data = request.query_params
            Salesforecast_Id=data['Salesforecast_Id']
            module = Salesforecast.objects.get(Salesforecast_Id=Salesforecast_Id)
            serialize = SalesforecastSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



    def put(self,request,*args,**kwargs):
        try:
            data = request.data
            Salesforecast_Id = data['Salesforecast_Id']
            module = Salesforecast.objects.get(Salesforecast_Id=Salesforecast_Id)
            serialize = SalesforecastSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Salesforecast_Id=data['Salesforecast_Id']
            module = Salesforecast.objects.get(Salesforecast_Id=Salesforecast_Id)
            serialize = SalesforecastSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output, content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    


class Salesforecastlist(ListAPIView):
    queryset = Salesforecast.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    serializer_class = SalesforecastSerialzers
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        Partner_Id=self.request.GET.get('Partner_Id')
        module = Salesforecast.objects.filter(Q(Partner_Id=Partner_Id) & Q(Is_Deleted = False)).order_by('-id')
        return module


class Salesforecastfilter(ListAPIView):
    queryset = Salesforecast.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    serializer_class = SalesforecastSerialzers
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        data=request.data
        data1 = {"Salesforecast_Id__icontains":data['Salesforecast_Id'],"Salesfocrcast_Name__icontains":data['Salesfocrcast_Name'],
                    "Partner_Name__icontains":data['Partner_Name'],"Created_Date__range":data['Created_Date'],"Is_Deleted":False}
        return requestget(model=Salesforecast,serializer=SalesforecastSerialzers,data=data1,page=data['page'],Operator='and') 


class Salesforecastsearch(ListAPIView):
    queryset = Salesforecast.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    serializer_class = SalesforecastSerialzers
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        Partner_Id=self.request.GET.get('Partner_Id')
        data = self.request.GET.get('search')
        module = Salesforecast.objects.filter(Q(Partner_Id=Partner_Id) & Q(Salesforecast_Id__icontains=data) | Q(Partner_Name__icontains=data)
                | Q(Salesfocrcast_Name__icontains=data) | Q(Forecast_Methods__icontains=data)).order_by('-id')
        return module



class SalesuserTargetCRUD(ListAPIView):
    queryset = SalesuserTarget.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = SalesuserTargetSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output=JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def get(self,request,*args,**kwargs):
        try:
            data = request.query_params
            SalesuserTarget_Id=data['SalesuserTarget_Id']
            module = SalesuserTarget.objects.get(SalesuserTarget_Id=SalesuserTarget_Id)
            serialize = SalesuserTargetSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



    def put(self,request,*args,**kwargs):
        try:
            data = request.data
            SalesuserTarget_Id = data['SalesuserTarget_Id']
            module = SalesuserTarget.objects.get(SalesuserTarget_Id=SalesuserTarget_Id)
            serialize = SalesuserTargetSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            SalesuserTarget_Id=data['SalesuserTarget_Id']
            module = SalesuserTarget.objects.get(SalesuserTarget_Id=SalesuserTarget_Id)
            serialize = SalesuserTargetSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output, content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    
class SalesuserTargetlist(ListAPIView):
    queryset = SalesuserTarget.objects.all()
    serializer_class = SalesuserTargetSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]   

    def get_queryset(self):
        module = SalesuserTarget.objects.filter(Is_Deleted=False).order_by('-id')
        return module

class SalesuserTargetfilter(ListAPIView):
    queryset = SalesuserTarget.objects.all()
    serializer_class=SalesuserTargetSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]    

    def post(self,request,*args,**kwargs):
        data=request.data
        data1={"Partner_Name__icontains":data['Partner_Name'],"Budget_Methods__icontains":data['Budget_Methods'],"Created_Date__range":data['Created_Date']}
        return requestget(model=SalesuserTarget,serializer=SalesuserTargetSerialzers,data=data1,page=data['page'],Operator='and')


class SalesuserTargetsearch(ListAPIView):
    queryset = SalesuserTarget.objects.all()
    serializer_class=SalesuserTargetSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data=self.request.GET.get('search')
        module = SalesuserTarget.objects.filter(Q(Partner_Name__icontains=data) |Q(SalesuserTarget_Id__icontains=data)|Q(Service__icontains=data)| Q(Service_Type__icontains=data)).order_by('-id')
        return module


class SubscriberTargetCRUD(ListAPIView):
    queryset = SubscriberTarget.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = SubscriberTargetSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output=JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def get(self,request,*args,**kwargs):
        try:
            data = request.query_params
            SubscriberTarget_Id=data['SubscriberTarget_Id']
            module = SubscriberTarget.objects.get(SubscriberTarget_Id=SubscriberTarget_Id)
            serialize = SubscriberTargetSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



    def put(self,request,*args,**kwargs):
        try:
            data = request.data
            SubscriberTarget_Id = data['SubscriberTarget_Id']
            module = SubscriberTarget.objects.get(SubscriberTarget_Id=SubscriberTarget_Id)
            serialize = SubscriberTargetSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            SubscriberTarget_Id=data['SubscriberTarget_Id']
            module = SubscriberTarget.objects.get(SubscriberTarget_Id=SubscriberTarget_Id)
            serialize = SubscriberTargetSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output, content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    
class SubscriberTargetlist(ListAPIView):
    queryset = SubscriberTarget.objects.all()
    serializer_class = SubscriberTargetSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]   

    def get_queryset(self):
        module = SubscriberTarget.objects.filter(Is_Deleted=False).order_by('-id')
        return module


class SubscriberTargetfilter(ListAPIView):
    queryset = SubscriberTarget.objects.all()
    serializer_class=SubscriberTargetSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]    

    def post(self,request,*args,**kwargs):
        data=request.data
        data1={"Partner_Name__icontains":data['Partner_Name'],"Service__icontains":data['Service'],"Service_Type__icontains":data['Service_Type'],"Created_Date__range":data['Created_Date']}
        return requestget(model=SubscriberTarget,serializer=SubscriberTargetSerialzers,data=data1,page=data['page'],Operator='and')


class SubscriberTargetsearch(ListAPIView):
    queryset = SubscriberTarget.objects.all()
    serializer_class=SubscriberTargetSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data=self.request.GET.get('search')
        module = SubscriberTarget.objects.filter(Q(Partner_Name__icontains=data) |Q(SubscriberTarget_Id__icontains=data)|Q(Service__icontains=data)| Q(Service_Type__icontains=data)).order_by('-id')
        return module


class ExpenseBudgetCRUD(ListAPIView):
    queryset = ExpenseBudget.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize=ExpenseBudgetSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            ExpenseBudget_Id=data['ExpenseBudget_Id']
            module = ExpenseBudget.objects.get(ExpenseBudget_Id=ExpenseBudget_Id)
            serialize = ExpenseBudgetSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            ExpenseBudget_Id=data['ExpenseBudget_Id']
            module = ExpenseBudget.objects.get(ExpenseBudget_Id=ExpenseBudget_Id)
            serialize = ExpenseBudgetSerialzer(module,data=request.data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)       


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            ExpenseBudget_Id=data['ExpenseBudget_Id']
            module = ExpenseBudget.objects.get(ExpenseBudget_Id=ExpenseBudget_Id)
            serialize = ExpenseBudgetSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class ExpenseBudgetlist(ListAPIView):
    queryset = ExpenseBudget.objects.all()
    serializer_class=ExpenseBudgetSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get_queryset(self):
        module = ExpenseBudget.objects.filter(Is_Deleted=False).order_by('-id')
        return module


class ExpenseBudgetmylist(ListAPIView):
    queryset = ExpenseBudget.objects.all()
    serializer_class=ExpenseBudgetSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get_queryset(self):
        Created_By = self.request.GET.get('Created_By')
        module = ExpenseBudget.objects.filter(Q(Created_By=Created_By)& Q(Is_Deleted=False)).order_by('-id')
        return module


class ExpenseBudgetfilter(ListAPIView):
    queryset = ExpenseBudget.objects.all()
    serializer_class=ExpenseBudgetSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        data=request.data
        data1={"Partner_Name__icontains":data['Partner_Name'],"Service__icontains":data['Service'],"Budget_Period__icontains":data['Budget_Period'],
            "Budget_Group__icontains":data['Budget_Group'],"Created_Date__range":data['Created_Date'],"Name__icontains":data['Name'],"Is_Deleted":False}
        return requestget(model=ExpenseBudget,serializer=ExpenseBudgetSerialzers,data=data1,page=data['page'],Operator='and')

class ExpenseBudgetsearch(ListAPIView):
    queryset = ExpenseBudget.objects.all()
    serializer_class=ExpenseBudgetSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions] 

    def get_queryset(self):
        data = self.request.GET.get('search')
        module = ExpenseBudget.objects.filter(Q(Partner_Name__icontains=data)|Q(Name__icontains=data) |
             Q(Service__icontains=data) | Q(Budget_Period__icontains=data) | 
             Q(Budget_Group__icontains=data) | Q(ExpenseBudget_Id__icontains=data),Is_Deleted=False)
        return module

# class Partnerfilter(ListAPIView):
#     queryset = Partner.objects.all()
#     serializer_class=PartnerSerialzer
#     authentication_classes = [JSONWebTokenAuthentication]
#     permission_classes = [DjangoModelPermissions]


#     def post(self,request,*args,**kwargs):
#         data=request.data
#         data1={"Partner_Name":data['Partner_Name'],"BusinessUnit_EMail":data['BusinessUnit_EMail'],"BusinessUnit_City":data['city']}
#         return requestget(model=Partner,serializer=PartnerSerialzer,data=data1,page=data['page'],Operator='and')


# class Partnersearch(ListAPIView):
#     queryset = Partner.objects.all()
#     serializer_class=PartnerSerialzer
#     authentication_classes = [JSONWebTokenAuthentication]
#     permission_classes = [DjangoModelPermissions]

#     def get(self,request,*args,**kwargs):
#         try:
#             data=request.query_params
#             module=Partner.objects.filter(Q(Partner_Name__icontains=data['Search'])|Q(BusinessUnit_EMail__icontains=data['Search'])|Q(BusinessUnit_City__icontains=data['Search']))
#             serialize=PartnerSerialzer(module,many=True)
#             output = JSONRenderer().render(serialize.data)
#             return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
#         except Exception as e:
#             output = JSONRenderer().render({"Error":str(e)})
#             return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class MaterialRequestDetailtransaction(ListAPIView):
    queryset = MaterialRequestDetail.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            item_name=data['item_name']
            print(item_name)
            module = MaterialRequestDetail.objects.filter(Item__icontains=item_name)
            serialize = MaterialRequestDetailSerialzer(module,many=True)
            output =JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)


class MaterialIssueDetailtransaction(ListAPIView):
    queryset = MaterialIssueDetail.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            item_name=data['item_name']
            module = MaterialIssueDetail.objects.filter(Item__icontains=item_name)
            serialize = MaterialIssueDetailSerialzer(module,many=True)
            output =JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)


class MaterialTransferDetailstransaction(ListAPIView):
    queryset = MaterialTransferDetails.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            item_name=data['item_name']
            module = MaterialTransferDetails.objects.filter(Item__icontains=item_name)
            serialize = MaterialTransferDetailsSerialzer(module,many=True)
            output =JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)



class GoodsTrfReceiptDetailstransaction(ListAPIView):
    queryset = GoodsTrfReceiptDetails.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            item_name=data['item_name']
            module = GoodsTrfReceiptDetails.objects.filter(Item__icontains=item_name)
            serialize = GoodsTrfReceiptDetailsSerialzer(module,many=True)
            output =JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)


class Purchaseordertransaction(ListAPIView):
    queryset = PODetails.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            item_name=data['item_name']
            module = PODetails.objects.filter(Items__icontains=item_name)
            serialize = PODetailsSerialzer(module,many=True)
            output =JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)


#Partner module API's 

class PartnerCRUD(ListAPIView):
    queryset = Partner.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize=PartnerSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Partner_Id=data['Partner_Id']
            module = Partner.objects.get(Partner_Id=Partner_Id)
            serialize = PartnerSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            Partner_Id=data['Partner_Id']
            module = Partner.objects.get(Partner_Id=Partner_Id)
            serialize = PartnerSerialzer(module,data=request.data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)       


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Partner_Id=data['Partner_Id']
            module = Partner.objects.get(Partner_Id=Partner_Id)
            serialize = PartnerSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class Partnerlist(ListAPIView):
    queryset = Partner.objects.all()
    serializer_class=PartnerSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get_queryset(self):
        module = Partner.objects.filter(Is_Deleted=False).order_by('-id')
        return module



class Partnerfilter(ListAPIView):
    queryset = Partner.objects.all()
    serializer_class=PartnerSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        data=request.data
        data1={"Partner_Name":data['Partner_Name'],"BusinessUnit_EMail":data['BusinessUnit_EMail'],"BusinessUnit_City":data['city']}
        return requestget(model=Partner,serializer=PartnerSerialzers,data=data1,page=data['page'],Operator='and')


class Partnersearch(ListAPIView):
    queryset = Partner.objects.all()
    serializer_class=PartnerSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module=Partner.objects.filter(Q(Partner_Name__icontains=data['Search'])|Q(BusinessUnit_EMail__icontains=data['Search'])|Q(BusinessUnit_City__icontains=data['Search']))
            serialize=PartnerSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class partnercitywiselist(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Partner.objects.filter(Q(Company_Id=data['Company_Id']) & Q(BusinessUnit_City=data['BusinessUnit_City']) & Q(Is_Deleted=False))
            serialize = PartnerSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class BusinessUnitCRUD(ListAPIView):
    queryset = BusinessUnit.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize=BusinessUnitSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            BusinessUnit_Id=data['BusinessUnit_Id']
            module = BusinessUnit.objects.get(BusinessUnit_Id=BusinessUnit_Id)
            serialize = BusinessUnitSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            BusinessUnit_Id=data['BusinessUnit_Id']
            module = BusinessUnit.objects.get(BusinessUnit_Id=BusinessUnit_Id)
            serialize = BusinessUnitSerialzer(module,data=request.data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)       


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            BusinessUnit_Id=data['BusinessUnit_Id']
            module = BusinessUnit.objects.get(BusinessUnit_Id=BusinessUnit_Id)
            serialize = BusinessUnitSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class BusinessUnitlist(ListAPIView):
    queryset = BusinessUnit.objects.all()
    serializer_class=BusinessUnitSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get_queryset(self):
        module = BusinessUnit.objects.filter(Is_Deleted=False).order_by('-id')
        return module



class BusinessUnitfilter(ListAPIView):
    queryset = BusinessUnit.objects.all()
    serializer_class=BusinessUnitSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        data=request.data
        data1={"BusinessUnit_Name":data['BusinessUnit_Name'],"BusinessUnit_EMail":data['BusinessUnit_EMail'],"BusinessUnit_City":data['BusinessUnit_City']}
        return requestget(model=BusinessUnit,serializer=BusinessUnitSerialzers,data=data1,page=data['page'],Operator='and')


class BusinessUnitsearch(ListAPIView):
    queryset = BusinessUnit.objects.all()
    serializer_class=BusinessUnitSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module=BusinessUnit.objects.filter(Q(BusinessUnit_Name__icontains=data['Search'])|Q(BusinessUnit_EMail__icontains=data['Search'])|Q(BusinessUnit_City__icontains=data['Search']))
            serialize=BusinessUnitSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

#front end purpose:

class Businessunitlistwithoutpage(APIView):
    def get(self,request,*args,**kwargs):
        try:
            module = BusinessUnit.objects.all()
            serialize = BusinessUnitSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class Businessunitsalestarget(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Budget.objects.filter(Q(Partner_Name=data['Partner_Name']) & Q(Service_Type=data['Service_Type']) & Q(Is_Deleted=False))
            serialize = BudgetSerialzer(module,many=True)
            output=serialize.data
            use=output[0]
            user=use['Users']
            revenue = use['revenue']
            output1={"user":user,"revenue":revenue}
            out=JSONRenderer().render(output1)
            return HttpResponse(out,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class DocumentapprovalCRUD(APIView):
    # queryset = Documentapproval.objects.all()
    # authentication_classes = [JSONWebTokenAuthentication]
    # permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Approval_Id=data['Approval_Id']
            app_id=[]
            for i in Approval_Id:
                serialize=ApprovalSerialzer(data=i)
                if serialize.is_valid():
                    serialize.save()
                    app=serialize.data
                    app_id.append(app['id'])
            data['Approval_Id']=app_id
            serialize = DocumentapprovalSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output=JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Documentapproval_Id=data['Documentapproval_Id']
            module = Documentapproval.objects.get(Documentapproval_Id=Documentapproval_Id)
            serialize = DocumentapprovalSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            Documentapproval_Id=data['Documentapproval_Id']
            module = Documentapproval.objects.get(Documentapproval_Id=Documentapproval_Id)
            serialize = DocumentapprovalSerialzers(module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)      


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Documentapproval_Id=data['Documentapproval_Id']
            module = Documentapproval.objects.get(Documentapproval_Id=Documentapproval_Id)
            serialize = DocumentapprovalSerialzers(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class Documentapprovallist(ListAPIView):
    queryset = Documentapproval.objects.all()
    serializer_class = DocumentapprovalSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get_queryset(self):
        module = Documentapproval.objects.filter(Is_Deleted=False)
        return module



class ApprovalCRUD(APIView):
    # queryset = Approval.objects.all()
    # authentication_classes = [JSONWebTokenAuthentication]
    # permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = ApprovalSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['id']
            module = Approval.objects.get(id=id)
            serialize = ApprovalSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['id']
            module = Approval.objects.get(id=id)
            serialize = ApprovalSerialzer(module,data=request.data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)






class Approvalflg(APIView):
    # queryset = Approval.objects.all()
    # authentication_classes = [JSONWebTokenAuthentication]
    # permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['id']
            module = Approval.objects.get(id=id)
            serialize = ApprovalSerialzer(module,{"Approved_flg":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)  


# class Approvername(APIView):
#     def get(self,request,*args,**kwargs):
#         try:
#             data=request.data

def appreg(request):
    user = {"Document_Type": request.GET.get('Document_Type'),
            "Request_No": request.GET.get('email'),
            "Initiated_By": request.GET.get('Initiated_By'),
            "Url": request.GET.get('Url'),
            "approver_name":request.GET.get('approver_name'),
            "Created_by": request.GET.get('Created_by'),
            "Designation":request.GET.get('Designation')}
    return render(request, 'DocumentApproval.html', {'user': user})


class Approvalsystem(APIView):
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            # Workflow_Id=data['Workflow_Id']
            Employee_Id=data['Employee_Id']
            # print(Depname['Department_Head'])
            # html =f'{Base_Url}/inventory/material-request/document-approval'
            Created_By=data['Created_By']
            first_name=data['approver_name']
            Approverid=data['Approverid']
            module=User.objects.get(id=Approverid)
            serialize = UserSerializer(module)
            userdata=serialize.data
            # use=userdata[0]
            To=userdata['email']
            module1 = MailConfiguration.objects.get(Created_By=Created_By)
            serialize = MailConfigurationSerializer(module1)
            com=serialize.data
            From=com['MailId']
            subject=data['mailsubject']
            # Body=html
            Host=com['SMTPHost']
            Port = com['SMTPPort']
            Password=com['MailPassword']
            if Employee_Id != "":
                module = EmployeeMaster.objects.get(Employee_Id=Employee_Id)
                serialize = EmployeeMasterSerialzer(module)
                emp=serialize.data
                Dep_id=emp['Department_Id']
                module = Department.objects.get(id=Dep_id)
                serialize = DepartmentSerialzer(module)
                Depname=serialize.data
                module = User.objects.get(id=Depname['Updated_By'])
                serialize = UserSerializer(module)
                head=serialize.data
                To=head['email']
                html = data['mailhtml'].replace('{{url}}',f'{Base_Url}/inventory/material-request/document-approval/?Document_Type='+str(data['Document_Type'])+"&id="+str(data['id'])+"&Initiated_By=" +data['Initiated_By']+"&Url=" +data['Url']+"&Comments=" +data['Comments']+"&index="+str(data['index'])+"&documentapprovalid="+data['documentapprovalid'] +"&Designation="+head['Designation']+"&Headname="+head['first_name'])
                Body = html
                output = {"Headname":head['first_name'],"rolename":head['Designation'],"Message": "Mail sent Successfully","id":head['id']}
                response = SendMail(subject, From, To,Body, Host, Port,Password)
                return HttpResponse(JSONRenderer().render(output),
                                            content_type='application/json', status=status.HTTP_200_OK)
            else:
                html = data['mailhtml'].replace('{{url}}',f'{Base_Url}/inventory/material-request/document-approval/?Document_Type='+str(data['Document_Type'])+"&id="+str(data['id'])+"&Initiated_By=" +data['Initiated_By']+"&Url=" +data['Url']+"&Comments=" +data['Comments']+"&index="+str(data['index'])+"&documentapprovalid="+data['documentapprovalid']+"&Designation="+data['Designation']+"&Headname="+data['Headname'])
                Body = html
                To=userdata['email']
                response = SendMail(subject, From, To,Body, Host, Port,Password)
                return HttpResponse(JSONRenderer().render({"Message": "Mail sent Successfully"}),
                                            content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            return HttpResponse(JSONRenderer().render({"Message": "Mail Not sent"}),
                                        content_type='application/json', status=status.HTTP_200_OK)


# Email purpose we create this api
class MaterialRequestapproval(APIView):

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            #data=request
            id=data['id']
            module=MaterialRequestDetail.objects.get(id=id)
            serialize = MaterialRequestDetailSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    # edit the material request
    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            #data=request
            id=data['id']
            module=MaterialRequestDetail.objects.get(id=id)
            serialize = MaterialRequestDetailSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            print('s',serialize.errors)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class workflowfilterapproval(APIView):

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Document_Name=data['Document_Name']
            module = Workflow.objects.filter(Document_Name=Document_Name)
            serialize = WorkflowSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_404_NOT_FOUND)

class getemployeeeid(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            email=data['email']
            module=EmployeeMaster.objects.filter(Email_Id=email)
            serialize=EmployeeMasterSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class Materialrequestworkflow(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Request_Id=data['Request_Id']
            module = Documentapproval.objects.filter(Request_Id=Request_Id)
            serialize = DocumentapprovalSerialzers(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class PayrollconfigurationCRUD(ListAPIView):
    queryset = Payrollconfiguration.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize=PayrollconfigurationSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Payrollconfig_Id=data['Payrollconfig_Id']
            module = Payrollconfiguration.objects.get(Payrollconfig_Id=Payrollconfig_Id)
            serialize = PayrollconfigurationSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            Payrollconfig_Id=data['Payrollconfig_Id']
            module = Payrollconfiguration.objects.get(Payrollconfig_Id=Payrollconfig_Id)
            serialize = PayrollconfigurationSerialzer(module,data=request.data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)       


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Payrollconfig_Id=data['Payrollconfig_Id']
            module = Payrollconfiguration.objects.get(Payrollconfig_Id=Payrollconfig_Id)
            serialize = PayrollconfigurationSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class Payrollconfigurationlist(ListAPIView):
    queryset = Payrollconfiguration.objects.all()
    serializer_class=PayrollconfigurationSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get_queryset(self):
        module = Payrollconfiguration.objects.filter(Is_Deleted=False).order_by('-id')
        return module



class TransactionalseriesCRUD(ListAPIView):
    queryset = Transactionalseries.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = TransactionalseriesSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Created_By']
            module = Transactionalseries.objects.get(Created_By=id)
            serialize = TransactionalseriesSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Transactionalseries_Id']
            module=Transactionalseries.objects.get(Transactionalseries_Id=id)
            serialize = TransactionalseriesSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Transactionalseries_Id']
            module=Transactionalseries.objects.get(Transactionalseries_Id=id)
            serialize = TransactionalseriesSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class Partneremployeelist(ListAPIView):
    queryset = EmployeeMaster.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Partner_Id=data['Partner_Id']
            module = EmployeeMaster.objects.filter(Partner_Id=Partner_Id)
            serialize = EmployeeMasterSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json')



class AssetsCRUD(ListAPIView):
    queryset = Assets.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = AssetsSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['id']
            module = Assets.objects.get(id=id)
            serialize = AssetsSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['id']
            module = Assets.objects.get(id=id)
            serialize = AssetsSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['id']
            module = Assets.objects.get(id=id)
            serialize = AssetsSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

#Front end purpose 
class Assetsget(ListAPIView):
    queryset = Assets.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Created_By']
            module = Assets.objects.get(Created_By=id)
            serialize = AssetsSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Equityget(ListAPIView):
    queryset = Equity.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Created_By']
            module = Equity.objects.get(Created_By=id)
            serialize = EquitySerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Liabilityget(ListAPIView):
    queryset = Liability.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Created_By']
            module = Liability.objects.get(Created_By=id)
            serialize = LiabilitySerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class Asseteslist(ListAPIView):
    queryset = Assets.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        try:
            data = request.query_params
            module = Assets.objects.filter(Partner_Id=data['Partner_Id'])
            serialize = AssetsSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)


class Equitylist(ListAPIView):
    queryset = Equity.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        try:
            data = request.query_params
            module = Equity.objects.filter(Partner_Id=data['Partner_Id'])
            serialize = EquitySerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)


class Liabilitylist(ListAPIView):
    queryset = Liability.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        try:
            data = request.query_params
            module = Liability.objects.filter(Partner_Id=data['Partner_Id'])
            serialize = LiabilitySerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)


class EquityCRUD(ListAPIView):
    queryset = Equity.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = EquitySerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['id']
            module = Equity.objects.get(id=id)
            serialize = EquitySerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['id']
            module = Equity.objects.get(id=id)
            serialize = EquitySerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['id']
            module = Equity.objects.get(id=id)
            serialize = EquitySerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class LiabilityCRUD(ListAPIView):
    queryset = Liability.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = LiabilitySerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['id']
            module = Liability.objects.get(id=id)
            serialize = LiabilitySerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['id']
            module = Liability.objects.get(id=id)
            serialize = LiabilitySerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['id']
            module = Liability.objects.get(id=id)
            serialize = LiabilitySerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)       



class PaycalendarCRUD(ListAPIView):
    queryset = Paycalendar.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = PaycalendarSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Created_By']
            module = Paycalendar.objects.get(Created_By=id)
            serialize = PaycalendarSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Created_By']
            module=Paycalendar.objects.get(Created_By=id)
            serialize = PaycalendarSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class Persionsalesorder(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = SalesOrder.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Order_Cancelled=False))
            serialize = SalesOrderSerialzer(module,many=True)
            Amount=[]
            Result={}
            ser = serialize.data
            for i in ser:
                module=User.objects.get(id=i['Created_By'])
                serialize=module.User_Name
                if serialize in Amount:
                    Amount_Paid=Amount_Paid + i['Amount_Paid']
                else:
                    Amount.append({"Name":serialize,"Amount_Paid":i['Amount_Paid']})
            for item in Amount:
                Name=item['Name']
                Amount = item['Amount_Paid']
                if Name in Result:
                    Result[Name] +=Amount
                else:
                    Result[Name]= Amount
            output = JSONRenderer().render(Result)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class Businessunitsalesorder(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = SalesOrder.objects.filter(Q(Partner_Id=data['Partner_Id'])& Q(Order_Cancelled=False) & Q(Is_Deleted=False)).order_by('-id')  
            serialize = SalesOrderSerialzer(module,many=True)
            ser=serialize.data
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)

class BusinessUnitSalesTargetArcheived(ListAPIView):
    queryset = Budget.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data = request.data
            module = Budget.objects.filter(
                Q(Partner_Id__exact=data['Partner_Id']), Q(Created_Date__year__range=data['Year']),Is_Deleted=False)
            serialize = BudgetSerialzers(module, many=True)
            archeivedlists = serialize.data
            result = []
            id = 1
            for exp in archeivedlists:
                company_id = exp['Partner_Id']
                company_detail = Partner.objects.filter(Partner_Id = company_id)
                serialize_company = PartnerSerialzers(company_detail,many=True)
                company_lists = serialize_company.data
                for company in company_lists:
                    # address = company['CompanyAddress']
                    city = company['BusinessUnit_City']
                    data = {
                        "Region": city,
                        "Target": exp['Sales_Target'],
                        "TotalSales": exp['Expense']
                    }
                    result.append(data)
            print(result)
            output = {}
            for item in result:
                city = item['Region']
                salesTarget = item['Target']
                totalSales = item['TotalSales']
                if city in output:
                    output[city]['Target'] += salesTarget
                    output[city]['TotalSales'] += totalSales
                else:
                    output[city] = {
                        "Region": city,
                        "Target": salesTarget,
                        "TotalSales": totalSales
                    }
            response_data = []
            for city, values in output.items():
                data1 = {
                    "Id": id,
                    "Region": city,
                    "Target": values['Target'],
                    "TotalSales": values['TotalSales']
                }
                response_data.append(data1)
                id +=1
            print(response_data)
            output1 = JSONRenderer().render(response_data)
            return HttpResponse(output1,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output1,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)




class BusinessUnitExpenses(ListAPIView):
    queryset = Voucherdetail.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data = request.data
            module = Voucherdetail.objects.filter(
                Q(Partner_Id__exact=data['Partner_Id']), Q(Created_Date__year__range=data['Year']),Is_Deleted=False)
            serialize = VoucherdetailSerialzers(module, many=True)
            expenselists = serialize.data
            result = []
            id = 1
            for exp in expenselists:
                created_by = exp['Created_By']
                employee_name = created_by['first_name']
                account = exp['Account']
                # account_name = account['accountName']
                for x in account:
                    account_type = Chartofaccount.objects.filter(Account_Name=x['accountName'])
                    serialize = ChartofaccountSerialzer(account_type,many=True)
                    acc=serialize.data
                    for i in acc:
                        if i['Account_Type'] == "Expense":
                            data = {
                                "Id": id,
                                "Date": exp['Created_Date'],
                                "ExpenseAccount": x['accountName'],
                                "Description": exp['Account_Description'],
                                "EmployeeName": employee_name,
                                "Amount": x['amount'],
                                "ApprovedBy": exp['Approved_By']
                            }
                            result.append(data)
                            id +=1
            output = JSONRenderer().render(result)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)




class BusinessUnitSalaries(ListAPIView):
    queryset = Attandancemaster.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data = request.data
            module = Attandancemaster.objects.filter(
                Q(Partner_Id__icontains=data['Partner_Id']), Q(Year__range=data['Year']),Is_Deleted=False)
            serialize = AttandancemasterSerialzer(module, many=True)
            employeelists = serialize.data
            result = []
            id = 1
            for employee in employeelists:
                this_month = employee['Month']
                noofEmployees=Attandancemaster.objects.filter(Q(Partner_Id=data['Partner_Id']), Q(Year__range=data['Year']) & Q(Month = this_month) & Q(Is_Deleted=False)).count()
                monthly_salary = employee['Monthly_Salary']
                one_day_salary = monthly_salary // 30
                Deductions=employee['UnPaid_Leave']*one_day_salary
                NetSalary=monthly_salary-Deductions
                AvgSalary = NetSalary//noofEmployees
                data1={
                    "Month":employee['Month'],
                    "NoofEmployees": noofEmployees,
                    "Earnings": monthly_salary,
                    "Deductions":Deductions,
                    "NetSalary":NetSalary,
                    "AvgSalary":AvgSalary
                }
                result.append(data1)
            print(result)
            output = {}
            for item in result:
                month = item['Month']
                noof_employees = item['NoofEmployees']
                earnings = item['Earnings']
                deductions = item['Deductions']
                net_salary = item['NetSalary']
                avg_salary = item['AvgSalary']
                if month in output:
                    output[month]['NoofEmployees'] = noof_employees
                    output[month]['Earnings'] += earnings
                    output[month]['Deductions'] += deductions
                    output[month]['NetSalary'] += net_salary
                    output[month]['AvgSalary'] += avg_salary
                else:
                    output[month] = {
                        'NoofEmployees': noof_employees,
                        'Earnings': earnings,
                        'Deductions': deductions,
                        'NetSalary': net_salary,
                        'AvgSalary': avg_salary
                    }

            response_data = []
            for month, values in output.items():
                data2 = {
                    "Id": id,
                    "Month": month,
                    "NoofEmployees": values['NoofEmployees'],
                    "Earnings": values['Earnings'],
                    "Deductions": values['Deductions'],
                    "NetSalary": values['NetSalary'],
                    "AvgSalary": values['AvgSalary']
                }
                response_data.append(data2)
                id +=1
            print("response_data",response_data)
            output1 = JSONRenderer().render(response_data)
            return HttpResponse(output1,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output1,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class BusinessUnitReimbursements(ListAPIView):
    queryset = Voucherdetail.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data = request.data
            period = data['Year']
            module = Voucherdetail.objects.filter(
                Q(Partner_Id__exact=data['Partner_Id']), Q(Created_Date__year__range=data['Year']),Is_Deleted=False)
            serialize = VoucherdetailSerialzers(module, many=True)
            amountlists = serialize.data
            result = []
            id = 1
            for exp in amountlists:
                created_by = exp['Created_By']
                employee_name = created_by['first_name']
                account = exp['Account']
                employees = EmployeeMaster.objects.filter(Employee_FirstName = employee_name)
                employeelists = EmployeeMasterSerialzers(employees, many=True).data
                for i in employeelists:
                    employeeId = i['Employee_Id']

                    for x in account:
                        account_type = Chartofaccount.objects.filter(Account_Name=x['accountName'])
                        serialize = ChartofaccountSerialzer(account_type,many=True)
                        acc=serialize.data
                        for i in acc:
                            if i['Account_Type'] == "Expense" and x['accountName'] == "Reimbursement":
                                account_head = i['Account_Group']
                                if exp['Approved_Flg'] == True:
                                    approvestatus = "Paid"
                                    claim_amount = x['amount']
                                else: 
                                    approvestatus = "UnPaid"
                                    claim_amount = 0.00
                                data = {
                                    "Id": id,
                                    "Date": exp['Created_Date'],
                                    "Employee_Id": employeeId,
                                    "EmployeeName": employee_name,
                                    "ExpenseAccount": x['accountName'],
                                    "Account_Head": account_head,
                                    "Description": exp['Account_Description'],
                                    "Amount_Paid": x['amount'],
                                    "Amount_Claimed": claim_amount,
                                    "ApprovedBy": exp['Approved_By'],
                                    "Status": approvestatus
                                }
                                result.append(data)
                                id += 1
            output = JSONRenderer().render(result)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)   



class BusinessUnitCommisionsPaid(ListAPIView):
    queryset = Budget.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data = request.data
            period = data['Year']
            module = Budget.objects.filter(
                Q(Partner_Id__exact=data['Partner_Id']), Q(Created_Date__year__range=period),Is_Deleted=False)
            serialize = BudgetSerialzers(module, many=True)
            commisionlists = serialize.data
            result = []
            id1 = 1
            for comm in commisionlists:
                partner = comm['Partner_Id']
                partnerId = partner['Partner_Id']
                partnerName = partner['Partner_Name']
                totalSales = comm['Sales_Target']
                totalExpense = comm['Expense']
                grossMargin = comm['Gross_Margin']
                commision = comm['Increment']
                if comm['Budget_Methods'] == "Budget fixed % increment month to month":
                    totalAmount = round(grossMargin * (commision / 100), 2)
                elif comm['Budget_Methods'] == "Budget fixed % increment last year":
                    commision = round(int(comm['Increment'])/12, 2)
                    totalAmount = round(grossMargin * (commision / 100), 2)
                elif comm['Budget_Methods'] == "Budget fixed amount increment month to month":
                    totalAmount = grossMargin + commision
                else:
                    totalAmount = round(grossMargin * (commision / 100), 2)
 
                paid_date = comm['Created_Date']
                #print(paid_date)
                paid_date_obj = datetime.strptime(paid_date, '%Y-%m-%d')
                month = paid_date_obj.strftime('%B')
                year = paid_date_obj.strftime('%Y')
                #print(month,year)
                data = {
                    "Month": month,
                    "TotalSales": totalSales,
                    "TotalExpense": totalExpense,
                    "GrossProfit": grossMargin,
                    "Commision%": commision,
                    "Amount": totalAmount,                        
                    "DateofPermission": paid_date
                }
                result.append(data)                  
            result1={}
            for item in result:
                #print(item)
                month = item['Month']
                totalSales = item['TotalSales']
                totalExpense = item['TotalExpense']
                grossMargin = item['GrossProfit']                    
                commision = item['Commision%']
                totalAmount = item['Amount']
                if month in result1:
                    result1[month]['TotalSales'] = totalSales
                    result1[month]['TotalExpense'] += totalExpense
                    result1[month]['GrossProfit'] += grossMargin
                    result1[month]['Commision%'] += commision
                    result1[month]['Amount'] += totalAmount
                else:
                    result1[month] = {
                        'TotalSales': totalSales,
                        'TotalExpense': totalExpense,
                        'GrossProfit': grossMargin,
                        'Commision%': commision,
                        'Amount': totalAmount
                    }
            response_data = []
            for month, values in result1.items():
                    data1 = {
                        "Id"              : id1,
                        "Month"           : month,
                        "Partner_Id"      : partnerId,
                        "PartnerName"     : partnerName,
                        "TotalSales"      : values['TotalSales'],
                        "TotalExpense"    : values['TotalExpense'],
                        "GrossAmount"     : values['GrossProfit'],
                        "Commision_%"     : values['Commision%'],
                        "Commision_Amount": values['Amount'],
                        "DateofPermission": paid_date,
                    }
                    response_data.append(data1)
                    id1 += 1
            output = JSONRenderer().render(response_data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)   



class BusinessUnitProfitLoss(ListAPIView):
    queryset = Budget.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data = request.data
            period = data['Year']
            module = Budget.objects.filter(
                Q(Partner_Id__exact=data['Partner_Id']), Q(Created_Date__year__range=period),Is_Deleted=False)
            serialize = BudgetSerialzers(module, many=True)
            commisionlists = serialize.data
            result = []
            id1 = 1
            for comm in commisionlists:
                partner = comm['Partner_Id']
                partnerId = partner['Partner_Id']
                partnerName = partner['Partner_Name']
                totalSales = comm['Sales_Target']
                totalExpense = comm['Expense']
                # grossMargin = totalSales - totalExpense
                # percentage = int((grossMargin / totalSales) * 100)

                paid_date = comm['Created_Date']
                #print(paid_date)
                paid_date_obj = datetime.strptime(paid_date, '%Y-%m-%d')
                month = paid_date_obj.strftime('%B')
                year = paid_date_obj.strftime('%Y')
                #print(month,year)
                data = {
                    "Month": month,
                    "TotalSales": totalSales,
                    "TotalExpense": totalExpense
                }
                result.append(data)                 
            result1={}
            for item in result:
                month = item['Month']
                totalSales1 = item['TotalSales']
                totalExpense1 = item['TotalExpense']
                if month in result1:
                    result1[month]['TotalSales'] += totalSales1
                    result1[month]['TotalExpense'] += totalExpense1
                else:
                    result1[month] = {
                        'TotalSales': totalSales1,
                        'TotalExpense': totalExpense1,
                    }
            response_data = []
            for month, values in result1.items():
                #print(values)
                pl = values['TotalSales'] - values['TotalExpense']
                if pl > 0:
                    profit_loss = "Profit"
                else:
                    profit_loss = "Loss"
                grossMargin = pl
                percentage = round((grossMargin / values['TotalSales']) * 100, 2)
                formatted_percentage = f"{abs(percentage):.2f}%"

                data1 = {
                    "Id"              : id1,
                    "Month"           : month,
                    "Partner_Id"      : partnerId,
                    "PartnerName"     : partnerName,
                    "TotalSales"      : values['TotalSales'],
                    "TotalExpense"    : values['TotalExpense'],
                    "GrossAmount"     : grossMargin,
                    "P_L"             : profit_loss,
                    "Percentage"      : formatted_percentage,
                    "DateofPermission": paid_date,
                }
                response_data.append(data1)
                id1 += 1
            output = JSONRenderer().render(response_data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)   





class BusinessUnitServices(ListAPIView):
    queryset = RoasterMaster.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data = request.data
            one_time = data['Onetime']
            subscription = data['Subscription']
            module = RoasterMaster.objects.filter(
                Q(Partner_Id__exact=data['Partner_Id']), Q(Created_Date__year__range=data['Year']),Is_Deleted=False)
            serialize = RoasterMasterSerialzers(module, many=True)
            employeelists = serialize.data
            result = []
            id = 1
            for service in employeelists:
                allocations = service['Allocations']
                base_amount = Services.objects.filter(Service_Name=service['Service_Name'])
                serialized_data = ServicesSerialzer(base_amount, many=True)
                amount_lists = serialized_data.data
                for i in allocations:
                    user = service['Employee_Id']
                    for j in amount_lists:
                        print(j['Ontime'])
                        if one_time and j['Ontime'] and j['Service_Name'] == service['Service_Name']:
                            amount = j['Ontime_Baseprice']
                        elif subscription and j['Subscription'] and j['Service_Name'] == service['Service_Name']:
                            amount = j['Subscription_Baseprice']
                        else:
                            continue
                        
                        data = {
                            "Id": id,
                            "Date": service['Created_Date'],
                            "CustomerName": i['customerName'],
                            "ServiceName": service['Service_Name'],
                            "Location": service['Location'],
                            "Amount": amount,
                            "Time": i['slotTime'],
                            "ServicedBy": user['Employee_FirstName']
                        }
                        result.append(data)
                        id += 1
            output = JSONRenderer().render(result)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)  


class BusinessUnitSalesOrderCancellation(APIView):
    queryset = SalesOrder.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data = request.data
            module = SalesOrder.objects.filter(
                Q(Partner_Id=data['Partner_Id']),
                Q(Created_Date__year__range=data["Year"]),
                Is_Deleted=False
            )
            serialize = SalesOrderSerialzers(module, many=True)
            orderlists = serialize.data
            result = []
            id = 1
            for order in orderlists:
                customers = order['Customer_Id']
                customer_name = customers['Salutation'] + "." + customers['Cus_FirstName']
                data = {
                    "Id": id,
                    "Date": order['SalesOrderDate'],
                    "Order_Id": order['SalesOrder_Id'],
                    "Customer_Name": customer_name,
                    "Order_Cancelled_By": order['Order_Cancelledby'],
                    "Order_Amount": order['Amount_Paid'],
                    "Reason": order['Cancellation_Reason'],
                    "Status": order['Amount_Refunded']
                }
                result.append(data)
                id += 1
            output = JSONRenderer().render(result)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)   



class BusinessUnitSalesOrderCancellationPDF(APIView):
    queryset = SalesOrder.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data = request.data
            module = SalesOrder.objects.filter(
                Q(Partner_Id=data['Partner_Id']),
                Q(Created_Date__year__range=data["Year"]),
                Is_Deleted=False
            )
            serialize = SalesOrderSerialzers(module, many=True)
            orderlists = serialize.data
            result = []
            buffer = BytesIO()
            pdf = canvas.Canvas(buffer)
            for order in orderlists:
                customers = order['Customer_Id']
                customer_name = customers['Salutation'] + "." + customers['Cus_FirstName']
                data = {
                    "Date": order['SalesOrderDate'],
                    "Order_Id": order['SalesOrder_Id'],
                    "Customer_Name": customer_name,
                    "Order_Cancelled_By": order['Order_Cancelledby'],
                    "Order_Amount": order['Amount_Paid'],
                    "Reason": order['Cancellation_Reason'],
                    "Status": order['Amount_Refunded']
                }
                result.append(data)
                pdf.drawString(100, 750, f"Order Date : {data['Date']}")
                pdf.drawString(100, 730, f"Order ID : {data['Order_Id']}")
                pdf.drawString(100, 710, f"Customer Name : {data['Customer_Name']}")
                pdf.drawString(100, 690, f"Order Cancelled By : {data['Order_Cancelled_By']}")
                pdf.drawString(100, 670, f"Order Amount : {data['Order_Amount']}")
                pdf.drawString(100, 650, f"Reason : {data['Reason']}")
                pdf.drawString(100, 630, f"Status : {data['Status']}")

                # Move to the next page if there is more data
                if len(result) < len(orderlists):
                    pdf.showPage()

            pdf.save()
            buffer.seek(0)
            response = HttpResponse(content_type='application/pdf')
            response['Content-Disposition'] = 'attachment; filename="orders.pdf"'   
            response.write(buffer.getvalue())
            return response
            # output = JSONRenderer().render(result)
            # return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST) 

  

class Bussinessunitrefundlist(ListAPIView):
    queryset = SalesOrder.objects.all()
    serializer_class = SalesOrderSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]  
      
    def get_queryset(self):
        Partner_Id=self.request.GET.get("Partner_Id")
        module = SalesOrder.objects.filter(Q(Partner_Id=Partner_Id) & Q(Amount_Refunded=True)).order_by('-id')
        return module
            

# budget for business unit


def calculate_total_budget(cost):
    total_budget = sum(cost) if isinstance(cost, list) else 0
    return total_budget



class BusinessUnitGroupBudget(APIView):
    queryset = Budget.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            partner_id = data['Partner_Id']
            year = data['Year']

            module = Budget.objects.filter(
                Q(Partner_Id=partner_id),
                Q(Created_Date__year__range=year),
                Is_Deleted=False
            )
            serialize = BudgetSerialzers(module, many=True)
            group_budget_lists = serialize.data

            monthly_budgets = {
                'Service_Cost': [0] * 12,
                'Material_Cost': [0] * 12,
                'Operational_Cost': [0] * 12,
                'IT_Cost': [0] * 12,
                'Business_Development_Cost': [0] * 12,
                'Shared_Service_Cost': [0] * 12,
                'Total_Cost': [0] * 12
            }

            for i in group_budget_lists:
                created_date = datetime.strptime(i['Created_Date'], '%Y-%m-%d')
                month = created_date.month

                service_cost = i['Service_Cost']
                monthly_budgets['Service_Cost'][month - 1] += calculate_total_budget(service_cost)

                material_cost = i['Material_Cost']
                monthly_budgets['Material_Cost'][month - 1] += calculate_total_budget(material_cost)

                operational_cost = i['Operational_Cost']
                monthly_budgets['Operational_Cost'][month - 1] += calculate_total_budget(operational_cost)

                bd_cost = i['Business_Development_Cost']
                monthly_budgets['Business_Development_Cost'][month - 1] += calculate_total_budget(bd_cost)

                it_cost = i['IT_Cost']
                monthly_budgets['IT_Cost'][month - 1] += calculate_total_budget(it_cost)

                ss_cost = i['Shared_Service_Cost']
                monthly_budgets['Shared_Service_Cost'][month - 1] += calculate_total_budget(ss_cost)

            total_cost = [sum(budget) for budget in zip(*monthly_budgets.values())]
            monthly_budgets['Total_Cost'][month - 1] += calculate_total_budget(total_cost)

            month_names = list(calendar.month_name)[1:]
            result = []
            id1 = 1

            output = {
                "Service_Cost": {month: budget for month, budget in zip(month_names, monthly_budgets['Service_Cost'])},
                "Material_Cost": {month: budget for month, budget in zip(month_names, monthly_budgets['Material_Cost'])},
                "Operational_Cost": {month: budget for month, budget in zip(month_names, monthly_budgets['Operational_Cost'])},
                "Business_Development_Cost": {month: budget for month, budget in zip(month_names, monthly_budgets['Business_Development_Cost'])},
                "IT_Cost": {month: budget for month, budget in zip(month_names, monthly_budgets['IT_Cost'])},
                "Shared_Service_Cost": {month: budget for month, budget in zip(month_names, monthly_budgets['Shared_Service_Cost'])},
                "Total_Cost": {month: budget for month, budget in zip(month_names, monthly_budgets['Total_Cost'])},
            }

            result = [{
                "Id": id1,
                "Total_Budget": output,
                "Actual_Budget": "",
                "Variance_Budget": ""
            }]
            id1 += 1

            output1 = JSONRenderer().render(result)
            return HttpResponse(output1, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output1, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)



class BusinessUnitBudgetCC(APIView):
    queryset = Budget.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data = request.data
            module = Budget.objects.filter(
                Q(Partner_Id=data['Partner_Id']),
                Q(Financial_Year=data['Financial_Year']),
                Q(CostCenterBudget_Id=data['CostCenterBudget_Id']),
                Q(Created_Date__year__range=data["Year"]),
                Is_Deleted=False
            )
            serialize = BudgetSerialzers(module, many=True)
            expenselists = serialize.data
            result = []
            id = 1
            for exp in expenselists:
                budget_heads = exp['Budgethead_Id']
                budget_head_lists = budget_heads['Distributionbudget_Id']
                for i in budget_head_lists:
                    distribution_lists = Distributionbudget.objects.filter(id=i)
                    distrib_budget_lists = DistributionbudgetSerialzers(distribution_lists, many=True).data
                    for j in distrib_budget_lists:
                        expenses = j['Accountant']
                        for k in expenses:
                            account_type = k['Account_Type']
                            account_type1 = "Expense"
                            if account_type == account_type1:
                                expense_group = k['Account_Group']
                                amount = k.get('Opening_Balance', 0)
                                date = k['Created_Date']
                                paid_date_obj = datetime.strptime(date, '%Y-%m-%d')
                                month = paid_date_obj.strftime('%B')
                                data = {
                                    "Id": id,
                                    "Expense_Group": expense_group,
                                    "Month": month,
                                    "Amount": amount
                                }
                                result.append(data)
            month_names = list(calendar.month_name)[1:]
            result1 = {}
            for item in result:
                month = item['Month']
                expense_group = item['Expense_Group']
                amount = item['Amount'] 
              
                if month and expense_group in result1:
                    result1[expense_group]['Expense_Group'] = expense_group
                    result1[expense_group]['Month'] = month
                    result1[expense_group]['Amount'] += amount
                else:
                    result1[expense_group] = {
                        'Expense_Group': expense_group,
                        'Month': month,
                        'Amount': amount
                    }

            response_data = []
            for expense_group, values in result1.items():
                    data1 = {
                        "Id"           : id,
                        "Expense_Group": expense_group,
                        "Month"        : values['Month'],
                        "Amount"       : values['Amount']
                    }
                    response_data.append(data1)
                    id += 1

            output = JSONRenderer().render(response_data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)  



# purchase for business unit
class BusinessUnitPurchasePO(APIView):
    queryset = PODetails.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data = request.data
            module = PODetails.objects.filter(
                Q(Partner_Id=data['Partner_Id']),
                Q(Created_Date__year__range=data["Year"]),
                Is_Deleted=False
            )
            serialize = PODetailsSerialzers(module, many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)   



class BusinessUnitPurchasePR(APIView):
    queryset = PRDetails.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data = request.data
            module = PRDetails.objects.filter(
                Q(Partner_Id=data['Partner_Id']),
                Q(Created_Date__year__range=data["Year"]),
                Is_Deleted=False
            )
            serialize = PRDetailsSerialzers(module, many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)   



# inventory
class BusinessUnitInventoryStocks(APIView):
    queryset = ItemMaster.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data = request.data
            module = ItemMaster.objects.filter(
                Q(Partner_Id=data['Partner_Id']),
                Q(Created_Date__year__range=data["Year"]),
                Is_Deleted=False
            )
            serialize = ItemMasterSerialzers(module, many=True)
            itemlists = serialize.data
            result = []
            id = 1
            for item in itemlists:
                created_by = item['Created_By']
                employee_name = created_by['first_name']
            
                data = {
                    "Id": id,
                    "Date": item['Created_Date'][:10],
                    "Item_Code": item['Item_Id'],
                    "Item_Name": item['Item_Name'],
                    "UOM": item['UOM'],
                    "Item_Group_Name": item['Item_GroupName'],
                    "Unit_Rate": item['Unit_Price'],
                    "Opening_Stock": item['Opening_Stock'],
                    "EmployeeName": employee_name,
                }
                result.append(data)
                id += 1
            output = JSONRenderer().render(result)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)   




class BusinessUnitGoodsReceipt(APIView):
    queryset = GoodsTrfReceiptDetails.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data = request.data
            module = GoodsTrfReceiptDetails.objects.filter(
                Q(Partner_Id=data['Partner_Id']),
                Q(Created_Date__year__range=data["Year"]),
                Is_Deleted=False
            )
            serialize = GoodsTrfReceiptDetailsSerialzers(module, many=True)
            receiptlists = serialize.data
            result = []
            id = 1
            for receipt in receiptlists:
                created_by = receipt['Created_By']
                employee_name = created_by['first_name']
                po_nos = receipt['PO_Id']
                po_id = po_nos['PO_Id']
                suppliers = receipt['Supplier_Name']
                supplier_name = suppliers['Company_Name']

                item_names = receipt['Item']
                
                for item_name in item_names:
                    items = ItemMaster.objects.filter(Item_Name=item_name['Item_Name'])
                    serialized_data = ItemMasterSerialzers(items, many=True).data

                    for item_data in serialized_data:
                        data = {
                            "Id": id,
                            "Date": receipt['Created_Date'],
                            "Item_Code": item_data['Item_Id'],
                            "Item_Name": item_data['Item_Name'],
                            "UOM": item_data['UOM'],
                            "Receipt_No": receipt['TrfIn_Id'],
                            "PO_No": po_id,
                            "Supplier_Name": supplier_name,
                            "Quantity": item_name['Quantity'],
                            "EmployeeName": employee_name,
                        }
                        result.append(data)
                        id += 1
            output = JSONRenderer().render(result)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


# Email purpose we create this api
class PurchaseApprovalsystem(APIView):
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            # Workflow_Id=data['Workflow_Id']
            Employee_Id=data['Employee_Id']
            # print(Depname['Department_Head'])
            # html =f'{Base_Url}/inventory/material-request/document-approval'
            Created_By=data['Created_By']
            first_name=data['approver_name']
            Approverid=data['Approverid']
            module=User.objects.get(id=Approverid)
            serialize = UserSerializer(module)
            userdata=serialize.data
            # use=userdata[0]
            To=userdata['email']
            module1 = MailConfiguration.objects.get(Created_By=Created_By)
            serialize = MailConfigurationSerializer(module1)
            com=serialize.data
            From=com['MailId']
            subject=data['mailsubject']
            # Body=html
            Host=com['SMTPHost']
            Port = com['SMTPPort']
            Password=com['MailPassword']
            if Employee_Id != "":
                module = EmployeeMaster.objects.get(Employee_Id=Employee_Id)
                serialize = EmployeeMasterSerialzer(module)
                emp=serialize.data
                Dep_id=emp['Department_Id']
                module = Department.objects.get(id=Dep_id)
                serialize = DepartmentSerialzer(module)
                Depname=serialize.data
                module = User.objects.get(id=Depname['Updated_By'])
                serialize = UserSerializer(module)
                head=serialize.data
                To=head['email']
                html = data['mailhtml'].replace('{{url}}',f'{Base_Url}/purchase/document-approval/?Document_Type='+str(data['Document_Type'])+"&id="+str(data['id'])+"&Initiated_By=" +data['Initiated_By']+"&Url=" +data['Url']+"&Comments=" +data['Comments']+"&index="+str(data['index'])+"&documentapprovalid="+data['documentapprovalid'] +"&Designation="+head['Designation']+"&Headname="+head['first_name'])
                Body = html
                output = {"Headname":head['first_name'],"rolename":head['Designation'],"Message": "Mail sent Successfully","id":head['id']}
                response = SendMail(subject, From, To,Body, Host, Port,Password)
                return HttpResponse(JSONRenderer().render(output),
                                            content_type='application/json', status=status.HTTP_200_OK)
            else:
                html = data['mailhtml'].replace('{{url}}',f'{Base_Url}/purchase/document-approval/?Document_Type='+str(data['Document_Type'])+"&id="+str(data['id'])+"&Initiated_By=" +data['Initiated_By']+"&Url=" +data['Url']+"&Comments=" +data['Comments']+"&index="+str(data['index'])+"&documentapprovalid="+data['documentapprovalid']+"&Designation="+data['Designation']+"&Headname="+data['Headname'])
                Body = html
                To=userdata['email']
                response = SendMail(subject, From, To,Body, Host, Port,Password)
                return HttpResponse(JSONRenderer().render({"Message": "Mail sent Successfully"}),content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            return HttpResponse(JSONRenderer().render({"Message": "Mail Not sent"}),content_type='application/json', status=status.HTTP_200_OK)

class PurchaseRequestapproval(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            #data=request
            id=data['id']
            module=PRDetails.objects.get(id=id)
            serialize = PRDetailsSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
    # edit the purchase request
    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            #data=request
            id=data['id']
            module=PRDetails.objects.get(id=id)
            serialize = PRDetailsSerialzers(instance=module,data=data)
            decoded_data = base64.b64decode((data['UploadFile']))
            data['UploadFile'] = ContentFile(decoded_data, name=data['User_File_Name'])
            del data['User_File_Name']
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
            return HttpResponse(str(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class Purchaserequestworkflow(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            Request_Id=data['Request_Id']
            module = Documentapproval.objects.filter(Request_Id=Request_Id)
            serialize = DocumentapprovalSerialzers(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class PurchaseOrderapproval(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            #data=request
            id=data['id']
            module=PODetails.objects.get(id=id)
            serialize = PODetailsSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
    # edit the purchase order
    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            #data=request
            id=data['id']
            module=PODetails.objects.get(id=id)
            serialize = PODetailsSerialzers(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            print('s',serialize.errors)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Refundapprovalsystem(APIView):
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            # Workflow_Id=data['Workflow_Id']
            Employee_Id=data['Employee_Id']
            # print(Depname['Department_Head'])
            # html =f'{Base_Url}/inventory/material-request/document-approval'
            Created_By=data['Created_By']
            first_name=data['approver_name']
            Approverid=data['Approverid']
            module=User.objects.get(id=Approverid)
            serialize = UserSerializer(module)
            userdata=serialize.data
            # use=userdata[0]
            To=userdata['email']
            module1 = MailConfiguration.objects.get(Created_By=Created_By)
            serialize = MailConfigurationSerializer(module1)
            com=serialize.data
            From=com['MailId']
            subject=data['mailsubject']
            # Body=html
            Host=com['SMTPHost']
            Port = com['SMTPPort']
            Password=com['MailPassword']
            if Employee_Id != "":
                module = EmployeeMaster.objects.get(Employee_Id=Employee_Id)
                serialize = EmployeeMasterSerialzer(module)
                emp=serialize.data
                Dep_id=emp['Department_Id']
                module = Department.objects.get(id=Dep_id)
                serialize = DepartmentSerialzer(module)
                Depname=serialize.data
                module = User.objects.get(id=Depname['Updated_By'])
                serialize = UserSerializer(module)
                head=serialize.data
                To=head['email']
                html = data['mailhtml'].replace('{{url}}',f'{Base_Url}/sales/DocumentApprovalForRefund/?Document_Type='+str(data['Document_Type'])+"&id="+str(data['id'])+"&Initiated_By=" +data['Initiated_By']+"&Url=" +data['Url']+"&Comments=" +data['Comments']+"&index="+str(data['index'])+"&documentapprovalid="+data['documentapprovalid'] +"&Designation="+head['Designation']+"&Headname="+head['first_name'])
                Body = html
                output = {"Headname":head['first_name'],"rolename":head['Designation'],"Message": "Mail sent Successfully","id":head['id']}
                response = SendMail(subject, From, To,Body, Host, Port,Password)
                return HttpResponse(JSONRenderer().render(output),
                                            content_type='application/json', status=status.HTTP_200_OK)
            else:
                html = data['mailhtml'].replace('{{url}}',f'{Base_Url}/sales/DocumentApprovalForRefund/?Document_Type='+str(data['Document_Type'])+"&id="+str(data['id'])+"&Initiated_By=" +data['Initiated_By']+"&Url=" +data['Url']+"&Comments=" +data['Comments']+"&index="+str(data['index'])+"&documentapprovalid="+data['documentapprovalid']+"&Designation="+data['Designation']+"&Headname="+data['Headname'])
                Body = html
                To=userdata['email']
                response = SendMail(subject, From, To,Body, Host, Port,Password)
                return HttpResponse(JSONRenderer().render({"Message": "Mail sent Successfully"}),content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            return HttpResponse(JSONRenderer().render({"Message": "Mail Not sent"}),content_type='application/json', status=status.HTTP_200_OK)



class POAmendmentapproval(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            #data=request
            id=data['id']
            module=POAmendDetails.objects.get(id=id)
            serialize = POAmendDetailsSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
    # edit the po amendment
    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            #data=request
            id=data['id']
            module=POAmendDetails.objects.get(id=id)
            serialize = POAmendDetailsSerialzers(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            print('s',serialize.errors)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class POCancellationapproval(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            #data=request
            id=data['id']
            module=POCancelDetails.objects.get(id=id)
            serialize = POCancelDetailsSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
    # edit the po cancellation
    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            #data=request
            id=data['id']
            module=POCancelDetails.objects.get(id=id)
            serialize = POCancelDetailsSerialzers(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            print('s',serialize.errors)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


# api for cc budget module
class CCBudgetCRUD(ListAPIView):
    queryset = CostCenterBudget.objects.all()
    serializer_class = CostCenterBudgetSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]
    # create a cc budget
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            # data=request
            if data == {}:
                raise TypeError
            serialize = CostCenterBudgetSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
            print(serialize.errors)
            return HttpResponse(str(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
    # view the cc budget
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            #data=request
            id=data['id']
            module=CostCenterBudget.objects.get(id=id)
            serialize = CostCenterBudgetSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
    # edit the cc budget
    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            #data=request
            id=data['id']
            module=CostCenterBudget.objects.get(id=id)
            serialize = CostCenterBudgetSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            print('s',serialize.errors)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
    # partial edit the cc budget
    def patch(self,request,*args,**kwargs):
        try:
            data=request.data
            #data=request
            id=data['id']
            module=CostCenterBudget.objects.get(id=id)
            serialize = CostCenterBudgetSerialzer(instance=module,data=data,partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
    # delete the cc budget
    def delete(self, request, *args, **kwargs):
        try:
            data = request.query_params
            #data=request
            id = data['id']
            module = CostCenterBudget.objects.get(id=id)
            serialize = CostCenterBudgetSerialzer(
                module, {"Is_Deleted": True}, partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)

class CCBudgetMyLists(ListAPIView):
    queryset = CostCenterBudget.objects.all()
    serializer_class = CostCenterBudgetSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]
    # my cc budget list api
    def get_queryset(self):
        try:
            data = self.request.query_params
            #data=self.request
            module = CostCenterBudget.objects.filter(Company_Id=data['Company_Id'], Created_By=data['Created_By'],
                                              Is_Deleted=False).order_by('-id')
            return module
        except:
            module = CostCenterBudget.objects.none()
            return module


class CCBudgetMyListswithouttoken(APIView):
    # my cc budget list api
    def get_queryset(self):
        try:
            data = self.request.query_params
            #data=self.request
            module = CostCenterBudget.objects.filter(Q(Company_Id=data['Company_Id']) &Q(Partner_Id=data['Partner_Id'])& Q( Created_By=data['Created_By']) & Q(Is_Deleted=False)).order_by('-id')
            serialize = CostCenterBudgetSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)                                  
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})                                  
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)



class CCBudgetallLists(ListAPIView):
    queryset = CostCenterBudget.objects.all()
    serializer_class = CostCenterBudgetSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]
    # my cc budget list api
    def get_queryset(self):
        try:
            data = self.request.query_params
            #data=self.request
            module = CostCenterBudget.objects.filter(Company_Id=data['Company_Id'],
                                              Is_Deleted=False).order_by('-id')
            return module
        except:
            module = CostCenterBudget.objects.none()
            return module





class CCBudgetsearch(ListAPIView):
    queryset = CostCenterBudget.objects.all()
    serializer_class = CostCenterBudgetSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]
    # my cc budget list api
    def get_queryset(self):
            data = self.request.GET.get('search')
            #data=self.request
            module = CostCenterBudget.objects.filter(Q(Partner_Name__icontains=data)|Q(CostCenterBudget_Id__icontains=data)|Q(Budget_Period__icontains=data) | Q(Costcenter_Name__icontains=data)
                |Q(Account_Head__icontains=data),Is_Deleted=False).order_by('-id')
            return module



class CCBudgetfilter(ListAPIView):
    queryset = CostCenterBudget.objects.all()
    serializer_class = CostCenterBudgetSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        data=request.data
        data1={"Created_Date__range":data['Created_Date'],"Partner_Name__icontains":data['Partner_Name'],"Costcenter_Name__icontains":data['Costcenter_Name'],"CostCenterBudget_Id__icontains":data['CostCenterBudget_Id'],"Is_Deleted":False}
        return requestget(model=CostCenterBudget,serializer=CostCenterBudgetSerialzers,data=data1,page=data['page'],Operator='and')
        


class ApprovedPRlist(ListAPIView):
    queryset = PRDetails.objects.all()
    serializer_class = PRDetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data=self.request.GET.get('Partner_Id')
        module = PRDetails.objects.filter(Q(Partner_Id=data) & Q(PO_closed=False) & Q(Approved_Flag=True) & Q(Is_Deleted=False)).order_by('-id')
        return module



class PendingPRlist(ListAPIView):
    queryset = PRDetails.objects.all()
    serializer_class = PRDetailsSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data=self.request.GET.get('Partner_Id')
        module = PRDetails.objects.filter(Q(Partner_Id=data) & Q(Approved_Flag=False) & Q(Is_Deleted=False)).order_by('-id')
        return module

class ApprovedPRlistsearch(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = PRDetails.objects.filter((Q(RequestedBy__icontains=data['search'])|Q(Department_Id__Department_Name__icontains=data['search']) 
                |Q(PR_Id__icontains=data['search']) | Q(Store_Name__icontains=data['search'])) & Q(Partner_Id=data['Partner_Id']) & Q(Approved_Flag=True) & Q(PO_closed=False) & Q(Is_Deleted=False)).order_by('-id')
            serialize = PRDetailsSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class PendingPRlistsearch(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = PRDetails.objects.filter((Q(RequestedBy__icontains=data['search'])|Q(Department_Id__Department_Name__icontains=data['search']) 
                |Q(PR_Id__icontains=data['search']) | Q(Store_Name__icontains=data['search']))&Q(Partner_Id=data['Partner_Id']) & Q(Approved_Flag=False) & Q(Is_Deleted=False)).order_by('-id')
            serialize = PRDetailsSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class EmailattachementCRUD(APIView):
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            encode_data=base64.b64decode(data['Attachfile'])
            data['Attachfile']=ContentFile(encode_data,name=data['File_Name'])
            del data['File_Name']
            serialize1=EmailattachmentSerialzer(data=data)
            if serialize1.is_valid():
                serialize1.save()
            output = JSONRenderer().render(serialize1.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Emailattachment.objects.get(Attachfile=data['Attachfile'])
            serialize = EmailattachmentSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Emailattachment.objects.get(id=data['id']).delete
            output = JSONRenderer().render("Successfully Deleted")
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)
    



class RFQwithattachment(APIView):
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            filename=data['filename']
            Created_By=data['Created_By']
            url = "http://185.190.140.163:8000/media/" + str(filename)
            module1 = MailConfiguration.objects.get(Created_By=Created_By)
            serialize = MailConfigurationSerializer(module1)
            com=serialize.data
            From=com['MailId']
            subject=data['mailsubject']
            Host=com['SMTPHost']
            Port = com['SMTPPort']
            Password=com['MailPassword']
            To=data['To']
            Body=data['Body'].replace('{{url}}',url)
            response = SendMail1(subject, From, To,Body, Host, Port,Password)
            return HttpResponse(JSONRenderer().render({"Message": "Mail sent Successfully"}),
                                            content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            return HttpResponse(JSONRenderer().render({"Message": "Mail Not sent"}),
                                        content_type='application/json', status=status.HTTP_200_OK)


class POwithattachment(APIView):
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            filename=data['filename']
            Created_By=data['Created_By']
            url = "http://185.190.140.163:8000/media/" + str(filename)
            module1 = MailConfiguration.objects.get(Created_By=Created_By)
            serialize = MailConfigurationSerializer(module1)
            com=serialize.data
            From=com['MailId']
            subject=data['mailsubject']
            Host=com['SMTPHost']
            Port = com['SMTPPort']
            Password=com['MailPassword']
            To=data['To']
            Body=data['Body'].replace('{{url}}',url)
            response = SendMail(subject, From, To,Body, Host, Port,Password)
            return HttpResponse(JSONRenderer().render({"Message": "Mail sent Successfully"}),
                                            content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            return HttpResponse(JSONRenderer().render({"Message": "Mail Not sent"}),
                                        content_type='application/json', status=status.HTTP_200_OK)




class Approvalrejectmail(APIView):
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Created_By=data['Created_By']
            module1 = MailConfiguration.objects.get(Created_By=Created_By)
            serialize = MailConfigurationSerializer(module1)
            com=serialize.data
            To=com['MailId']
            subject=data['mailsubject']
            # Body=html
            Host=settings.EMAIL_HOST
            Port = settings.EMAIL_PORT
            Password=settings.EMAIL_HOST_PASSWORD
            From=settings.EMAIL_HOST_USER
            html = data['mailhtml']          
            Body = html
            output = {"Message": "Mail sent Successfully"}
            response = SendMail(subject, From, To,Body, Host, Port,Password)
            return HttpResponse(JSONRenderer().render(output),
                                            content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            return HttpResponse(JSONRenderer().render({"Message": "Mail Not sent"}),
                                        content_type='application/json', status=status.HTTP_200_OK)



class SalesOrderCRUD(ListAPIView):
    queryset = SalesOrder.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = SalesOrderSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
            return HttpResponse(JSONRenderer().render(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['SalesOrder_Id']
            module = SalesOrder.objects.get(SalesOrder_Id=id)
            serialize = SalesOrderSerialzers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['SalesOrder_Id']
            module = SalesOrder.objects.get(SalesOrder_Id=id)
            serialize = SalesOrderSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
            return HttpResponse(JSONRenderer().render(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['SalesOrder_Id']
            module = SalesOrder.objects.get(SalesOrder_Id=id)
            serialize = SalesOrderSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class SalesOrderList(ListAPIView):
    queryset = SalesOrder.objects.all()
    serializer_class = SalesOrderSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        Company_Id=self.request.GET.get('Company_Id')
        module = SalesOrder.objects.filter(Q(Company_Id=Company_Id) & Q(Is_Deleted=False) & Q(Order_Cancelled=False)).order_by('-id')
        return module

class MyOrderList(ListAPIView):
    queryset = SalesOrder.objects.all()
    serializer_class = SalesOrderSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        Partner_Id=self.request.GET.get('Partner_Id')
        Customer_Id=self.request.GET.get('Customer_Id')
        SalesOrder_Type = self.request.GET.get('SalesOrder_Type')
        module = SalesOrder.objects.filter(Q(Partner_Id=Partner_Id)&Q(Customer_Id=Customer_Id) & Q(SalesOrder_Type=SalesOrder_Type)& Q(Is_Deleted=False) & Q(Order_Cancelled=False)).order_by('-id')
        return module


#PR attachment list api

class PRattachmentlist(ListAPIView):
    queryset = PRDetails.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = PRDetails.objects.get(PR_Id=data['PR_Id'])
            serialize = PRDetailsSerialzer(module)
            output = serialize.data
            data1={"Date":output['PR_Date'],"Document_No":output['PR_Id'],"Attached_By":output['RequestedBy'],"File_Type":"Pdf","file":output['UploadFile']}
            output1 = JSONRenderer().render(data1)
            return HttpResponse(output1,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)



class Vourcharchatofaccountfilterlist(ListAPIView):
    queryset = Voucherdetail.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Voucherdetail.objects.filter(Account__contains=[{"account":data['account']}])
            serialize = VoucherdetailSerialzer(module,many=True)
            output = serialize.data
            module1 = Contravoucher.objects.filter(Account__contains=[{"account":data['account']}])
            serialize1 = ContravoucherSerialzer(module1,many=True)
            output1 = serialize1.data
            module2 = Journal.objects.filter(Account__contains=[{"account":data['account']}])
            serialize2 = JournalSerialzer(module2,many=True)
            output2 = serialize2.data
            serialize3={"Voucher":output,"Contarvouchar":output1,"Journal":output2}
            output3 = JSONRenderer().render(serialize3)
            return HttpResponse(output3,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class PRlistwithoutpagination(ListAPIView):
    queryset = PRDetails.objects.all()
    serializer_class = PRDetailsSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = PRDetails.objects.filter(Q(Company_Id=data['Company_Id']) & Q(Is_Deleted=False))
            serialize = PRDetailsSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output, content_type = 'application/json', status=status.HTTP_400_BAD_REQUEST)


class RFQlistwithoutpagination(ListAPIView):
    queryset = RFQdetails.objects.all()
    serializer_class = RFQdetailsSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = RFQdetails.objects.filter(Q(Company_Id=data['Company_Id']) & Q(Is_Deleted=False))
            serialize = RFQdetailsSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output, content_type = 'application/json', status=status.HTTP_400_BAD_REQUEST)


class Cataloguewithoutpaginationlist(ListAPIView):
    queryset = Catalogue.objects.all()
    serializer_class = CatalogueSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Catalogue.objects.filter(Q(Is_Deleted=False) & Q(Expiry_Flg=False))
            serialize = CatalogueSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output, content_type = 'application/json', status=status.HTTP_400_BAD_REQUEST)



class Updaterecordscrud(ListAPIView):
    queryset = Updaterecords.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            serialize = UpdaterecordsSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_201_CREATED)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, *args, **kwargs):
        try:
            data = request.query_params
            id = data['id']
            module = Updaterecords.objects.get(id=id)
            serialize = UpdaterecordsSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, *args, **kwargs):
        try:
            data = request.data
            id = data['id']
            module = Updaterecords.objects.get(id=id)
            serialize = UpdaterecordsSerialzer(instance=module, data=request.data, partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data = request.query_params
            id =data['id']
            module = Updaterecords.objects.get(id=id)
            serialize = UpdaterecordsSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)


class Updaterecordslist(ListAPIView):
    queryset = Updaterecords.objects.all()
    serializer_class = UpdaterecordsSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        Partner_Id = self.request.GET.get('Partner_Id')
        module = Updaterecords.objects.filter(Q(Partner_Id=Partner_Id) & Q(Is_Deleted = False)).order_by('-id')
        return module


class Updaterecordsfilter(APIView):

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Updaterecords.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Document_Id=data['Document_Id'])).order_by('-id')
            serialize = UpdaterecordsSerialzer(module , many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output, content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)



class Notificationscrud(ListAPIView):
    queryset = Notifications.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            serialize = NotificationsSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_201_CREATED)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, *args, **kwargs):
        try:
            data = request.query_params
            id = data['id']
            module = Notifications.objects.get(id=id)
            serialize = NotificationsSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, *args, **kwargs):
        try:
            data = request.data
            id = data['id']
            module = Notifications.objects.get(id=id)
            serialize = NotificationsSerialzer(instance=module, data=request.data, partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data = request.query_params
            id =data['id']
            module = Notifications.objects.get(id=id)
            serialize = NotificationsSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)



class Notificationslist(ListAPIView):
    queryset = Notifications.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Notifications.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(User_Id=data['User_Id']) & Q(Is_Deleted=False)).order_by('-id')
            serialize = NotificationsSerialzers(module , many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output, content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)


# front end purpose

class Notificationscrudwithoutpagination(APIView):

    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            serialize = NotificationsSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_201_CREATED)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, *args, **kwargs):
        try:
            data = request.query_params
            id = data['id']
            module = Notifications.objects.get(id=id)
            serialize = NotificationsSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, *args, **kwargs):
        try:
            data = request.data
            id = data['id']
            module = Notifications.objects.get(id=id)
            serialize = NotificationsSerialzer(instance=module, data=request.data, partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data = request.query_params
            id =data['id']
            module = Notifications.objects.get(id=id)
            serialize = NotificationsSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)



class Remindercrud(ListAPIView):
    queryset = Reminder.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            serialize = ReminderSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_201_CREATED)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, *args, **kwargs):
        try:
            data = request.query_params
            id = data['id']
            module = Reminder.objects.get(id=id)
            serialize = ReminderSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, *args, **kwargs):
        try:
            data = request.data
            id = data['id']
            module = Reminder.objects.get(id=id)
            serialize = ReminderSerialzer(instance=module, data=request.data, partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data = request.query_params
            id =data['id']
            module = Reminder.objects.get(id=id)
            serialize = ReminderSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)



class Reminderlist(ListAPIView):
    queryset = Reminder.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Reminder.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(User_Id=data['User_Id']) & Q(Is_Deleted=False)).order_by('-id')
            serialize = ReminderSerialzers(module , many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output, content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)

# front end purpose 

class Remindercrudwithouttoken(APIView):

    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            serialize = ReminderSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_201_CREATED)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, *args, **kwargs):
        try:
            data = request.query_params
            id = data['id']
            module = Reminder.objects.get(id=id)
            serialize = ReminderSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, *args, **kwargs):
        try:
            data = request.data
            id = data['id']
            module = Reminder.objects.get(id=id)
            serialize = ReminderSerialzer(instance=module, data=request.data, partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data = request.query_params
            id =data['id']
            module = Reminder.objects.get(id=id)
            serialize = ReminderSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)

class POTransactionfilter(ListAPIView):
    queryset = PODetails.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = POAmendDetails.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(PO_No=data['PO_Id']))
            serialize = POAmendDetailsSerialzer(module,many=True)
            output=serialize.data
            module1 = POCancelDetails.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(PO_No=data['PO_Id']))
            serialize1 = POCancelDetailsSerialzer(module1, many=True)
            output1 = serialize1.data
            module2 = GoodsTrfReceiptDetails.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(PO_Id__PO_Id__icontains=data['PO_Id']))
            serialize2 = GoodsTrfReceiptDetailsSerialzer(module2,many=True)
            output2 = serialize2.data
            data1={"POAmned":output,"POCancel":output1,"Material":output2}
            output3 = JSONRenderer().render(data1)
            return HttpResponse(output3,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'appliocation/json',status=status.HTTP_400_BAD_REQUEST)


class Itemtransactionfilter(ListAPIView):
    queryset = ItemMaster.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = GoodsTrfReceiptDetails.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Item__contains=[{"Item_Name":data['Item_Name']}]))
            serialize = GoodsTrfReceiptDetailsSerialzer(module,many=True)
            output=serialize.data
            module1 = MaterialRequestDetail.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Item__contains=[{"Item_Name":data['Item_Name']}]))
            serialize1 = MaterialRequestDetailSerialzer(module1, many=True)
            output1 = serialize1.data
            module2 = Materialreturndetails.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Item__contains=[{"Item_Name":data['Item_Name']}]))
            serialize2 = MaterialreturndetailsSerialzer(module2,many=True)
            output2 = serialize2.data
            module3 = MaterialTransferDetails.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Item__contains=[{"Item_Name":data['Item_Name']}]))
            serialize3 = MaterialTransferDetailsSerialzer(module2,many=True)
            output3 = serialize3.data
            data1={"GoodsRecipt":output,"MeterialRequest":output1,"Materialreturn":output2,"Materialtransfer":output3}
            output4 = JSONRenderer().render(data1)
            return HttpResponse(output4,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'appliocation/json',status=status.HTTP_400_BAD_REQUEST)


class Itemopeningstock(APIView):

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Storemaster.objects.filter(Q(Store_Name=data['Store_Name']) & Q(Items__contains=[{"Item_Name":data['Item_Name']}]))
            serialize = StoremasterSerialzers(module,many=True)
            output = serialize.data
            out=[]
            for y in output:
                for i in y['Items']:
                    if i['Item_Name'] == data['Item_Name']:
                        out.append({"Opening_Stock":i["Opening_Stock"]})
            output1 = JSONRenderer().render(out)
            return HttpResponse(output1,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output, content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

#Car API functions

class CarCRUD(ListAPIView):
    queryset = Car.objects.all()
    serializer_class = CarSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            decoded_data = base64.b64decode(data['Car_Image'])
            data['Car_Image'] = ContentFile(decoded_data, name=data['Car_Image_Name'])
            del data['Car_Image_Name']
            serialize = CarSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['id']
            module = Car.objects.get(id=id)
            serialize = CarSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            if data['Car_Image'] !='':
                decoded_data = base64.b64decode((data['Car_Image']))
                data['Car_Image'] = ContentFile(decoded_data, name=data['Car_Image_Name'])
                del data['Car_Image_Name']
            id=data['id']
            module=Car.objects.get(id=id)
            serialize = CarSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['id']
            module=Car.objects.get(id=id)
            serialize = CarSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Mycarlist(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Car.objects.filter(Created_By=data['Created_By']).order_by('-Created_By')
            serialize = CarSerialzers(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Financedocumentapproval(APIView):
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            # Workflow_Id=data['Workflow_Id']
            Employee_Id=data['Employee_Id']
            # print(Depname['Department_Head'])
            # html =f'{Base_Url}/inventory/material-request/document-approval'
            Created_By=data['Created_By']
            first_name=data['approver_name']
            Approverid=data['Approverid']
            module=User.objects.get(id=Approverid)
            serialize = UserSerializer(module)
            userdata=serialize.data
            # use=userdata[0]
            To=userdata['email']
            module1 = MailConfiguration.objects.get(Created_By=Created_By)
            serialize = MailConfigurationSerializer(module1)
            com=serialize.data
            From=com['MailId']
            subject=data['mailsubject']
            # Body=html
            Host=com['SMTPHost']
            Port = com['SMTPPort']
            Password=com['MailPassword']
            if Employee_Id != "":
                module = EmployeeMaster.objects.get(Employee_Id=Employee_Id)
                serialize = EmployeeMasterSerialzer(module)
                emp=serialize.data
                Dep_id=emp['Department_Id']
                module = Department.objects.get(id=Dep_id)
                serialize = DepartmentSerialzer(module)
                Depname=serialize.data
                module = User.objects.get(id=Depname['Updated_By'])
                serialize = UserSerializer(module)
                head=serialize.data
                To=head['email']
                html = data['mailhtml'].replace('{{url}}',f'{Base_Url}/finance/DocumentApprovalForSupplier/?Document_Type='+str(data['Document_Type'])+"&id="+str(data['id'])+"&Initiated_By=" +data['Initiated_By']+"&Url=" +data['Url']+"&Comments=" +data['Comments']+"&index="+str(data['index'])+"&documentapprovalid="+data['documentapprovalid'] +"&Designation="+head['Designation']+"&Headname="+head['first_name'])
                Body = html
                output = {"Headname":head['first_name'],"rolename":head['Designation'],"Message": "Mail sent Successfully","id":head['id']}
                response = SendMail(subject, From, To,Body, Host, Port,Password)
                return HttpResponse(JSONRenderer().render(output),
                                            content_type='application/json', status=status.HTTP_200_OK)
            else:
                html = data['mailhtml'].replace('{{url}}',f'{Base_Url}/finance/DocumentApprovalForSupplier/?Document_Type='+str(data['Document_Type'])+"&id="+str(data['id'])+"&Initiated_By=" +data['Initiated_By']+"&Url=" +data['Url']+"&Comments=" +data['Comments']+"&index="+str(data['index'])+"&documentapprovalid="+data['documentapprovalid']+"&Designation="+data['Designation']+"&Headname="+data['Headname'])
                Body = html
                To=userdata['email']
                response = SendMail(subject, From, To,Body, Host, Port,Password)
                return HttpResponse(JSONRenderer().render({"Message": "Mail sent Successfully"}),
                                            content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            return HttpResponse(JSONRenderer().render({"Message": "Mail Not sent"}),
                                        content_type='application/json', status=status.HTTP_200_OK)


class Trailbalancereport(ListAPIView):
    queryset = Chartofaccount.objects.all()
    serializer_class = ChartofaccountSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Voucharmodule = Voucherdetail.objects.filter(Q(Partner_Id=data['Partner_Id'])& Q(Voucher_Date__range=data['Date']))
            Voucherserialize = VoucherdetailSerialzer(Voucharmodule,many=True)
            Voucharof = Voucherserialize.data
            journalmodule = Journal.objects.filter(Q(Partner_Id=data['Partner_Id'])& Q(Journal_Date__range=data['Date']))
            journalserialize = JournalSerialzer(journalmodule,many=True)
            journalof = journalserialize.data
            contravoucharmodule = Contravoucher.objects.filter(Q(Partner_Id=data['Partner_Id'])& Q(Voucher_Date__range=data['Date']))
            contravoucherserialize = ContravoucherSerialzer(contravoucharmodule,many=True)
            contravoucharof = contravoucherserialize.data
            data1={"Vouchar":Voucharof,"Journal":journalof,"Contra":contravoucharof}
            output = JSONRenderer().render(data1)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)


class Customertransactionhistory(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module1 = Customer.objects.get(Customer_Id=data['Customer_Id'])
            customerserialize = CustomerSerialzer(module1)
            id=customerserialize.data
            ordermodule = SalesOrder.objects.filter(Q(Customer_Id=id['id']) & Q(Order_Cancelled=False))
            orderserialize = SalesOrderSerialzer(ordermodule,many=True)
            orderout=orderserialize.data
            cancelmodule = SalesOrder.objects.filter(Q(Customer_Id=id['id']) & Q(Order_Cancelled=True))
            cancelserialize = SalesOrderSerialzer(cancelmodule,many=True)
            cancelout=cancelserialize.data
            data1={"SalesOrder":orderout,"SalesCancel":cancelout}
            output = JSONRenderer().render(data1)
            return HttpResponse(output , content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)


class MonthlyusertargetCRUD(ListAPIView):
    queryset = Monthlyusertarget.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize = MonthlyusertargetSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Monthlyusertarget_Id']
            module = Monthlyusertarget.objects.get(Monthlyusertarget_Id=id)
            serialize = MonthlyusertargetSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Monthlyusertarget_Id']
            module=Monthlyusertarget.objects.get(Monthlyusertarget_Id=id)
            serialize = MonthlyusertargetSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Monthlyusertarget_Id']
            module=Monthlyusertarget.objects.get(Monthlyusertarget_Id=id)
            serialize = MonthlyusertargetSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


# this is used for monthlyuser target api
class Monthlyusertargetlist(ListAPIView):
    queryset = Monthlyusertarget.objects.all()
    serializer_class = MonthlyusertargetSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        Company_Id=self.request.GET.get('Company_Id')
        # Partner_Id = self.request.GET.get('Partner_Id')
        module = Monthlyusertarget.objects.filter(Q(Company_Id=Company_Id)& Q(Is_Deleted=False))
        return module


class Monthlyusertargetlistwithpartner(ListAPIView):
    queryset = Monthlyusertarget.objects.all()
    serializer_class = MonthlyusertargetSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        Company_Id=self.request.GET.get('Company_Id')
        Partner_Id = self.request.GET.get('Partner_Id')
        module = Monthlyusertarget.objects.filter(Q(Company_Id=Company_Id)& Q(Partner_Id=Partner_Id) & Q(Is_Deleted=False))
        return module

class Monthlyusertargetdata(APIView):
    queryset = Monthlyusertarget.objects.all()
    serializer_class = MonthlyusertargetSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get(self,request,*args,**kwargs):
        try:
            data = request.query_params
            module = Monthlyusertarget.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Salespersion__contains=[{"User_Name":data['User_Name']}]) & Q(Is_Deleted=False))
            serialize = MonthlyusertargetSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output =JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)

class Monthlyusertargetsearch(ListAPIView):
    queryset = Monthlyusertarget.objects.all()
    serializer_class = MonthlyusertargetSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get_queryset(self):
        data = self.request.GET.get('search')
        # data = request
        package = Monthlyusertarget.objects.filter(Q(Partner_Name__icontains=data)| Q(Service_Name__icontains=data) | Q(Service_Type__icontains=data)|
            Q(Budget_Name__icontains=data)|Q(Salespersion__contains=[{"User_Name":data}]),Is_Deleted=False)
        return package



class Monthlyusertargetfilter(ListAPIView):
    queryset = Monthlyusertarget.objects.all()
    serializer_class = MonthlyusertargetSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        data = request.data
        # data = request
        data1 = {"Partner_Name__icontains":data['Partner_Name'],"Service_Name__icontains":data['Service_Name'],"Service_Type__icontains":data['Service_Type'],
            "Budget_Name__icontains":data['Budget_Name'],"Created_Date__range":data['Created_Date'],"Is_Deleted":False}
        return requestget(model=Monthlyusertarget,serializer=MonthlyusertargetSerialzer,data=data1,page=data['page'],Operator='and')


class Documentapprovallist(ListAPIView):
    queryset = Documentapproval.objects.all()
    serializer_class = DocumentapprovalSerialzers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data=self.request.GET.get('Approvername')
        module = Documentapproval.objects.filter(Approve__contains=[{"Approvername":data}])
        return module


class Itemgroupwithoutpagination(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = ItemGroup.objects.filter(Q(Is_Deleted=False))
            serialize = ItemGroupSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_404_NOT_FOUND)


class Salestargetzonewise(ListAPIView):
    queryset = SalesOrder.objects.all()
    serializer_class = SalesOrderSerialzer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Partner_Id=data['Partner_Id']
            module = SalesOrder.objects.filter(Q(Partner_Id=Partner_Id) & Q(Order_Cancelled=False)& Q(Is_Deleted=False))
            serialize = SalesOrderSerialzer(module,many=True)
            outputserialize = serialize.data
            print(outputserialize)
            result=[]
            output={}
            for i in outputserialize:
                partnermodule = Partner.objects.get(id=i['Partner_Id'])
                zone = partnermodule.BusinessUnit_Zone
                data1={"Zone":zone,"Amount":i['Amount_Paid']}
                result.append(data1)
            for item in result:
                Zone=item['Zone']
                Amount=item['Amount']
                if Zone in output:
                    output[Zone] +=Amount
                else:
                    output[Zone]= Amount
            output1 = JSONRenderer().render(output)
            return HttpResponse(output1,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class salestargetmonthwise(ListAPIView):
    queryset = SalesOrder.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            Partner_Id=data['Partner_Id']
            module = SalesOrder.objects.filter(Q(Partner_Id=Partner_Id) & Q(Order_Cancelled=False)& Q(Is_Deleted=False))
            serialize = SalesOrderSerialzer(module,many=True)
            outputserialize = serialize.data
            result=[]
            output={}
            for i in outputserialize:
                partnermodule = Partner.objects.get(id=i['Partner_Id'])
                if data['SalesOrder_Type']==i['SalesOrder_Type']:
                    datestring = i['SalesOrderDate']
                    date_object = datetime.strptime(datestring, "%Y-%m-%d")
                    month_name = date_object.strftime("%B")
                    data1={"Month":month_name,"Amount":i['Amount_Paid']}
                    result.append(data1)
            for item in result:
                Month=item['Month']
                Amount=item['Amount']
                if Month in output:
                    output[Month] +=Amount
                else:
                    output[Month]= Amount
            output1 = JSONRenderer().render(output)
            return HttpResponse(output1,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class warcalculation(APIView):
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            module = Storemaster.objects.get(Q(id=data['id']) & Q(Partner_Id=data['Partner_Id']))
            serialize = StoremasterSerialzer(module)
            stock = serialize.data
            stockinhand = stock['Items']
            openingstock = 0
            for i in stockinhand:
                if i['id']==data['Item_Id']:
                    openingstock=openingstock + i['Opening_Stock']
                    break
            module1 = GoodsTrfReceiptDetails.objects.filter(Q(Partner_Id=data['Partner_Id'])&Q(Store_Id=data['id'])).order_by('id').last()
            serialize1 = GoodsTrfReceiptDetailsSerialzer(module1)
            value = serialize1.data
            receiptvalue=0
            for y in value['Item']:
                if y['id']== data['Item_Id']:
                    print(y['Unit_Price'],y['GI_Quantity'])
                    receiptvalue=receiptvalue + y['Unit_Price']
                    openingstock=openingstock + y['GI_Quantity']
                    break
            out1 = receiptvalue // openingstock
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(out1,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)


class customersubscriptionCRUD(APIView):
    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            if data['Subscription_Type']=='Alternative_Days':
                start_date = datetime.strptime(data['startdate'], "%Y-%m-%d")
                end_date = datetime.strptime(data['enddate'], "%Y-%m-%d")

                # Define the list of days available (Monday to Sunday)
                available_days = [0, 1, 2, 3, 4, 5, 6]  # Monday to Sunday

                # Create a list to store day-wise data
                week_records = []

                # Calculate the week numbers
                week_numbers = set()

                # Determine the days to skip based on the start_date
                start_day = start_date.weekday()
                skip_days = []

                # Skip 1 and 3 and 5 if start date day is 0
                if start_day == 0:
                    skip_days = [1, 3, 5]
                # Skip 2 and 4 and 6 if start date day is 1
                elif start_day == 1:
                    skip_days = [0,2, 4, 6]
                elif start_day == 2:
                    skip_days = [1,3, 5, 0]
                elif start_day == 3:
                    skip_days = [2,4, 6, 1]
                elif start_day == 4:
                    skip_days = [3,5, 0, 2]
                elif start_day == 5:
                    skip_days = [4,6, 1, 3]

                # Iterate through the dates and calculate week numbers
                current_date = start_date
                while current_date <= end_date:
                    week_number = current_date.isocalendar()[1]
                    week_numbers.add(week_number)
                    current_date += timedelta(days=1)

                # Iterate through the weeks and available days
                for week_number in week_numbers:
                    week_data = {
                    "Customer_Id": data['Customer_Id'],
                    "WeekNo": week_number,
                    "Days": []
                    }

                    for day in available_days:
                        if day not in skip_days:  # Check if the day should not be skipped
                            current_date = start_date
                            while current_date <= end_date:
                                if (
                                    current_date.isocalendar()[1] == week_number
                                    and current_date.weekday() == day
                                ):
                                    # Define your customer details here
                                    customer_details = {
                                        "day": current_date.weekday(),
                                        # "customer_details": {
                                        "MobileNo": data['MobileNo'],
                                        "Customer_Id": data['Customer_Id'],
                                        "Customer_Name": data['Cus_FirstName'],
                                        # Add other customer details
                                        # }
                                    }
                                    week_data["Days"].append(customer_details)
                                current_date += timedelta(days=1)

                    if week_data["Days"]:
                        week_records.append(week_data)

                # Get the Partner instance based on Partner_Id
                partner = Partner.objects.get(id=data['Partner_Id'])

                # Create WeeklyRecord instances and save them to the database
                for week_data in week_records:
                    weekly_record = subscriptioncustomer(
                        Customer_Id=week_data["Customer_Id"],
                        WeekNo=week_data["WeekNo"],
                        Days=week_data["Days"],  # Store Days as a list
                        Location=data['Location'],
                        Partner_Id=partner,
                    )
                    weekly_record.save()
            else:
                start_date = datetime.strptime(data['startdate'], "%Y-%m-%d")
                end_date = datetime.strptime(data['enddate'], "%Y-%m-%d")

                # Define the list of days available (excluding Sunday)
                available_days = [0, 1, 2, 3, 4, 5]  # Monday to Saturday

                # Create a list to store day-wise data
                week_records = []

                # Calculate the week numbers
                week_numbers = set()

                # Iterate through the dates and calculate week numbers
                current_date = start_date
                while current_date <= end_date:
                    week_number = current_date.isocalendar()[1]
                    week_numbers.add(week_number)
                    current_date += timedelta(days=1)

                # Iterate through the weeks and available days
                for week_number in week_numbers:
                    week_data = {
                        "Customer_Id": data['Customer_Id'],
                        "WeekNo": week_number,
                        "Days": []
                    }

                    for day in available_days:
                        current_date = start_date
                        while current_date <= end_date:
                            if (
                                current_date.isocalendar()[1] == week_number
                                and current_date.weekday() == day
                            ):
                                # Define your customer details here
                                customer_details = {
                                    "day": current_date.weekday(),
                                    # "customer_details": {
                                    "Customer_Id": data['Customer_Id'],
                                    "Customer_Name": data['Cus_FirstName'],
                                    "MobileNo": data['MobileNo'],
                                    # Add other customer details
                                    # }
                                }
                                week_data["Days"].append(customer_details)
                            current_date += timedelta(days=1)

                    if week_data["Days"]:
                        week_records.append(week_data)

                # Get the Partner instance based on Partner_Id
                partner = Partner.objects.get(id=data['Partner_Id'])

                # Create WeeklyRecord instances and save them to the database
                for week_data in week_records:
                    weekly_record = subscriptioncustomer(
                        Customer_Id=week_data["Customer_Id"],
                        WeekNo=week_data["WeekNo"],
                        Days=week_data["Days"],
                        Location=data['Location'],
                        Partner_Id=partner,
                    )
                    weekly_record.save()               

            output = JSONRenderer().render({"message": "Records created successfully"})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)


    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = subscriptioncustomer.objects.get(id=data['id'])
            serialize = subscriptioncustomerSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class Salespersionachivement(APIView):
    def get(self, request, *args, **kwargs):
        try:
            data = request.query_params
            module = Invoice.objects.filter(Partner_Id=data['Partner_Id'])
            serialize = InvoiceSerialzer(module, many=True)
            sales_serialize = serialize.data
            sales_data = defaultdict(lambda: defaultdict(float))
            for i in sales_serialize:
                Created_Date = i['Created_Date']
                date_object = datetime.strptime(Created_Date, "%Y-%m-%d")
                month_name = date_object.strftime("%B")
                salesperson = i['SalesPerson']
                amount = i['Amount']
                sales_data[salesperson][month_name] += amount
            result_list = [
                {
                    "SalesPerson": salesperson,
                    "Month": month_data
                }
                for salesperson, month_data in sales_data.items()
            ]
            output = JSONRenderer().render(result_list)
            return HttpResponse(output, content_type='application/json', status=status.HTTP_200_OK)
        except Exception as e:
            error_output = JSONRenderer().render({"Error": str(e)})
            return HttpResponse(error_output, content_type='application/json', status=status.HTTP_400_BAD_REQUEST)


class citywiseprice(APIView):
    def get(self,request,*ags,**kwargs):
        try:
            data=request.query_params
            module = Partner.objects.get(id=data['Partner_Id'])
            serialize = PartnerSerialzer(module)
            partnerserilaize = serialize.data
            City_Name = partnerserilaize['BusinessUnit_City']
            pricemodule = Serviceprice.objects.filter(City_Name=City_Name)
            priceserialize = ServicepriceSerialzer(pricemodule,many=True)
            output = JSONRenderer().render(priceserialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class citywibudgethead(APIView):
    def get(self,request,*ags,**kwargs):
        try:
            data=request.query_params
            module = Partner.objects.get(id=data['Partner_Id'])
            serialize = PartnerSerialzer(module)
            partnerserilaize = serialize.data
            City_Name = partnerserilaize['BusinessUnit_City']
            pricemodule = Budgethead.objects.filter(City_Name=City_Name)
            priceserialize = BudgetheadSerialzers(pricemodule,many=True)
            output = JSONRenderer().render(priceserialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class ServicecancellationsCRUD(APIView):
    def post(self,request,*args,**kwargs):
        try:
            data=request.data
            serialize=ServicecancellationsSerialzer(data=data)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
            return HttpResponse(JSONRenderer().render(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Cancellation_Id']
            module = Servicecancellations.objects.get(Cancellation_Id=id)
            serialize = ServicecancellationsSerialzer(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            id=data['Cancellation_Id']
            module=Servicecancellations.objects.get(Cancellation_Id=id)
            serialize = ServicecancellationsSerialzer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
                output = JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
            return HttpResponse(JSONRenderer().render(serialize.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            id=data['Cancellation_Id']
            module=Servicecancellations.objects.get(Cancellation_Id=id)
            serialize = ServicecancellationsSerialzer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class TicketCRUD(APIView):
    def post(self, request,*args,**kwargs):
        try:
            data=request.data
            model=TicketSerializer(data=data)
            if model.is_valid():
                model.save()
                data1=JSONRenderer().render(model.data)
                return HttpResponse(data1,content_type="application/json",status=status.HTTP_201_CREATED)
            return HttpResponse(JSONRenderer().render(model.errors),content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            error= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(error,content_type="application/Json",status=status.HTTP_400_BAD_REQUEST)

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module=Ticket.objects.get(Ticket_Id=data["Ticket_Id"])
            serialize=TicketSerializers(module)
            output=JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type="Application/Json",status=status.HTTP_200_OK)
        except Exception as e:
            error= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(error,content_type="application/Json",status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            module= Ticket.objects.get(Ticket_Id=data["Ticket_Id"])
            serialize= TicketSerializer(instance=module, data=data)
            if serialize.is_valid():
                serialize.save()
                output=JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type="Application/Json",status=status.HTTP_200_OK)
            return HttpResponse(JSONRenderer().render(serialize.errors),status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            error= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(error,content_type="application/Json",status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module= Ticket.objects.get(Ticket_Id=data["Ticket_Id"])
            serialize=TicketSerializer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output=JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type="Application/Json",status=status.HTTP_200_OK)
        except Exception as e:
            error= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(error,content_type="application/Json",status=status.HTTP_400_BAD_REQUEST)


class TicketList(ListAPIView):
    queryset = Ticket.objects.all()
    serializer_class = TicketSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]
    
    def get_queryset(self):
        try:
            Partner_Id=self.request.GET.get("Partner_Id")
            module=Ticket.objects.filter(Q(Partner_Id = Partner_Id) & Q(Is_Deleted=False)).order_by('-id')
            return module
        except Exception as e:
            error= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(error,content_type="application/Json",status=status.HTTP_400_BAD_REQUEST)


class TicketMylist(APIView):        
    queryset = Ticket.objects.all()
    serializer_class = TicketSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]
    
    def get_queryset(self):
        try:
            Partner_Id=self.request.GET.get("Partner_Id")
            Created_by =  self.request.GET.get('Created_by')
            module=Ticket.objects.filter(Q(Created_by =Created_by)& Q(Is_Deleted=False) & Q(Partner_Id = Partner_Id)).order_by('-id')
            return module
        except Exception as e:
            error= JSONRenderer().render({"Error":str(e)})
            return HttpResponse(error,content_type="application/Json",status=status.HTTP_400_BAD_REQUEST)


class Ticketfilter(ListAPIView):
    queryset = Ticket.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    serializer_class = TicketSerializers
    permission_classes = [DjangoModelPermissions]
    def post(self,request,*args,**kwargs):
        data=request.data
        data1={"Ticket_Id__icontains":data['Ticket_Id'],"Severity__icontains":data['Severity'],
            "Created_Date__range":data['Created_Date'],"Partner_Id":data['Partner_Id']}
        return requestget(model=Ticket,serializer=TicketSerializers,data=data1,page=data['page'],Operator='and')


class Ticketsearch(ListAPIView):
    queryset = Ticket.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    serializer_class = TicketSerializers
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data=self.request.GET.get('search')
        Partner_Id = self.request.GET.get('Partner_Id')
        module = Ticket.objects.filter((Q(Ticket_Id__icontains=data) | Q(Details__icontains=data) | Q(Severity__icontains=data) |
         Q(Ticket_Related_to__icontains=data)) & Q(Is_Deleted=False) & Q(Partner_Id =Partner_Id)).order_by('-id')
        return module


class AssigningTicketsCRUD(APIView):
    def post(self, request,*args,**kwargs):
        try:
            data=request.data
            model=AssigningTicketSerializer(data=data)
            if model.is_valid():
                model.save()
                data1=JSONRenderer().render(model.data)
                return HttpResponse(data1,content_type="application/json",status=status.HTTP_201_CREATED)
            return HttpResponse(JSONRenderer().render(model.errors),status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            error=JSONRenderer().render({"error": str(e)})
            return HttpResponse(error,content_type="application/Json",status=status.HTTP_400_BAD_REQUEST)
             

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module=AssigningTicket.objects.get(AssigningTicket_Id=data["AssigningTicket_Id"])
            serialize=AssigningTicketSerializer(module)
            output=JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type="application/Json",status=status.HTTP_200_OK)
        except Exception as e:
            error =JSONRenderer().render({"error": str(e)})
            return HttpResponse(error,content_type="application/Json",status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            module= AssigningTicket.objects.get(AssigningTicket_Id=data["AssigningTicket_Id"])
            serialize= AssigningTicketSerializer(instance=module, data=data)
            if serialize.is_valid():
                serialize.save()
                output=JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type="application/Json",status=status.HTTP_200_OK)
        except Exception as e:
            error =JSONRenderer().render({"error": str(e)})
            return HttpResponse(error,content_type="application/Json",status=status.HTTP_400_BAD_REQUEST)

            
    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module= AssigningTicket.objects.get(AssigningTicket_Id=data["AssigningTicket_Id"])
            serialize=AssigningTicketSerializer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                    serialize.save()
            output=JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type="application/Json")
        except Exception as e:
            error=JSONRenderer().render({"error":str(e)})
            return HttpResponse(error,content_type="Application/Json")


class  AssigningTicketsList(ListAPIView):
    queryset = AssigningTicket.objects.all()
    serializer_class = AssigningTicketSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset (self):
        try:
            Partner_Id=self.request.GET.get('Partner_Id')
            module=  AssigningTicket.objects.filter(Q(Partner_Id = Partner_Id) & Q(Is_Deleted=False)).order_by('-id')
            return module
        except Exception as e:
            error=JSONRenderer().render({"error":str(e)})
            return HttpResponse(error,content_type="Application/Json",status=status.HTTP_400_BAD_REQUEST)


class AssigningTicketMylist(ListAPIView):
    queryset = AssigningTicket.objects.all()
    serializer_class = AssigningTicketSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset (self):
        try:
            Partner_Id=self.request.GET.get('Partner_Id')
            Created_by = self.request.GET.get('Created_by')
            module=AssigningTicket.objects.filter(Q(Created_by =Created_by)& Q(Is_Deleted=False) & Q(Partner_Id = Partner_Id)).order_by('-id')
            return module
        except Exception as e:
            error=JSONRenderer().render({"error":str(e)})
            return HttpResponse(error,content_type="Application/Json",status=status.HTTP_400_BAD_REQUEST)


class AssigningTicketsFilter(ListAPIView) :
    queryset = AssigningTicket.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    serializer_class = AssigningTicketSerializerS
    permission_classes = [DjangoModelPermissions]

    def post(self,request,*args,**kwargs):
        data=request.data
        data1={"AssigningTicket_Id__icontains":data['AssigningTicket_Id'],"AssigningTicket_Id__icontains":data['Raised_by'],"Severity__icontains":data['Severity'],
            "Assigned_to__icontains":data['Assigned_to'],"Created_Date__range":data['Created_Date'],"Partner_Id":data['Partner_Id']}
        return requestget(model=AssigningTicket,serializer=AssigningTicketSerializerS,data=data1,page=data['page'],Operator='and')


class AssignTicketsearch(ListAPIView):
    queryset = AssigningTicket.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    serializer_class = AssigningTicketSerializerS
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        data=self.request.GET.get('search')
        Partner_Id = self.request.GET.get('Partner_Id')
        module = AssigningTicket.objects.filter((Q(AssigningTicket_Id__icontains=data) | Q(AssigningTicket_Id__icontains=data) | Q(Severity__icontains=data) |
            Q(Assigned_to__icontains=data) | Q(Details__icontains=data) | Q(Notes__icontains=data))& Q(Partner_Id=Partner_Id) & Q(Is_Deleted=False)).order_by('-id')
        return module



class ContactsCRUD(APIView):
    def post(self, request,*args,**kwargs):
        try:
            data=request.data
            model=ContactsSerializer(data=data)
            if model.is_valid():
                model.save()
                data1=JSONRenderer().render(model.data)
                return HttpResponse(data1,content_type="application/json",status=status.HTTP_201_CREATED)
            return HttpResponse(JSONRenderer().render(model.errors),status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            error=JSONRenderer().render({"error": str(e)})
            return HttpResponse(error,content_type="application/Json",status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, *args,**kwargs):
        try:
            data=request.query_params
            module= Contacts.objects.get(id=data["id"])
            serialize= ContactsSerializer(module)
            output=JSONRenderer().render(serialize.data)
            return HttpResponse(output, content_type="application/Json")
        except Exception as e:
            error=JSONRenderer().render({"error": str(e)})
            return HttpResponse(error, content_type="application/Json")

    def put(self, request, *args,**kwargs):
        try:
            data=request.data
            module=Contacts.objects.get(id=data["id"])
            serialize= ContactsSerializer(module, data=data)
            if serialize.is_valid():
                serialize.save()
                output=JSONRenderer().render(serialize.data)
                return HttpResponse(output,content_type="application/Json",status=status.HTTP_200_OK)
            return HttpResponse(JSONRenderer().render(serialize.errors),status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            error=JSONRenderer().render({"error":str(e)})
            return HttpResponse(error,content_type="application/Json",status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module=Contacts.objects.get(id=data["id"])
            serialize=ContactsSerializer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
                output=JSONRenderer().render(serialize.data)
                return HttpResponse(output, content_type="application/Json", status=status.HTTP_200_OK)
            return HttpResponse(JSONRenderer().render(serialize.errors),status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            error=JSONRenderer().render({"error":str(e)})
            return HttpResponse(error, content_type="application/Json",status=status.HTTP_400_BAD_REQUEST)

class ContactList(ListAPIView):
    queryset = Contacts.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    serializer_class = ContactsSerializerS
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        try:
            Partner_Id = self.request.GET.get('Partner_Id')
            module=Contacts.objects.filter(Q(Partner_Id=Partner_Id) & Q(Is_Deleted=False)).order_by('-id')
            return module
        except Exception as e:
            error =JSONRenderer().render({"error":str(e)})
            return HttpResponse(error, content_type="Application/Json",status=status.HTTP_400_BAD_REQUEST)

class ContactMyList(ListAPIView):
    queryset = Contacts.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    serializer_class = ContactsSerializerS
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        try:
            Partner_Id = self.request.GET.get('Partner_Id')
            Created_By = self.request.GET.get('Created_By')
            module=Contacts.objects.filter(Q(Created_By=Created_By) & Q(Is_Deleted=False) & Q(Partner_Id=Partner_Id)).order_by('-id')
        except Exception as e:
            error =JSONRenderer().render({"error":str(e)})
            return HttpResponse(error, content_type="Application/Json",status=status.HTTP_400_BAD_REQUEST)


class Contactsearch(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=self.request.GET.get('search')
            module = Contacts.objects.filter(
                    (Q(Name__icontains=data) |
                    Q(AddressLine1__icontains=data) |
                    Q(AddressLine2__icontains=data) |
                    Q(City__icontains=data) |
                    Q(State__icontains=data) |
                    Q(Country__icontains=data) |
                    Q(EmailId__icontains=data)) & Q(
                        Is_Deleted=False)).order_by('-id')
            serialize=ContactsSerializerS(module,many=True)
            output=JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type="application/json",status=status.HTTP_200_OK)
        except Exception as e:
            error=JSONRenderer().render({"error": str(e)})
            return HttpResponse(error,content_type="application/Json",status=status.HTTP_400_BAD_REQUEST)


class Contactfilter(APIView):
    queryset=Contacts.objects.all()
    def post(self,request,*args,**kwargs):
        data=request.data
        data1={"Name__icontains":data['Name'],"City__icontains":data["City"],
                "State__icontains": data["State"],"Country__icontains":data["Country"] ,"Created_Date__range":data["Created_Date"], "Is_Deleted":False}
        return requestget(model=Contacts,serializer=ContactsSerializerS,data=data1,page=data['page'],Operator='and')


class TicketResponseCRUD(APIView):
    def post(self, request,*args,**kwargs):
        try:
            data=request.data
            model=TicketResponseSerializer(data=data)
            if model.is_valid():
                model.save()
                data1=JSONRenderer().render(model.data)
                return HttpResponse(data1,content_type="application/json",status=status.HTTP_201_CREATED)
            return HttpResponse(JSONRenderer().render(model.errors),status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            error=JSONRenderer().render({"error": str(e)})
            return HttpResponse(error,content_type="application/Json",status=status.HTTP_400_BAD_REQUEST)

    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module=TicketResponse.objects.get(id=data["id"])
            serialize=TicketResponseSerializerS(module)
            output=JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type="Application/Json")
        except Exception as e:
            error=JSONRenderer().render({"error": str(e)})
            return HttpResponse(error,content_type="application/Json")


    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            module=TicketResponse.objects.get(id=data["id"])
            serialize=TicketResponseSerializer(module, data=data)
            if serialize.is_valid():
                serialize.save()
                output=JSONRenderer().render(serialize.data)
                return HttpResponse(output, content_type="Application/Json",status=status.HTTP_200_OK)
            return HttpResponse(JSONRenderer().render(serialize.errors),status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            error=JSONRenderer().render({"error": str(e)})
            return HttpResponse(error,content_type="appliaction/Json",status=status.HTTP_400_BAD_REQUEST)


    def delete(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module= TicketResponse.objects.get(id=data["id"])
            serialize= TicketResponseSerializer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
                output=JSONRenderer().render(serialize.data)
                return HttpResponse(output, content_type="Application/Json",status=status.HTTP_200_OK)
            return HttpResponse(JSONRenderer().render(serialize.errors),status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            error=JSONRenderer().render({"error": str(e)})
            return HttpResponse(error,content_type="appliaction/Json",status=status.HTTP_400_BAD_REQUEST)


class  TicketResponseList(ListAPIView):
    queryset = TicketResponse.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    serializer_class = TicketResponseSerializerS
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        try:
            Partner_Id = self.request.GET.get('Partner_Id')
            module=  TicketResponse.objects.filter(Q(Partner_Id=Partner_Id) & Q(Is_Deleted=False)).order_by('-id')
            return module
        except Exception as e:
            error=JSONRenderer().render({"error":str(e)})
            return HttpResponse(error,content_type="Application/Json",status=status.HTTP_400_BAD_REQUEST)


class TicketResponseMylist(ListAPIView):
    queryset = TicketResponse.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    serializer_class = TicketResponseSerializerS
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        try:
            Partner_Id = self.request.GET.get('Partner_Id')
            Created_By = self.request.GET.get('Created_By')
            module=TicketResponse.objects.filter(Q(Partner_Id=Partner_Id)& Q(Created_By =Created_By)& Q(Is_Deleted=False)).order_by('-id')
            return module
        except Exception as e:
            error =JSONRenderer().render({"error": str(e)})
            return HttpResponse(error,content_type="Application/Json",status=status.HTTP_400_BAD_REQUEST)


class TicketResponsesearch(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=self.request.GET.get('search')
            module = TicketResponse.objects.filter(
                    (Q(TicketRelateTo__icontains=data) |
                    Q(TicketDescription__icontains=data) |
                    Q(Response__icontains=data) |
                    Q(TicketSeverity__icontains=data) |
                    Q(TicketStatus__icontains=data) )
                    & Q(Is_Deleted=False)).order_by('-id')
            serialize=TicketResponseSerializer(module,many=True)
            output=JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type="application/json")
        except Exception as e:
            error=JSONRenderer().render({"error": str(e)})
            return HttpResponse(error,content_type="application/Json")


class Ticketresponsefilter(APIView):
    queryset= TicketResponse.objects.all()
    def post(self, request, *args,**kwargs):
        try:
            data= request.data
            module={"TicketRelateTo__icontains":data["TicketRelateTo"],
                    "TicketSeverity__icontains":data["TicketSeverity"], "TicketStatus__icontains": data["TicketStatus"],"Created_Date__range":data["Created_Date"] ,"Is_Deleted": False }
            return requestget(model=TicketResponse, serializer=TicketResponseSerializer, data=module, page=data["page"], Operator= "and")
        except Exception as e:
            error=JSONRenderer().render({"error": str(e)})
            return HttpResponse(error,content_type="application/Json")

class partnerwiseservicelist(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Services.objects.filter(Q(Partner_Id=data['Partner_Id']) & Q(Is_Deleted=False))
            serialize = ServicesSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class partnerbudgetchecklist(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Partner.objects.filter(Is_Deleted=False)
            partmodule = Partner.objects.filter(Is_Deleted=False).count()
            serialize = PartnerSerialzer(module,many=True)
            partserialize = serialize.data
            employeelist = []
            for i in partserialize:
                timesheetmodule = Budget.objects.filter(Q(Partner_Id=i['id']) & Q(Approved_Flag=True) & Q(Is_Deleted=False))
                timeserialize = BudgetSerialzer(timesheetmodule,many=True)
                if timeserialize==[]:
                    employeelist.append(i['id'])
                else:
                    pass
            if sum(employeelist) == 0:
                return "All pertner had budget"
            else:
                return "Some Partner didnot had budget"
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


class LeadCRUD(ListAPIView):
    queryset = Lead.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            serialize = LeadSerializer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['Lead_Id']
            module = Lead.objects.get(Lead_Id=id)
            serialize = LeadSerializers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            id=data['Lead_Id']
            module=Lead.objects.get(Lead_Id=id)
            serialize = LeadSerializer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['Lead_Id']
            module=Lead.objects.get(Lead_Id=id)
            serialize = LeadSerializer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)    
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class Myleadlist(ListAPIView):
    queryset = Lead.objects.all()
    serializer_class = LeadSerializers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        Created_By=self.request.GET.get('Created_By')
        Partner_Id = self.request.GET.get('Partner_Id')
        module = Lead.objects.filter(Q(Created_By=Created_By) & Q(Partner_Id=Partner_Id) & Q(Is_Deleted=False)).order_by('-id')
        return module


class Leadlist(ListAPIView):
    queryset = Lead.objects.all()
    serializer_class = LeadSerializers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        Partner_Id = self.request.GET.get('Partner_Id')
        module = Lead.objects.filter(Q(Partner_Id=Partner_Id) & Q(Is_Deleted=False)).order_by('-id')
        return module

class Leadfilter(ListAPIView):
    queryset = Lead.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
            data=request.data
            data1={"Company_Name__icontains":data['Company_Name'],"First_Name__icontains":data['First_Name'],"Job_Title__icontains":data['Job_Title'],"Created_Date__range":data['Created_Date'],"Is_Deleted":False}
            return requestget(model=Lead,serializer=LeadSerializers,data=data1,page=data['page'],Operator='and')

class Leadsearch(ListAPIView):
    queryset = Lead.objects.all()
    serializer_class = LeadSerializer
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get_queryset(self):
        data=self.request.GET.get('search')
        module = Lead.objects.filter((Q(Company_Name__icontains=data)| Q(Lead_Id__icontains=data) | Q(First_Name__icontains=data)| Q(Job_Title__icontains=data)| Q(City__icontains=data) | Q(Job_Title__icontains=data))& Q(Is_Deleted=False)).order_by('-id')
        return module



class DealCRUD(ListAPIView):
    queryset = Deal.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            serialize = DealSerializer(data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_201_CREATED)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)
        

    def get(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['Deal_Id']
            module = Deal.objects.get(Deal_Id=id)
            serialize = DealSerializers(module)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output=JSONRenderer().render({"error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)


    def put(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.data
            id=data['Deal_Id']
            module=Deal.objects.get(Deal_Id=id)
            serialize = DealSerializer(instance=module,data=data)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,*args,**kwargs):
        try:
            # data=request
            data=request.query_params
            id=data['Deal_Id']
            module=Deal.objects.get(Deal_Id=id)
            serialize = DealSerializer(module,{"Is_Deleted":True},partial=True)
            if serialize.is_valid():
                serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)    
        except Exception as e:
            output=JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class Mydeallist(ListAPIView):
    queryset = Deal.objects.all()
    serializer_class = DealSerializers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        Created_By=self.request.GET.get('Created_By')
        Partner_Id = self.request.GET.get('Partner_Id')
        module = Deal.objects.filter(Q(Created_By=Created_By) & Q(Partner_Id=Partner_Id) & Q(Is_Deleted=False)).order_by('-id')
        return module


class Deallist(ListAPIView):
    queryset = Deal.objects.all()
    serializer_class = DealSerializers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        Partner_Id = self.request.GET.get('Partner_Id')
        module = Deal.objects.filter(Q(Partner_Id=Partner_Id) & Q(Is_Deleted=False)).order_by('-id')
        return module

class Dealfilter(ListAPIView):
    queryset = Deal.objects.all()
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def post(self,request,*args,**kwargs):
            data=request.data
            data1={"Deal_Name__icontains":data['Deal_Name'],"Deal_Stage__icontains":data['Deal_Stage'],"Deal_Type__icontains":data['Deal_Type'],"Created_Date__range":data['Created_Date'],"Is_Deleted":False}
            return requestget(model=Deal,serializer=DealSerializers,data=data1,page=data['page'],Operator='and')

class Dealsearch(ListAPIView):
    queryset = Deal.objects.all()
    serializer_class = DealSerializers
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [DjangoModelPermissions]


    def get_queryset(self):
        data=self.request.GET.get('search')
        Partner_Id=self.request.GET.get('Partner_Id')
        module = Deal.objects.filter((Q(Deal_Name__icontains=data)| Q(Deal_Probablity__icontains=data)| Q(Deal_Id__icontains=data) | Q(Lead_Name__icontains=data)| Q(Deal_Stage__icontains=data)| Q(Deal_Type__icontains=data) | Q(Contact_Persion__icontains=data))& Q(Partner_Id=Partner_Id) & Q(Is_Deleted=False)).order_by('-id')
        return module


class Budgetlock(APIView):
    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            for i in data['Partner_Id']:
                module = Partner.objects.get(id=i['id'])
                serialize = PartnerSerialzer(module,{"Budget_flg":i['Budget_flg']},partial=True)
                if serialize.is_valid():
                    serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)

class Budgetremaininglocklist(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Partner.objects.filter(Q(Budget_flg=False) & Q(Is_Deleted=False))
            serialize = PartnerSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)


class Budgetlocklist(APIView):
    def get(self,request,*args,**kwargs):
        try:
            data=request.query_params
            module = Partner.objects.filter(Q(Budget_flg=True) & Q(Is_Deleted=False))
            serialize = PartnerSerialzer(module,many=True)
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type = 'application/json',status=status.HTTP_400_BAD_REQUEST)



class Budgeteditlock(APIView):
    def put(self,request,*args,**kwargs):
        try:
            data=request.data
            for i in data['Partner_Id']:
                module = Partner.objects.get(id=i)
                serialize = PartnerSerialzer(module,{"Budget_Edit":data['Budget_Edit']},partial=True)
                if serialize.is_valid():
                    serialize.save()
            output = JSONRenderer().render(serialize.data)
            return HttpResponse(output,content_type='application/json',status=status.HTTP_200_OK)
        except Exception as e:
            output = JSONRenderer().render({"Error":str(e)})
            return HttpResponse(output,content_type='application/json',status=status.HTTP_400_BAD_REQUEST)