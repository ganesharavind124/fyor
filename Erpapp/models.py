from django.db import models

from email.policy import default
from locale import currency
from pyexpat import model
from unittest.util import _MAX_LENGTH
from xmlrpc.client import Server
from django.db import models
from django.utils import timezone
from datetime import date
from django.contrib.auth.models import User,Group,Permission

#String ID create functionality

def Configuration_I():
    Configuratio=Configuration.objects.all().order_by('Configuration_Id').last()
    if not Configuratio:
        return "CON" + "000000001"
    Configurati = Configuratio.Configuration_Id
    Configurat=int(Configurati[4:])
    Configura=Configurat + 1
    Configur = "CON" + str(Configura).zfill(9)
    return Configur



def Organization_I():
    Organizatio=Organization.objects.all().order_by('Organization_Id').last()
    if not Organizatio:
        return "ORG" + "000000001"
    Organizati = Organizatio.Organization_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "ORG" + str(Organiza).zfill(9)
    return Configur


def Partner_I():
    Organizatio=Partner.objects.all().order_by('Partner_Id').last()
    if not Organizatio:
        return "PAR" + "000000001"
    Organizati = Organizatio.Partner_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "PAR" + str(Organiza).zfill(9)
    return Configur


def BusinessUnit_I():
    Organizatio=BusinessUnit.objects.all().order_by('BusinessUnit_Id').last()
    if not Organizatio:
        return "BUI" + "000000001"
    Organizati = Organizatio.BusinessUnit_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "BUI" + str(Organiza).zfill(9)
    return Configur






def Address_I():
    Organizatio=Communication_Address.objects.all().order_by('Address_Id').last()
    if not Organizatio:
        return "ADD" + "000000001"
    Organizati = Organizatio.Address_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "ADD" + str(Organiza).zfill(9)
    return Configur


def Businesshours_I():
    Organizatio=Business_Hours.objects.all().order_by('Businesshours_Id').last()
    if not Organizatio:
        return "BUS" + "000000001"
    Organizati = Organizatio.Businesshours_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "BUS" + str(Organiza).zfill(9)
    return Configur


def Module_I():
    Organizatio=ERPModules.objects.all().order_by('Module_Id').last()
    if not Organizatio:
        return "MOD" + "000000001"
    Organizati = Organizatio.Module_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "MOD" + str(Organiza).zfill(9)
    return Configur


def Form_I():
    Organizatio=FormPermission.objects.all().order_by('FormId').last()
    if not Organizatio:
        return "FOR" + "000000001"
    Organizati = Organizatio.FormId
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "FOR" + str(Organiza).zfill(9)
    return Configur


def Formmaster_I():
    Organizatio=FormMaster.objects.all().order_by('FormmasterId').last()
    if not Organizatio:
        return "FMO" + "000000001"
    Organizati = Organizatio.FormmasterId
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "FMO" + str(Organiza).zfill(9)
    return Configur

def Autogen_I():
    Organizatio=AutogenNos.objects.all().order_by('Autogen_Id').last()
    if not Organizatio:
        return "AUT" + "000000001"
    Organizati = Organizatio.Autogen_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "AUT" + str(Organiza).zfill(9)
    return Configur


def Error_I():
    Organizatio=ErrorCodes.objects.all().order_by('Error_Id').last()
    if not Organizatio:
        return "ERR" + "000000001"
    Organizati = Organizatio.Error_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "ERR" + str(Organiza).zfill(9)
    return Configur


def Notification_I():
    Organizatio=Notifications.objects.all().order_by('Notification_Id').last()
    if not Organizatio:
        return "NOT" + "000000001"
    Organizati = Organizatio.Notification_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "NOT" + str(Organiza).zfill(9)
    return Configur


def Calendar_I():
    Organizatio=Financial_Calendar_Details.objects.all().order_by('Calendar_Id').last()
    if not Organizatio:
        return "CAL" + "000000001"
    Organizati = Organizatio.Calendar_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "CAL" + str(Organiza).zfill(9)
    return Configur


def Territory_I():
    Organizatio=Territory.objects.all().order_by('Territory_Id').last()
    if not Organizatio:
        return "TER" + "000000001"
    Organizati = Organizatio.Territory_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "TER" + str(Organiza).zfill(9)
    return Configur


def HRCalendarmonths_I():
    Organizatio=HRCalendarmonths.objects.all().order_by('HRCalendarmonths_Id').last()
    if not Organizatio:
        return "HRC" + "000000001"
    Organizati = Organizatio.HRCalendarmonths_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "HRC" + str(Organiza).zfill(9)
    return Configur


def Template_I():
    Organizatio=Templates.objects.all().order_by('Template_Id').last()
    if not Organizatio:
        return "TEM" + "000000001"
    Organizati = Organizatio.Template_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "TEM" + str(Organiza).zfill(9)
    return Configur


def WorkflowRule_I():
    Organizatio=WorkflowRule.objects.all().order_by('WorkflowRule_Id').last()
    if not Organizatio:
        return "WOR" + "000000001"
    Organizati = Organizatio.WorkflowRule_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "WOR" + str(Organiza).zfill(9)
    return Configur


def WorkflowName_I():
    Organizatio=WorkflowName.objects.all().order_by('WorkflowName_Id').last()
    if not Organizatio:
        return "WOR" + "000000001"
    Organizati = Organizatio.WorkflowName_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "WOR" + str(Organiza).zfill(9)
    return Configur


def Assignworkflow_I():
    Organizatio=Assignworkflow.objects.all().order_by('Assignworkflow_Id').last()
    if not Organizatio:
        return "AWR" + "000000001"
    Organizati = Organizatio.Assignworkflow_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "AWR" + str(Organiza).zfill(9)
    return Configur



def Workflow_I():
    Organizatio=Workflow.objects.all().order_by('Workflow_Id').last()
    if not Organizatio:
        return "WOR" + "000000001"
    Organizati = Organizatio.Workflow_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "WOR" + str(Organiza).zfill(9)
    return Configur





def Workfloworigin_I():
    Organizatio=Workfloworigin.objects.all().order_by('Workfloworigin_Id').last()
    if not Organizatio:
        return "WRK" + "000000001"
    Organizati = Organizatio.Workfloworigin_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "WRK" + str(Organiza).zfill(9)
    return Configur





def Role_I():
    Organizatio=Group.objects.all().order_by('Role_Id').last()
    if not Organizatio:
        return "ROL" + "000000001"
    Organizati = Organizatio.Role_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "ROL" + str(Organiza).zfill(9)
    return Configur


def Rolepermission_I():
    Organizatio=RolePermission.objects.all().order_by('Rolepermission_Id').last()
    if not Organizatio:
        return "RPE" + "000000001"
    Organizati = Organizatio.Rolepermission_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "RPE" + str(Organiza).zfill(9)
    return Configur


def User_I():
    Organizatio=User.objects.all().order_by('User_Id').last()
    if not Organizatio:
        return "USE" + "000000001"
    Organizati = Organizatio.User_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "USE" + str(Organiza).zfill(9)
    return Configur


def Activity_I():
    Organizatio=ERPUsersActivity.objects.all().order_by('Activity_Id').last()
    if not Organizatio:
        return "ACT" + "000000001"
    Organizati = Organizatio.Activity_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "ACT" + str(Organiza).zfill(9)
    return Configur


def UserLoginStatus_I():
    Organizatio=UserLoginStatus.objects.all().order_by('UserLoginStatus_Id').last()
    if not Organizatio:
        return "ULS" + "000000001"
    Organizati = Organizatio.UserLoginStatus_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "ULS" + str(Organiza).zfill(9)
    return Configur



def UserChangePwd_I():
    Organizatio=UserChangePwd.objects.all().order_by('UserChangePwd_Id').last()
    if not Organizatio:
        return "UCP" + "000000001"
    Organizati = Organizatio.UserChangePwd_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "UCP" + str(Organiza).zfill(9)
    return Configur


def Item_I():
    Organizatio=ItemMaster.objects.all().order_by('Item_Id').last()
    if not Organizatio:
        return "ITE" + "000000001"
    Organizati = Organizatio.Item_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "ITE" + str(Organiza).zfill(9)
    return Configur



def Batch_N():
    Organizatio=Batchtable.objects.all().order_by('Batch_No').last()
    if not Organizatio:
        return "BTC "+"000000001"
    Organizati = Organizatio.Batch_No
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "BTC" + str(Organiza).zfill(9)
    return Configur

def Category_I():
    Organizatio=Servicecategory.objects.all().order_by('Category_Id').last()
    if not Organizatio:
        return "SCA" + "000000001"
    Organizati = Organizatio.Category_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "SCA" + str(Organiza).zfill(9)
    return Configur



def Service_I():
    Organizatio=Services.objects.all().order_by('Service_Id').last()
    if not Organizatio:
        return "SER" + "000000001"
    Organizati = Organizatio.Service_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "SER" + str(Organiza).zfill(9)
    return Configur



def Plan_I():
    Organizatio=Plan.objects.all().order_by('Plan_Id').last()
    if not Organizatio:
        return "PLA" + "000000001"
    Organizati = Organizatio.Plan_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "PLA" + str(Organiza).zfill(9)
    return Configur


def Subscription_I():
    Organizatio=Subscription.objects.all().order_by('Subscription_Id').last()
    if not Organizatio:
        return "SUB" + "000000001"
    Organizati = Organizatio.Subscription_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "SUB" + str(Organiza).zfill(9)
    return Configur



def Slot_I():
    Organizatio=Slot.objects.all().order_by('Slot_Id').last()
    if not Organizatio:
        return "SLO" + "000000001"
    Organizati = Organizatio.Slot_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "SLO" + str(Organiza).zfill(9)
    return Configur




def Serviceprice_I():
    Organizatio=Serviceprice.objects.all().order_by('Serviceprice_Id').last()
    if not Organizatio:
        return "SPR" + "000000001"
    Organizati = Organizatio.Serviceprice_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "SPR" + str(Organiza).zfill(9)
    return Configur

def Tax_I():
    Organizatio=Taxsetup.objects.all().order_by('Tax_Id').last()
    if not Organizatio:
        return "TAX" + "000000001"
    Organizati = Organizatio.Tax_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "TAX" + str(Organiza).zfill(9)
    return Configur


def Target_I():
    Organizatio=Territorysalestarget.objects.all().order_by('Target_Id').last()
    if not Organizatio:
        return "TAR" + "000000001"
    Organizati = Organizatio.Target_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "TAR" + str(Organiza).zfill(9)
    return Configur


def Salestarget_I():
    Organizatio=Servicesalestarget.objects.all().order_by('Salestarget_Id').last()
    if not Organizatio:
        return "STA" + "000000001"
    Organizati = Organizatio.Salestarget_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "STA" + str(Organiza).zfill(9)
    return Configur


def Servicemonthlysalestarget_I():
    Organizatio=Servicemonthlysalestarget.objects.all().order_by('Servicemonthlysalestarget_Id').last()
    if not Organizatio:
        return "SMT" + "000000001"
    Organizati = Organizatio.Servicemonthlysalestarget_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "SMT" + str(Organiza).zfill(9)
    return Configur


def Monthlyusertarget_I():
    Organizatio=Monthlyusertarget.objects.all().order_by('Monthlyusertarget_Id').last()
    if not Organizatio:
        return "MUT" + "000000001"
    Organizati = Organizatio.Monthlyusertarget_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "MUT" + str(Organiza).zfill(9)
    return Configur


def CustAddress_I():
    Organizatio=CustomerAddress.objects.all().order_by('CustAddress_Id').last()
    if not Organizatio:
        return "CAD" + "000000001"
    Organizati = Organizatio.CustAddress_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "CAD" + str(Organiza).zfill(9)
    return Configur


def Customer_I():
    Organizatio=Customer.objects.all().order_by('Customer_Id').last()
    if not Organizatio:
        return "CUS" + "000000001"
    Organizati = Organizatio.Customer_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "CUS" + str(Organiza).zfill(9)
    return Configur


def CustomerPaymentmethods_I():
    Organizatio=CustomerPaymentmethods.objects.all().order_by('CustomerPaymentmethods_Id').last()
    if not Organizatio:
        return "CPM" + "000000001"
    Organizati = Organizatio.CustomerPaymentmethods_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "CPM" + str(Organiza).zfill(9)
    return Configur


def SalesOrder_I():
    Organizatio=SalesOrder.objects.all().order_by('SalesOrder_Id').last()
    if not Organizatio:
        return "SOR" + "000000001"
    Organizati = Organizatio.SalesOrder_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "SOR" + str(Organiza).zfill(9)
    return Configur


def Invoice_I():
    Organizatio=Invoice.objects.all().order_by('Invoice_Id').last()
    if not Organizatio:
        return "INV" + "000000001"
    Organizati = Organizatio.Invoice_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "INV" + str(Organiza).zfill(9)
    return Configur


def Contact_I():
    Organizatio=Contacts.objects.all().order_by('Contact_Id').last()
    if not Organizatio:
        return "CON" + "000000001"
    Organizati = Organizatio.Contact_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "CON" + str(Organiza).zfill(9)
    return Configur



def Employeecheckin_I():
    Organizatio=Employeecheckin.objects.all().order_by('Employeecheckin_Id').last()
    if not Organizatio:
        return "ECH" + "000000001"
    Organizati = Organizatio.Employeecheckin_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "ECH" + str(Organiza).zfill(9)
    return Configur


def Employeeclaim_I():
    Organizatio=Employeeclaims.objects.all().order_by('Employeeclaim_Id').last()
    if not Organizatio:
        return "ECL" + "000000001"
    Organizati = Organizatio.Employeeclaim_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "ECL" + str(Organiza).zfill(9)
    return Configur



def Lead_I():
    Organizatio=Lead.objects.all().order_by('Lead_Id').last()
    if not Organizatio:
        return "LEA" + "000000001"
    Organizati = Organizatio.Lead_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "LEA" + str(Organiza).zfill(9)
    return Configur



def Deal_I():
    Organizatio=Deal.objects.all().order_by('Deal_Id').last()
    if not Organizatio:
        return "DEA" + "000000001"
    Organizati = Organizatio.Deal_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "DEA" + str(Organiza).zfill(9)
    return Configur



def Payment_I():
    Organizatio=Customerpayments.objects.all().order_by('Payment_Id').last()
    if not Organizatio:
        return "CIN" + "000000001"
    Organizati = Organizatio.Payment_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "CIN" + str(Organiza).zfill(9)
    return Configur


def Feedback_I():
    Organizatio=Customerfeedback.objects.all().order_by('Feedback_Id').last()
    if not Organizatio:
        return "FED" + "000000001"
    Organizati = Organizatio.Feedback_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "FED" + str(Organiza).zfill(9)
    return Configur


def Complain_I():
    Organizatio=Customercomplains.objects.all().order_by('Complain_Id').last()
    if not Organizatio:
        return "CMP" + "000000001"
    Organizati = Organizatio.Complain_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "CMP" + str(Organiza).zfill(9)
    return Configur


def Customerticketresolution_I():
    Organizatio=Customerticketresolution.objects.all().order_by('Customerticketresolution_Id').last()
    if not Organizatio:
        return "CRS" + "000000001"
    Organizati = Organizatio.Customerticketresolution_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "CRS" + str(Organiza).zfill(9)
    return Configur


def Cancellation_I():
    Organizatio=Servicecancellations.objects.all().order_by('Cancellation_Id').last()
    if not Organizatio:
        return "CAN" + "000000001"
    Organizati = Organizatio.Cancellation_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "CAN" + str(Organiza).zfill(9)
    return Configur


def Refund_I():
    Organizatio=Customerrefunds.objects.all().order_by('Refund_Id').last()
    if not Organizatio:
        return "CRF" + "000000001"
    Organizati = Organizatio.Refund_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "CRF" + str(Organiza).zfill(9)
    return Configur


def GCostCenter_I():
    Organizatio=Groupcostcenter_or_Profitcenter.objects.all().order_by('GCostCenter_Id').last()
    if not Organizatio:
        return "GCO" + "000000001"
    Organizati = Organizatio.GCostCenter_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "GCO" + str(Organiza).zfill(9)
    return Configur

def Journal_I():
    Organizatio=Journal.objects.all().order_by('Journal_Id').last()
    if not Organizatio:
        return "JOU" + "000000001"
    Organizati = Organizatio.Journal_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "JOU" + str(Organiza).zfill(9)
    return Configur
    
def CostCenter_I():
    Organizatio=Costcenter.objects.all().order_by('CostCenter_Id').last()
    if not Organizatio:
        return "CCE" + "000000001"
    Organizati = Organizatio.CostCenter_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "CCE" + str(Organiza).zfill(9)
    return Configur


def Distributionbudget_I():
    Organizatio=Distributionbudget.objects.all().order_by('Distributionbudget_Id').last()
    if not Organizatio:
        return "DBD" + "000000001"
    Organizati = Organizatio.Distributionbudget_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "DBD" + str(Organiza).zfill(9)
    return Configur



def Budget_I():
    Organizatio=Budget.objects.all().order_by('Budget_Id').last()
    if not Organizatio:
        return "BUD" + "000000001"
    Organizati = Organizatio.Budget_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "BUD" + str(Organiza).zfill(9)
    return Configur


def ConsolidatedBudget_I():
    Organizatio=ConsolidatedBudget.objects.all().order_by('ConsolidatedBudget_Id').last()
    if not Organizatio:
        return "CON" + "000000001"
    Organizati = Organizatio.ConsolidatedBudget_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "CON" + str(Organiza).zfill(9)
    return Configur


def CentralBudget_I():
    Organizatio=CentralBudget.objects.all().order_by('CentralBudget_Id').last()
    if not Organizatio:
        return "CEN" + "000000001"
    Organizati = Organizatio.CentralBudget_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "CEN" + str(Organiza).zfill(9)
    return Configur


def Salesforecast_I():
    Organizatio=Salesforecast.objects.all().order_by('Salesforecast_Id').last()
    if not Organizatio:
        return "SFC" + "000000001"
    Organizati = Organizatio.Salesforecast_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "SFC" + str(Organiza).zfill(9)
    return Configur


def CostCenterBudget_I():
    Organizatio=CostCenterBudget.objects.all().order_by('CostCenterBudget_Id').last()
    if not Organizatio:
        return "CCB" + "000000001"
    Organizati = Organizatio.CostCenterBudget_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "CCB" + str(Organiza).zfill(9)
    return Configur


def ExpenseBudget_I():
    Organizatio=ExpenseBudget.objects.all().order_by('ExpenseBudget_Id').last()
    if not Organizatio:
        return "EBD" + "000000001"
    Organizati = Organizatio.ExpenseBudget_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "EBD" + str(Organiza).zfill(9)
    return Configur



def SalesuserTarget_I():
    Organizatio=SalesuserTarget.objects.all().order_by('SalesuserTarget_Id').last()
    if not Organizatio:
        return "SUT" + "000000001"
    Organizati = Organizatio.SalesuserTarget_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "SUT" + str(Organiza).zfill(9)
    return Configur



def SubscriberTarget_I():
    Organizatio=SubscriberTarget.objects.all().order_by('SubscriberTarget_Id').last()
    if not Organizatio:
        return "SBT" + "000000001"
    Organizati = Organizatio.SubscriberTarget_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "SBT" + str(Organiza).zfill(9)
    return Configur


def Budgethead_I():
    Organizatio=Budgethead.objects.all().order_by('Budgethead_Id').last()
    if not Organizatio:
        return "BDH" + "000000001"
    Organizati = Organizatio.Budgethead_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "BDH" + str(Organiza).zfill(9)
    return Configur


def ServiceJob_I():
    Organizatio=ServiceJobDetails.objects.all().order_by('ServiceJob_Id').last()
    if not Organizatio:
        return "JOB" + "000000001"
    Organizati = Organizatio.ServiceJob_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "JOB" + str(Organiza).zfill(9)
    return Configur


def Accountcalendar_I():
    Organizatio=Accountingcalendar.objects.all().order_by('Accountcalendar_Id').last()
    if not Organizatio:
        return "ACL" + "000000001"
    Organizati = Organizatio.Accountcalendar_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "ACL" + str(Organiza).zfill(9)
    return Configur


def Accountingcalendaritem_I():
    Organizatio=Accountingcalendaritem.objects.all().order_by('Accountingcalendaritem_Id').last()
    if not Organizatio:
        return "ACI" + "000000001"
    Organizati = Organizatio.Accountingcalendaritem_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "ACI" + str(Organiza).zfill(9)
    return Configur



def Account_SegI():
    Organizatio=AccountSegment.objects.all().order_by('Account_SegId').last()
    if not Organizatio:
        return "ASG" + "000000001"
    Organizati = Organizatio.Account_SegId
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "ASG" + str(Organiza).zfill(9)
    return Configur


def Account_I():
    Organizatio=Chartofaccount.objects.all().order_by('Account_Id').last()
    if not Organizatio:
        return "ANT" + "000000001"
    Organizati = Organizatio.Account_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "ANT" + str(Organiza).zfill(9)
    return Configur


def Bank_I():
    Organizatio=Bank.objects.all().order_by('Bank_Id').last()
    if not Organizatio:
        return "BAN" + "000000001"
    Organizati = Organizatio.Bank_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "BAN" + str(Organiza).zfill(9)
    return Configur


def Acctgroup_I():
    Organizatio=Acctgroup.objects.all().order_by('AcctgroupId').last()
    if not Organizatio:
        return "AGR" + "000000001"
    Organizati = Organizatio.AcctgroupId
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "AGR" + str(Organiza).zfill(9)
    return Configur


def Distribution_I():
    Organizatio=Invoicedistributionitems.objects.all().order_by('Distribution_Id').last()
    if not Organizatio:
        return "DIS" + "000000001"
    Organizati = Organizatio.Distribution_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "DIS" + str(Organiza).zfill(9)
    return Configur

def Customerinvoicedetail_I():
    Organizatio=Customerinvoicedetail.objects.all().order_by('Customerinvoicedetail_Id').last()
    if not Organizatio:
        return "CIN" + "000000001"
    Organizati = Organizatio.Customerinvoicedetail_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "CIN" + str(Organiza).zfill(9)
    return Configur



def Supplierinvoicedetail_I():
    Organizatio=Supplierinvoicedetail.objects.all().order_by('Supplierinvoicedetail_Id').last()
    if not Organizatio:
        return "SIN" + "000000001"
    Organizati = Organizatio.Supplierinvoicedetail_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "SIN" + str(Organiza).zfill(9)
    return Configur




# def Voucher_I():
#     Organizatio=Voucherdetail.objects.all().order_by('Voucher_Id').last()
#     if not Organizatio:
#         return "VOU" + "000000001"
#     Organizati = Organizatio.Voucher_Id
#     Organizat=int(Organizati[4:])
#     Organiza=Organizat + 1
#     Configur = "VOU" + str(Organiza).zfill(9)
#     return Configur


def Contravoucher_I():
    Organizatio=Contravoucher.objects.all().order_by('Contravoucher_Id').last()
    if not Organizatio:
        return "CON" + "000000001"
    Organizati = Organizatio.Contravoucher_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "CON" + str(Organiza).zfill(9)
    return Configur



def Memo_I():
    Organizatio=MemoDetail.objects.all().order_by('Memo_Id').last()
    if not Organizatio:
        return "MEM" + "000000001"
    Organizati = Organizatio.Memo_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "MEM" + str(Organiza).zfill(9)
    return Configur


def Schedule_I():
    Organizatio=Schedules.objects.all().order_by('Schedule_Id').last()
    if not Organizatio:
        return "SCH" + "000000001"
    Organizati = Organizatio.Schedule_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "SCH" + str(Organiza).zfill(9)
    return Configur


def HRCalendar_I():
    Organizatio=HRcalendar.objects.all().order_by('HRCalendar_Id').last()
    if not Organizatio:
        return "HRC" + "000000001"
    Organizati = Organizatio.HRCalendar_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "HRC" + str(Organiza).zfill(9)
    return Configur


def Paycalendar_I():
    Organizatio=Paycalendar.objects.all().order_by('Paycalendar_Id').last()
    if not Organizatio:
        return "PCL" + "000000001"
    Organizati = Organizatio.Paycalendar_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "PCL" + str(Organiza).zfill(9)
    return Configur



def Level_I():
    Organizatio=Organisationstructurelevels.objects.all().order_by('Level_Id').last()
    if not Organizatio:
        return "LVL" + "000000001"
    Organizati = Organizatio.Level_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "LVL" + str(Organiza).zfill(9)
    return Configur


def EmployeeAddress_I():
    Organizatio=EmployeeAddress.objects.all().order_by('EmployeeAddress_Id').last()
    if not Organizatio:
        return "EAD" + "000000001"
    Organizati = Organizatio.EmployeeAddress_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "EAD" + str(Organiza).zfill(9)
    return Configur


def Employee_I():
    Organizatio=EmployeeMaster.objects.all().order_by('Employee_Id').last()
    if not Organizatio:
        return "EMP" + "000000001"
    Organizati = Organizatio.Employee_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "EMP" + str(Organiza).zfill(9)
    return Configur



def EmployeeQualification_I():
    Organizatio=EmployeeQualification.objects.all().order_by('EmployeeQualification_Id').last()
    if not Organizatio:
        return "EQL" + "000000001"
    Organizati = Organizatio.EmployeeQualification_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "EQL" + str(Organiza).zfill(9)
    return Configur


def EmployeeExperience_I():
    Organizatio=EmployeeExperience.objects.all().order_by('EmployeeExperience_Id').last()
    if not Organizatio:
        return "EXP" + "000000001"
    Organizati = Organizatio.EmployeeExperience_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "EXP" + str(Organiza).zfill(9)
    return Configur



def Employeesalesmonthlytarget_I():
    Organizatio=Employeesalesmonthlytarget.objects.all().order_by('Employeesalesmonthlytarget_Id').last()
    if not Organizatio:
        return "EMT" + "000000001"
    Organizati = Organizatio.Employeesalesmonthlytarget_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "EMT" + str(Organiza).zfill(9)
    return Configur


def Empmonthusertarget_I():
    Organizatio=Employeemonthlyusertarget.objects.all().order_by('Empmonthusertarget_Id').last()
    if not Organizatio:
        return "EUT" + "000000001"
    Organizati = Organizatio.Empmonthusertarget_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "EUT" + str(Organiza).zfill(9)
    return Configur


def Job_I():
    Organizatio=Servicejob.objects.all().order_by('Job_Id').last()
    if not Organizatio:
        return "JOB" + "000000001"
    Organizati = Organizatio.Job_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "JOB" + str(Organiza).zfill(9)
    return Configur



def Department_I():
    Organizatio=Department.objects.all().order_by('Department_Id').last()
    if not Organizatio:
        return "DEP" + "000000001"
    Organizati = Organizatio.Department_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "DEP" + str(Organiza).zfill(9)
    return Configur



def Skill_I():
    Organizatio=Skill.objects.all().order_by('Skill_Id').last()
    if not Organizatio:
        return "SKL" + "000000001"
    Organizati = Organizatio.Skill_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "SKL" + str(Organiza).zfill(9)
    return Configur



def Cadre_I():
    Organizatio=Cadre.objects.all().order_by('Cadre_Id').last()
    if not Organizatio:
        return "CAD" + "000000001"
    Organizati = Organizatio.Cadre_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "CAD" + str(Organiza).zfill(9)
    return Configur




def Designation_I():
    Organizatio=Designation.objects.all().order_by('Designation_Id').last()
    if not Organizatio:
        return "DES" + "000000001"
    Organizati = Organizatio.Designation_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "DES" + str(Organiza).zfill(9)
    return Configur



def Qualification_I():
    Organizatio=Qualification.objects.all().order_by('Qualification_Id').last()
    if not Organizatio:
        return "QUA" + "000000001"
    Organizati = Organizatio.Qualification_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "QUA" + str(Organiza).zfill(9)
    return Configur



def Employeealocation_I():
    Organizatio=Employeealocation.objects.all().order_by('Employeealocation_Id').last()
    if not Organizatio:
        return "EAL" + "000000001"
    Organizati = Organizatio.Employeealocation_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "EAL" + str(Organiza).zfill(9)
    return Configur


def JobAlocationMaster_I():
    Organizatio=JobAlocationMaster.objects.all().order_by('JobAlocationMaster_Id').last()
    if not Organizatio:
        return "JAL" + "000000001"
    Organizati = Organizatio.JobAlocationMaster_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "JAL" + str(Organiza).zfill(9)
    return Configur



def RoasterMaster_I():
    Organizatio=RoasterMaster.objects.all().order_by('RoasterMaster_Id').last()
    if not Organizatio:
        return "ROS" + "000000001"
    Organizati = Organizatio.RoasterMaster_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "ROS" + str(Organiza).zfill(9)
    return Configur



def Jobtimecard_I():
    Organizatio=Jobtimecard.objects.all().order_by('Jobtimecard_Id').last()
    if not Organizatio:
        return "JOT" + "000000001"
    Organizati = Organizatio.Jobtimecard_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "JOT" + str(Organiza).zfill(9)
    return Configur



def Holidaylist_I():
    Organizatio=Holidaylist.objects.all().order_by('Holidaylist_Id').last()
    if not Organizatio:
        return "HOL" + "000000001"
    Organizati = Organizatio.Holidaylist_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "HOL" + str(Organiza).zfill(9)
    return Configur

def Leavetype_I():
    Organizatio=Leavetype.objects.all().order_by('Leavetype_Id').last()
    if not Organizatio:
        return "LEA" + "000000001"
    Organizati = Organizatio.Leavetype_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "LEA" + str(Organiza).zfill(9)
    return Configur


def LeaveEmpRec_I():
    Organizatio=LeaveEmployeeRecord.objects.all().order_by('LeaveEmpRec_Id').last()
    if not Organizatio:
        return "LEM" + "000000001"
    Organizati = Organizatio.LeaveEmpRec_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "LEM" + str(Organiza).zfill(9)
    return Configur



def Application_I():
    Organizatio=Leave.objects.all().order_by('Application_Id').last()
    if not Organizatio:
        return "APL" + "000000001"
    Organizati = Organizatio.Application_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "APL" + str(Organiza).zfill(9)
    return Configur



def LeaveCancellation_I():
    Organizatio=Leavecancellation.objects.all().order_by('Cancellation_Id').last()
    if not Organizatio:
        return "LCN" + "000000001"
    Organizati = Organizatio.Cancellation_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "LCN" + str(Organiza).zfill(9)
    return Configur


def LvExtn_I():
    Organizatio=LeaveExtension.objects.all().order_by('LvExtn_Id').last()
    if not Organizatio:
        return "LEX" + "000000001"
    Organizati = Organizatio.LvExtn_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "LEX" + str(Organiza).zfill(9)
    return Configur



def Payrolsetup_I():
    Organizatio=Payrollsetup.objects.all().order_by('Payrolsetup_Id').last()
    if not Organizatio:
        return "PRL" + "000000001"
    Organizati = Organizatio.Payrolsetup_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "PRL" + str(Organiza).zfill(9)
    return Configur



def Payelement_I():
    Organizatio=Payelements.objects.all().order_by('Payelement_Id').last()
    if not Organizatio:
        return "PEL" + "000000001"
    Organizati = Organizatio.Payelement_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "PEL" + str(Organiza).zfill(9)
    return Configur


def Employeepayelement_I():
    Organizatio=Employeepayelement.objects.all().order_by('Employeepayelement_Id').last()
    if not Organizatio:
        return "EPL" + "000000001"
    Organizati = Organizatio.Employeepayelement_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "EPL" + str(Organiza).zfill(9)
    return Configur


def Formula_I():
    Organizatio=Paycalculationformula.objects.all().order_by('Formula_Id').last()
    if not Organizatio:
        return "FML" + "000000001"
    Organizati = Organizatio.Formula_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "FML" + str(Organiza).zfill(9)
    return Configur


def Payroll_I():
    Organizatio=PayCalcElements.objects.all().order_by('Payroll_Id').last()
    if not Organizatio:
        return "PAR" + "000000001"
    Organizati = Organizatio.Payroll_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "PAR" + str(Organiza).zfill(9)
    return Configur



def Payslip_I():
    Organizatio=PayslipHeader.objects.all().order_by('Payslip_Id').last()
    if not Organizatio:
        return "PSL" + "000000001"
    Organizati = Organizatio.Payslip_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "PSL" + str(Organiza).zfill(9)
    return Configur


def Supplieraddress_I():
    Organizatio=Supplieraddress.objects.all().order_by('Supplieraddress_Id').last()
    if not Organizatio:
        return "SAD" + "000000001"
    Organizati = Organizatio.Supplieraddress_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "SAD" + str(Organiza).zfill(9)
    return Configur


def Supplier_I():
    Organizatio=Supplier.objects.all().order_by('Supplier_Id').last()
    if not Organizatio:
        return "SUP" + "000000001"
    Organizati = Organizatio.Supplier_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "SUP" + str(Organiza).zfill(9)
    return Configur


def PR_I():
    Organizatio=PRDetails.objects.all().order_by('PR_Id').last()
    if not Organizatio:
        return "PRD" + "000000001"
    Organizati = Organizatio.PR_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "PRD" + str(Organiza).zfill(9)
    return Configur



def RFQ_I():
    Organizatio=RFQdetails.objects.all().order_by('RFQ_Id').last()
    if not Organizatio:
        return "RFQ" + "000000001"
    Organizati = Organizatio.RFQ_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "RFQ" + str(Organiza).zfill(9)
    return Configur



def Quotation_I():
    Organizatio=QuoteDetails.objects.all().order_by('Quotation_Id').last()
    if not Organizatio:
        return "QUO" + "000000001"
    Organizati = Organizatio.Quotation_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "QUO" + str(Organiza).zfill(9)
    return Configur


def Catalogue_I():
    Organizatio=Supplierpricedetails.objects.all().order_by('Catalogue_Id').last()
    if not Organizatio:
        return "SPD" + "000000001"
    Organizati = Organizatio.Catalogue_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "SPD" + str(Organiza).zfill(9)
    return Configur


def Buyer_I():
    Organizatio=Buyermaster.objects.all().order_by('Buyer_Id').last()
    if not Organizatio:
        return "BUY" + "000000001"
    Organizati = Organizatio.Buyer_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "BUY" + str(Organiza).zfill(9)
    return Configur


def PO_I():
    Organizatio=PODetails.objects.all().order_by('PO_Id').last()
    if not Organizatio:
        return "POD" + "000000001"
    Organizati = Organizatio.PO_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "POD" + str(Organiza).zfill(9)
    return Configur


def Invconfig_I():
    Organizatio=Inventoryconfiguration.objects.all().order_by('Invconfig_Id').last()
    if not Organizatio:
        return "INC" +"000000001"
    Organizati = Organizatio.Invconfig_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "INC" + str(Organiza).zfill(9)
    return Configur




def Mgr_I():
    Organizatio=MaterialGroup.objects.all().order_by('Mgr_Id').last()
    if not Organizatio:
        return "MTG" +"000000001"
    Organizati = Organizatio.Mgr_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "MTG" + str(Organiza).zfill(9)
    return Configur


def Store_I():
    Organizatio=Storemaster.objects.all().order_by('Store_Id').last()
    if not Organizatio:
        return "STO" +"000000001"
    Organizati = Organizatio.Store_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "STO" + str(Organiza).zfill(9)
    return Configur


def Storeitem_I():
    Organizatio=StoreItems.objects.all().order_by('Storeitem_Id').last()
    if not Organizatio:
        return "SIT" +"000000001"
    Organizati = Organizatio.Storeitem_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "SIT" + str(Organiza).zfill(9)
    return Configur



def GI_I():
    Organizatio=Goodsinwardetail.objects.all().order_by('GI_Id').last()
    if not Organizatio:
        return "GOI" +"000000001"
    Organizati = Organizatio.GI_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "GOI" + str(Organiza).zfill(9)
    return Configur


def Trfout_I():
    Organizatio=GoodsTrfOutDetail.objects.all().order_by('Trfout_Id').last()
    if not Organizatio:
        return "GTR" +"000000001"
    Organizati = Organizatio.Trfout_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "GTR" + str(Organiza).zfill(9)
    return Configur


def TrfIn_I():
    Organizatio=GoodsTrfReceiptDetails.objects.all().order_by('TrfIn_Id').last()
    if not Organizatio:
        return "GTI" +"000000001"
    Organizati = Organizatio.TrfIn_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "GTI" + str(Organiza).zfill(9)
    return Configur


def MatReq_I():
    Organizatio=MaterialRequestDetail.objects.all().order_by('MatReq_Id').last()
    if not Organizatio:
        return "MAT" +"000000001"
    Organizati = Organizatio.MatReq_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "MAT" + str(Organiza).zfill(9)
    return Configur


def MatIsu_I():
    Organizatio=MaterialIssueDetail.objects.all().order_by('MatIsu_Id').last()
    if not Organizatio:
        return "MAI" +"000000001"
    Organizati = Organizatio.MatIsu_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "MAI" + str(Organiza).zfill(9)
    return Configur


def MatRet_I():
    Organizatio=Materialreturndetails.objects.all().order_by('MatRet_Id').last()
    if not Organizatio:
        return "MAR" +"000000001"
    Organizati = Organizatio.MatRet_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "MAR" + str(Organiza).zfill(9)
    return Configur

# new add
def MatTransf_I():    
    Organizatio=MaterialTransferDetails.objects.all().order_by('MatTransf_Id').last()
    if not Organizatio:
        return "MAT" +"000000001"
    Organizati = Organizatio.MatTransf_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "MAT" + str(Organiza).zfill(9)
    return Configur



def SCN_I():
    Organizatio=StoresCreditNote.objects.all().order_by('SCN_Id').last()
    if not Organizatio:
        return "SCN" +"000000001"
    Organizati = Organizatio.SCN_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "SCN" + str(Organiza).zfill(9)
    return Configur

def Company_I():
    Organizatio=Company.objects.all().order_by('Company_Id').last()
    if not Organizatio:
        return "CMP" +"000000001"
    Organizati = Organizatio.Company_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "CMP" + str(Organiza).zfill(9)
    return Configur


# add by ponkumar
def Item_Group_I():
    Organizatio=ItemGroup.objects.all().order_by('Item_Group_Id').last()
    if not Organizatio:
        return "ITG" + "000000001"
    Organizati = Organizatio.Item_Group_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "ITG" + str(Organiza).zfill(9)
    return Configur


def Item_Category_I():
    Organizatio=ItemCategory.objects.all().order_by('Item_Category_Id').last()
    if not Organizatio:
        return "ITC" + "000000001"
    Organizati = Organizatio.Item_Category_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "ITC" + str(Organiza).zfill(9)
    return Configur

def Organization_Structure_I():
    Organizatio=OrganizationStructure.objects.all().order_by('Organization_Structure_Id').last()
    if not Organizatio:
        return "ORG" + "0000000001"
    Organizati = Organizatio.Organization_Structure_Id
    Organizat=int(Organizati[0:])
    Organiza=Organizat + 1
    Configur = "ORG"+str(Organiza).zfill(10)
    return Configur


def Entity_I():
    Organizatio=Entity.objects.all().order_by('Entity_Id').last()
    if not Organizatio:
        return "1"
    Organizati = Organizatio.Entity_Id
    Organizat=int(Organizati[0:])
    Organiza=Organizat + 1
    Configur = str(Organiza).zfill(1)
    return Configur


def Country_I():
    Organizatio=Country.objects.all().order_by('Country_Id').last()
    if not Organizatio:
        return "001"
    Organizati = Organizatio.Country_Id
    Organizat=int(Organizati[0:])
    Organiza=Organizat + 1
    Configur = str(Organiza).zfill(3)
    return Configur


def Region_I():
    Organizatio=Region.objects.all().order_by('Region_Id').last()
    if not Organizatio:
        return "1"
    Organizati = Organizatio.Region_Id
    Organizat=int(Organizati[0:])
    Organiza=Organizat + 1
    Configur = str(Organiza).zfill(1)
    return Configur


def State_I():
    Organizatio=State.objects.all().order_by('State_Id').last()
    if not Organizatio:
        return "001"
    Organizati = Organizatio.State_Id
    Organizat=int(Organizati[0:])
    Organiza=Organizat + 1
    Configur = str(Organiza).zfill(3)
    return Configur


def City_I():
    Organizatio=City.objects.all().order_by('City_Id').last()
    if not Organizatio:
        return "0001"
    Organizati = Organizatio.City_Id
    Organizat=int(Organizati[0:])
    Organiza=Organizat + 1
    Configur = str(Organiza).zfill(4)
    return Configur

def TimeSheet_I():
    Organizatio=TimeSheet.objects.all().order_by('TimeSheet_Id').last()
    if not Organizatio:
        return "TIM" + "000000001"
    Organizati = Organizatio.TimeSheet_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "TIM"+str(Organiza).zfill(9)
    return Configur


def Time_I():
    Organizatio=TimeManagement.objects.all().order_by('Time_Id').last()
    if not Organizatio:
        return "TMN" + "000000001"
    Organizati = Organizatio.Time_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "TMN" +str(Organiza).zfill(9)
    return Configur



def Attandance_I():
    Organizatio=AttandanceManagement.objects.all().order_by('Attandance_Id').last()
    if not Organizatio:
        return "ATN" + "000000001"
    Organizati = Organizatio.Attandance_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "ATN" +str(Organiza).zfill(9)
    return Configur




def Attandancemaster_I():
    Organizatio=Attandancemaster.objects.all().order_by('Attandancemaster_Id').last()
    if not Organizatio:
        return "ATN" + "000000001"
    Organizati = Organizatio.Attandancemaster_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "ATN" +str(Organiza).zfill(9)
    return Configur

def Payrollconfig_I():
    Organizatio=Payrollconfiguration.objects.all().order_by('Payrollconfig_Id').last()
    if not Organizatio:
        return "PYC" + "000000001"
    Organizati = Organizatio.Payrollconfig_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "PYC" +str(Organiza).zfill(9)
    return Configur



def AnnualCTC_I():
    Organizatio=EmployeeAnnualCTC.objects.all().order_by('AnnualCTC_Id').last()
    if not Organizatio:
        return "EAC" +"000000001"
    Organizati = Organizatio.AnnualCTC_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "EAC" + str(Organiza).zfill(9)
    return Configur

def Employeepf_I():
    Organizatio=EmployeeProvidentfund.objects.all().order_by('Employeepf_Id').last()
    if not Organizatio:
        return "EPF" +"000000001"
    Organizati = Organizatio.Employeepf_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "EPF" + str(Organiza).zfill(9)
    return Configur



def EmployeeESI_I():
    Organizatio=EmployeeESI.objects.all().order_by('EmployeeESI_Id').last()
    if not Organizatio:
        return "ESI" +"000000001"
    Organizati = Organizatio.EmployeeESI_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "ESI" + str(Organiza).zfill(9)
    return Configur



def EmployeePT_I():
    Organizatio=EmployeeProfessionalTax.objects.all().order_by('EmployeePT_Id').last()
    if not Organizatio:
        return "EPT" +"000000001"
    Organizati = Organizatio.EmployeePT_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "EPT" + str(Organiza).zfill(9)
    return Configur



def Earning_I():
    Organizatio=EmployeeEarning.objects.all().order_by('Earning_Id').last()
    if not Organizatio:
        return "EER" +"000000001"
    Organizati = Organizatio.Earning_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "EER" + str(Organiza).zfill(9)
    return Configur



def Deduction_I():
    Organizatio=EmployeeDeduction.objects.all().order_by('Deduction_Id').last()
    if not Organizatio:
        return "EED" +"000000001"
    Organizati = Organizatio.Deduction_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "EED" + str(Organiza).zfill(9)
    return Configur


def POC_I():
    Organizatio=POCancelDetails.objects.all().order_by('POCancel_Id').last()
    if not Organizatio:
        return "POC" + "000000001"
    Organizati = Organizatio.POCancel_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "POC" + str(Organiza).zfill(9)
    return Configur



def POA_I():
    Organizatio=POAmendDetails.objects.all().order_by('POAmend_Id').last()
    if not Organizatio:
        return "POA" + "000000001"
    Organizati = Organizatio.POAmend_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "POA" + str(Organiza).zfill(9)
    return Configur


def StoreAdust_I():
    Organizatio=StoreAdjustment.objects.all().order_by('StoreAdj_Id').last()
    if not Organizatio:
        return "SIA" +"000000001"
    Organizati = Organizatio.StoreAdj_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "SIA" + str(Organiza).zfill(9)
    return Configur



def Cat_I():
    Organizatio=Catalogue.objects.all().order_by('Cat_Id').last()
    if not Organizatio:
        return "CAT" + "000000001"
    Organizati = Organizatio.Cat_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "CAT" + str(Organiza).zfill(9)
    return Configur



def Itempreference_I():
    Organizatio=Itempreference.objects.all().order_by('Itempreference_Id').last()
    if not Organizatio:
        return "ITP" + "000000001"
    Organizati = Organizatio.Itempreference_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "ITP" + str(Organiza).zfill(9)
    return Configur


def Salesorderpreference_I():
    Organizatio=Salesorderpreference.objects.all().order_by('Salesorderpreference_Id').last()
    if not Organizatio:
        return "SOP" + "000000001"
    Organizati = Organizatio.Salesorderpreference_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "SOP" + str(Organiza).zfill(9)
    return Configur



def Invoicepreference_I():
    Organizatio=Invoicepreference.objects.all().order_by('Invoicepreference_Id').last()
    if not Organizatio:
        return "INP" + "000000001"
    Organizati = Organizatio.Invoicepreference_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "INP" + str(Organiza).zfill(9)
    return Configur



def Purchasepreference_I():
    Organizatio=Purchasepreference.objects.all().order_by('Purchasepreference_Id').last()
    if not Organizatio:
        return "PUP" + "000000001"
    Organizati = Organizatio.Purchasepreference_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "PUP" + str(Organiza).zfill(9)
    return Configur


def Budgetpreference_I():
    Organizatio=Budgetpreference.objects.all().order_by('Budgetpreference_Id').last()
    if not Organizatio:
        return "BUP" + "000000001"
    Organizati = Organizatio.Budgetpreference_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "BUP" + str(Organiza).zfill(9)
    return Configur


def Inventorypreference_I():
    Organizatio=Inventorypreference.objects.all().order_by('Inventorypreference_Id').last()
    if not Organizatio:
        return "IVP" + "000000001"
    Organizati = Organizatio.Inventorypreference_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "IVP" + str(Organiza).zfill(9)
    return Configur



def Transactionapprovalpreference_I():
    Organizatio=Transactionapprovalpreference.objects.all().order_by('Transactionapprovalpreference_Id').last()
    if not Organizatio:
        return "TRP" + "000000001"
    Organizati = Organizatio.Transactionapprovalpreference_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "TRP" + str(Organiza).zfill(9)
    return Configur



def Transactionalseries_I():
    Organizatio=Transactionalseries.objects.all().order_by('Transactionalseries_Id').last()
    if not Organizatio:
        return "TSS" + "000000001"
    Organizati = Organizatio.Transactionalseries_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "TSS" + str(Organiza).zfill(9)
    return Configur





def Dropdown_I():
    Organizatio=Dropdowntable.objects.all().order_by('Dropdown_Id').last()
    if not Organizatio:
        return "DRO" + "000000001"
    Organizati = Organizatio.Dropdown_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "DRO" + str(Organiza).zfill(9)
    return Configur



def Documentapproval_I():
    Organizatio=Documentapproval.objects.all().order_by('Documentapproval_Id').last()
    if not Organizatio:
        return "DOA" + "000000001"
    Organizati = Organizatio.Documentapproval_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "DOA" + str(Organiza).zfill(9)
    return Configur


def Ticket_I():
    Organizatio=Ticket.objects.all().order_by('Ticket_Id').last()
    if not Organizatio:
        return "TIC" + "000000001"
    Organizati = Organizatio.Ticket_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "TIC" + str(Organiza).zfill(9)
    return Configur


def AssigningTicket_I():
    Organizatio=AssigningTicket.objects.all().order_by('AssigningTicket_Id').last()
    if not Organizatio:
        return "AST" + "000000001"
    Organizati = Organizatio.AssigningTicket_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "AST" + str(Organiza).zfill(9)
    return Configur



def TicketResponse_I():
    Organizatio=TicketResponse.objects.all().order_by('TicketResponse_Id').last()
    if not Organizatio:
        return "TRS" + "000000001"
    Organizati = Organizatio.TicketResponse_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "TRS" + str(Organiza).zfill(9)
    return Configur


def Attachment_I():
    Attachmen=Emailattachment.objects.all().order_by('Attachment_Id').last()
    if not Attachmen:
        return "ATT" + "000000001"
    Attachme=Attachmen.Attachment_Id
    Attachm = int(Attachme[4:])
    Attach = Attachm + 1
    Attac = "ATT" + str(Attach).zfill(9)
    return Attac


def Approval_I():
    Organizatio=Approval.objects.all().order_by('Approval_Id').last()
    if not Organizatio:
        return "APR" + "000000001"
    Organizati = Organizatio.Approval_Id
    Organizat=int(Organizati[4:])
    Organiza=Organizat + 1
    Configur = "APR" + str(Organiza).zfill(9)
    return Configur



#Tables for the Configuration management

class Configuration(models.Model):

    Configuration_Id=models.CharField(max_length=20,default=Configuration_I,unique=True)
    Date_Format = models.DateTimeField(default=timezone.now(),blank=True)
    Date_Seperator=models.CharField(max_length=10,blank=True)
    List_View_Datesorting=models.CharField(max_length=20,blank=True)  
    Password_Expiry_Days=models.CharField(max_length=20,blank=True)  
    Multidevice_Login=models.BooleanField(default=False)
    Admins_UerId=models.CharField(max_length=20)  
    Admin_Password=models.CharField(max_length=50)
    Admin_EmailId=models.EmailField(max_length=50)
    Smtp_EmailId=models.EmailField(max_length=50)
    Smtp_EmailPassword=models.CharField(max_length=50)
    AP_Invoice_Matching=models.CharField(max_length=50,blank=True)  #
    Trackreceipts_AR=models.CharField(max_length=50,blank=True)  #
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdconfiguration')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedconfiguration')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Configuration'


class Organization(models.Model):

    Organization_Id=models.CharField(max_length=20,unique=True,default=Organization_I) #
    Organization_Name=models.CharField(max_length=50,blank=True)
    Organization_Web=models.CharField(max_length=100,blank=True)
    Organization_Status=models.CharField(max_length=100,blank=True)
    Organization_PAN=models.CharField(max_length=100,blank=True)
    Organization_GST=models.CharField(max_length=100,blank=True)
    Organization_PFNo=models.CharField(max_length=100,blank=True)
    Organization_EsiNo=models.CharField(max_length=100,blank=True)
    Organization_Logo=models.CharField(max_length=100,blank=True)#
    Organizationincorporation_Date=models.DateTimeField(default=timezone.now())
    Organizationbusiness_Entity=models.CharField(max_length=100,blank=True)
    Organizationbusiness_Industry=models.CharField(max_length=100,blank=True)
    Organization_Directorname1=models.CharField(max_length=100,blank=True)
    Organization_Directorname2=models.CharField(max_length=100,blank=True)
    Organization_Directorname3=models.CharField(max_length=100,blank=True)
    Organization_ProftaxNo=models.CharField(max_length=50,blank=True)
    Organization_IECNo=models.CharField(max_length=50,blank=True)
    Organization_CINNo=models.CharField(max_length=50,blank=True)
    Organization_Phone=models.CharField(max_length=50,blank=True)
    Organization_Email=models.EmailField(unique=True,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdOrganization')#
    Created_Date = models.DateField(default=date.today())#
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedOrganization')#
    Updated_Date = models.DateField(default=date.today())#

    class Meta:
        db_table = 'Organization'





class Communication_Address(models.Model):

    Address_Id=models.CharField(max_length=20,unique=True,default=Address_I)#
    # Organisation_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)#
    Address_Type=models.CharField(max_length=100,blank=True)
    Building_Name=models.CharField(max_length=200,blank=True)
    Building_Street=models.CharField(max_length=200,blank=True)
    Building_Area=models.CharField(max_length=200,blank=True)
    Building_City=models.CharField(max_length=200,blank=True)
    Building_State=models.CharField(max_length=50,blank=True)
    Building_Country=models.CharField(max_length=50,blank=True)
    Zip_Code=models.CharField(max_length=50,blank=True)#
    Active_Status=models.BooleanField(default=True)#
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdCommunicationaddress')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedCommunicationaddress')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Communication_Address'




class Business_Hours(models.Model):
    Businesshours_Id = models.CharField(max_length=20,unique=True,default=Businesshours_I)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Monday_Chekbox=models.BooleanField(default=False)#
    Monday_Starttime=models.CharField(max_length=20, blank=True)
    Monday_EndTime=models.CharField(max_length=20, blank=True)
    Tuesday_Chekbox=models.BooleanField(default=False)
    Tuesday_Starttime=models.CharField(max_length=20, blank=True)
    Tuesday_Endtime=models.CharField(max_length=20, blank=True)
    Wednessday_Chekbox=models.BooleanField(default=False)
    Wednessday_Starttime=models.CharField(max_length=20, blank=True)
    Wednessday_Endtime=models.CharField(max_length=20, blank=True)
    Thursday_Chekbox=models.BooleanField(default=False)
    Thursday_Starttime=models.CharField(max_length=20, blank=True)
    Thursday_Endtime=models.CharField(max_length=20, blank=True)
    Friday_Chekbox=models.BooleanField(default=False)
    Friday_Starttime=models.CharField(max_length=20, blank=True)
    Friday_Endtime=models.CharField(max_length=20, blank=True)
    Saturday_Chekbox=models.BooleanField(default=False)
    Saturday_Starttime=models.CharField(max_length=20, blank=True)
    Saturday_Endtime=models.CharField(max_length=20, blank=True)
    Sunday_Chekbox=models.BooleanField(default=False)
    Sunday_Starttime=models.CharField(max_length=20, blank=True)
    Sunday_Endtime=models.CharField(max_length=20, blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdBusinesshour')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedBusinesshour')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Business_Hours'





# class Financial_Calendar_Header(models.Model):

#     #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
#     Calendar_Key=models.IntegerField(primary_key=True)
#     Calendar_Id=models.CharField(max_length=1000)
#     CalendarName=models.CharField(max_length=100)
#     Calendar_FinancialYear=models.DateField(default=date.today())#
#     Calendar_From_Date=models.DateField(default=date.today())#
#     Calendar_To_Date=models.DateField(default=date.today())#
#     Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdFinancial_Calendar')
#     Created_Date = models.DateField(default=date.today())
#     Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedFinancial_Calendar')
#     Created_Date = models.DateField(default=date.today())

#     class Meta:
#         db_table = 'Financial_Calendar_Header'


class Financial_Calendar_Details(models.Model):

    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Calendar_Id=models.CharField(max_length=20,unique=True,default=Calendar_I)
    Calendar_MonthName=models.CharField(max_length=20,blank=True)#
    CalendarName=models.CharField(max_length=100,blank=True)
    Calender_Year=models.DateField(default=date.today())#
    Calandermonth = models.CharField(max_length=50,blank=True)
    Calendar_FinancialYear=models.DateField(default=date.today())#
    Calender_QuarterName=models.CharField(max_length=50,blank=True)#
    Calender_MonthFromDate=models.DateField(default=date.today())#
    Calendar_From_Date=models.DateField(default=date.today())#
    Calendar_To_Date=models.DateField(default=date.today())#
    Calender_MonthTodate=models.DateField(default=date.today())#
    Calender_MonthClosedFlag=models.DateField(default=date.today())#
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdFinancial_Calendar_Details')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedFinancial_Calendar_Details')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Financial_Calendar_Details'




        
class Company(models.Model):

    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Company_Id=models.CharField(max_length=20,default=Company_I,unique=True)
    Company_Name = models.CharField(max_length=100,blank=True)
    CompanyLogo= models.FileField(max_length=100000,blank=True,default="default.png")
    CompanyMail = models.CharField(max_length=50)
    CompanyAddress=models.ForeignKey(Communication_Address,on_delete=models.CASCADE,blank=True)
    DateFormat = models.CharField(max_length=50,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Fiscal_Year = models.ForeignKey(Financial_Calendar_Details,on_delete=models.CASCADE)
    Business_Hour = models.ForeignKey(Business_Hours,on_delete=models.CASCADE)
    Updated_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdcompany')
    Created_Date = models.DateField(default=date.today())
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedcompany')
    Currencies = models.CharField(max_length=50,blank=True)

    class Meta:
        db_table='Company'


class Partner(models.Model):
    Company_Id = models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id=models.CharField(max_length=20,unique=True,default=Partner_I)
    Partner_Name=models.CharField(max_length=50,blank=True)
    Operated_By =  models.CharField(max_length=50,blank=True)
    HeadOf_Operation=models.CharField(max_length=100,blank=True)#
    BusinessUnit_Phone=models.CharField(max_length=200,blank=True)
    BusinessUnit_EMail=models.EmailField(max_length=200,blank=True,unique=True)
    BusinessUnit_Web=models.CharField(max_length=50,blank=True)
    BusinessUnit_Addressline1=models.CharField(max_length=100,blank=True)#
    BusinessUnit_City=models.CharField(max_length=100,blank=True)
    BusinessUnit_Country=models.CharField(max_length=100,blank=True)
    BusinessUnit_State=models.CharField(max_length=100,blank=True)
    BusinessUnit_Zone=models.CharField(max_length=100,blank=True)
    BusinessUnit_Pincode=models.CharField(max_length=50,blank=True)
    BusinessUnit_Logo=models.FileField(default='default.png',blank=True)
    BusinessUnit_PanNo=models.CharField(max_length=100,blank=True)
    BusinessUnit_GSTNo=models.CharField(max_length=100,blank=True)
    Agreement_RefNo=models.CharField(max_length=200,blank=True)
    Agreement_Date=models.DateField(default=date.today())#
    Agreement_Validupto=models.DateField(default=date.today())#
    Budget_flg = models.BooleanField(default=False)
    Budget_Edit = models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdPartner')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedPartner')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Partner'


class BusinessUnit(models.Model):
    BusinessUnit_Id=models.CharField(max_length=20,unique=True,default=BusinessUnit_I)
    BusinessUnit_Name=models.CharField(max_length=50,blank=True)
    Operated_By =  models.CharField(max_length=50,blank=True)
    HeadOf_Operation=models.CharField(max_length=100,blank=True)#
    BusinessUnit_Phone=models.CharField(max_length=200,blank=True)
    BusinessUnit_EMail=models.EmailField(max_length=200,blank=True,unique=True)
    BusinessUnit_Web=models.CharField(max_length=50,blank=True)
    BusinessUnit_Addressline1=models.CharField(max_length=100,blank=True)#
    BusinessUnit_City=models.CharField(max_length=100,blank=True)
    BusinessUnit_Country=models.CharField(max_length=100,blank=True)
    BusinessUnit_State=models.CharField(max_length=100,blank=True)
    BusinessUnit_Pincode=models.CharField(max_length=50,blank=True)
    BusinessUnit_Logo=models.FileField(default='default.png',blank=True)
    BusinessUnit_PanNo=models.CharField(max_length=100,blank=True)
    BusinessUnit_GSTNo=models.CharField(max_length=100,blank=True)
    Agreement_RefNo=models.CharField(max_length=200,blank=True)
    Agreement_Date=models.DateField(default=date.today())#
    Agreement_Validupto=models.DateField(default=date.today())#
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdBusinessUnit')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedBusinessUnit')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'BusinessUnit'





class ERPModules(models.Model):

    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Module_Id=models.CharField(max_length=20,unique=True,default=Module_I)
    Module_Name=models.CharField(max_length=50,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdERPmodules')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedERPmodules')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'ERPModules'


class ERPForms(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Module_Id=models.ForeignKey(ERPModules, on_delete=models.CASCADE)
    Form_Id=models.CharField(max_length=20,unique=True,default=Form_I)
    Form_Name=models.CharField(max_length=50,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdERPForm')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedERPForm')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'ERPForms'


class AutogenNos(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Autogen_Id=models.CharField(max_length=20,unique=True,default=Autogen_I)#
    Form_Id=models.ForeignKey(ERPForms, on_delete=models.CASCADE)
    Autogen_Type=models.CharField(max_length=50,blank=True)
    Autogen_Prefix=models.CharField(max_length=50,blank=True)#
    Autogen_Suffix=models.CharField(max_length=50,blank=True)#
    Autogen_Startno=models.CharField(max_length=50,blank=True)#
    Autogen_Endno=models.CharField(max_length=50,blank=True)#
    Approval_Required=models.CharField(max_length=20,blank=True)#
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdAutogenNOS')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedAutogenNos')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'AutogenNos'


class ErrorCodes(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Error_Id=models.CharField(max_length=20,unique=True,default=Error_I)
    Error_Category=models.CharField(max_length=1000,blank=True)
    Errordisplay_Name=models.CharField(max_length=1000,blank=True)
    Error_Remarks=models.CharField(max_length=1000,blank=True)
    Error_Type=models.CharField(max_length=1000,blank=True)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdErrordodes')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedErrorcodes')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'ErrorCodes'



class Notifications(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id = models.ForeignKey(Partner,on_delete=models.CASCADE,null=True,blank=True)
    User_Id=models.ForeignKey(User,on_delete=models.CASCADE,null=True,blank=True)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Notification_Id=models.CharField(max_length=20,unique=True,default=Notification_I)
    Notification_Msg=models.CharField(max_length=2500,blank=True)
    Notification_Time = models.DateTimeField(default=timezone.now())
    Open_flg = models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdNotification')
    Created_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Notifications'


class Reminder(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id = models.ForeignKey(Partner,on_delete=models.CASCADE,null=True,blank=True)
    User_Id=models.ForeignKey(User,on_delete=models.CASCADE,null=True,blank=True)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Reminder_Msg=models.CharField(max_length=2500,blank=True)
    Reminder_Time = models.DateTimeField(default=timezone.now())
    Open_flg = models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdReminder')
    Created_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Reminder'


class Territory(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Territory_Id=models.CharField(max_length=20,unique=True,default=Territory_I)
    Territory_Name=models.CharField(max_length=100,blank=True)
    Territorry_Manager=models.CharField(max_length=100,blank=True)
    Parent_Child=models.CharField(max_length=100,blank=True)#
    Parent_Teritory=models.CharField(max_length=100,blank=True)#
    Teritory_Operation = models.CharField(max_length=250, blank=True)
    Assign_Values = models.JSONField(default={},blank=True,null=True)
    Assign_Sales_Persion = models.JSONField(default={},blank=True,null=True)
    User_Keys=models.ForeignKey(User, on_delete=models.CASCADE)#
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdTerritory')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedTerritory')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Territory'

# class HRCalendar(models.Model):
#     Organization_Key=models.ForeignKey(Organization, on_delete=models.CASCADE)
#     Calendar_Key=models.IntegerField(primary_key=True)
#     Calendar_Id=models.CharField(max_length=100)
#     Calendar_Name=models.CharField(max_length=100)
#     Holidaymaster=models.CharField(max_length=100)
#     Leave_CalculationMonth=models.CharField(max_length=100)#
#     Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdHRCalendar ')
#     Created_Date = models.DateField(default=date.today())
#     Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedHRCalendar')
#     Created_Date = models.DateField(default=date.today())

#     class Meta:
#         db_table = ' HRCalendar'


class Templates(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Template_Id=models.CharField(max_length=20,unique=True,default=Template_I)
    Template_Name=models.CharField(max_length=50,blank=True)
    Template_RelatesTo=models.CharField(max_length=50,blank=True)#
    Fields_used=models.JSONField(blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdTemplates')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedTemplates')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Templates'

class WorkflowRule(models.Model):
    Partner_Id=models.ForeignKey(Partner, on_delete=models.CASCADE)
    WorkflowRule_Id=models.CharField(max_length=20,unique=True,default=WorkflowRule_I)
    Rule_Name = models.CharField(max_length=100,blank=True)
    Action = models.CharField(max_length=250,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdworkflowrule')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedworkflowrule')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'WorkflowRule'


class WorkflowName(models.Model):
    Partner_Id=models.ForeignKey(Partner, on_delete=models.CASCADE)
    WorkflowName_Id= models.CharField(max_length=20,unique=True,default=WorkflowName_I)
    WorkflowRule_Id=models.ForeignKey(WorkflowRule,on_delete=models.CASCADE,blank=True,default=1)
    Rule_Name = models.CharField(max_length=100,blank=True)
    Workflow_Name=models.CharField(max_length=50,blank=True)
    Workflow_Basedon=models.CharField(max_length=50,blank=True)
    Noof_Level=models.IntegerField(blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdworkflowname')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedworkflowname')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'WorkflowName'


class Assignworkflow(models.Model):
    Partner_Id=models.ForeignKey(Partner, on_delete=models.CASCADE)
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    WorkflowName_Id= models.ForeignKey(WorkflowName,on_delete=models.CASCADE,blank=True,null=True)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Assignworkflow_Id=models.CharField(max_length=20,unique=True,default=Assignworkflow_I)
    Workflow_Name=models.CharField(max_length=50,blank=True)
    Rule_Name = models.CharField(max_length=100,blank=True)
    Workflow_Basedon=models.CharField(max_length=50,blank=True)#
    Document_Name = models.CharField(max_length=100,blank=True)
    Field_Name=models.CharField(max_length=100,blank=True)
    Noof_Level=models.IntegerField(blank=True)
    Head_Approval = models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdAssignworkflow')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedAssignworkflow')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Assignworkflow'


class Workflow(models.Model):
    Partner_Id=models.ForeignKey(Partner, on_delete=models.CASCADE)
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Assignworkflow_Id = models.ForeignKey(Assignworkflow,on_delete=models.CASCADE,null=True)
    # WorkflowName_Id= models.ForeignKey(WorkflowName,on_delete=models.CASCADE,blank=True,null=True)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Workflow_Id=models.CharField(max_length=20,unique=True,default=Workflow_I)
    Workflow_Name=models.CharField(max_length=50,blank=True)
    Workflow_Basedon=models.CharField(max_length=50,blank=True)#
    Document_Name = models.CharField(max_length=100,blank=True)
    Field_Name=models.CharField(max_length=100,blank=True)
    Noof_Level=models.IntegerField(blank=True)
    Amountjosn = models.JSONField(default={},blank=True)
    leveljson = models.JSONField(default={},blank=True)
    Head_Approval = models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdworkflow')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedworkflow')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Workflow'



class Workfloworigin(models.Model):
    Workfloworigin_Id=models.CharField(max_length=20,unique=True,default=Workfloworigin_I)
    Form_Name = models.CharField(max_length=100)
    Field_Name = models.CharField(max_length=100)
    FieldShort_Name = models.CharField(max_length=100,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdorigin')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedorigin')
    Updated_Date = models.DateField(default=date.today())
  
    class Meta:
        db_table = 'Workfloworigin'




#Tables for the user management

class Role(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)# need to clarify
    Role_Id=models.CharField(max_length=20)# need to clarify
    Role_Name=models.CharField(max_length=50,blank=True)
    Role_Description=models.CharField(max_length=500,blank=True)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdRole')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedRole')
    Updated_Date = models.DateField(default=date.today())
    Is_Deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'Role'



class RolePermission(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)# need to clarify
    Rolepermission_Id=models.CharField(max_length=20,unique=True,default=Rolepermission_I)
    Role_Id=models.ForeignKey(Role, on_delete=models.CASCADE)
    Form_Id=models.ManyToManyField(ERPForms)
    Create_Permission=models.CharField(max_length=50,blank=True)#
    Edit_Permission=models.CharField(max_length=50,blank=True)#
    Remove_Permission=models.CharField(max_length=50,blank=True)#
    AllPermission=models.CharField(max_length=50,blank=True)#
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdRolePermission')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedRolePermission')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'RolePermission'


User.add_to_class("Organization_Id", models.IntegerField(default=1))
# # User.add_to_class("Role_Id",models.ForeignKey(Role,on_delete=models.CASCADE,default='',blank=True))
User.add_to_class("User_Name",models.CharField(max_length=100,default='',blank=True))
User.add_to_class("User_emailId",models.CharField(unique=True,max_length=100,blank=True))
User.add_to_class("User_Id", models.CharField(default=User_I,max_length=20, unique=True,blank=True))
User.add_to_class("User_OldPassword", models.CharField(max_length=100, default='', blank=True))
User.add_to_class("Designation", models.CharField(max_length=100, default='', blank=True))
User.add_to_class("User_PwdUpdatedate", models.DateField(default=date.today()))
User.add_to_class("User_Login_Status", models.BooleanField(default=True, blank=True))
User.add_to_class("User_Photo", models.FileField(max_length=1000000, default='default.png', blank=True))
User.add_to_class("PhoneNo", models.CharField(max_length=100, default='', blank=True))
User.add_to_class("BuildingHuseNo", models.CharField(max_length=100,blank=True,default=''))
User.add_to_class("Street", models.CharField(max_length=100, default='', blank=True))
User.add_to_class("Area", models.CharField(max_length=100, default='', blank=True))
User.add_to_class("City", models.CharField(max_length=100, default='', blank=True))
User.add_to_class("State", models.CharField(max_length=100, default='', blank=True))
User.add_to_class("Country", models.CharField(max_length=100, default='', blank=True))
Choice = (("SuperAdmin", "SuperAdmin"), ("Admin", "Admin"), ("User", "User"))
User.add_to_class("UserType", models.CharField(max_length=100, default="Admin"))
User.add_to_class("Business_Unit",models.CharField(max_length=150,blank=True))
User.add_to_class("Url", models.CharField(max_length=1000, default='', blank=True))
User.add_to_class("Created_Date",models.DateField(default=date.today(),blank=True))
User.add_to_class("Updated_Date",models.DateField(default=date.today(),blank=True))
User.add_to_class("Is_Deleted",models.BooleanField(default=False))



class FormPermission(models.Model):
    # RolePermission_Id=models.CharField(default=RolePermissionI,max_length=20,unique=True)
    # Organization_Id=models.ForeignKey(Organizationmaster,on_delete=models.CASCADE)
    FormId = models.CharField(default=Form_I, max_length=20, unique=True)
    ModuleName = models.CharField(max_length=100,blank=True)
    FormName = models.CharField(max_length=100,blank=True)
    Field_Name = models.JSONField(default={},blank=True)
    PermissionName = models.JSONField(blank=True, default={})
    ModulePermission = models.ManyToManyField(Permission)
    AllPermission = models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE)
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedpermision')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'FormPermission'

class FormMaster(models.Model):
    FormmasterId = models.CharField(default=Formmaster_I, max_length=20, unique=True)
    ModuleName = models.CharField(max_length=100,blank=True)
    FormId= models.ManyToManyField(FormPermission)
    Is_Deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'FormMaster'


Group.add_to_class("RolePermissions", models.ManyToManyField(RolePermission,blank=True))
Group.add_to_class("FormPermissions",models.ManyToManyField(ERPForms,blank=True))
# Group.add_to_class("Organization_Id", models.ForeignKey(Organization, on_delete=models.CASCADE,blank=True))
Group.add_to_class("Role_Id", models.CharField(default=Role_I,unique=True, max_length=20, blank=True))
Group.add_to_class("Role_Name",models.CharField(max_length=50,blank=True))
Group.add_to_class("Description", models.CharField(max_length=1000, blank=True))
Group.add_to_class('Is_Deleted', models.BooleanField(default=False))
Group.add_to_class('Created_By', models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdGroupPermission'))
Group.add_to_class('Updated_By',models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedGroupPermission'))
Group.add_to_class("Created_Date",models.DateField(default=date.today()))
Group.add_to_class("Updated_Date",models.DateField(default=date.today()))
Group.add_to_class("FormNames",models.ManyToManyField(FormPermission,blank=True))
Group.add_to_class("Modelname",models.ManyToManyField(FormMaster,blank=True))


class MailConfiguration(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    # Organization_Id = models.ForeignKey(Organization, on_delete=models.CASCADE)
    User_Id = models.ForeignKey(User, on_delete=models.CASCADE, unique=True)
    SMTPHost = models.CharField(max_length=250)
    SMTPPort = models.CharField(max_length=25)
    IMAPHost = models.CharField(max_length=250, blank=True,null=True)
    IMAPPort = models.CharField(max_length=25,blank=True,null=True)
    MailId = models.EmailField(unique=True)
    MailPassword = models.CharField(max_length=250)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createduseremail')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updateuseremail')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'MailConfiguration'

class MailMaster(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    MailId = models.CharField(max_length=25, unique=True)
    Organization_Id = models.ForeignKey(Organization, on_delete=models.CASCADE)
    MailConfiguration = models.ForeignKey(MailConfiguration, on_delete=models.CASCADE)
    MailSubject = models.CharField(max_length=25000)
    MailBody = models.TextField()
    RecevierMailId = models.CharField(max_length=250)
    NoofTimesMailOpened = models.IntegerField(default=0)
    NoofTimesMailClicked = models.IntegerField(default=0)
    NoofTimesFileisDownloaded = models.IntegerField(default=0)
    RecevierResponded = models.BooleanField(default=False)
    MailBouned = models.BooleanField(default=False)
    MailReplied = models.BooleanField(default=False)
    MailSentDate = models.DateTimeField(default=timezone.now())

    class Meta:
        db_table = 'MailMaster'



class Emailattachment(models.Model):
    Attachment_Id=models.CharField(max_length=20,unique=True,default=Attachment_I)
    Attachfile=models.FileField(max_length=1000000, default='default.png', blank=True)
    filename = models.CharField(max_length=500,blank=True)
    Uploadby=models.CharField(max_length=250,blank=True)
    filetype = models.CharField(max_length=100,blank=True)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdattachment')
    Created_Date = models.DateField(default=date.today())
    class Meta:
        db_table = 'Emailattachment'


class ERPUsers(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    User_Id=models.CharField(max_length=20,unique=True)
    Role_Id=models.ForeignKey(Role, on_delete=models.CASCADE)# need to clarify role permission or role
    User_Name=models.CharField(max_length=50,blank=True)
    User_emailId=models.EmailField(unique=True)
    User_Password=models.CharField(max_length=50,blank=True)
    User_OldPassword=models.CharField(max_length=50,blank=True)
    User_PwdUpdatedate= models.DateField(default=date.today())#
    User_LoginStatus=models.BooleanField(default=False)#
    User_Photo=models.FileField(max_length=1000000,default='default.png',blank=True)#
    Phone_No=models.CharField(max_length=50,blank=True)
    Boarding_House=models.CharField(max_length=100,blank=True)
    Street=models.CharField(max_length=100,blank=True)
    Area=models.CharField(max_length=100,blank=True)
    City=models.CharField(max_length=100,blank=True)
    State=models.CharField(max_length=100,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Country=models.CharField(max_length=100,blank=True)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdERPusers')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedERPusers')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = ' ERPUsers'



class ERPUsersActivity(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    User_Id=models.ForeignKey(ERPUsers, on_delete=models.CASCADE)
    Activity_Id=models.CharField(max_length=20,unique=True,default=Activity_I)
    Activity_DateTime=models.DateTimeField(default=timezone.now())#
    Activity_Details=models.CharField(max_length=1000,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdERPUsersActivity')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedERPUsersActivity')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'ERPUsersActivity'


class UserLoginStatus(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    User_Id=models.ForeignKey(ERPUsers, on_delete=models.CASCADE)
    UserLoginStatus_Id = models.CharField(max_length=20,unique=True,default=UserLoginStatus_I)
    Last_LoginDateTime=models.DateTimeField(default=timezone.now())#
    Login_Status=models.BooleanField(default=False)#

    class Meta:
        db_table = 'UserLoginStatus'



class UserChangePwd(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    User_Id=models.ForeignKey(ERPUsers, on_delete=models.CASCADE)
    UserChangePwd_Id = models.CharField(max_length=20,unique=True,default=UserChangePwd_I)
    LastPwdchange_DateTime=models.DateTimeField(default=timezone.now())#
    Old_Password=models.CharField(max_length=100,blank=True)
    New_Password=models.CharField(max_length=100,blank=True)
    Dateof_Password=models.DateField(default=date.today())#

    class Meta:
        db_table = 'UserChangePwd'




# Tables for the Partner Management

class PartnerMaster(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_ID=models.CharField(max_length=20,unique=True)
    Partner_Name=models.CharField(max_length=100,blank=True)
    Partner_Web=models.CharField(max_length=100,blank=True)
    Partner_Status=models.CharField(max_length=100,blank=True)#
    PartnerPAN_Number=models.CharField(max_length=100,blank=True)
    PartnerGST_No=models.CharField(max_length=100,blank=True)
    PartnerPF_No=models.CharField(max_length=100,blank=True)
    PartnerEsi_No=models.CharField(max_length=100,blank=True)
    Partner_Logo=models.CharField(max_length=100,blank=True)
    Incorporation_Date=models.DateField(default=date.today())#
    PartnerBusiness_Entity=models.CharField(max_length=100,blank=True)
    PartnerBusiness_Industry=models.CharField(max_length=500,blank=True)
    PartnerDirectorName_1=models.CharField(max_length=50,blank=True)
    PartnerDirectorName_2=models.CharField(max_length=50,blank=True)
    Partner_Phone=models.CharField(max_length=50,blank=True)
    Partner_Mail=models.EmailField(max_length=50,blank=True)
    Partner_ShipAgreementDate=models.DateField(default=date.today())#
    Renewal_Period=models.CharField(max_length=100,blank=True)#
    Investment_Amount=models.FloatField(blank=True)#
    Refundable=models.BooleanField(default=False)#
    Sales_Targetbasis=models.CharField(max_length=100,blank=True)#
    Commission_Percentage=models.CharField(max_length=100,blank=True)
    Comission_Paid=models.CharField(max_length=50,blank=True)
    ExitNotice_Period=models.CharField(max_length=50,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdPartnerMaster')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedPartnerMaster')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'PartnerMaster'


# Tabels for the Service Management

class Servicecategory(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Category_Id=models.CharField(max_length=20,unique=True,default=Category_I)
    # Parent_Child=models.CharField(max_length=50,blank=True)#
    Description=models.CharField(max_length=250,blank=True)#
    Category_Name=models.CharField(max_length=50,blank=True)
    Category_Image=models.FileField(default='default.png',blank=True)
    Is_Active=models.BooleanField(default=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdServicecategory')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedServicecategory')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = ' Servicecategory'


class Services(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id = models.ForeignKey(Partner,on_delete=models.CASCADE,blank=True,null=True)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Category_Id=models.ForeignKey(Servicecategory, on_delete=models.CASCADE)
    Service_Id=models.CharField(max_length=20,unique=True,default=Service_I)
    Service_Name=models.CharField(max_length=500,blank=True)
    Description=models.CharField(max_length=500,blank=True)
    Category_Name=models.CharField(max_length=50,blank=True)
    Service_Type = models.CharField(max_length=100,blank=True)
    Service_Image=models.FileField(default='default.png',blank=True)
    No_Of_Service = models.IntegerField(default=0)
    Service_Duration = models.TimeField(default=timezone.now())
    # Ontime = models.BooleanField(default=False)
    # Subscription = models.BooleanField(default=False)
    # Ontime_Baseprice = models.FloatField(blank=True)
    # Subscription_Baseprice = models.FloatField(blank=True)
    Components = models.JSONField(default={})
    Is_Active=models.BooleanField(default=True)#
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdServices')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedServices')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Services'


class Plan(models.Model):
    Plan_Id = models.CharField(max_length=20,unique=True,default=Plan_I)
    Category_Id=models.ForeignKey(Servicecategory, on_delete=models.CASCADE)
    Category_Name=models.CharField(max_length=50,blank=True)
    Service_Id=models.ForeignKey(Services,on_delete=models.CASCADE)
    Service_Name=models.CharField(max_length=500,blank=True)
    PlanName = models.CharField(max_length=100)
    # OneTime_Plan = models.BooleanField(default=False)
    # oneTime_Json = models.JSONField(default={},blank=True,null=True)
    # Monthly_Plan = models.BooleanField(default=False)
    # Sunday_Json = models.JSONField(default={},blank=True,null=True)
    # Monday_Json = models.JSONField(default={},blank=True,null=True)
    # Tuesday_Json = models.JSONField(default={},blank=True,null=True)
    # Wednesday_Json = models.JSONField(default={},blank=True,null=True)
    # Thursday_Json = models.JSONField(default={},blank=True,null=True)
    # Friday_Json = models.JSONField(default={},blank=True,null=True)
    # Saterday_Json = models.JSONField(default={},blank=True,null=True)
    Exterior_Clean = models.BooleanField(default=False)
    Interior_Clean = models.BooleanField(default=False)
    Dry_Clean = models.BooleanField(default=False)
    Shampoo_Clean = models.BooleanField(default=False)
    Description = models.CharField(max_length=250,blank=True)
    Effective_From = models.DateField(default=date.today())
    Effective_To = models.DateField(default=date.today())
    PlanImage = models.FileField(blank=True,default='default.png')
    Price = models.FloatField()
    Discount = models.FloatField()
    No_Of_Service = models.IntegerField(default=0)
    Service_Duration = models.TimeField(default=timezone.now())
    Approved_Flag=models.BooleanField(default=False)
    Status = models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdplan')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedplan')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Plan'



class Slot(models.Model):
    Partner_Id= models.ForeignKey(Partner,on_delete=models.CASCADE,blank=True,null=True)
    Slot_Id = models.CharField(max_length=20,default=Slot_I,unique=True)
    Service_Name=models.CharField(max_length=500,blank=True)
    Service_Id = models.ForeignKey(Services,on_delete=models.CASCADE)
    Start_Time = models.TimeField(default=timezone.now())
    Providing_Service=models.CharField(max_length=100,blank=True)
    End_Time = models.TimeField(default=timezone.now())
    Noof_slots = models.IntegerField(default=0,blank=True,null=True)
    Service_Duration = models.IntegerField(default=0)
    # Slot_Name =  models.CharField(max_length=50)
    Slots = models.JSONField(default={},blank=True,null=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdslot')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedsubslot')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Slot'



class Serviceprice(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id = models.ForeignKey(Partner,on_delete=models.CASCADE,blank=True,null=True)
    Partner_Name=models.CharField(max_length=100,blank=True,null=True)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Service_Id=models.ForeignKey(Services, on_delete=models.CASCADE)
    Serviceprice_Id = models.CharField(max_length=20,unique=True,default=Serviceprice_I)
    Servicecategory_Name=models.CharField(max_length=100,blank=True)
    Service_Name=models.CharField(max_length=100,blank=True)
    Price=models.FloatField(blank=True)
    GST = models.FloatField(blank=True)
    Cost_to_Customer = models.FloatField(blank=True)
    Payment_Gateway = models.FloatField(blank=True)
    GST_On_PG = models.FloatField(blank=True)
    Net_PG_Cost = models.FloatField(blank=True)
    Discount_vouchar = models.FloatField(blank=True)
    Revenue_Per_Service = models.FloatField(blank=True)
    GST_per = models.IntegerField(blank=True,default=0)
    Cost_to_Customer_per = models.IntegerField(blank=True,default=0)
    Payment_Gateway_per = models.IntegerField(blank=True,default=0)
    GST_On_PG_per = models.IntegerField(blank=True,default=0)
    Net_PG_Cost_per = models.IntegerField(blank=True,default=0)
    Discount_vouchar_per = models.IntegerField(blank=True,default=0)
    Revenue_Per_Service_per = models.IntegerField(blank=True,default=0)
    City_Name = models.CharField(max_length=500,blank=True,null=True)
    Heads = models.JSONField(default={},null=True,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdServiceprice')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedServiceprice')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Serviceprice'


class Taxsetup(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Tax_Id=models.CharField(max_length=20,unique=True,default=Tax_I)
    Tax_Name=models.CharField(max_length=50,blank=True)
    TaxState_InOut=models.CharField(max_length=50,blank=True)# need to clarify
    Tax_Percentage=models.CharField(max_length=50,blank=True)
    HSN_Code=models.CharField(max_length=50,blank=True)
    Is_Active=models.BooleanField(default=True)#
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdTaxsetup')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedTaxsetup')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Taxsetup'

# Tables for the sales management

class Territorysalestarget(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Target_Id=models.CharField(max_length=20,unique=True,default=Target_I)
    Target_Name=models.CharField(max_length=50,blank=True)
    Territory_Id=models.ForeignKey(Territory, on_delete=models.CASCADE)
    Target_FinalYear=models.CharField(max_length=50,blank=True)# need to clarify
    Revenue_Target=models.CharField(max_length=50,blank=True)#
    Customer_Target=models.IntegerField(blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdTerritorysalestarget')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedTerritorysalestarget')
    Updated_Date = models.DateField(default=date.today())

    
    class Meta:
        db_table = 'Territorysalestarget'



class Servicesalestarget(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Target_Id=models.ForeignKey(Territorysalestarget, on_delete=models.CASCADE)
    Territory_Id=models.ForeignKey(Territory, on_delete=models.CASCADE)# Need to clarify
    Salestarget_Id=models.CharField(max_length=20,unique=True,default=Salestarget_I)
    Service_Id=models.ForeignKey(Services, on_delete=models.CASCADE)
    Revenue_Target=models.CharField(max_length=100,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdServicesalestarget')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedServicesalestarget')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Servicesalestarget'



class Servicemonthlysalestarget(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Servicemonthlysalestarget_Id = models.CharField(max_length=20,unique=True,default=Servicemonthlysalestarget_I)
    Target_Id=models.ForeignKey(Territorysalestarget, on_delete=models.CASCADE)
    Territory_Id=models.ForeignKey(Territory, on_delete=models.CASCADE)# need to clarify
    Service_Id=models.ForeignKey(Services, on_delete=models.CASCADE)
    Month_Yr=models.DateField(default=date.today())# need to clarify
    MonthlyRevenue_Target=models.FloatField(blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdServicemonthlysalestarget')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedServicemonthlysalestarget')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Servicemonthlysalestarget'



class Monthlyusertarget(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id = models.ForeignKey(Partner,on_delete=models.CASCADE,blank=True,null=True)
    Partner_Name = models.CharField(max_length=100,blank=True)
    Service_Name = models.CharField(max_length=100,blank=True)
    Service_Type = models.CharField(max_length=100,blank=True)
    Budget_Name = models.CharField(max_length=250,blank=True)
    Financial_Year = models.CharField(max_length=250,blank=True)
    Busines_Salestarget = models.IntegerField(blank=True,default=0)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    # Target_Id=models.ForeignKey(Territorysalestarget, on_delete=models.CASCADE)
    Monthlyusertarget_Id = models.CharField(max_length=20,unique=True,default=Monthlyusertarget_I)
    # Territory_Id=models.ForeignKey(Territory, on_delete=models.CASCADE)# need to clarify
    # Month_Yr=models.DateField(default=date.today())# need to clarify
    # MonthlyRevenue_Target=models.FloatField(blank=True)
    Salespersion = models.JSONField(default={},blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdMonthlyusertarget')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedMonthlyusertarget')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Monthlyusertarget'


class CustomerAddress(models.Model):

    CustAddress_Id=models.CharField(max_length=20,unique=True,default=CustAddress_I)
    # Customer_Key=models.CharField(max_length=100,blank=True)# Need to clarify
    Building_Name=models.CharField(max_length=100,blank=True)
    Building_Street=models.CharField(max_length=100,blank=True)
    Building_Area=models.CharField(max_length=100,blank=True)
    Building_City=models.CharField(max_length=50,blank=True)
    Building_State=models.CharField(max_length=50,blank=True)
    Building_Country=models.CharField(max_length=50,blank=True)
    Zip_Code=models.CharField(max_length=100,blank=True)
    Land_Mark=models.CharField(max_length=100,blank=True)
    GPSlocation_Address=models.CharField(max_length=200,blank=True)
    Active_Status=models.BooleanField(default=True)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdCustomerAddress')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedCustomerAddress')
    Updated_Date = models.DateField(default=date.today())
    
    class Meta:
        db_table = 'CustomerAddress'


class Car(models.Model):
    # Car_Image = models.FileField(max_length=1000000,default='default.png',blank=True,null=True)
    Registration_Number = models.CharField(max_length=50,blank=True)
    Car_Manufacturar = models.CharField(max_length=250,blank=True)
    Car_Model = models.CharField(max_length=250,blank=True)
    Car_Color = models.CharField(max_length=50,blank=True)
    Car_Name = models.CharField(max_length=100,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdCar')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedCar')
    Updated_Date = models.DateField(default=date.today())
    
    class Meta:
        db_table = 'Car' 



class Customer(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id = models.ForeignKey(Partner,on_delete=models.CASCADE,blank=True,null=True)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Customer_Id=models.CharField(max_length=20,unique=True,default=Customer_I)
    Salutation=models.CharField(max_length=50,blank=True)
    Cus_FirstName=models.CharField(max_length=50,blank=True)
    Cus_LastName=models.CharField(max_length=50,blank=True)
    MobileNo=models.CharField(max_length=50,blank=True)
    Email_Id=models.EmailField(unique=True)#
    CustAddress_Id = models.ManyToManyField(CustomerAddress)
    # Customer_Type=models.CharField(max_length=100,blank=True)
    Customer_Car = models.ManyToManyField(Car)
    # Knowabout_Us=models.CharField(max_length=1000,blank=True)
    Location = models.CharField(max_length=250,blank=True)
    # Website=models.CharField(max_length=500,blank=True)
    # Opening_Balance = models.FloatField(blank=True,default=0.0)
    # Due_Date = models.DateField(default=date.today())
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdCustomer')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedCustomer')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Customer'



class CustomerPaymentmethods(models.Model):
    CustomerPaymentmethods_Id = models.CharField(max_length=20,unique=True,default=CustomerPaymentmethods_I)
    Customer_Id=models.ForeignKey(Customer, on_delete=models.CASCADE)#
    Territory_Id=models.ForeignKey(Territory, on_delete=models.CASCADE)
    Payment_Method=models.CharField(max_length=100,blank=True)
    DebitCardNo_Last4Digit=models.IntegerField(blank=True)
    CreditCardNo_Last4Digit=models.IntegerField(blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdCustomerPaymentmethods')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedCustomerPaymentmethods')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'CustomerPaymentmethods'


class Subscription(models.Model):
    Partner_Id = models.ForeignKey(Partner,on_delete=models.CASCADE,null=True,blank=True)
    Subscription_Id = models.CharField(max_length=20,unique=True,default=Subscription_I)
    # Category_Id=models.ForeignKey(Servicecategory, on_delete=models.CASCADE)
    Service_Id = models.ForeignKey(Services , on_delete=models.CASCADE,blank=True,null=True)
    Customer_Id = models.ForeignKey(Customer,on_delete=models.CASCADE,null=True,blank=True)
    Subscriber_Name = models.CharField(max_length=100,blank=True)
    Category_Name=models.CharField(max_length=50,blank=True)
    Car_Number = models.CharField(max_length=50,blank=True)
    Car_Brand = models.CharField(max_length=250,blank=True)
    Car_Type = models.CharField(max_length=250,blank=True)
    Service_Name=models.CharField(max_length=500,blank=True)
    Plan_Id = models.ForeignKey(Plan,on_delete=models.CASCADE)
    PlanName = models.CharField(max_length=100)
    Effectfrom_Month = models.CharField(max_length=50)
    Tenure = models.CharField(max_length=50)
    Amount_Invoiced = models.FloatField()
    Payment_Date = models.DateField(default=date.today())
    Renewal_Date = models.DateField(default=date.today())
    Transaction_Id = models.CharField(max_length=50,blank=True)
    # Services_Included = models.ManyToManyField(Services)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdsubscription')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedsubscription')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Subscription'
        

class subscriptioncustomer(models.Model):
    Partner_Id=models.ForeignKey(Partner, on_delete=models.CASCADE)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Customer_Id=models.CharField(max_length=50,blank=True,null=True)
    WeekNo = models.IntegerField(default=0)
    Days = models.JSONField(default={},blank=True,null=True)
    Location = models.CharField(max_length=250,blank=True,null=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_Date = models.DateField(default=date.today())
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'subscriptioncustomer'


class SalesOrder(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id=models.ForeignKey(Partner, on_delete=models.CASCADE)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Customer_Id=models.ForeignKey(Customer, on_delete=models.CASCADE)
    # Car=models.ManyToManyField(Car)
    # Territory_Id=models.ForeignKey(Territory, on_delete=models.CASCADE)
    SalesOrder_Id=models.CharField(max_length=20,unique=True,default=SalesOrder_I)
    Parking_Notes=models.CharField(max_length=1000,blank=True)
    SalesOrderDate=models.DateField(default=date.today())
    Service_Id=models.ForeignKey(Services, on_delete=models.CASCADE)
    Service_Location=models.CharField(max_length=1000,blank=True)#
    # Noof_Months=models.IntegerField(blank=True)
    choice = [('OneTime','OneTime'),('Subscription','Subscription')]
    SalesOrder_Type = models.CharField(max_length=100,blank=True,choices=choice)
    Card_expiry_date=models.DateField(default=date.today())
    Recurring_EndDate=models.DateField(default=date.today())
    Prefered_Date=models.DateField(default=date.today())#
    Prefered_Time=models.TimeField(default=timezone.now)#
    Amount_Paid=models.FloatField(blank=True)#
    Payment_Method=models.CharField(max_length=100,blank=True)#
    DebitCardNo=models.IntegerField(blank=True)
    Card_Cvv=models.IntegerField(blank=True)
    Assignedservice_Personal=models.CharField(max_length=100,blank=True)#
    Order_Attended=models.BooleanField(default=False)#
    Order_Cancelled=models.BooleanField(default=False)#
    Booking_Details=models.CharField(max_length=1000,blank=True)
    Card_Holder_Name=models.CharField(max_length=100,blank=True)
    # Amount_Refunded=models.FloatField(blank=True)#
    # TotalAmt_Refunded=models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdSalesOrder')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedSalesOrder')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'SalesOrder'



class Invoice(models.Model):
    Invoice_Id=models.CharField(max_length=20,unique=True,default=Invoice_I)#
    Customer_Id=models.ForeignKey(Customer, on_delete=models.CASCADE)
    Invoice_DateTime=models.TimeField(default=timezone.now())
    SalesOrder_Id = models.ForeignKey(SalesOrder,on_delete=models.CASCADE)
    SalesPerson=models.CharField(max_length=20,blank=True)
    Service_Id=models.ForeignKey(Services, on_delete=models.CASCADE)
    Currency=models.CharField(max_length=100,blank=True)  # need to clarify
    Account_Code=models.CharField(max_length=100,blank=True)  # need to clarify
    Amount=models.FloatField()
    Tax_Amt=models.FloatField()
    Net_Amt=models.FloatField()
    Partner_Id=models.ForeignKey(Partner, on_delete=models.CASCADE)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdInvoice')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedInvoice')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Invoice'



class Customerpayments(models.Model):

    Payment_Id=models.CharField(max_length=20,unique=True,default=Payment_I)
    Invoice_Id=models.ForeignKey(Invoice, on_delete=models.CASCADE)
    Customer_Id=models.ForeignKey(Customer, on_delete=models.CASCADE)
    Payment_DateTime=models.DateTimeField(default=timezone.now())
    Payment_Method=models.CharField(max_length=100,blank=True)
    Payment_RefNo=models.CharField(max_length=100,blank=True)
    Payment_DateTime=models.CharField(max_length=100,blank=True)
    Payment_Amount=models.IntegerField()
    Partners_Commission=models.CharField(max_length=100,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdpayment')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedpayment')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Customerpayments'

# Tables for  Customer Relationship Mangement


class Customerfeedback(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Customer_Id=models.ForeignKey(Customer, on_delete=models.CASCADE) # need to clarify
    Feedback_Id=models.CharField(max_length=20,unique=True,default=Feedback_I)
    ServicePersonal_Friendly=models.CharField(max_length=50,blank=True)
    Service_Knowledge=models.CharField(max_length=150,blank=True)
    Service_Quickness=models.CharField(max_length=150,blank=True)
    FutureUse_Service=models.CharField(max_length=50,blank=True)
    Improve_ServiceRecomendation=models.CharField(max_length=500,blank=True)
    Refer_Serviceoothers=models.CharField(max_length=50,blank=True)
    Invoice_Id=models.ForeignKey(Invoice, on_delete=models.CASCADE)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdCustomerfeedback')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedCustomerfeedback')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Customerfeedback'


class Customercomplains(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Customer_Id=models.ForeignKey(Customer, on_delete=models.CASCADE)
    Complain_Id=models.CharField(max_length=20,unique=True,default=Complain_I)
    Complain_Subject=models.CharField(max_length=1000,blank=True)
    Complaingn_Regarding=models.CharField(max_length=500,blank=True)
    Service=models.CharField(max_length=500,blank=True)
    Detail_Complain=models.CharField(max_length=500,blank=True)
    Complain_Assigned=models.BooleanField(default=False)#
    Complain_AssignedTo=models.CharField(max_length=500,blank=True)
    Complain_Status=models.CharField(max_length=50,blank=True)
    Resolution_Date=models.DateField(default=date.today())
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdCustomercomplains')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedCustomercomplains')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Customercomplains' 


class Customerticketresolution(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Customerticketresolution_Id = models.CharField(max_length=20,unique=True,default=Customerticketresolution_I)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Customer_Id=models.ForeignKey(Customer, on_delete=models.CASCADE)
    Complain_Id=models.ForeignKey(Customercomplains, on_delete=models.CASCADE)
    Complain_Attendedby=models.CharField(max_length=500,blank=True)
    Complain_Status=models.CharField(max_length=50,blank=True)
    Complainresolution_Details=models.CharField(max_length=500,blank=True)
    Resolution_Date=models.DateField(default=date.today())
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdCustomerticketresolution')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedCustomerticketresolution')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Customerticketresolution' 


class Servicecancellations(models.Model):
    # Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id = models.ForeignKey(Partner,on_delete=models.CASCADE,blank=True,null=True)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Customer_Id=models.ForeignKey(Customer, on_delete=models.CASCADE)
    Cancellation_Id=models.CharField(max_length=20,unique=True,default=Cancellation_I)
    ServiceOrder_Id=models.ForeignKey(SalesOrder, on_delete=models.CASCADE)
    Service_AssignedTo=models.CharField(max_length=500,blank=True)
    # Complain_AttendedBy=models.CharField(max_length=500,blank=True)
    # Complain_Status=models.CharField(max_length=50,blank=True)
    Cancel_Reason=models.CharField(max_length=250,blank=True)
    Cancel_Status = models.BooleanField(default=False)
    Cancel_Amount=models.FloatField(default=0.0,blank=True,null=True)
    Resolution_Date=models.DateField(default=date.today())
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdServicecancellations')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedServicecancellations')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Servicecancellations' 


class Customerrefunds(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id = models.ForeignKey(Partner,on_delete=models.CASCADE,blank=True,null=True)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Customer_Id=models.ForeignKey(Customer, on_delete=models.CASCADE)
    Refund_Id=models.CharField(max_length=20,unique=True,default=Refund_I)
    Cancellation_Id=models.ForeignKey(Servicecancellations,on_delete=models.CASCADE)  #Need to clarify
    ServiceOrder_Id=models.ForeignKey(SalesOrder, on_delete=models.CASCADE)
    # Service_AssignedTo=models.CharField(max_length=500,blank=True)
    Order_Amount = models.FloatField(blank=True,default=0.0)
    Paid_Amount = models.FloatField(blank=True,default=0.0)
    Refund_Date=models.DateField(default=date.today())
    Order_Date=models.DateField(default=date.today())
    Date=models.DateField(default=date.today())
    Customer_Name=models.CharField(max_length=50,blank=True)
    Service_Name=models.CharField(max_length=50,blank=True)
    Invoice_Id = models.CharField(max_length=50,blank=True,null=True)
    # Refund_Remarks=models.CharField(max_length=500,blank=True)
    Approved_Flag=models.BooleanField(default=False)
    Status = models.BooleanField(default=False)
    Notes = models.CharField(max_length=500,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdCustomerrefunds')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedCustomerrefunds')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Customerrefunds' 



# Tables for the Finance


class Accountingcalendar(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Accountcalendar_Id=models.CharField(max_length=20,unique=True,default=Accountcalendar_I)
    Calendar_Name=models.CharField(max_length=50,blank=True)
    Financial_Year=models.DateField(default=date.today())
    Period_FromMonthYear=models.DateField(default=date.today())
    Period_ToMonthYear=models.DateField(default=date.today())
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdAccountingcalendar')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedAccountingcalendar')
    Updated_Date = models.DateField(default=date.today())


    class Meta:
        db_table = 'Accountingcalendar' 


class Accountingcalendaritem(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Accountingcalendaritem_Id = models.CharField(max_length=20,unique=True,default=Accountingcalendaritem_I)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Accountcalendar_Id=models.ForeignKey(Accountingcalendar, on_delete=models.CASCADE)
    Year= models.DateField(default=date.today())#need to clarify
    month= models.DateField(default=date.today()) #need to clarify
    Quarter= models.DateField(default=date.today())#need to clarify
    Month_Startdate=models.DateField(default=date.today()) #need to clarify
    Month_Enddate=models.DateField(default=date.today()) #need to clarify
    Period_ClosedFlag=models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdAccountingcalendaritem')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedAccountingcalendaritem')
    Updated_Date = models.DateField(default=date.today())


    class Meta:
        db_table = ' Accountingcalendaritem' 


class AccountSegment(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Account_SegId=models.CharField(max_length=20,unique=True,default=Account_SegI)
    Parent_Child=models.CharField(max_length=50,blank=True)
    Accounting_Type=models.CharField(max_length=100,blank=True)
    Allow_Budgeting=models.CharField(max_length=100,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdAccounSegment')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedAccounSegment')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'AccountSegment' 

class Acctgroup(models.Model):
    AcctgroupId = models.CharField(max_length=20,default=Acctgroup_I,unique=True)
    Account_Type=models.CharField(max_length=100,blank=True)
    Account_Groupname=models.CharField(max_length=100,blank=True)
    Description=models.CharField(max_length=100,blank=True)
    Is_Deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'Acctgroup'


class Chartofaccount(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id=models.ForeignKey(Partner, on_delete=models.CASCADE,blank=True,null=True)
    Acct=models.CharField(max_length=100,blank=True)
    AcctgroupId=models.ForeignKey(Acctgroup,on_delete=models.Case)
    Account_Id=models.CharField(max_length=20,unique=True,default=Account_I)
    Account_Name=models.CharField(max_length=100,blank=True)
    Account_Type=models.CharField(max_length=100,blank=True)
    Ledger_Type=models.CharField(max_length=100,blank=True)
    Account_Group=models.CharField(max_length=100,blank=True)
    Opening_Balance=models.FloatField()
    Description=models.CharField(max_length=100,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdChartofaccount')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedChartofaccount')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Chartofaccount'

class Bank(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,blank=True,null=True)
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE,blank=True,null=True)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Bank_Name=models.CharField(max_length=100,blank=True)
    Chartofaccount_Id=models.ForeignKey(Chartofaccount,on_delete=models.CASCADE)
    Chartofaccount_Name = models.CharField(max_length=100,blank=True)
    Bank_Id=models.CharField(max_length=20,unique=True,default=Bank_I)
    Bank_Address=models.CharField(max_length=250,blank=True)
    Account_No=models.CharField(max_length=20,blank=True)
    IFSC_Code=models.CharField(max_length=100,blank=True)
    Swift_Code=models.CharField(max_length=100,blank=True)
    Opening_Balance=models.FloatField(blank=True)
    # Closing_Balance=models.FloatField(blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdbank')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedbank')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Bank'


class Bankbookentry(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,blank=True,null=True)
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE,blank=True,null=True)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Bank_Name=models.CharField(max_length=100,blank=True)
    Transaction_Id = models.CharField(max_length=100,blank=True)
    Value_Date=models.DateField(default=date.today())
    Transaction_Date=models.DateField(default=date.today())
    Cheque_No=models.CharField(max_length=100,blank=True)
    Description=models.CharField(max_length=500,blank=True)
    CR_Or_DR=models.CharField(max_length=100,blank=True)
    Amount=models.FloatField(blank=True)
    # Closing_Balance=models.FloatField(blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdbankbookentry')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedbankbookentry')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Bankbookentry'



class Invoicedistributionitems(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Distribution_Id=models.CharField(max_length=20,unique=True,default=Distribution_I)  #Need to clarify
    Item_Key=models.CharField(max_length=50,blank=True)
    Account_Id=models.ForeignKey(Chartofaccount, on_delete=models.CASCADE)
    Debit_Credit=models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdInvoicedistributionitems')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedInvoicedistributionitems')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = ' Invoicedistributionitems' 



# class Customerinvoiceheader(models.Model):

#     #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
#     Invoice_Key=models.IntegerField(primary_key=True)
#     Invoice_No=models.CharField(max_length=100)
#     Invoice_Type=models.CharField(max_length=100)
#     Customer_Id=models.ForeignKey(Customer, on_delete=models.CASCADE)
#     Invoice_Date=models.DateField(default=date.today())
#     SalesOrder_Id=models.ForeignKey(SalesOrder,on_delete=models.CASCADE)
#     Customer_OrderRef=models.CharField(max_length=500)
#     Invoice_Amount=models.FloatField()
#     Tax_Amount=models.FloatField()
#     Invoice_Status=models.BooleanField(default=False)
#     Payment_RefNo=models.CharField(max_length=100)
#     Payment_method=models.CharField(max_length=100)
#     Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdCustomerinvoiceheader')
#     Created_Date = models.DateField(default=date.today())
#     Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedCustomerinvoiceheader')
#     Created_Date = models.DateField(default=date.today())

#     class Meta:
#         db_table = 'Customerinvoiceheader' 


class Customerinvoicedetail(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE,null=True,blank=True,default=1)
    Customerinvoicedetail_Id=models.CharField(max_length=20,unique=True,default=Customerinvoicedetail_I)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Invoice_Id=models.ForeignKey(Invoice, on_delete=models.CASCADE)
    SalesOrder_Id=models.ForeignKey(SalesOrder,on_delete=models.CASCADE)
    Customer_Id=models.ForeignKey(Customer, on_delete=models.CASCADE)
    Invoice_Date=models.DateField(default=date.today())
    Invoice_Type=models.CharField(max_length=100,blank=True)
    Invoice_No=models.CharField(max_length=100,blank=True)
    Service_Id=models.ForeignKey(Services, on_delete=models.CASCADE)
    Customer_OrderRef=models.CharField(max_length=500,blank=True)
    Invoice_Amount=models.FloatField()
    Tax_Amount=models.FloatField()
    Invoice_Status=models.BooleanField(default=False)
    Service_Name=models.CharField(max_length=100,blank=True)
    Rate=models.CharField(max_length=100,blank=True)
    Amount=models.FloatField()
    Payment_RefNo=models.CharField(max_length=100,blank=True)
    Payment_method=models.CharField(max_length=100,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdCustomerinvoicedetail')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedCustomerinvoicedetail')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Customerinvoicedetail' 




# class Supplierinvoiceheader(models.Model):

#     #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
#     Invoice_Key=models.IntegerField(primary_key=True)
#     Invoice_No=models.CharField(max_length=100)
#     Supplier_BillNo=models.CharField(max_length=100)
#     Supplier_BillDt=models.DateField(default=date.today())
#     GIN0=models.CharField(max_length=100)
#     PONo=models.CharField(max_length=100)
#     Invoice_Type=models.CharField(max_length=100)
#     Supplier_Id=models.CharField(max_length=100)  #need to clarify
#     Invoice_Amount=models.FloatField()
#     Tax_Amount=models.FloatField()
#     Freight_Amt=models.FloatField()
#     Notes=models.CharField(max_length=1000)
#     Invoice_Status=models.BooleanField(default=False)
#     Payment_RefNo=models.CharField(max_length=100)
#     Payment_method=models.CharField(max_length=100)
#     Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdSupplierinvoiceheader')
#     Created_Date = models.DateField(default=date.today())
#     Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedSupplierinvoiceheader')
#     Created_Date = models.DateField(default=date.today())

#     class Meta:
#         db_table = 'Supplierinvoiceheader' 





# Tables for the Cost Centre and Budget

class Groupcostcenter_or_Profitcenter(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    GCostCenter_Id=models.CharField(max_length=20,unique=True,default=GCostCenter_I)
    GCostCenter_Name=models.CharField(max_length=50,blank=True)
    Details_Costcentre=models.CharField(max_length=500,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdGroupcostcenter_or_Profitcenter')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedCustomerrefundsGroupcostcenter_or_Profitcenter')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Groupcostcenter_or_Profitcenter' 


class Costcenter(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id = models.ForeignKey(Partner,on_delete=models.CASCADE,null=True,blank=True)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    CostCenter_Id=models.CharField(max_length=20,unique=True,default=CostCenter_I)
    Financial_Year = models.CharField(max_length=300,blank=True,null=True)
    # GCostCenter_Id=models.ForeignKey(Groupcostcenter_or_Profitcenter,on_delete=models.CASCADE) #Need to clarify
    Costcenter_Name=models.CharField(max_length=50,blank=True)
    Costcenter_Details=models.CharField(max_length=500,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdCostcenter')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedCostcenter')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Costcenter' 



class Journal(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id=models.ForeignKey(Partner, on_delete=models.CASCADE)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Journal_Date = models.DateField(default=date.today())
    Journal_Id=models.CharField(max_length=20,unique=True)
    Journal_Name=models.CharField(max_length=100,blank=True)
    Reference=models.CharField(max_length=100,blank=True)
    Currency = models.CharField(max_length=100,blank=True)
    Notes = models.CharField(max_length=250,blank=True)
    Account = models.JSONField(default={})
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdjournal')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedournal')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Journal'



class Voucherdetail(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id=models.ForeignKey(Partner, on_delete=models.CASCADE)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Voucher_Type=models.CharField(max_length=100,blank=True)
    Bank_Cash=models.CharField(max_length=100,blank=True)
    Voucher_Id=models.CharField(max_length=20,unique=True)
    Transaction_No=models.CharField(max_length=100,blank=True)
    Voucher=models.CharField(max_length=100,blank=True)
    Amount = models.FloatField()
    Reference = models.CharField(max_length=50,blank=True)
    Currency = models.CharField(max_length=100,blank=True)
    Bank_Name = models.CharField(blank=True,max_length=100)
    CostCenter_Id=models.ForeignKey(Costcenter, on_delete=models.CASCADE)
    Account_Description=models.CharField(max_length=500,blank=True)
    Transaction_Amount=models.FloatField()
    Account = models.JSONField(default={})
    Notes = models.CharField(max_length=250,blank=True)
    Voucher_Date = models.DateField(default=date.today())
    Cash_Or_Bank = models.BooleanField(default=False)
    Bank_Or_Cash =  models.BooleanField(default=False)
    Conversion_Rate = models.CharField(max_length=50,blank=True)
    Debitor_Credit_List=models.CharField(max_length=100,blank=True)
    Instrument_Type = models.CharField(max_length=250,blank=True)
    Instrument_No = models.CharField(max_length=250,blank=True)
    Approved_Flg = models.BooleanField(default=False) #add new
    Approved_By = models.CharField(max_length=100,blank=True)   #add new
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdVoucherdetail')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedVoucherdetail')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Voucherdetail' 


class Contravoucher(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id=models.ForeignKey(Partner, on_delete=models.CASCADE)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Voucher_Type=models.CharField(max_length=100,blank=True)
    Bank_Cash=models.CharField(max_length=100,blank=True)
    Contravoucher_Id=models.CharField(max_length=20,unique=True,default=Contravoucher_I)
    Transaction_No=models.CharField(max_length=100,blank=True)
    Voucher=models.CharField(max_length=100,blank=True)
    Amount = models.FloatField()
    Reference = models.CharField(max_length=50,blank=True)
    Currency = models.CharField(max_length=100,blank=True)
    Bank_Name = models.CharField(blank=True,max_length=100)
    CostCenter_Id=models.ForeignKey(Costcenter, on_delete=models.CASCADE)
    Account_Description=models.CharField(max_length=500,blank=True)
    Transaction_Amount=models.FloatField()
    Account = models.JSONField(default={})
    Notes = models.CharField(max_length=250,blank=True)
    Voucher_Date = models.DateField(default=date.today())
    Cash_Or_Bank = models.BooleanField(default=False)
    Bank_Or_Cash =  models.BooleanField(default=False)
    Debitor_Credit_List=models.CharField(max_length=100,blank=True)
    Instrument_Type = models.CharField(max_length=250,blank=True)
    Instrument_No = models.CharField(max_length=250,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdContravoucher')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedContravoucher')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Contravoucher' 

class Assets(models.Model):
    Partner_Id = models.ForeignKey(Partner,on_delete=models.CASCADE,null=True,blank=True)
    operating_Account =models.JSONField(default={},blank=True)
    Cash_Depositor =models.JSONField(default={},blank=True)
    Petty_Cash =models.JSONField(default={},blank=True)
    REC_Trade =models.JSONField(default={},blank=True)
    REC_Trade_Notes =models.JSONField(default={},blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdAssets')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedAssets')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Assets' 

class Equity(models.Model):
    Partner_Id = models.ForeignKey(Partner,on_delete=models.CASCADE,null=True,blank=True)
    Common_stock =models.JSONField(default={},blank=True)
    Paid_in_Capital =models.JSONField(default={},blank=True)
    Partners_Capital =models.JSONField(default={},blank=True)
    Retained_Earnings =models.JSONField(default={},blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdEquity')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedEquity')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Equity' 



class Liability(models.Model):
    Partner_Id = models.ForeignKey(Partner,on_delete=models.CASCADE,null=True,blank=True)
    AP_Trade =models.JSONField(default={},blank=True)
    Accounts_Payable =models.JSONField(default={},blank=True)
    Commisions =models.JSONField(default={},blank=True)
    REC_Trade1 =models.JSONField(default={},blank=True)
    Unemployment =models.JSONField(default={},blank=True)
    Total = models.JSONField(default={},blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdLiability')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedLiability')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Liability' 


# class DrCrMemoHeader(models.Model):

#     #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
#     Memo_Type=models.CharField(max_length=100)
#     Memo_Id=models.CharField(max_length=20)
#     Memo_Date=models.DateField(default=date.today())
#     Subledger_Code=models.CharField(max_length=100)
#     Memo_RefNo=models.CharField(max_length=100)
#     Naration=models.CharField(max_length=1500)
#     Debit_Amount=models.FloatField()
#     Credit_Amount=models.FloatField()
#     Transaction_Posted=models.IntegerField(default=0)
#     Postedto_GLBy=models.CharField(max_length=100)#need to clarify
#     Postedto_GlDate=models.DateField(default=date.today())
#     Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdDrCrMemoHeader')
#     Created_Date = models.DateField(default=date.today())
#     Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedDrCrMemoHeader')
#     Created_Date = models.DateField(default=date.today())

#     class Meta:
#         db_table = 'DrCrMemoHeader' 

class MemoDetail(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Memo_Type=models.CharField(max_length=100,blank=True)
    Memo_Id=models.CharField(max_length=20,unique=True,default=Memo_I)
    Memo_Date=models.DateField(default=date.today())
    Subledger_Code=models.CharField(max_length=100,blank=True)
    Memo_RefNo=models.CharField(max_length=100,blank=True)
    Naration=models.CharField(max_length=1500,blank=True)
    Debit_Amount=models.FloatField()
    Credit_Amount=models.FloatField()
    Transaction_Posted=models.IntegerField()
    Postedto_GLBy=models.CharField(max_length=100,blank=True)#need to clarify
    Postedto_GlDate=models.DateField(default=date.today())
    Transaction_No=models.CharField(max_length=100,blank=True)  # Need to clarify the SI No
    Account_Id=models.ForeignKey(Chartofaccount, on_delete=models.CASCADE)
    Account_Description=models.CharField(max_length=1000,blank=True)
    Transaction_Amount=models.FloatField()
    Debitor_Credit_List=models.CharField(max_length=100,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdMemoDetail')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedMemoDetail')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'MemoDetail' 


class Schedules(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Schedule_Type=models.CharField(max_length=100,blank=True)
    Schedule_Id=models.CharField(max_length=20,unique=True,default=Schedule_I)
    Schedule_Name=models.CharField(max_length=100,blank=True)
    Schedule_Desc=models.CharField(max_length=1000,blank=True)
    Account_Heads=models.CharField(max_length=100,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdSchedules')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedSchedules')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Schedules' 



class CostCenterBudget(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE,blank=True,null=True)  #add new ponkumar
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    CostCenterBudget_Id=models.CharField(max_length=20,unique=True,default=CostCenterBudget_I)
    # GCostCenter_Id=models.ForeignKey(Groupcostcenter_or_Profitcenter,on_delete=models.CASCADE)
    Costcenter_Name=models.CharField(max_length=100,blank=True)
    Partner_Name = models.CharField(max_length=250,blank=True)
    # Month_Yr=models.CharField(max_length=50,blank=True)#
    Monthlyexpense_Budget=models.CharField(max_length=100,blank=True)
    # Account_Head=models.CharField(max_length=50,blank=True)
    # CostCenter_Id = models.ForeignKey(Costcenter,on_delete=models.CASCADE,default=1,blank=True,null=True)   #add new ponkumar
    Budget_Period = models.CharField(max_length=20,blank=True)   #add new ponkumar
    Rent_Json = models.JSONField(default={})  #add new ponkumar
    # Material_Cost_Json = models.JSONField(default={})   #add new ponkumar
    # Revenue_Json = models.JSONField(default={})   #add new ponkumar
    # Cash_OperatingSystem_Json = models.JSONField(default={})  #add new ponkumar
    # Salary_Json = models.JSONField(default={})  #add new ponkumar
    Total_Json = models.JSONField(default={})  #add new ponkumar
    Total_Budget = models.IntegerField(default=0)
    Allocated_Budget = models.IntegerField(default=0)
    Available_Budget = models.IntegerField(default=0)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdCostCenterBudget')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedCostCenterBudget')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'CostCenterBudget'


class Distributionbudget(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Distributionbudget_Id=models.CharField(max_length=20,unique=True,default=Distributionbudget_I)
    Unit_Price = models.IntegerField(blank=True,null=True,default=0)
    Accountans = models.JSONField(default={})
    Accountant = models.ManyToManyField(Chartofaccount)
    Heads = models.CharField(max_length=250,blank=True)
    Is_Checked = models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdDistributionbudget')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedDistributionbudget')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Distributionbudget'


class Budgethead(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Budgethead_Id=models.CharField(max_length=20,unique=True,default=Budgethead_I)
    Name = models.CharField(max_length=250,blank=True)
    # GCostCenter_Id=models.ForeignKey(Groupcostcenter_or_Profitcenter,on_delete=models.CASCADE)
    Distributionbudget_Id = models.ManyToManyField(Distributionbudget)
    Financial_Year = models.CharField(max_length=250,blank=True)
    # Calendar_Id = models.ForeignKey(Financial_Calendar_Details,on_delete=models.CASCADE)
    Budget_Period=models.CharField(max_length=100,blank=True)
    # Service = models.CharField(max_length=250,blank=True)
    # No_Of_Services = models.IntegerField()
    # GST = models.IntegerField()
    # GST_Calculation = models.FloatField(blank=True)
    # Misc = models.IntegerField()
    # Misc_Caluation = models.FloatField(blank=True)
    # Service_Category = models.CharField(max_length=250,blank=True)
    # Base_Price = models.FloatField()
    # Payment_Gateway = models.IntegerField()
    # Gateway_Calculation = models.FloatField(blank=True)
    # Discount_Vouchar = models.IntegerField()
    # Vouchar_Calculation = models.FloatField(blank=True)
    # Earning_Perservice = models.FloatField()
    # Service_Id = models.ForeignKey(Services,on_delete=models.CASCADE)
    City_Name = models.CharField(max_length=500,blank=True,null=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdBudgethead')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedBudgethead')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Budgethead'


class Budget(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE)
    # CostCenterBudget_Id = models.ForeignKey(CostCenterBudget,on_delete=models.CASCADE, blank=True, null=True)   #add new
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Name = models.CharField(max_length=250,blank=True)
    Budget_Id=models.CharField(max_length=20,unique=True,default=Budget_I)
    Budgethead_Id = models.ForeignKey(Budgethead,on_delete=models.CASCADE)
    # Service = models.CharField(max_length=100,blank=True)
    Financial_Year = models.CharField(max_length=250,blank=True)
    Budget_Period = models.CharField(max_length=100,blank=True)
    Partner_Name = models.CharField(max_length=250)
    # Price_Name = models.CharField(max_length=250,blank=True)
    # Service_Type = models.CharField(max_length=100,blank=True)
    Budget_Methods= models.CharField(max_length=250)
    # No1OfUsers = models.JSONField(default={},blank=True)
    Users = models.JSONField(default={},blank=True)
    # Service_Cost= models.JSONField(default={},blank=True)
    # Material_Cost= models.JSONField(default={},blank=True)
    # Operational_Cost= models.JSONField(default={},blank=True)
    # Business_Development_Cost= models.JSONField(default={},blank=True)
    # Business_Salary = models.JSONField(default={},blank=True)
    # Central_IT_Cost= models.JSONField(default={},blank=True)
    # Central_Salary= models.JSONField(default={},blank=True)
    # Central_Finance= models.JSONField(default={},blank=True)
    # Central_HR= models.JSONField(default={},blank=True)
    # Central_Bus_Development= models.JSONField(default={},blank=True)
    # Central_Procurment_Cost= models.JSONField(default={},blank=True)
    # Central_Operation_Cost= models.JSONField(default={},blank=True)
    revenue = models.JSONField(default={},blank=True)
    Total_Users = models.JSONField(default={},blank=True,null=True)
    Total = models.JSONField(default={},blank=True)
    Total_Revenue = models.JSONField(default={},blank=True)
    Gross_Margin_total = models.JSONField(default={},blank=True)
    # First_Month = models.FloatField(blank=True)
    Increment = models.IntegerField(blank=True)
    Sales_Target = models.IntegerField(blank=True)
    Expense = models.IntegerField(blank=True)
    Gross_Margin = models.IntegerField(blank=True)
    Approved_Flag=models.BooleanField(default=False)
    Status = models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdBudget')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedBudget')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Budget'


class ConsolidatedBudget(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Budget_Name = models.CharField(max_length=250,blank=True)
    ConsolidatedBudget_Id=models.CharField(max_length=20,unique=True,default=ConsolidatedBudget_I)
    Financial_Year = models.CharField(max_length=250,blank=True)
    Budget_Period = models.CharField(max_length=100,blank=True)
    Total_Budget = models.FloatField(blank=True,default=0.0)
    Product_Revenue = models.JSONField(default={},blank=True)
    Service_Cost= models.JSONField(default={},blank=True)
    Material_Cost= models.JSONField(default={},blank=True)
    Operational_Cost= models.JSONField(default={},blank=True)
    Business_Development_Cost= models.JSONField(default={},blank=True)
    Business_Salary = models.JSONField(default={},blank=True)
    Central_IT_Cost= models.JSONField(default={},blank=True)
    Central_Salary= models.JSONField(default={},blank=True)
    Central_Finance= models.JSONField(default={},blank=True)
    Central_HR= models.JSONField(default={},blank=True)
    Central_Bus_Development= models.JSONField(default={},blank=True)
    Central_Procurment_Cost= models.JSONField(default={},blank=True)
    Central_Operation_Cost= models.JSONField(default={},blank=True)
    Total = models.JSONField(default={},blank=True)
    Approved_Flag=models.BooleanField(default=False)
    Status = models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdConsolidatedBudget')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedConsolidatedBudget')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'ConsolidatedBudget'

class CentralBudget(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Budget_Name = models.CharField(max_length=250,blank=True)
    CentralBudget_Id=models.CharField(max_length=20,unique=True,default=CentralBudget_I)
    Financial_Year = models.CharField(max_length=250,blank=True)
    Budget_Period = models.CharField(max_length=100,blank=True)
    Total_Budget = models.FloatField(blank=True,default=0.0)
    Central_IT_Cost= models.JSONField(default={},blank=True)
    Central_Salary= models.JSONField(default={},blank=True)
    Central_Finance= models.JSONField(default={},blank=True)
    Central_HR= models.JSONField(default={},blank=True)
    Central_Bus_Development= models.JSONField(default={},blank=True)
    Central_Procurment_Cost= models.JSONField(default={},blank=True)
    Central_Operation_Cost= models.JSONField(default={},blank=True)
    Total = models.JSONField(default={},blank=True)
    Approved_Flag=models.BooleanField(default=False)
    Status = models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdCentralBudget')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedCentralBudget')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'CentralBudget'


class Salesforecast(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE,null=True,blank=True)
    Salesfocrcast_Name = models.CharField(max_length=250, blank=True, null=True)   #add new
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Salesforecast_Id=models.CharField(max_length=20,unique=True,default=Salesforecast_I)
    Service_Category = models.CharField(max_length=100,blank=True)
    Financial_Year = models.CharField(max_length=250,blank=True)
    Forecast_Period = models.CharField(max_length=100,blank=True)
    Partner_Name = models.CharField(max_length=250)
    Service_Type = models.CharField(max_length=100,blank=True)
    Forecast_Methods= models.CharField(max_length=250)
    First_Month_Subscribers = models.IntegerField(default=0)
    First_Month_Onetimeusers = models.IntegerField(default=0)
    Increment = models.IntegerField(default=0)
    Subscription_users = models.JSONField(default={},blank=True)
    Ontime_users= models.JSONField(default={},blank=True)
    Subscription_Transactions= models.JSONField(default={},blank=True)
    Onetime_Transactions= models.JSONField(default={},blank=True)
    Total_Transactions= models.JSONField(default={},blank=True)
    Avarage_Daily_Transactions = models.JSONField(default={},blank=True)
    Subscription_Revenue= models.JSONField(default={},blank=True)
    Onetime_Revenue= models.JSONField(default={},blank=True)
    Total_Revenue= models.JSONField(default={},blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdforecast')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedforecast')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Salesforecast'



class ExpenseBudget(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Name = models.CharField(max_length=250,blank=True)
    Budget_Id=models.ForeignKey(Budget,on_delete=models.CASCADE,blank=True)
    ExpenseBudget_Id=models.CharField(max_length=20,unique=True,default=ExpenseBudget_I)
    # Budgethead_Id = models.ForeignKey(Budgethead,on_delete=models.CASCADE)
    Service = models.CharField(max_length=100,blank=True)
    Financial_Year = models.CharField(max_length=250,blank=True)
    Budget_Period = models.CharField(max_length=100,blank=True)
    Partner_Name = models.CharField(max_length=250)
    Budget_Group = models.CharField(max_length=100,blank=True)
    # Budget_Methods= models.CharField(max_length=250)
    # NoOfUsers = models.IntegerField(default=0)
    Cost = models.JSONField(default={},blank=True)
    Rent= models.JSONField(default={},blank=True)
    Utilities= models.JSONField(default={},blank=True)
    Material_Cost= models.JSONField(default={},blank=True)
    Maitanence= models.JSONField(default={},blank=True)
    Travel= models.JSONField(default={},blank=True)
    Office_Supplies= models.JSONField(default={},blank=True)
    # First_Month = models.FloatField(blank=True)
    # Increment = models.IntegerField(blank=True)
    # Sales_Target = models.IntegerField(blank=True)
    # Expense = models.IntegerField(blank=True)
    # Gross_Margin = models.IntegerField(blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdExpenseBudget')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedExpenseBudget')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'ExpenseBudget'



class SalesuserTarget(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    SalesuserTarget_Id=models.CharField(max_length=20,unique=True,default=SalesuserTarget_I)
    Service = models.CharField(max_length=100,blank=True)
    Service_Type = models.CharField(max_length=100,blank=True)
    Partner_Name = models.CharField(max_length=250)
    Previousyear_User = models.JSONField(default={},blank=True)
    Newusers= models.JSONField(default={},blank=True)
    Cumulative_Users= models.JSONField(default={},blank=True)
    Avgtransaction_Peruser= models.JSONField(default={},blank=True)
    Transaction_permonth= models.JSONField(default={},blank=True)
    Totalsales_Target= models.JSONField(default={},blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdSalesuserTarget')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedSalesuserTarget')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'SalesuserTarget'


class SubscriberTarget(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    SubscriberTarget_Id=models.CharField(max_length=20,unique=True,default=SubscriberTarget_I)
    Service = models.CharField(max_length=100,blank=True)
    Service_Type = models.CharField(max_length=100,blank=True)
    Partner_Name = models.CharField(max_length=250)
    Previousyear_User = models.JSONField(default={},blank=True)
    Newsubscribers= models.JSONField(default={},blank=True)
    Cumulative_Users= models.JSONField(default={},blank=True)
    Totalsales_Target= models.JSONField(default={},blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdSubscriberTarget')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedSubscriberTarget')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'SubscriberTarget'


# Tables for the HR Management
# 1.HR adminstration

class HRcalendar(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    HRCalendar_Id=models.CharField(max_length=20,unique=True,default=HRCalendar_I)
    Calendar_Name=models.CharField(max_length=100,blank=True)
    Holiday_master=models.CharField(max_length=100,blank=True)  # need to clarify
    Leave_CalculationMonth=models.DateTimeField(default=timezone.now())
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdHRcalendar')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedHRcalendar')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'HRcalendar' 



class HRCalendarmonths(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    HRCalendarmonths_Id=models.CharField(max_length=20,unique=True,default=HRCalendarmonths_I)
    Month=models.DateField(default=date.today())
    Year=models.DateField(default=date.today())
    Attendance_LastDate=models.DateField(default=date.today())
    Payroll_ProcDate=models.DateField(default=date.today())
    Payroll_ProcessFlag=models.CharField(max_length=100,blank=True) # need to clarify
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdHRCalendarmonths')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedHRCalendarmonths')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'HRCalendarmonths' 



class Organisationstructurelevels(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Department_Key=models.CharField(max_length=100,blank=True) #Need to clarify
    Level_Id=models.CharField(max_length=20,unique=True,default=Level_I)
    Level_Name=models.CharField(max_length=100,blank=True)
    Parent_Child=models.CharField(max_length=100,blank=True)
    Parent_Level=models.CharField(max_length=100,blank=True) # clarify about level_key
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdOrganisationstructurelevels')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedOrganisationstructurelevels')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Organisationstructurelevels' 
        
#2.Personal Management

class EmployeeQualification(models.Model):
    EmployeeQualification_Id=models.CharField(max_length=20,unique=True,default=EmployeeQualification_I)
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    # Employee_Id=models.ForeignKey(EmployeeMaster, on_delete=models.CASCADE,related_name='EmployeeQualificationEmployee_Key1')
    # Employee_code=models.ForeignKey(EmployeeMaster, on_delete=models.CASCADE,related_name='EmployeeQualificationEmployee_code1')
    # YrOf_Passing=models.DateField(default=date.today())
    Passedout_Year = models.CharField(max_length=20,blank=True)
    # To_Year = models.CharField(max_length=20,blank=True)
    Degree=models.CharField(max_length=100,blank=True)
    Insttution_Name=models.CharField(max_length=100,blank=True)
    Percentageof_MarkGrade=models.CharField(max_length=100,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdEmployeeQualification')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedEmployeeQualification')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'EmployeeQualification'


class EmployeeExperience(models.Model):
    EmployeeExperience_Id=models.CharField(max_length=20,unique=True,default=EmployeeExperience_I)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    # Employee_Id=models.ForeignKey(EmployeeMaster, on_delete=models.CASCADE,related_name='EmployeeExperienceEmployee_Key2')
    # Employee_code=models.ForeignKey(EmployeeMaster, on_delete=models.CASCADE,related_name='EmployeeExperienceEmployee_code2')
    Organisation_Name=models.CharField(max_length=100,blank=True)
    Work_FromDate=models.DateField(default=date.today())
    Work_toDate=models.DateField(default=date.today())
    Designation=models.CharField(max_length=100,blank=True)
    Company_Name=models.CharField(max_length=100,blank=True)
    # NoOfYearsMonths=models.DateField(default=date.today())
    NoOfYearsMonth = models.CharField(max_length=20,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdEmployeeExperience')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedEmployeeExperience')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'EmployeeExperience'

class Department(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id = models.ForeignKey(Partner,on_delete=models.CASCADE,null=True,blank=True)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Department_Id=models.CharField(max_length=20,unique=True,default=Department_I)
    Department_Name=models.CharField(max_length=100,unique=True)
    # CostCenter_Id-models.CharField(max_length=20)
    # CostCenter_Id=models.ForeignKey(Costcenter,on_delete=models.CASCADE)
    Department_Head = models.CharField(max_length=100,blank=True) #add new
    Parent_Department = models.CharField(max_length=100,blank=True) #add new
    Department_Description = models.CharField(max_length=100,blank=True) #add new
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdDepartment')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedDepartment')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Department'

class EmployeeAnnualCTC(models.Model):
    AnnualCTC_Id=models.CharField(max_length=20,unique=True,default=AnnualCTC_I)
    Salarycomponents = models.JSONField(default={},blank=True)
    monthly =models.FloatField()
    Yearly = models.FloatField()
    AnnualCTC =models.FloatField()
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdEmployeeannual')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedEmployeeannual')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'EmployeeAnnualCTC'


class EmployeeProvidentfund(models.Model):
    Employeepf_Id=models.CharField(max_length=20,unique=True,default=Employeepf_I)
    Employeepf_Number=models.CharField(max_length=100,blank=True)
    Deduction_Cycle = models.CharField(max_length=50,blank=True)
    EPF_EmployeeContribution = models.CharField(max_length=50,blank=True)
    EPF_EmployeerContribution = models.CharField(max_length=50,blank=True)
    FPF_EmployeeContribution = models.CharField(max_length=50,blank=True)
    FPF_EmployeerContribution = models.CharField(max_length=50,blank=True)
    Admin_EmployeeCharges = models.CharField(max_length=50,blank=True)
    Admin_EmployeerCharges = models.CharField(max_length=50,blank=True)
    EDLI_EmployeeCharges = models.CharField(max_length=50,blank=True)
    EDLI_EmployeerCharges = models.CharField(max_length=50,blank=True)
    PF_flg = models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdEmploypf')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedEmployeepf')
    Updated_Date = models.DateField(default=date.today())
    
    class Meta:
        db_table='EmployeeProvidentfund'


class EmployeeESI(models.Model):
    EmployeeESI_Id=models.CharField(max_length=20,unique=True,default=EmployeeESI_I)
    Esi_Number=models.CharField(max_length=100,blank=True)
    Deduction_Cycle = models.CharField(max_length=50,blank=True)
    Esi_flg = models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdEmployesi')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedEmployeesi')
    Updated_Date = models.DateField(default=date.today())
    

    class Meta:
        db_table='EmployeeESI'


class EmployeeProfessionalTax(models.Model):
    EmployeePT_Id=models.CharField(max_length=20,unique=True,default=EmployeePT_I)
    EmployeePT_Number=models.CharField(max_length=100,blank=True)
    Deduction_Cycle = models.CharField(max_length=50,blank=True)
    start_Range = models.FloatField(blank=True)
    End_Range = models.FloatField(blank=True)
    Tax_Amount = models.FloatField(blank=True)
    Tax_Range= models.JSONField(blank=True,default={})
    PT_flg = models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdEmploypt')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedEmployeept')
    Updated_Date = models.DateField(default=date.today())
    
    class Meta:
        db_table='EmployeeProfessionalTax'


class EmployeeEarning(models.Model):
    Earning_Id=models.CharField(max_length=20,unique=True,default=Earning_I)
    Earning_Name=models.CharField(max_length=100,blank=True)
    Earning_Type = models.CharField(max_length=50,blank=True)
    Name_Of_Payslip = models.CharField(max_length=50,blank=True)
    Calculation_Type = models.BooleanField(default=False)
    Calculation = models.CharField(max_length=50)
    Percentage = models.CharField(max_length=50,blank=True)
    PT_flg = models.BooleanField(default=False)
    PF_flg = models.BooleanField(default=False)
    Salaryflg=models.BooleanField(default=False)
    Workingdaysflg = models.BooleanField(default=False)
    Payslipflg = models.BooleanField(default=False)
    OneorRecuring = models.CharField(max_length=50,blank=True)
    Esi_flg = models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Earning_flg = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdEmployearning')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedEmployeeearning')
    Updated_Date = models.DateField(default=date.today())
    
    class Meta:
        db_table='EmployeeEarning'



class EmployeeDeduction(models.Model):
    Deduction_Id=models.CharField(max_length=20,unique=True,default=Deduction_I)
    Deduction_Name=models.CharField(max_length=100,blank=True)
    Deduction_Type = models.CharField(max_length=50,blank=True)
    Name_Of_Payslip = models.CharField(max_length=50,blank=True)
    Rata_Basis = models.BooleanField(default=False)
    CalculationType=models.CharField(max_length=50,blank=True)
    Percentage = models.CharField(max_length=50,blank=True)
    Pre_Tax=models.CharField(max_length=50,blank=True)
    PT_flg = models.BooleanField(default=False)
    PF_flg = models.BooleanField(default=False)
    Esi_flg = models.BooleanField(default=False)
    Recuring_flg = models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Dedcution_flg = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdEmploydeduction')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedEmployeededuction')
    Updated_Date = models.DateField(default=date.today())
    
    class Meta:
        db_table='EmployeeDeduction'

class EmployeeAddress(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    # Employee_Id=models.ForeignKey(EmployeeMaster, on_delete=models.CASCADE,related_name='EmployeeAddressEmployee_Key3')
    # Employee_code=models.ForeignKey(EmployeeMaster, on_delete=models.CASCADE,related_name='EmployeeAddressEmployee_code3')
    Address_Type=models.CharField(max_length=100,blank=True)
    EmployeeAddress_Id=models.CharField(max_length=20,unique=True,default=EmployeeAddress_I)
    Door_No=models.CharField(max_length=100,blank=True)
    Building_Name=models.CharField(max_length=100,blank=True)
    Street_Name=models.CharField(max_length=100,blank=True)
    Area_Name=models.CharField(max_length=100,blank=True)
    City_Name=models.CharField(max_length=100,blank=True)
    State_Name=models.CharField(max_length=100,blank=True)
    Country_Name=models.CharField(max_length=100,blank=True)
    PIN_Code=models.CharField(max_length=100,blank=True)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdEmployeeAddress')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedEmployeeAddress')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'EmployeeAddress'


class EmployeeMaster(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Employee_Id=models.CharField(max_length=20,unique=True,default=Employee_I)
    EmployeeQualification_Id=models.ManyToManyField(EmployeeQualification)
    EmployeeExperience_Id=models.ManyToManyField(EmployeeExperience)
    AnnualCTC_Id=models.ForeignKey(EmployeeAnnualCTC,on_delete=models.CASCADE)
    Department_Id=models.ForeignKey(Department,on_delete=models.CASCADE)  #Need to clarify about department
    Longitude=models.FloatField(blank=True)
    Lotitude=models.FloatField(blank=True)
    Employee_FirstName=models.CharField(max_length=100,blank=True)
    Employee_LasttName=models.CharField(max_length=100,blank=True)
    Employee_MiddleName=models.CharField(max_length=100,blank=True)
    Gender=models.CharField(max_length=10,blank=True)
    DateOf_Joining=models.DateField(default=date.today())
    Cadre = models.CharField(max_length=50,blank=True)
    Designation=models.CharField(max_length=100,blank=True)
    Seating_Location = models.CharField(max_length=250,blank=True)
    Department = models.CharField(max_length=100,blank=True)
    Email_Id = models.CharField(unique=True,max_length=100)
    Employee_Type = models.CharField(max_length=50,blank=True)
    Portal_Access = models.BooleanField(default=False)
    EPF=models.BooleanField(default=False)
    PF_No=models.CharField(max_length=50,blank=True)
    UAN_No=models.CharField(max_length=50,blank=True)
    ESI_YN=models.BooleanField(default=False)
    ESI_No=models.CharField(max_length=50,blank=True)
    PT=models.BooleanField(default=False)
    Persional_Mail = models.CharField(unique=True,max_length=100)
    Mobile_No = models.CharField(max_length=50,blank=True)
    Alternative_Contact_No=models.CharField(max_length=50,blank=True)
    Passport_No = models.CharField(max_length=50,blank=True)
    Passport_Experiry_Date = models.DateField(default=date.today())
    Blood_Group = models.CharField(max_length=50,blank=True)
    DateOf_Birth=models.DateField(default=date.today())
    Age = models.IntegerField(default=18)
    Father_Name=models.CharField(max_length=50,blank=True)
    Adhar_No=models.CharField(max_length=50,blank=True)
    PAN_No=models.CharField(max_length=50,blank=True)
    EmployeeAddress_Id=models.ForeignKey(EmployeeAddress,on_delete=models.CASCADE)
    Marital_Status=models.CharField(max_length=20,blank=True)
    Reportingto=models.CharField(max_length=100,blank=True)
    Designation1=models.CharField(max_length=100,blank=True)
    Department1=models.CharField(max_length=100,blank=True)
    Reporting_City=models.CharField(max_length=50,blank=True)
    Worklocation=models.CharField(max_length=100,blank=True)
    Mother_Tongue=models.CharField(max_length=100,blank=True)
    Employee_Type1=models.CharField(max_length=100,blank=True)
    Nationality=models.CharField(max_length=100) #need to check
    DateOf_Joining1=models.DateField(default=date.today())
    Religion = models.CharField(max_length=100,blank=True)
    Gender = models.CharField(max_length=50,blank=True)
    Appraisal_Manager=models.CharField(max_length=100,blank=True)
    Role = models.CharField(max_length=100,blank=True)
    Employee_Status = models.BooleanField(default=True)
    Employee_Status1 = models.CharField(max_length=20,blank=True)
    Experience = models.IntegerField(default=0)
    Bank_Name = models.CharField(max_length=50,blank=True)
    Bank_AccountNo = models.CharField(max_length=50,blank=True)
    Bank_IFSCCode = models.CharField(max_length=50,blank=True)
    Abscand_Flg = models.BooleanField(default=False)
    Dismissed_Flg = models.BooleanField(default=False)
    Resigned_Flg = models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdEmployeeMaster')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedEmployeeMaster')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'EmployeeMaster'


class Employeesalesmonthlytarget(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Target_Id=models.ForeignKey(Territorysalestarget, on_delete=models.CASCADE)
    Employeesalesmonthlytarget_Id=models.CharField(max_length=20,unique=True,default=Employeesalesmonthlytarget_I)
    Territory_Id=models.ForeignKey(Territory, on_delete=models.CASCADE,related_name="empsTerritory_Id1")# need to clarify
    Service_Id=models.ForeignKey(Services, on_delete=models.CASCADE)
    Employee_Id = models.ForeignKey(EmployeeMaster,on_delete=models.CASCADE)
    Month_Yr=models.DateField(default=date.today())# need to clarify
    MonthlyRevenue_Target=models.FloatField()
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdEmployeesalesmonthlytarget')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedEmployeesalesmonthlytarget')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Employeesalesmonthlytarget'



class Employeemonthlyusertarget(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Empmonthusertarget_Id=models.CharField(max_length=20,unique=True,default=Empmonthusertarget_I)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Target_Id=models.ForeignKey(Territorysalestarget, on_delete=models.CASCADE)
    Territory_Id=models.ForeignKey(Territory, on_delete=models.CASCADE, related_name="EmployeemonthlyTerritory_Id2")# need to clarify
    Employee_Id=models.ForeignKey(EmployeeMaster, on_delete=models.CASCADE,related_name="EmployeemonthlyEmployee_Id2") #need to clarify
    Month_Yr=models.DateField(default=date.today())# need to clarify
    MonthlyRevenue_Target=models.FloatField()
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdEmployeemonthlyusertarget')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedEmployeemonthlyusertarget')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Employeemonthlyusertarget'



class Servicejob(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Job_Id=models.CharField(max_length=20,unique=True,default=Job_I)
    Employee_Id=models.ForeignKey(EmployeeMaster, on_delete=models.CASCADE)#need to clarify
    Costcentre_Id=models.ForeignKey(Costcenter, on_delete=models.CASCADE)
    Job_Date=models.DateField(default=date.today())
    Service_Id=models.ForeignKey(Services, on_delete=models.CASCADE)
    ServiceOrder_Id=models.ForeignKey(SalesOrder,on_delete=models.CASCADE,blank=True)
    Customer=models.CharField(max_length=500,blank=True)
    JobStart_Time=models.TimeField(default=timezone.now())
    JobEnd_Time=models.TimeField(default=timezone.now())
    Manpower_cost=models.FloatField()
    Material_Cost=models.FloatField()
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdServicejob')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedServicejob')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Servicejob' 





class Skill(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Skill_Id=models.CharField(max_length=20,unique=True,default=Skill_I)
    Skill_Name=models.CharField(max_length=100,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdSkill')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedSkill')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Skill'



class Cadre(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Cadre_Id=models.CharField(max_length=20,unique=True,default=Cadre_I)
    Cadre_Name=models.CharField(max_length=100,blank=True)
    Payscale_From=models.CharField(max_length=100,blank=True)
    Pascale_To=models.CharField(max_length=100,blank=True)
    Description = models.CharField(max_length=500,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdCadre')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedCadre')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Cadre'



class Designation(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Designation_Id=models.CharField(max_length=20,unique=True,default=Designation_I)
    Cadre_Id=models.ForeignKey(Cadre, on_delete=models.CASCADE)
    Cadre_Name=models.CharField(max_length=100,blank=True)
    Designation_Name = models.CharField(max_length=250,blank=True)
    Description = models.CharField(max_length=500,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdDesignation')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedDesignation')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Designation'



class Qualification(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Qualification_Id=models.CharField(max_length=20,unique=True,default=Qualification_I)
    Qualification_Name=models.CharField(max_length=100,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdQualification')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedQualification')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Qualification'

class Paycalendar(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Paycalendar_Id=models.CharField(max_length=20,unique=True,default=Paycalendar_I)
    Calendar_Name=models.CharField(max_length=100,blank=True)
    Monday=models.BooleanField(default=False)
    Tuesday=models.BooleanField(default=False)
    Wednesday=models.BooleanField(default=False)
    Thursday=models.BooleanField(default=False)
    Friday=models.BooleanField(default=False)
    Saterday=models.BooleanField(default=False)
    Sunday=models.BooleanField(default=False)
    Actualdays = models.BooleanField(default=False)
    Workingdays = models.IntegerField(default=0,blank=True)
    Lastday = models.BooleanField(default=False)
    Actualdays = models.CharField(max_length=50,blank=True)
    Ofc_Starttime = models.TimeField(default=timezone.now())
    Ofc_Endtime = models.TimeField(default=timezone.now())
    Total_hours = models.TimeField(default=timezone.now())
    # Employee_Exempted = models.CharField(max_length=100,blank=True)
    Employee_Exempted = models.ManyToManyField(Cadre)
    Lastday_Month = models.CharField(max_length=100,blank=True,null=True)
    Monthlast_Day = models.CharField(max_length=100,blank=True,null=True)
    Permissable_Hours = models.FloatField(default=0,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdPaycalendar')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedPaycalendar')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Paycalendar'



class Employeealocation(models.Model):
    Employeealocation_Id=models.CharField(max_length=20,unique=True,default=Employeealocation_I)
    Week_Number = models.IntegerField()
    Employee_Name = models.CharField(max_length=50)
    Employee_Id = models.ForeignKey(EmployeeMaster,on_delete=models.CASCADE)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdalocation')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedalocation')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Employeealocation'


class JobAlocationMaster(models.Model):
    Partner_Id = models.ForeignKey(Partner,on_delete=models.CASCADE,null=True,blank=True)
    Employee_Id = models.ForeignKey(EmployeeMaster,on_delete=models.CASCADE,null=True,blank=True)
    JobAlocationMaster_Id=models.CharField(max_length=20,unique=True,default=JobAlocationMaster_I)
    WeekNo = models.IntegerField()
    Employee_Name = models.CharField(max_length=50)
    Day = models.CharField(max_length=50)
    Location = models.CharField(max_length=500,blank=True)
    Service_Name = models.CharField(max_length=250)
    Allocations = models.JSONField(default={})
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdjobaloc')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedjobaloc')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'JobAlocationMaster'


class RoasterMaster(models.Model):
    Partner_Id = models.ForeignKey(Partner,on_delete=models.CASCADE,null=True,blank=True)
    # Employee_Id = models.ForeignKey(EmployeeMaster,on_delete=models.CASCADE,null=True,blank=True)
    RoasterMaster_Id=models.CharField(max_length=20,unique=True,default=RoasterMaster_I)
    Slot_Id = models.ForeignKey(Slot,on_delete=models.CASCADE,blank=True,null=True)
    Partner_Name = models.CharField(max_length=100,blank=True,null=True)
    WeekNo = models.IntegerField()
    Month = models.CharField(max_length=100,blank=True,null=True)
    Start_Date = models.DateField(default=date.today())
    End_Date = models.DateField(default=date.today())
    Roster_Name = models.CharField(max_length=250)
    Radius = models.IntegerField(blank=True,default=0)
    Day = models.CharField(max_length=50,blank=True,null=True)
    Location = models.CharField(max_length=500,blank=True)
    Service_Name = models.CharField(max_length=250)
    Allocations = models.JSONField(default={})
    leave = models.CharField(max_length=50,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='creatRoasterMaster')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatRoasterMaster')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'RoasterMaster'


class Jobtimecard(models.Model):
    Employee_Id = models.ForeignKey(EmployeeMaster,blank=True,null=True,on_delete=models.CASCADE)
    Jobtimecard_Id=models.CharField(max_length=20,unique=True,default=Jobtimecard_I)
    Customer_Id = models.ForeignKey(Customer,on_delete=models.CASCADE)
    service_Id = models.ForeignKey(Services,on_delete=models.CASCADE)
    Service_Name = models.CharField(max_length=250,blank=True,null=True)
    Date = models.DateField(default=date.today())
    Year = models.IntegerField(default=0)
    Month = models.CharField(max_length=100,blank=True,null=True)
    Jobcarriedover = models.JSONField(default={})
    Starttime = models.TimeField(default=timezone.now())
    Endtime = models.TimeField(default=timezone.now())
    Actual_Starttime = models.TimeField(default=timezone.now())
    Actual_Endtime = models.TimeField(default=timezone.now())
    Remarks = models.CharField(max_length=250,blank=True,null=True)
    Jobcompleted_Flg = models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdjobtime')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedjobtime')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Jobtimecard'



#3.Leave Management

class Holidaylist(models.Model):
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE)
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Holidaylist_Id=models.CharField(max_length=20,unique=True,default=Holidaylist_I)
    Holiday_Name=models.CharField(max_length=100,blank=True,unique=True)
    Year=models.IntegerField(default=0,blank=True)
    Holidays = models.JSONField(default={})
    Approved_flg = models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdHolidaylist')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedHolidaylist')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Holidaylist'



class Leavetype(models.Model):
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE)
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Leavetype_Id=models.CharField(max_length=20,unique=True,default=Leavetype_I)
    Name=models.CharField(max_length=100,blank=True,unique=True)
    Leave_Type=models.CharField(max_length=50,blank=True)
    Unit=models.CharField(max_length=50,blank=True)
    Description = models.CharField(max_length=250,blank=True)
    Applicable_Male=models.BooleanField(default=False)
    Applicable_Female=models.BooleanField(default=False)
    Applicable_Others=models.BooleanField(default=False)
    Validity_From = models.DateField(default=date.today())
    Validity_To = models.DateField(default=date.today())
    Effective_After = models.JSONField(default={})
    Accural = models.JSONField(default={})
    Reset = models.JSONField(default={})
    Carry_Forward = models.JSONField(default={})
    Encashment = models.JSONField(default={})
    Countas_Leaveafter=models.BooleanField(default=False)
    Days = models.IntegerField(default=0)
    Countas_Leave=models.BooleanField(default=False)
    Countas_Leaveafter1=models.BooleanField(default=False)
    Days1 = models.IntegerField(default=0)
    Week_Ends = models.CharField(max_length=50,blank=True)
    Presents = models.CharField(max_length=50,blank=True)
    Countas_Leave1=models.BooleanField(default=False)
    Leave_Code = models.CharField(max_length=50,blank=True)
    NoOf_Days = models.IntegerField(default=0,blank=True)
    Accuralflg = models.BooleanField(default=False)
    Resetflg = models.BooleanField(default=False)
    Carryforwardflg = models.BooleanField(default=False)
    Encashmentflg = models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdLeavetype')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedLeavetype')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Leavetype'


class Leaveopeningbalance(models.Model):
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE)
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Leavetype_Id=models.ForeignKey(Leavetype, on_delete=models.CASCADE) # need to dicuss
    Leavetype=models.CharField(max_length=250,blank=True)
    Year = models.IntegerField(default=0)
    Leaveopeningbalance = models.IntegerField(default=0,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdLeaveopeningbalance')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedLeaveopeningbalance')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Leaveopeningbalance'


class Leave(models.Model):
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE)
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Employee_Id = models.ForeignKey(EmployeeMaster,on_delete=models.CASCADE)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Application_Id=models.CharField(max_length=20,unique=True,default=Application_I)
    Appplication_Date=models.DateField(default=date.today())
    Leavetype_Id=models.ForeignKey(Leavetype, on_delete=models.CASCADE) # need to dicuss
    Leavetype=models.CharField(max_length=250,blank=True)
    Year = models.IntegerField(default=0)
    Month = models.CharField(max_length=50,blank=True)
    WeekNo = models.IntegerField(default=0)
    Day = models.JSONField(blank=True,default={})
    From_Date=models.DateField(default=date.today())
    To_Date=models.DateField(default=date.today())
    ReasonOf_Leave=models.CharField(max_length=1500,blank=True)
    Sanctioned_Date=models.DateField(default=date.today())
    Sancleavefrom_Date=models.DateField(default=date.today())
    Sancleaveto_Date=models.DateField(default=date.today())
    Team_Mail = models.CharField(max_length=100,blank=True)
    Approved_Flag = models.BooleanField(default=False)
    Status = models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdLeave')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedLeave')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Leave'


class LeaveEmployeeRecord(models.Model):
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE)
    LeaveEmpRec_Id=models.CharField(max_length=20,unique=True,default=LeaveEmpRec_I)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Employee_Id=models.ForeignKey(EmployeeMaster, on_delete=models.CASCADE)
    # Leavetype_Id=models.ForeignKey(Leavetype, on_delete=models.CASCADE) # need to discuss
    Application_Id = models.ForeignKey(Leave,on_delete=models.CASCADE)
    Opening_Balance=models.IntegerField()
    Leave_taken=models.IntegerField()
    Leave_Balance=models.IntegerField(default=0) # need to discuss
    Lossof_Pay=models.IntegerField(blank=True)# need to discuss
    Leave_ProcessedDate=models.DateField(default=date.today())
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdLeaveEmployeeRecord')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedLeaveEmployeeRecord')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'LeaveEmployeeRecord'




class Leavecancellation(models.Model):
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE)
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Cancellation_Id=models.CharField(max_length=20,unique=True,default=LeaveCancellation_I)
    Application_Id=models.ForeignKey(Leave,on_delete=models.CASCADE) #need to discuss
    Leavetype_Id=models.ForeignKey(Leavetype, on_delete=models.CASCADE) # need to dicuss
    Cancel_FromDate=models.DateField(default=date.today())
    Cancel_ToDate=models.DateField(default=date.today())
    Is_Deleted = models.BooleanField(default=False)
    Leavecancel_flg = models.BooleanField(default=False)
    ReasonOf_Cancelation=models.CharField(max_length=1500,blank=True)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdLeavecancellation')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedLeavecancellation')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Leavecancellation'


class LeaveExtension(models.Model):
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE)
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    LvExtn_Id=models.CharField(max_length=20,unique=True,default=LvExtn_I)
    Application_Id=models.ForeignKey(Leave,on_delete=models.CASCADE) #need to discuss
    Leavetype_Id=models.ForeignKey(Leavetype, on_delete=models.CASCADE) # need to dicuss
    Extend_FromDate=models.DateField(default=date.today())
    Extend_ToDate=models.DateField(default=date.today())
    ReasonOf_Extension=models.DateField(default=date.today()) 
    Sanctioned_Date=models.DateField(default=date.today())
    Sanctioned_By=models.CharField(max_length=100,blank=True)# need to dicuss
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdLeaveExtension')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedLeaveExtension')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'LeaveExtension'


class TimeSheet(models.Model):
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE,null=True,blank=True)
    Employee_Id = models.ForeignKey(EmployeeMaster,on_delete=models.CASCADE,null=True,blank=True)
    TimeSheet_Id=models.CharField(max_length=20,unique=True,default=TimeSheet_I)
    Timesheet_Name = models.CharField(max_length=100,blank=True)
    # FromTime=models.TimeField(default=timezone.now())
    # ToTime=models.TimeField(default=timezone.now())
    FromDate=models.DateField(default=date.today())
    ToDate=models.DateField(default=date.today()) # need to dicuss
    # TaskCarried_Over=models.CharField(max_length=100,blank=True)
    # Total_Time=models.FloatField(blank=True)
    Work_Report = models.CharField(max_length=500,blank=True,null=True)
    Month = models.CharField(max_length=50,blank=True)
    Year = models.IntegerField(default=0)
    Weekno = models.IntegerField(default=0)
    Timemanagement = models.JSONField(default={})
    Draftflg = models.BooleanField(default=False)
    Approved_Flag=models.BooleanField(default=False)
    Status = models.BooleanField(default=False)
    # Is_Approvedflg = models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdtimesheet')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedtimesheet')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'TimeSheet'



class TimeManagement(models.Model):
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE)
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Time_Id=models.CharField(max_length=20,unique=True,default=Time_I)
    Employee_Id=models.ForeignKey(EmployeeMaster, on_delete=models.CASCADE)
    Timesheet_Name = models.CharField(max_length=100,blank=True)
    FromDate=models.DateField(default=date.today())
    ToDate=models.DateField(default=date.today())
    TimeSheet_Id = models.ManyToManyField(TimeSheet)
    Is_Approvedflg = models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdtime')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedtime')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'TimeManagement'



class AttandanceManagement(models.Model):
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE)
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Attandance_Id=models.CharField(max_length=20,unique=True,default=Attandance_I)
    Employee_Id=models.ManyToManyField(EmployeeMaster)
    Date=models.DateField(default=date.today())
    Month = models.CharField(max_length=50,blank=True)
    Year = models.IntegerField(default=0)
    Login_Or_Not = models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdatt')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedatt')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'AttandanceManagement'


class Attandancemaster(models.Model):
    Partner_Id=models.IntegerField(default=0,blank=True)
    # Attandancemaster_Id = models.CharField(max_length=20,default=Attandancemaster_I,unique=True)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Employee_Id=models.CharField(max_length=100,blank=True)
    Month = models.CharField(max_length=50,blank=True)
    Year = models.IntegerField(default=0)
    EmployeeName = models.CharField(max_length=100,blank=True)
    Designation = models.CharField(max_length=100,blank=True)
    Department = models.CharField(max_length=100,blank=True)
    Nofodays_Present = models.IntegerField(default=0,blank=True)
    NoofDays_Leave = models.IntegerField(default=0,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Monthly_Salary = models.IntegerField(default=0,blank=True)
    This_Month_Salary = models.IntegerField(default=0,blank=True)
    Other_Details = models.JSONField(default={},blank=True)
    UnPaid_Leave = models.IntegerField(default=0,blank=True)
    Paid_Leave = models.IntegerField(default=0,blank=True)
    # Is_Approved = models.BooleanField(default=False)
    Approved_Flag=models.BooleanField(default=False)
    Status = models.BooleanField(default=False)
    Created_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Attandancemaster'



#Payroll Management

class Payrollconfiguration(models.Model):
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE)
    Payrollconfig_Id = models.CharField(max_length=20,unique=True,default=Payrollconfig_I)
    Timsheetsub = models.CharField(max_length=100,blank=True)
    Cutofdate=models.CharField(max_length=100,blank=True)
    Cutofdateofmonth=models.DateField(default=date.today())
    payrollprocessing_Date = models.DateField(default=date.today())
    payrollpartner_Date = models.DateField(default=date.today())
    Timesheetsubmission=models.CharField(max_length=100,blank=True)
    Cadre_Id=models.ForeignKey(Cadre,on_delete=models.CASCADE)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createpayconfig')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatepayconfig')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table='Payrollconfiguration'


class Payrollsetup(models.Model):
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE)
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Payrolsetup_Id=models.CharField(max_length=20,unique=True,default=Payrolsetup_I)
    Setup_Name=models.CharField(max_length=50,blank=True)
    PF_Perc=models.CharField(max_length=50,blank=True)
    FPF_Perc=models.CharField(max_length=50,blank=True)
    ProfTax_Range1from=models.CharField(max_length=50,blank=True)# doubt
    ProfTax_Range1to=models.CharField(max_length=50,blank=True)# doubt
    ProfTax_Amt1=models.FloatField(blank=True)
    ProfTax_Range2from=models.CharField(max_length=50,blank=True)# doubt
    ProfTax_Range2to=models.CharField(max_length=50,blank=True)# doubt
    ProfTax_Amt2=models.FloatField(blank=True)
    ProfTax_Range3from=models.CharField(max_length=50,blank=True)# doubt
    ProfTax_Range3to=models.CharField(max_length=50,blank=True)# doubt
    ProfTax_Amt3=models.FloatField(blank=True)
    ProfTax_Range4from=models.CharField(max_length=50,blank=True)# doubt
    ProfTax_Range4to=models.CharField(max_length=50,blank=True)# doubt
    ProfTax_Amt4=models.FloatField(blank=True)
    ESI_Perc=models.CharField(max_length=50,blank=True)
    Default_Payelement=models.CharField(max_length=50,blank=True)
    HRCalendar_Id=models.ForeignKey(HRcalendar, on_delete=models.CASCADE) #need to discuss
    Is_Deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'Payrollsetup'


class Payelements(models.Model):
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE)
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Payelement_Id=models.CharField(max_length=20,unique=True,default=Payelement_I)
    Payelement_Name=models.CharField(max_length=50,blank=True)
    Payelement_PrintName=models.CharField(max_length=50,blank=True)
    Payelement_Type=models.CharField(max_length=50,blank=True)
    Payelement_Method=models.CharField(max_length=50,blank=True)
    Taxable=models.CharField(max_length=50,blank=True)
    Attenedance_Based=models.BooleanField(default=True)
    ITSection_Code=models.CharField(max_length=100,blank=True)
    Payslip=models.BooleanField(default=True)
    Account_Id=models.ForeignKey(Chartofaccount, on_delete=models.CASCADE) #need to discuss
    RoundOff_Type=models.CharField(max_length=50,blank=True)
    Round_Carryover=models.CharField(max_length=100,blank=True)
    Effective_From=models.CharField(max_length=100,blank=True) # need to know field
    Effective_To=models.CharField(max_length=100,blank=True)  # need to field
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdPayelements')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedPayelements')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Payelements'



class Employeepayelement(models.Model):
    Employeepayelement_Id=models.CharField(max_length=20,unique=True,default=Employeepayelement_I)
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Employee_Id=models.ForeignKey(EmployeeMaster, on_delete=models.CASCADE)
    Payelement_Id=models.ForeignKey(Payelements, on_delete=models.CASCADE)
    Amount=models.FloatField() # need to discuss
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdEmployeepayelement')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedEmployeepayelement')
    Updated_Date = models.DateField(default=date.today())


    class Meta:
        db_table = 'Employeepayelement'


class Paycalculationformula(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Formula_Id=models.CharField(max_length=20,unique=True,default=Formula_I)
    Formula_Name=models.CharField(max_length=100,blank=True)
    Payelement_Id=models.ForeignKey(Payelements, on_delete=models.CASCADE)#need to clarify
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdPaycalculationformula')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedPaycalculationformula')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Paycalculationformula'



class PayCalcElements(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Payroll_Id=models.CharField(max_length=20,unique=True,default=Payroll_I) #need to clarify
    MNYR= models.DateField(default=date.today())#need to clarify MNR
    Employee_Id=models.ForeignKey(EmployeeMaster, on_delete=models.CASCADE)
    Payelement_Id=models.ForeignKey(Payelements,on_delete=models.CASCADE)
    Amount=models.FloatField()
    PayrollProc_Datetime=models.DateTimeField(default=timezone.now())
    Run_By=models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdPayCalcElements')
    Is_Deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'PayCalcElements'


class PayslipHeader(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id=models.IntegerField(default=0,blank=True)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Payslip_Id=models.CharField(max_length=20,unique=True,default=Payslip_I) #need to clarify about patroll
    # MNYR= models.DateField(default=date.today())
    Year = models.IntegerField(default=0,blank=True)
    Month = models.CharField(max_length=100,blank=True)
    Employee_Id=models.CharField(max_length=50,blank=True)
    Employee_Name=models.CharField(max_length=50,blank=True)
    # Department=models.ForeignKey(EmployeeMaster, on_delete=models.CASCADE) # cloumn not availablCustomere in employee master
    Department=models.CharField(max_length=100,blank=True)
    # Bank_Name=models.CharField(max_length=50)
    Noof_Daysinmonth=models.DateField(default=date.today())#need to clarify month or days in the month
    Noof_DaysPresent=models.IntegerField()#need to clarify month or days in the month
    # LOP=models.IntegerField()
    Deduction=models.JSONField(default={},blank=True)
    Is_Deleted = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'PayslipHeader'


# class Rostering(models.Model):
#     RoasterMaster_Id=models.CharField(max_length=100,blank=True)
#     #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
#     WeekNo=models.IntegerField(default=0,blank=True) #need to clarify about patroll
#     Employee_Name= models.CharField(max_length=100,blank=True)
#     Day=models.CharField(max_length=100,blank=True)
#     Location=models.CharField(max_length=100,blank=True)
#     Service_Name=models.CharField(max_length=50,blank=True)
#     Allocations = models.JSONField(default={},blank=True)
#     leave=models.CharField(max_length=20,blank=True)
#     Partner_Id=models.IntegerField(default=0)
#     Employee_Id=models.IntegerField(default=0)#need to clarify month or days in the month
#     LOP=models.IntegerField()
#     Is_Deleted = models.BooleanField(default=False)
    
#     class Meta:
#         db_table = 'Rostering'

# Tables for the procurement

class Supplieraddress(models.Model):

    Supplieraddress_Id=models.CharField(max_length=20,unique=True,default=Supplieraddress_I)
    Customer_Key=models.CharField(max_length=50,blank=True)
    Building_Name=models.CharField(max_length=200,blank=True)
    Building_Street=models.CharField(max_length=200,blank=True)
    Building_Area=models.CharField(max_length=200,blank=True)
    Building_City=models.CharField(max_length=200,blank=True)
    Building_State=models.CharField(max_length=50,blank=True)
    Building_Country=models.CharField(max_length=50,blank=True)
    Zip_Code=models.CharField(max_length=50,blank=True)#
    Active_Status=models.BooleanField(default=True)#
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdSupplieraddress')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedSupplieraddress')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Supplieraddress'



class Supplier(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id = models.ForeignKey(Partner,on_delete=models.CASCADE,null=True,blank=True)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    # Supplier_Key=models.IntegerField(primary_key=True)
    Supplier_Id=models.CharField(max_length=20,unique=True,default=Supplier_I)
    Company_Name=models.CharField(max_length=50,blank=True)
    Salutation=models.CharField(max_length=50,blank=True)
    ContactPerson_FirstName=models.CharField(max_length=50,blank=True)
    ContactPerson_LastName=models.CharField(max_length=50,blank=True)
    Mobile_No=models.CharField(max_length=50,blank=True)
    Email_Id=models.EmailField(unique=True)
    Currency = models.CharField(max_length=250,blank=True)
    GST = models.CharField(max_length=100,blank=True)
    Website=models.CharField(max_length=100,blank=True)
    TermsandCondition = models.CharField(max_length=1000,blank=True)
    Supplieraddress_Id = models.ForeignKey(Supplieraddress,on_delete=models.CASCADE)
    Bank_Name=models.CharField(max_length=100,blank=True)
    Bank_Address=models.CharField(max_length=250,blank=True)
    Account_No=models.CharField(max_length=20,blank=True)
    IFSC_Code=models.CharField(max_length=100,blank=True)
    Swift_Code=models.CharField(max_length=100,blank=True)
    Opening_Balance = models.FloatField(blank=True,default=0.0)
    Due_Date = models.DateField(default=date.today())
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdSupplier')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedSupplier')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Supplier'


class Supplierinvoicedetail(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Invoice_Id=models.ForeignKey(Invoice, on_delete=models.CASCADE)
    Supplier_Id=models.ForeignKey(Supplier,on_delete=models.CASCADE,blank=True,null=True)
    Supplierinvoicedetail_Id=models.CharField(max_length=20,unique=True,default=Supplierinvoicedetail_I)
    Supplier_BillNo=models.CharField(max_length=100,blank=True)
    Supplier_BillDt=models.DateField(default=date.today())
    GIN0=models.CharField(max_length=100,blank=True)
    PONo=models.CharField(max_length=100,blank=True)
    # Item=models.CharField(max_length=100,blank=True)#need to clarify po no from sales header
    Items = models.JSONField(default={},blank=True)
    Bill_Discount=models.CharField(max_length=100,blank=True)
    Discount=models.CharField(max_length=100,blank=True)
    Invoice_Amount=models.FloatField()
    Tax_Amount=models.FloatField()
    Freight_Amt=models.FloatField()
    Supplier_Name=models.CharField(max_length=100,blank=True)
    Invoice_Status=models.BooleanField(default=False)
    Approved_Flag=models.BooleanField(default=False)
    Status = models.BooleanField(default=False)
    # Payment_RefNo=models.CharField(max_length=100,blank=True)
    Payment_method=models.CharField(max_length=100,blank=True)
    # Quantity=models.IntegerField()# doubt
    # Amount=models.FloatField()
    Accounts = models.JSONField(default={},blank=True)
    Net_Amount=models.FloatField()
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdSupplierinvoicedetail')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedSupplierinvoicedetail')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Supplierinvoicedetail'

class ItemMaster(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Item_Id=models.CharField(max_length=20,unique=True,default=Item_I)
    Item_Name=models.CharField(max_length=100,blank=True)
    Material_GroupCode=models.CharField(max_length=100,blank=True)
    categragy=models.CharField(max_length=100,blank=True) # new Add
    Dimension=models.CharField(max_length=100,blank=True) # new Add
    Item_GroupName=models.CharField(max_length=100,blank=True)
    # Item_group_Name=models.ForeignKey(ItemGroup,on_delete=models.CASCADE) # new Add
    Purchase_Account=models.ForeignKey(Chartofaccount,on_delete=models.CASCADE, related_name='Purchase_Account_name') # new Add
    Purchase_Description=models.CharField(max_length=100,blank=True) # new Add
    Inventory_Account=models.ForeignKey(Chartofaccount,on_delete=models.CASCADE, related_name='Inventory_Accountname') # new Add
    Inventory_Description=models.CharField(max_length=100,blank=True) # new Add
    Manufacture=models.CharField(max_length=100,blank=True) # new Add
    Brand=models.CharField(max_length=100,blank=True) # new Add
    ReOrderLevel=models.CharField(max_length=100,blank=True) # new Add
    Upload_Image=models.FileField(blank=True,max_length=1000000,default='image.png') # new Add
    Prevender=models.ForeignKey(Supplier,on_delete=models.CASCADE) # new Add
    MiniumStackLevel=models.CharField(max_length=100,blank=True) # new Add
    Cost_price=models.FloatField(max_length=100,blank=True) # new Add
    UOM=models.CharField(max_length=50,blank=True)
    Unit_Price=models.FloatField(blank=True)
    Opening_Stock=models.IntegerField(blank=True)
    Opening_StockDate=models.DateField(default=date.today())
    Is_Deleted = models.BooleanField(default=False)
    Is_Batch = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdItemMaster1')
    Created_Date = models.DateTimeField(default=timezone.now())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedItemMaster1')
    Updated_Date = models.DateTimeField(default=timezone.now())
    
    class Meta:
        db_table = 'ItemMaster'

class Batchtable(models.Model):
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE,blank=True,null=True)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Batch_No=models.CharField(max_length=20,blank=True,default=Batch_N)
    # Item_Name = models.CharField(max_length=250,blank=True,blank=True)
    # Receipt_Qty = models.FloatField(blank=True)
    # Item_Id = models.IntegerField(blank=True,null=True)
    Batch = models.JSONField(default={},blank=True)
    # Expiry_Date = models.DateTimeField(default=timezone.now())
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdbatch')
    Created_Date = models.DateTimeField(default=timezone.now())
    
    class Meta:
        db_table = 'Batchtable'


class ItemGroup(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE),
    Item_Group_Id=models.CharField(max_length=20,unique=True,default=Item_Group_I)
    Item_GroupName=models.CharField(max_length=100,blank=True)
    Description=models.CharField(max_length=100,blank=True)
    Brand=models.CharField(max_length=100,blank=True)
    Categragy=models.CharField(max_length=100,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Item_Id = models.ManyToManyField(ItemMaster,blank=True,null=True)
    Is_Active = models.BooleanField(default=True)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdItem_Group')
    Created_Date = models.DateTimeField(default=timezone.now())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedItem_Group')
    Updated_Date = models.DateTimeField(default=timezone.now())

    class Meta:
        db_table = 'ItemGroup'


class ItemCategory(models.Model):
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE,blank=True,null=True)
    Item_Category_Id=models.CharField(max_length=20,default=Item_Category_I)
    Item_Group_Id=models.ManyToManyField(ItemGroup,blank=True,null=True)
    Category_Name = models.CharField(max_length=100,blank=True,null=True)
    Description = models.CharField(max_length=250,blank=True,null=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdItem_Cat')
    Created_Date = models.DateTimeField(default=timezone.now())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedItem_Cat')
    Updated_Date = models.DateTimeField(default=timezone.now())

    class Meta:
        db_table = 'ItemCategory'


class ServiceJobDetails(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    ServiceJob_Id=models.CharField(max_length=20,unique=True,default=ServiceJob_I)
    Item_Id=models.ForeignKey(ItemMaster, on_delete=models.CASCADE,related_name="ServiceJobDetailsitemname15") 
    # UOM=models.ForeignKey(ItemMaster, on_delete=models.CASCADE,related_name="ServiceJobDetailsUOM15")#need to clarify the if item master is not there
    Unit_Price=models.ForeignKey(ItemMaster, on_delete=models.CASCADE,related_name="itemunitprice15") #need to clarify the if item master is not there
    Quantity_Used=models.PositiveIntegerField(default=0)# must be more than zero
    CostOf_Material=models.FloatField()
    Remark=models.CharField(max_length=1500,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdServiceJobDetails')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedServiceJobDetails')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'ServiceJobDetails' 


# class PRHeader(models.Model):

#     #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
#     PR_No=models.CharField(max_length=20)
#     CostCentre_Id=models.ForeignKey(Costcenter, on_delete=models.CASCADE) #cost center code is not available in costcenter
#     PR_Date=models.DateField(default=date.today())
#     Expected_Date=models.DateField(default=date.today())
#     Approver=models.CharField(max_length=50)
#     Approved_Flag=models.BooleanField(default=False)
#     Approved_Date=models.DateField(default=date.today())
#     PR_Status=models.CharField(max_length=50)
#     Rejected_Reason=models.CharField(max_length=1500)
#     Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdPRHeader')
#     Created_Date = models.DateField(default=date.today())
#     Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedPRHeader')
#     Created_Date = models.DateField(default=date.today())

#     class Meta:
#         db_table = 'PRHeader'

class PRDetails(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    PR_Id=models.CharField(max_length=20,unique=True)
    CostCentre_Id=models.ForeignKey(Costcenter, on_delete=models.CASCADE)
    Department_Id=models.ForeignKey(Department , on_delete=models.CASCADE)   #new add
    Item_LineNo=models.IntegerField(blank=True)  # need to discuss
    PR_Date=models.DateField(default=date.today())
    Expected_Date=models.DateField(default=date.today())
    Approver=models.CharField(max_length=50,blank=True)
    Approved_Flag=models.BooleanField(default=False)
    Status = models.BooleanField(default=False)
    Approved_Date=models.DateField(default=date.today())
    PR_Status=models.CharField(max_length=50,blank=True)
    Rejected_Reason=models.CharField(max_length=1500,blank=True)
    ReasonOfPurchase = models.CharField(max_length=250,blank=True)
    RequestedBy = models.CharField(max_length=50,blank=True)
    Available_Budget = models.FloatField(default= 0,max_length=150,blank=True)  #new add
    TotalEstValue = models.IntegerField(blank=True)
    NotesofBuyer = models.CharField(max_length=1000,blank=True)
    Attached_file=models.ManyToManyField(Emailattachment,blank=True,null=True)
    # UploadFile=models.FileField(blank=True,null=True, default='Flow_Chart_Login_Roles__Permission_vUWlXes_mDESZyG.png')   #new add
    Item = models.JSONField(blank=True,null=True,default=dict)    #new add
    Quantity=models.IntegerField()
    # Business_Unit = models.ForeignKey(User, on_delete=models.CASCADE)   #new add
    Store_Name = models.CharField(max_length=100,blank=True)
    Address = models.CharField(max_length=500,blank=True)
    PO_closed = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdPRDetails')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedPRDetails')
    Updated_Date = models.DateField(default=date.today())
    Is_Deleted = models.BooleanField(default = False)

    class Meta:
        db_table = 'PRDetails'

# class RFQHeader(models.Model):

#     Organization_Key=models.ForeignKey(Organization, on_delete=models.CASCADE)
#     RFQ_Key=models.IntegerField(primary_key=True)
#     RFQ_No=models.CharField(max_length=50)
#     RFQ_Suppliers=models.ForeignKey(Supplier, on_delete=models.CASCADE) # need to discuss about cloumn
#     RFQ_Content=models.CharField(max_length=1500)
#     Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdRFQHeader')
#     Created_Date = models.DateField(default=date.today())
#     Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedRFQHeader')
#     Created_Date = models.DateField(default=date.today())

#     class Meta:
#         db_table = 'RFQHeader'

class RFQdetails(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE,blank=True,null=True)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    RFQ_Id=models.CharField(max_length=20,unique=True,default=RFQ_I)
    Item_SlNo=models.IntegerField(blank=True) # dicuss about field
    # Item_code=models.ManyToManyField(ItemMaster) # need to clarify the if item master is not there
    Item_code=models.JSONField(default={})
    RFQ_Suppliers=models.ManyToManyField(Supplier)
    #UOM=models.ForeignKey(ItemMaster, on_delete=models.CASCADE,related_name="RFQdetailsUOM3")  # need to clarify the if item master is not there
    Quantity=models.IntegerField()
    QuantityName = models.CharField(max_length=100,blank=True)
    Quotation_Name = models.CharField(max_length=250,blank=True)
    NotestoSupplier = models.CharField(max_length=500,blank=True)
    Approved_Flag=models.BooleanField(default=False)
    Status = models.BooleanField(default=False)
    Order_Placed = models.BooleanField(default=False)
    Order_Closed = models.BooleanField(default=False)
    Order_Exceeded = models.BooleanField(default=False)
    ResponseRequestbyDate = models.DateField(default=date.today())
    NeedbyDate = models.DateField(default=date.today())
    Attached_file=models.ManyToManyField(Emailattachment,blank=True,null=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdRFQdetails')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedRFQdetails')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'RFQdetails'


# class QuoteHeader(models.Model):

#     #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
#     Quotation_Id=models.CharField(max_length=20)
#     Supplier_Id=models.ForeignKey(Supplier, on_delete=models.CASCADE) # supplier code cloumn not available 
#     Freight=models.CharField(max_length=50)
#     Delivery_Period=models.IntegerField()
#     Discount=models.CharField(max_length=50)
#     TermCondition=models.CharField(max_length=2000)
#     Advance=models.FloatField() # need to discuss
#     Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdQuoteHeader')
#     Created_Date = models.DateField(default=date.today())
#     Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedQuoteHeader')
#     Created_Date = models.DateField(default=date.today())

#     class Meta:
#         db_table = 'QuoteHeader'

class QuotationItem(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Item_description=models.CharField(max_length=100,blank=True)
    items=models.ManyToManyField(ItemMaster,blank=True,null=True)
    UOM=models.CharField(max_length=100,blank=True)
    Brand=models.CharField(max_length=100,blank=True)
    Quantity=models.FloatField(blank=True)
    Unit_Price=models.FloatField(blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdQuotationItem')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedQuotationItem')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'QuotationItem'


class QuoteDetails(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Quotation_Id=models.CharField(max_length=20,unique=True,default=Quotation_I)
    Supplier_Id=models.ForeignKey(Supplier, on_delete=models.CASCADE) # supplier code cloumn not available 
    Quotation_Name = models.CharField(max_length=250,blank=True)
    Freight=models.CharField(max_length=50,blank=True)
    Delivery_Period=models.IntegerField(blank=True)
    Discount=models.CharField(max_length=50,blank=True)
    TermCondition=models.CharField(max_length=2000,blank=True)
    Catalogue_Name = models.CharField(max_length=100,blank=True)
    Catalogue_Number = models.CharField(max_length=100, blank=True)
    RFQNumber = models.CharField(max_length=100,blank=True)
    Advance=models.FloatField(blank=True)
    AgainstRFQ=models.BooleanField(default=False)
    Item_SlNo=models.IntegerField(blank=True)# need to discuss
    Item_Code=models.JSONField(default={},blank=True) # need to clarify the if item master is not there
    # UOM=models.ForeignKey(ItemMaster, on_delete=models.CASCADE,related_name="QuoteDetailsUOM4")#need to clarify the if item master is not there
    Quantity=models.IntegerField()
    Unit_Rate=models.CharField(max_length=50,blank=True)
    # Tax_Id=models.ForeignKey(Taxsetup,on_delete=models.CASCADE)  #taxcode cloumn not available
    Tax_Perc=models.CharField(max_length=50,blank=True)
    Amount=models.FloatField()
    Attached_file=models.ManyToManyField(Emailattachment,blank=True,null=True)
    # Attached_file=models.FileField(max_length=1000000,blank=True,default='default.png',null=True)
    Draft_Flg = models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdQuoteDetails')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedQuoteDetails')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'QuoteDetails'

        
# class Suppliepriceheader(models.Model):

#     #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
#     Supplier_Key=models.ForeignKey(Supplier, on_delete=models.CASCADE)
#     Catalogue_Key=models.IntegerField(primary_key=True)
#     Catalogue_No=models.CharField(max_length=50)
#     Effective_Date=models.DateField(default=date.today())
#     Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdSuppliepriceheader')
#     Created_Date = models.DateField(default=date.today())
#     Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedSuppliepriceheader')
#     Created_Date = models.DateField(default=date.today())

#     class Meta:
#         db_table = 'Suppliepriceheader'


class Supplierpricedetails(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Supplier_Id=models.ForeignKey(Supplier, on_delete=models.CASCADE)
    Catalogue_Id=models.CharField(max_length=20,unique=True,default=Catalogue_I)
    Effective_Date=models.DateField(default=date.today())
    Item_Id=models.ForeignKey(ItemMaster, on_delete=models.CASCADE,related_name="SupplierpricedetailsItemcode5") # need to clarify the if item master is not there
    # UOM=models.ForeignKey(ItemMaster, on_delete=models.CASCADE,related_name="SupplierpricedetailsUOM5")#need to clarify the if item master is not there
    Unit_Rate=models.CharField(max_length=50,blank=True)
    Tax_Include=models.BooleanField(default=True)
    Tax_Id=models.ForeignKey(Taxsetup, on_delete=models.CASCADE)#need to clarify the tax code cloumn is not available
    Tax_Perc=models.CharField(max_length=50,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdSupplierpricedetails')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedSupplierpricedetails')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Supplierpricedetails'



class Buyermaster(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Buyer_Id=models.CharField(max_length=20,unique=True,default=Buyer_I)
    Employee_Id=models.ForeignKey(EmployeeMaster, on_delete=models.CASCADE)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdBuyermaster')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedBuyermaster')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Buyermaster'



# class POHeader(models.Model):

#     #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
#     PR_Id=models.ForeignKey(PRDetails, on_delete=models.CASCADE)
#     Supplier_Id=models.ForeignKey(Supplier, on_delete=models.CASCADE)
#     PO_Id=models.CharField(max_length=50)
#     Po_Date=models.DateField(default=date.today())
#     Payment_Term=models.CharField(max_length=100)
#     Pay_Mode=models.CharField(max_length=100)
#     Price_term=models.CharField(max_length=100)
#     Freight=models.CharField(max_length=100)
#     Desp_Instruction=models.CharField(max_length=1000)
#     Tax_comp=models.CharField(max_length=100)
#     Delivery_scheduledate=models.DateField(default=date.today())
#     Total_POAmount=models.FloatField()
#     POCancel_Flag=models.CharField(max_length=100)
#     POAmend_Flag=models.CharField(max_length=100)
#     PO_Remarks=models.CharField(max_length=1000)
#     Advpaid_Flag=models.BooleanField(default=False)
#     Advance_Amount=models.FloatField()
#     Cur_code=models.CharField(max_length=100)
#     Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdPOHeader')
#     Created_Date = models.DateField(default=date.today())
#     Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedPOHeader')
#     Created_Date = models.DateField(default=date.today())

#     class Meta:
#         db_table = 'POHeader'


class Catalogue(models.Model):
    Cat_Id=models.CharField(max_length=20,default=Cat_I,unique=True)
    Catalogue_Name = models.CharField(max_length=100,blank=True)
    Supplier_Name = models.CharField(max_length=100,blank=True)
    Supplier_Id = models.ForeignKey(Supplier,on_delete=models.CASCADE,blank=True)
    Item=models.JSONField(blank=True,default=dict)
    Quotation_Id = models.CharField(max_length=100,blank=True)    
    Is_Deleted=models.BooleanField(default=False)
    Expiry_Date = models.DateField(default=date.today())
    Expiry_Flg = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdCatalogue')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedCatalogue')
    Updated_Date = models.DateField(default=date.today())
    class Meta:
        db_table = 'Catalogue'


class StoreAdjustment(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    StoreAdj_Id=models.CharField(max_length=20,unique=True,default=StoreAdust_I)
    Hub_Name=models.JSONField(blank=True,default=dict)
    Adjusted_By=models.CharField(max_length=1000)
    Items=models.JSONField(blank=True,default=dict)
    Approved_Flag=models.BooleanField(default=False)
    Status = models.BooleanField(default=False)
    Apply=models.BooleanField(default=False)
    Is_Deleted=models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdStoreAdjustment')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedStoreAdjustment')
    Updated_Date = models.DateField(default=date.today())
    class Meta:
        db_table = 'StoreAdjustment'


class PODetails(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    PR_Id=models.ForeignKey(PRDetails, on_delete=models.CASCADE)
    Supplier_Id=models.ForeignKey(Supplier, on_delete=models.CASCADE)
    PO_Id=models.CharField(max_length=20,unique=True)
    Po_Date=models.DateField(default=date.today())
    Buyer_Name = models.CharField(max_length=50,blank=True)
    Delivery_Date = models.DateField(default=date.today())
    Payment_Term=models.CharField(max_length=100,blank=True)
    Pay_Mode=models.CharField(max_length=100,blank=True)
    Price_term=models.CharField(max_length=100,blank=True)
    Freight=models.CharField(max_length=100,blank=True)
    Desp_Instruction=models.CharField(max_length=1000,blank=True)
    Tax_comp=models.CharField(max_length=100,blank=True)
    Total_POAmount=models.FloatField()
    POCancel_Flag=models.BooleanField(default=False)
    POAmend_Flag=models.BooleanField(default=False)
    PO_Remarks=models.CharField(max_length=1000,blank=True)
    Advpaid_Flag=models.BooleanField(default=False)
    Advance_Amount=models.FloatField(blank=True)
    Cur_code=models.CharField(max_length=100,blank=True)
    Items=models.JSONField(blank=True)
    Itemshcedule=models.JSONField(default=dict,blank=True)
    Is_Deleted=models.BooleanField(default=False)
    Delivery_Address=models.JSONField(default=dict,blank=True)
    Approved_Flag=models.BooleanField(default=False)
    Status = models.BooleanField(default=False)
    Order_Placed = models.BooleanField(default=False)
    Order_Closed = models.BooleanField(default=False)
    Termsandcondition = models.CharField(max_length=2500,blank=True)
    Attached_file=models.ManyToManyField(Emailattachment,blank=True,null=True)
    # UOM=models.ForeignKey(PRDetails, on_delete=models.CASCADE,related_name="PODetailsUOM6")#need to clarify the
    Unit_Rate=models.CharField(max_length=50)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdPODetails')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedPODetails')
    Updated_Date = models.DateField(default=date.today())
    class Meta:
        db_table = 'PODetails'


class POCancelDetails(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Supplier_Id=models.ForeignKey(Supplier, on_delete=models.CASCADE)
    Supplier_Name=models.CharField(max_length=100,blank=True)
    POCancel_Id=models.CharField(max_length=20,unique=True,default=POC_I)
    PO_Amount=models.FloatField()
    PO_No=models.CharField(max_length=100)
    PO_Id=models.ForeignKey(PODetails,on_delete=models.CASCADE)
    Flag=models.BooleanField(default=False)
    Cancellation_Reason=models.CharField(max_length=1000)
    Items=models.JSONField(blank=True,default=dict)
    Approved_Flag=models.BooleanField(default=False)
    Status = models.BooleanField(default=False)
    Order_Closed = models.BooleanField(default=False)
    Is_Deleted=models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdPOCancel')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedPOCancel')
    Updated_Date = models.DateField(default=date.today())
    class Meta:
        db_table = 'POCancelDetails'


class POAmendDetails(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id = models.ForeignKey(Partner,on_delete=models.CASCADE,null=True,blank=True)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    # Supplier_Id=models.ForeignKey(Supplier, on_delete=models.CASCADE)
    Supplier_Name=models.CharField(max_length=100,blank=True)
    POAmend_Id=models.CharField(max_length=20,unique=True,default=POA_I)
    PO_Amount=models.FloatField()
    PO_No=models.CharField(max_length=100)
    Amendment_Reason=models.CharField(max_length=250,blank=True)
    PO_Id=models.ForeignKey(PODetails,on_delete=models.CASCADE)
    supplier_reference=models.CharField(max_length=1000)
    Items=models.JSONField(blank=True,default=dict)
    Flag=models.BooleanField(default=False)
    Approved_Flag=models.BooleanField(default=False)
    Status = models.BooleanField(default=False)
    Order_Placed = models.BooleanField(default=False)
    Order_Closed = models.BooleanField(default=False)
    Is_Deleted=models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdPOAmendDetails')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedPOAmendDetails')
    Updated_Date = models.DateField(default=date.today())
    class Meta:
        db_table = 'POAmendDetails'




# Tables for the inventory management

class Inventoryconfiguration(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Invconfig_Id=models.CharField(max_length=20,unique=True,default=Invconfig_I)
    ROL_Quantity=models.IntegerField()
    AcatMatl_Approve=models.CharField(max_length=50,blank=True)
    MSLItem_PRAuto=models.CharField(max_length=50,blank=True)
    Inventory_ValuationMethod=models.CharField(max_length=50,blank=True)
    Reject_ExpiryMatls=models.BooleanField(default=False)
    ACatItem_Valuefrom=models.CharField(max_length=50,blank=True)
    ACatItem_ValueTo=models.CharField(max_length=50,blank=True)
    BCatItem_Valuefrom=models.CharField(max_length=50,blank=True)
    BCatItem_ValueTo=models.CharField(max_length=50,blank=True)
    CCatItem_Valuefrom=models.CharField(max_length=50,blank=True)
    CCatItem_ValueTo=models.CharField(max_length=50,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdInventoryconfiguration')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedInventoryconfiguration')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Inventoryconfiguration'



class MaterialGroup(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Mgr_Id=models.CharField(max_length=20,unique=True,default=Mgr_I)
    Mgr_Name=models.CharField(max_length=100,blank=True)
    Account_Id=models.ForeignKey(Chartofaccount, on_delete=models.CASCADE,related_name="Accountcode")
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdMaterialGroup')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedMaterialGroup')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'MaterialGroup'



class Storemaster(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Store_Id=models.CharField(max_length=20,unique=True,default=Store_I)
    Store_Name=models.CharField(max_length=100,blank=True)
    StoreBuilding_Name=models.CharField(max_length=200,blank=True)
    Store_Street=models.CharField(max_length=200,blank=True)
    Store_Area=models.CharField(max_length=200,blank=True)
    Store_City=models.CharField(max_length=200,blank=True)
    Store_State=models.CharField(max_length=50,blank=True)
    Store_Country=models.CharField(max_length=50,blank=True)
    Own_Partner=models.CharField(max_length=50,blank=True)
    Partner_Id=models.ForeignKey(Partner, on_delete=models.CASCADE,default=1)
    Partner_OrganizationId=models.CharField(max_length=50,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Items = models.JSONField(default={})
    Storeitems = models.ManyToManyField(ItemMaster,null=True,blank=True)
    Store_Incharge=models.CharField(max_length=50,blank=True) # need to discuss
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdStoremaster')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedStoremaster')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Storemaster'



class StoreItems(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Store_Id=models.ForeignKey(Storemaster, on_delete=models.CASCADE)
    Store_Name=models.CharField(max_length=250,blank=True,null=True)
    Storeitem_Id=models.CharField(max_length=20,unique=True,default=Storeitem_I)
    Item_Name=models.CharField(max_length=100,blank=True)
    Item_Category=models.CharField(max_length=100,blank=True)
    Item_Group=models.CharField(max_length=100,blank=True)
    # Mgr_Id=models.ForeignKey(MaterialGroup,on_delete=models.CASCADE)  # need to clarify about the master class
    UOM=models.CharField(max_length=50,blank=True)    # need to clarify about the master class
    Unit_Price=models.FloatField(blank=True)
    Opening_Stock=models.IntegerField(blank=True)
    Opening_StockDate=models.DateField(default=date.today())
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdStoreItems')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedStoreItems')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'StoreItems'


# class Goodsinwardheader(models.Model):

#     #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
#     Store_Id=models.ForeignKey(Storemaster, on_delete=models.CASCADE)
#     GI_Id=models.CharField(max_length=20)
#     GI_Date=models.DateField(default=date.today())
#     Supplier_Id=models.ForeignKey(Supplier, on_delete=models.CASCADE) # # supplier code cloumn not available
#     PO_Id=models.ForeignKey(PODetails, on_delete=models.CASCADE)
#     Supplier_ChallanNo=models.CharField(max_length=100)
#     Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdGoodsinwardheader')
#     Created_Date = models.DateField(default=date.today())
#     Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedGoodsinwardheader')
#     Created_Date = models.DateField(default=date.today())

#     class Meta:
#         db_table = 'Goodsinwardheader'


class Goodsinwardetail(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Store_Id=models.ForeignKey(Storemaster, on_delete=models.CASCADE)
    GI_Id=models.CharField(max_length=20,unique=True,default=GI_I)
    GI_Date=models.DateField(default=date.today())
    Supplier_Id=models.ForeignKey(Supplier, on_delete=models.CASCADE) # # supplier code cloumn not available
    PO_Id=models.ForeignKey(PODetails, on_delete=models.CASCADE)
    Supplier_ChallanNo=models.CharField(max_length=100,blank=True)
    Item_LineNo=models.IntegerField(blank=True)
    Item_Id=models.ForeignKey(ItemMaster, on_delete=models.CASCADE,related_name="GoodsinwardetailItemcode7")
    # UOM=models.ForeignKey(ItemMaster, on_delete=models.CASCADE,related_name="Goodsinwardetailuom7")  # need to clarify
    # Item_Name=models.ForeignKey(ItemMaster, on_delete=models.CASCADE,related_name="GoodsinwardetailItemname7")
    Challan_Quantity=models.IntegerField(blank=True)
    Quantity_Recevied=models.IntegerField(blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdGoodsinwardetail')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedGoodsinwardetail')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Goodsinwardetail'



# class GoodsTrfOutHeader(models.Model):

#     #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
#     Store_Id=models.ForeignKey(Storemaster, on_delete=models.CASCADE)
#     Trfout_Id=models.CharField(max_length=100)
#     Trfout_Date=models.DateField(default=date.today())
#     Requester_Id=models.ForeignKey(User, on_delete=models.CASCADE)
#     Receipt_Flag=models.CharField(max_length=100)
#     Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdGoodsTrfOutHeader')
#     Created_Date = models.DateField(default=date.today())
#     Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedGoodsTrfOutHeader')
#     Created_Date = models.DateField(default=date.today())

#     class Meta:
#         db_table = 'GoodsTrfOutHeader'



class GoodsTrfOutDetail(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    GI_Id=models.ForeignKey(Goodsinwardetail, on_delete=models.CASCADE) # cloumn not available in GI header
    Store_Id=models.ForeignKey(Storemaster, on_delete=models.CASCADE)
    Trfout_Id=models.CharField(max_length=20,unique=True,default=Trfout_I)
    Trfout_Date=models.DateField(default=date.today())
    Requester_Id=models.ForeignKey(User, on_delete=models.CASCADE)
    Receipt_Flag=models.BooleanField(default=False)
    Item_LineNo=models.IntegerField(blank=True)  # need to check
    Item_Id=models.ForeignKey(ItemMaster, on_delete=models.CASCADE,related_name="GoodsTrfOutDetailItemcode8")
    # UOM=models.ForeignKey(ItemMaster, on_delete=models.CASCADE,related_name="GoodsTrfOutDetailUOM8")  # need to clarify
    # Item_Name=models.ForeignKey(ItemMaster, on_delete=models.CASCADE,related_name="GoodsTrfOutDetailItemname8")
    Quantity_Transfered=models.CharField(max_length=100,blank=True) # need to clarify
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdGoodsTrfOutDetail')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedGoodsTrfOutDetail')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'GoodsTrfOutDetail'


class  GoodsTrfReceiptDetails(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    TrfIn_Id=models.CharField(max_length=20,unique=True,default=TrfIn_I)  # need to clarify the cloumn key not avaialble GI header
    Item_LineNo=models.IntegerField(blank=True)
    Store_Id=models.ForeignKey(Storemaster, on_delete=models.CASCADE)   #add new
    Trfin_Date=models.DateField(default=date.today())
    Trf_FromStore=models.CharField(max_length=100,blank=True)
    # GI_Id=models.ForeignKey(Goodsinwardetail, on_delete=models.CASCADE)
    Item=models.JSONField(default=dict)
    Quantity_Recevied=models.IntegerField()
    Attached_File =models.ManyToManyField(Emailattachment,blank=True,null=True)
    Supplier_Name=models.ForeignKey(Supplier, on_delete=models.CASCADE)   #add new
    Challan_No=models.CharField(max_length=50,blank=True)   #add new
    Received_By=models.ForeignKey(User, on_delete=models.CASCADE)   #add new
    PO_Id=models.ForeignKey(PODetails, on_delete=models.CASCADE)   #add new
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdGoodsTrfReceiptDetails')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedGoodsTrfReceiptDetails')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'GoodsTrfReceiptDetails'


class MaterialRequestDetail(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE,blank=True,null=True)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    # GI_Id=models.ForeignKey(Goodsinwardetail, on_delete=models.CASCADE)
    Store_Id=models.ForeignKey(Storemaster, on_delete=models.CASCADE)
    MatReq_Id=models.CharField(max_length=20,unique=True,default=MatReq_I)
    Request_Date=models.DateField(default=date.today())
    CostCentre_Id=models.ForeignKey(Costcenter, on_delete=models.CASCADE)  # cost center code not available
    Department_Id=models.ForeignKey(Department , on_delete=models.CASCADE)
    Requester_Id=models.ForeignKey(User, on_delete=models.CASCADE)
    Approve_Flag=models.BooleanField(default=False) 
    Status = models.BooleanField(default=False)
    Approved_By=models.ForeignKey(User,on_delete=models.CASCADE,related_name='materialapproved') # need to clarify
    Reject_Reason=models.CharField(max_length=1000,blank=True)
    Item_LineNo=models.IntegerField(blank=True)
    Item=models.JSONField(blank=True,null=True, default=dict)
    Quantity_Recevied=models.IntegerField()
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdMaterialRequestDetail')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedMaterialRequestDetail')
    Updated_Date = models.DateField(default=date.today())
    Is_Deleted = models.BooleanField(default=False)   #new add

    class Meta:
        db_table = 'MaterialRequestDetail'


class MaterialIssueDetail(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id = models.ForeignKey(Partner,on_delete=models.CASCADE,blank=True,null=True)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Store_Id=models.ForeignKey(Storemaster, on_delete=models.CASCADE)
    MatIsu_Id=models.CharField(max_length=20,unique=True,default=MatIsu_I)
    MatReq_Id=models.ForeignKey(MaterialRequestDetail, on_delete=models.CASCADE)
    Requested_By = models.CharField(max_length=100,blank=True)
    Issue_Date=models.DateField(default=date.today())
    CostCentre_Id=models.ForeignKey(Costcenter, on_delete=models.CASCADE)
    Department_Id=models.ForeignKey(Department , on_delete=models.CASCADE)
    Issued_By=models.ForeignKey(User, on_delete=models.CASCADE)
    Issue_Price = models.FloatField(blank=True)
    Issue_Amount = models.FloatField(blank=True)
    Item_LineNo=models.IntegerField(blank=True)
    Item=models.JSONField(blank=True,null=True, default=dict)
    Quantity_Requested=models.BooleanField(default=False)
    Quantity_Issued=models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdMaterialIssueDetail')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedMaterialIssueDetail')
    Updated_Date = models.DateField(default=date.today())
    Is_Deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'MaterialIssueDetail'


class Materialreturndetails(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE,blank=True,null=True)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Item_LineNo=models.IntegerField(blank=True)
    Store_Id=models.ForeignKey(Storemaster, on_delete=models.CASCADE)
    MatRet_Id=models.CharField(max_length=20,unique=True,default=MatRet_I)
    MatIsu_Id=models.ForeignKey(MaterialIssueDetail, on_delete=models.CASCADE)  # add new
    Issue_Date=models.DateField(default=date.today())
    CostCentre_No=models.ForeignKey(Costcenter, on_delete=models.CASCADE)  # add new
    Department_Name=models.ForeignKey(Department , on_delete=models.CASCADE)  # add new
    Received_By=models.CharField(max_length=50, blank=True)  # add new
    Issued_By=models.ForeignKey(User, on_delete=models.CASCADE,related_name="MaterialReturnHeaderIssued_By") # need to clarify
    Return_By=models.ForeignKey(User, on_delete=models.CASCADE,related_name="MaterialReturnHeaderReturn_By")
    Item=models.JSONField(blank=True,null=True,default=dict)  # add new
    Item_Id=models.ForeignKey(ItemMaster, on_delete=models.CASCADE,related_name="Materialreturndetailsitemcode12")
    Quantity_Issued=models.BooleanField(default=False)
    Quantity_Returned=models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdMaterialreturndetails')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedMaterialreturndetails')
    Updated_Date = models.DateField(default=date.today())
    Is_Deleted = models.BooleanField(default=False)  # add new

    class Meta:
        db_table = 'Materialreturndetails'


# add new table by ponkumar
class MaterialTransferDetails(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    Partner_Id=models.ForeignKey(Partner,on_delete=models.CASCADE,blank=True,null=True)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Store_Id=models.ForeignKey(Storemaster, on_delete=models.CASCADE)
    MatTransf_Id=models.CharField(max_length=20,unique=True,default=MatTransf_I)
    MatTransf_Date=models.DateField(default=date.today())
    Requester_Id=models.ForeignKey(User, on_delete=models.CASCADE)
    Receipt_Flag=models.BooleanField(default=False)
    Transfer_Store_Name=models.CharField(max_length=50,blank=True)
    From_Store_Name=models.CharField(max_length=50,blank=True)
    Issued_By=models.CharField(max_length=50,blank=True)
    Reference_Number = models.CharField(max_length=100,blank=True)
    CostCenter_Id=models.ForeignKey(Costcenter, on_delete=models.CASCADE)
    Item_LineNo=models.IntegerField(blank=True)  # need to check
    Item=models.JSONField(blank=True,null=True,default=dict)
    Approve_Flag=models.BooleanField(default=False) 
    Status = models.BooleanField(default=False)
    Quantity_Transfered=models.CharField(max_length=100,blank=True) # need to clarify
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdMaterialTransferDetail')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedMaterialTransferDetail')
    Updated_Date = models.DateField(default=date.today())
    Is_Deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'MaterialTransferDetails'



class StoresCreditNote(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
    #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    SCN_Id=models.CharField(max_length=20,unique=True,default=SCN_I)
    Item_LineNo=models.IntegerField(blank=True)
    Item_Id=models.ForeignKey(ItemMaster, on_delete=models.CASCADE,related_name="MaterialreturndetailsItemcode")
    # UOM=models.ForeignKey(ItemMaster, on_delete=models.CASCADE,related_name="Materialreturndetails") 
    # Item_Name=models.ForeignKey(ItemMaster, on_delete=models.CASCADE,related_name="Materialreturndetailsitemname")
    StockQuantity_Before=models.IntegerField()
    Quantity=models.CharField(max_length=100,blank=True)
    Increase_Reduce=models.CharField(max_length=100,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdStoresCreditNote')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedStoresCreditNote')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'StoresCreditNote'



# add new module by ponkumar
# class ItemGroup(models.Model):
#     #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
#     ItemGroup_Id=models.CharField(max_length=20,unique=True,default=Item_Group_I)
#     ItemGroup_Name=models.CharField(max_length=50,blank=True)
#     Item_LineNo=models.IntegerField(blank=True)
#     Description=models.CharField(max_length=250,blank=True)
#     Brand=models.CharField(max_length=50,blank=True)
#     Category=models.CharField(max_length=50,blank=True)
#     Is_Active=models.BooleanField(default=False)
#     Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdItem_Group')
#     Created_Date = models.DateField(default=date.today())
#     Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedItem_Group')
#     Created_Date = models.DateField(default=date.today())
#     Is_Deleted = models.BooleanField(default=False)

#     class Meta:
#         db_table = 'ItemGroup'




#ponkumar
class Entity(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
     #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Entity_Id = models.CharField(max_length=20,unique=True,default=Entity_I)
    Entity_Name = models.CharField(max_length=100,blank=True)
    Entity_Description = models.CharField(max_length=150,blank=True)
    Entity_Type_Code = models.IntegerField(default=1, blank=True)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='create_entity')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='update_entity')
    Updated_Date = models.DateField(default=date.today())
    Is_Deleted = models.BooleanField(default=False)
    class Meta:
        db_table = 'Entity'

#ponkumar
class Country(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
     #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Entity_Id = models.ForeignKey(Entity, on_delete = models.CASCADE)
    Country_Id = models.CharField(max_length=20,unique=True,default=Country_I)
    Country_Name = models.CharField(max_length=100,blank=True)
    Country_Description = models.CharField(max_length=150,blank=True)
    Country_Type_Code = models.IntegerField(default=2, blank=True)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='create_Country')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='update_Country')
    Updated_Date = models.DateField(default=date.today())
    Is_Deleted = models.BooleanField(default=False)
    class Meta:
        db_table = 'Country'

#ponkumar
class Region(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
     #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Entity_Id = models.ForeignKey(Entity, on_delete = models.CASCADE)
    Country_Id = models.ForeignKey(Country, on_delete = models.CASCADE)
    Region_Id = models.CharField(max_length=20,unique=True,default=Region_I)
    Region_Name = models.CharField(max_length=100,blank=True)
    Region_Description = models.CharField(max_length=150,blank=True)
    Region_Type_Code = models.IntegerField(default=3, blank=True)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='create_Region')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='update_Region')
    Updated_Date = models.DateField(default=date.today())
    Is_Deleted = models.BooleanField(default=False)
    class Meta:
        db_table = 'Region'
    
#ponkumar
class State(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
     #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Entity_Id = models.ForeignKey(Entity, on_delete = models.CASCADE)
    Country_Id = models.ForeignKey(Country, on_delete = models.CASCADE)
    Region_Id = models.ForeignKey(Region, on_delete = models.CASCADE)
    State_Id = models.CharField(max_length=20,unique=True,default=State_I)
    State_Name = models.CharField(max_length=100,blank=True)
    State_Description = models.CharField(max_length=150,blank=True)
    State_Type_Code = models.IntegerField(default=4, blank=True)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='create_State')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='update_State')
    Updated_Date = models.DateField(default=date.today())
    Is_Deleted = models.BooleanField(default=False)
    class Meta:
        db_table = 'State'

#ponkumar
class City(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
     #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Entity_Id = models.ForeignKey(Entity, on_delete = models.CASCADE)
    Country_Id = models.ForeignKey(Country, on_delete = models.CASCADE)
    Region_Id = models.ForeignKey(Region, on_delete = models.CASCADE)
    State_Id = models.ForeignKey(State, on_delete = models.CASCADE)
    City_Id = models.CharField(max_length=20,unique=True,default=City_I)
    City_Name = models.CharField(max_length=100,blank=True)
    City_Description = models.CharField(max_length=150,blank=True)
    City_Type_Code = models.IntegerField(default=5, blank=True)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='create_City')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='update_City')
    Updated_Date = models.DateField(default=date.today())
    Is_Deleted = models.BooleanField(default=False)
    class Meta:
        db_table = 'City'



class OrganizationStructure(models.Model):
    Company_Id=models.ForeignKey(Company,on_delete=models.CASCADE,default=1)
     #Organization_Id=models.ForeignKey(Organization, on_delete=models.CASCADE)
    Organization_Structure_Id = models.CharField(max_length=100, unique=True,default=Organization_Structure_I)
    Entity_Id = models.ForeignKey(Entity, on_delete = models.CASCADE)
    Country_Id = models.ForeignKey(Country, on_delete = models.CASCADE)
    Region_Id = models.ForeignKey(Region, on_delete = models.CASCADE)
    State_Id = models.ForeignKey(State, on_delete = models.CASCADE)
    City_Id = models.ForeignKey(City, on_delete = models.CASCADE)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdorganizationstructure')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedorganizationstructure')
    Updated_Date = models.DateField(default=date.today())
    Is_Deleted = models.BooleanField(default=False)
    class Meta:
        db_table = 'OrganizationStructure'



#Settings Tables

class Itempreference(models.Model):
    Partner_Id=models.ForeignKey(Partner, on_delete=models.CASCADE)
    Itempreference_Id = models.CharField(max_length=20,default=Itempreference_I)
    rate_for_item = models.IntegerField()
    HSN_codeOrSAC_code = models.BooleanField(default=False)
    Inventry_Tracking = models.BooleanField(default=False)
    Inventry_startdate = models.DateField(default=date.today())
    Stock_belowzero = models.BooleanField(default=False)
    Out_Of_stock = models.BooleanField(default=False)
    Re_order_point = models.BooleanField(default=False)
    Notifyto = models.CharField(unique=True,max_length=100)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createditempre')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updateditempre')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Itempreference'


class Salesorderpreference(models.Model):
    Partner_Id=models.ForeignKey(Partner, on_delete=models.CASCADE)
    Salesorderpreference_Id = models.CharField(max_length=20,default=Salesorderpreference_I)
    Address = models.BooleanField(default=False)
    Customernotes = models.BooleanField(default=False)
    Termsandcondition = models.BooleanField(default=False)
    Notes = models.CharField(max_length=1000,blank=True)
    Termsand_condition = models.CharField(max_length=1000,blank=True)
    Refund_Percentage=models.IntegerField(default=0,blank=True,null=True)
    Cancelbefore_Service = models.IntegerField(default=0,blank=True,null=True)
    Pausebefore_Service = models.IntegerField(default=0,blank=True,null=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdsalesorderpre')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedsalesorderpre')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Salesorderpreference'



class Invoicepreference(models.Model):
    Partner_Id=models.ForeignKey(Partner, on_delete=models.CASCADE)
    Invoicepreference_Id = models.CharField(max_length=20,default=Invoicepreference_I)
    Show_Primary= models.BooleanField(default=False)
    Allow_Editing = models.BooleanField(default=False)
    Email = models.CharField(max_length=100,unique=True)
    # Usesalesorder = models.BooleanField(default=False)
    # Salesorderref = models.BooleanField(default=False)
    usesaleorder = models.CharField(max_length=100,blank=True)
    Getnotify = models.BooleanField(default=False)
    Payreceipt = models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdinvpre')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedinvpre')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Invoicepreference'


class Purchasepreference(models.Model):
    Partner_Id=models.ForeignKey(Partner, on_delete=models.CASCADE)
    Purchasepreference_Id = models.CharField(max_length=20,default=Purchasepreference_I)
    PO_schedule = models.BooleanField(default=False)
    Goods_received = models.BooleanField(default=False)
    Stock_quantity = models.BooleanField(default=False)
    PR_quantity = models.BooleanField(default=False)
    Bill_overdue = models.BooleanField(default=False)
    Notifyto = models.CharField(unique=True,max_length=100)
    Termsandcondition = models.CharField(max_length=2500,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdpurpre')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedpurpre')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Purchasepreference'



class Budgetpreference(models.Model):
    Partner_Id=models.ForeignKey(Partner, on_delete=models.CASCADE)
    Budgetpreference_Id = models.CharField(max_length=20,default=Budgetpreference_I)
    Budget_creation = models.CharField(max_length=100,blank=True)
    Budget_period = models.CharField(max_length=100,blank=True)
    Rateperuser = models.IntegerField()
    Rateper_subscription = models.IntegerField()
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdbudgetpre')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedbudgetpre')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Budgetpreference'


class Inventorypreference(models.Model):
    Partner_Id=models.ForeignKey(Partner, on_delete=models.CASCADE)
    Inventorypreference_Id = models.CharField(max_length=20,default=Inventorypreference_I)
    Inventorybasedon = models.CharField(unique=True,max_length=100)
    Category1 = models.CharField(unique=True,max_length=100)
    Category2 = models.CharField(unique=True,max_length=100)
    Category3 = models.CharField(unique=True,max_length=100)
    Donotissue = models.BooleanField(default=False)
    Adjustment = models.BooleanField(default=False)
    Itemsused = models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdinvenpre')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedinvenpre')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Inventorypreference'


class Transactionapprovalpreference(models.Model):
    Partner_Id=models.ForeignKey(Partner, on_delete=models.CASCADE)
    Transactionapprovalpreference_Id = models.CharField(max_length=20,default=Transactionapprovalpreference_I)
    Sales = models.BooleanField(default=False)
    Purchase = models.BooleanField(default=False)
    Notify = models.BooleanField(default=False)
    Toallsepecific = models.CharField(max_length=100,blank=True)
    Approved = models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdtranpre')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedtranpre')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Transactionapprovalpreference'



class Transactionalseries(models.Model):
    Company_Id = models.ForeignKey(Company,on_delete=models.CASCADE)
    Transactionalseries_Id=models.CharField(max_length=20,unique=True,default=Transactionalseries_I)
    Transaction_Name = models.CharField(max_length=250,blank=True)
    Credit_Note = models.CharField(max_length=20,blank=True)
    Journal_vouchar = models.CharField(max_length=20,blank=True)
    Cash_Vouchar = models.CharField(max_length=20,blank=True)
    Bank_Vouchar = models.CharField(max_length=20,blank=True)
    Purchase_Request = models.CharField(max_length=20,blank=True)
    Purchase_Order = models.CharField(max_length=20,blank=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createdtransactionsereries')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedtransactionsereries')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Transactionalseries'



class Pincodetable(models.Model):
    Pincode = models.CharField(max_length=10,primary_key=True)
    Zonename = models.CharField(max_length=200)
    cityname = models.CharField(max_length=200)

    class Meta:
        db_table = 'Pincodetable'


class Zone(models.Model):
    Company_Id = models.ForeignKey(Company, on_delete=models.CASCADE)
    City_Id = models.ForeignKey(City, on_delete=models.CASCADE)
    Zone_Name = models.CharField(max_length=100, blank=True)
    Pincode = models.CharField(max_length=20, blank=True)
    Zone_Type_Code = models.IntegerField(default=6, blank=True)
    Created_By = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='create_Zone')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='update_Zone')
    Updated_Date = models.DateField(default=date.today())
    Is_Deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'Zone'



class Dropdowntable(models.Model):
    Dropdown_Id = models.CharField(max_length=20,unique=True,default=Dropdown_I)
    Lookupname = models.CharField(max_length=250,unique=True)
    value = models.JSONField(default={})
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createddrop')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updateddrop')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Dropdowntable'


class Approval(models.Model):
    Approval_Id=models.CharField(max_length=20,unique=True,default=Approval_I)
    Approvername = models.CharField(max_length=200,blank=True)
    Comments=models.CharField(max_length=500,blank=True)
    Approved_flg=models.BooleanField(default=False)
    status=models.BooleanField(default=False)
    
    class Meta:
        db_table = 'Approval'


class Documentapproval(models.Model):
    Documentapproval_Id=models.CharField(max_length=20,unique=True,default=Documentapproval_I)
    Request_Id=models.CharField(max_length=50)
    Approval_Id=models.ManyToManyField(Approval)
    Approve = models.JSONField(blank=True,default={})
    Is_Deleted = models.BooleanField(default=False)
    Created_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='createddocument')
    Created_Date = models.DateField(default=date.today())
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updateddocument')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Documentapproval'


class Updaterecords(models.Model):
    Partner_Id=models.ForeignKey(Partner, on_delete=models.CASCADE, null=True,blank=True)
    Document_Id=models.CharField(max_length=100,blank=True)
    Updated_Person=models.CharField(max_length=100,blank=True)
    Updated_Field = models.CharField(max_length=250,blank=True)
    Updated_Time = models.TimeField(default=timezone.now())
    Is_Deleted = models.BooleanField(default=False)
    Updated_By = models.ForeignKey(User, on_delete=models.CASCADE, related_name='updatedrecord')
    Updated_Date = models.DateField(default=date.today())

    class Meta:
        db_table = 'Updaterecords'


class Ticket(models.Model):
    Partner_Id = models.ForeignKey(Partner, on_delete=models.CASCADE,null=True,blank=True)
    Ticket_Related_to=models.CharField(max_length=100)
    Ticket_Id = models.CharField(max_length=20,default=Ticket_I)
    Details=models.CharField(max_length=500 ,blank=True)
    Severity=models.CharField(max_length=300, blank=True)
    Created_Date=models.DateField(default=date.today())
    Created_by=models.ForeignKey(User, on_delete=models.CASCADE, null=True,related_name="Ticketcreated_by")
    Updated_Date=models.DateField(default=date.today())
    Updated_By=models.ForeignKey(User, on_delete=models.CASCADE, null=True,related_name="TicketUpdated_By")
    Is_Deleted=models.BooleanField(default=False)
    Status=models.BooleanField(default=False)

    class Meta:
        db_table= "Ticket"

    
class AssigningTicket(models.Model):
    Partner_Id = models.ForeignKey(Partner, on_delete=models.CASCADE,null=True,blank=True)
    # Ticket_No= models.IntegerField()
    Ticket_Id = models.ForeignKey(Ticket,on_delete=models.CASCADE,null=True,blank=True)
    AssigningTicket_Id = models.CharField(max_length=20,default=AssigningTicket_I)
    Raised_by=models.CharField(max_length=300, blank=True)
    Date_Time=models.DateField(default=date.today())
    Severity=models.CharField(max_length=300, blank=True)
    Details=models.CharField(max_length=500 ,blank=True)
    Assigned_to=models.CharField(max_length=500 ,blank=True)
    Expected_time=models.IntegerField()
    Created_Date=models.DateField(default=date.today())
    Created_by=models.ForeignKey(User, on_delete=models.CASCADE, null=True,related_name="TicketCreated")
    Updated_Date=models.DateField(default=date.today())
    Updated_By=models.ForeignKey(User, on_delete=models.CASCADE, null=True,related_name="TicketUpdated")
    Is_Deleted=models.BooleanField(default=False)
    Notes=models.CharField(max_length=500, blank=True)

    class Meta:
        db_table= "AssigningTicket"


class TicketResponse(models.Model):
    TicketResponse_Id = models.CharField(max_length=20,default=TicketResponse_I)
    Partner_Id = models.ForeignKey(Partner, on_delete=models.CASCADE,null=True,blank=True)
    TicketRelateTo=models.CharField(max_length=300, blank=True)
    # ElapsedTime=models.DateField(default=date.today())
    Ticket_No= models.IntegerField()
    ElapsedTime=models.TimeField(default=timezone.now())
    TicketDescription=models.CharField(max_length=500, blank=True)
    Response=models.CharField(max_length=500, blank=True)
    TicketSeverity=models.CharField(max_length=500, blank=True)
    TicketStatus=models.CharField(max_length=500, blank=True)
    Created_Date=models.DateField(default=date.today())
    Created_By=models.ForeignKey(User, on_delete=models.CASCADE, null=True,related_name="CreatedB")
    Updated_Date=models.DateField(default=date.today())
    Updated_By=models.ForeignKey(User, on_delete=models.CASCADE, null=True,related_name="UpdatedB")
    Is_Deleted=models.BooleanField(default=False)

    class Meta:
        db_table= "TicketResponse"


class Contacts(models.Model):
    Contact_Id = models.CharField(max_length=20,default=Contact_I,unique=True)
    Partner_Id = models.ForeignKey(Partner, on_delete=models.CASCADE,null=True,blank=True)
    Name =models.CharField(max_length=300, blank=True)
    AddressLine1= models.CharField(max_length=300, blank=True)
    AddressLine2 =models.CharField(max_length=300, blank=True)
    City=models.CharField(max_length=300, blank=True)
    State=models.CharField(max_length=300, blank=True)
    Country=models.CharField(max_length=300, blank=True)
    ContactNo=models.CharField(max_length=50,blank=True)
    EmailId=models.CharField(max_length=300, blank=True)
    Created_Date=models.DateField(default=date.today())
    Created_By=models.ForeignKey(User, on_delete=models.CASCADE, null=True,related_name="CreatedA")
    Updated_Date=models.DateField(default=date.today())
    Updated_By=models.ForeignKey(User, on_delete=models.CASCADE, null=True,related_name="UpdatedA")
    Is_Deleted=models.BooleanField(default=False)

    class Meta:
        db_table="Contacts"


class Employeecheckin(models.Model):
    Partner_Id = models.ForeignKey(Partner, on_delete=models.CASCADE,null=True,blank=True)
    Employeecheckin_Id=models.CharField(max_length=20,unique=True,default=Employeecheckin_I)
    Employee_Id=models.ForeignKey(EmployeeMaster,on_delete=models.CASCADE,blank=True,null=True)
    Employee_name=models.CharField(max_length=100,blank=True,null=True)
    Date = models.DateField(default=date.today())
    Checkin_Time = models.TimeField(blank=True,null=True)
    Checkout_Time = models.TimeField(blank=True,null=True)
    Location = models.CharField(max_length=500,blank=True,null=True)
    Shift = models.CharField(max_length=50,blank=True,null=True)
    Year = models.IntegerField(default=0)
    Month = models.CharField(max_length=100,null=True,blank=True)
    Working_Hours = models.IntegerField(default=0)
    Checkin_Flag = models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_Date = models.DateField(default=date.today())

    class Meta:
        db_table = "Employeecheckin"


class Employeeclaims(models.Model):
    Partner_Id = models.ForeignKey(Partner, on_delete=models.CASCADE,null=True,blank=True)
    Employeeclaim_Id = models.CharField(max_length=20,unique=True,default=Employeeclaim_I)
    Reimbursement_Type = models.CharField(max_length=250,blank=True,null=True)
    Bill_Number=models.IntegerField(default=0)
    Bill_Details = models.CharField(max_length=1000,blank=True,null=True)
    Claim_Amount = models.FloatField(default=0.0)
    Bills = models.JSONField(default=dict)
    Claim_Attachment = models.FileField(default='default.png')
    Approved_Flag=models.BooleanField(default=False)
    Status = models.BooleanField(default=False)
    Is_Deleted = models.BooleanField(default=False)
    Created_Date = models.DateField(default=date.today())
    Created_By=models.ForeignKey(User, on_delete=models.CASCADE, null=True,related_name="CreatedEmployeeclaims")
    Updated_Date=models.DateField(default=date.today())

    class Meta:
        db_table = "Employeeclaims"


class Lead(models.Model):
    Partner_Id = models.ForeignKey(Partner, on_delete=models.CASCADE,null=True,blank=True)
    Lead_Id = models.CharField(max_length=20,unique=True,default=Lead_I)
    First_Name = models.CharField(max_length=100,blank=True,null=True)
    Last_Name = models.CharField(max_length=100,blank=True,null=True)
    Company_Name = models.CharField(max_length=300,blank=True,null=True)
    Job_Title = models.CharField(max_length=100,blank=True,null=True)
    Phone_Number = models.CharField(max_length=100,blank=True,null=True)
    Email_Id = models.CharField(max_length=100,blank=True,null=True,unique=True)
    Street_Name = models.CharField(max_length=100,blank=True,null=True)
    Area_Name = models.CharField(max_length=100,blank=True,null=True)
    Building_Name = models.CharField(max_length=100,blank=True,null=True)
    Country = models.CharField(max_length=100,blank=True,null=True)
    State = models.CharField(max_length=100,blank=True,null=True)
    City = models.CharField(max_length=100,blank=True,null=True)
    Source = models.CharField(max_length=100,blank=True,null=True)
    Company_Turnover = models.IntegerField(default=0,blank=True,null=True)
    Dicission_Maker = models.CharField(max_length=100,blank=True,null=True)
    Lead_Industry = models.CharField(max_length=100,blank=True,null=True)
    No_Of_Employees = models.IntegerField(default=0,blank=True,null=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_Date = models.DateField(default=date.today())
    Created_By=models.ForeignKey(User, on_delete=models.CASCADE, null=True,related_name="Createlead")
    Updated_Date=models.DateField(default=date.today())
    Updated_By=models.ForeignKey(User, on_delete=models.CASCADE, null=True,related_name="Updatelead")

    class Meta:
        db_table = "Lead"



class Deal(models.Model):
    Partner_Id = models.ForeignKey(Partner, on_delete=models.CASCADE,null=True,blank=True)
    Deal_Id = models.CharField(max_length=20,unique=True,default=Deal_I)
    Lead_Id = models.ForeignKey(Lead, on_delete=models.CASCADE,null=True,blank=True)
    Deal_Name = models.CharField(max_length=300,blank=True,null=True)
    Lead_Name = models.CharField(max_length=300,blank=True,null=True)
    Account_Owner_Name = models.CharField(max_length=300,blank=True,null=True)
    Deal_Amount = models.IntegerField(default=0,blank=True,null=True)
    Deal_Stage = models.CharField(max_length=100,blank=True,null=True)
    Deal_Type = models.CharField(max_length=100,blank=True,null=True,unique=True)
    Contact_Persion = models.CharField(max_length=100,blank=True,null=True)
    Deal_Probablity = models.CharField(max_length=100,blank=True,null=True)
    Deal_Note = models.CharField(max_length=500,blank=True,null=True)
    Lead_Source = models.CharField(max_length=100,blank=True,null=True)
    Minimum_Turnarround = models.IntegerField(default=0,blank=True,null=True)
    Is_Deleted = models.BooleanField(default=False)
    Created_Date = models.DateField(default=date.today())
    Created_By=models.ForeignKey(User, on_delete=models.CASCADE, null=True,related_name="CreateDeal")
    Updated_Date=models.DateField(default=date.today())
    Updated_By=models.ForeignKey(User, on_delete=models.CASCADE, null=True,related_name="UpdateDeal")

    class Meta:
        db_table = "Deal"


