from django.contrib import admin
from .models import User
from Erpapp.models import *

# Register your models here.
# admin.site.register(User)
admin.site.register(Role)
admin.site.register(ItemMaster)
admin.site.register(RFQdetails)
admin.site.register(Supplier)
admin.site.register(Supplieraddress)
admin.site.register(QuoteDetails)
admin.site.register(Taxsetup)
admin.site.register(Chartofaccount)
admin.site.register(ItemGroup)
admin.site.register(PODetails)
admin.site.register(PRDetails)
admin.site.register(Groupcostcenter_or_Profitcenter)
admin.site.register(Department)
admin.site.register(QuotationItem)
admin.site.register(POAmendDetails)
admin.site.register(POCancelDetails)
admin.site.register(StoreAdjustment)
admin.site.register(RolePermission)
admin.site.register(MailConfiguration)
admin.site.register(MailMaster)
admin.site.register(ERPModules)
admin.site.register(ERPForms)
admin.site.register(Batchtable)
admin.site.register(Organization)
admin.site.register(Storemaster)
admin.site.register(Costcenter)
admin.site.register(Goodsinwardetail)
admin.site.register(GoodsTrfOutDetail)
admin.site.register(MaterialRequestDetail)
admin.site.register(MaterialIssueDetail)
admin.site.register(GoodsTrfReceiptDetails)
admin.site.register(Materialreturndetails)
admin.site.register(MaterialTransferDetails)
admin.site.register(StoresCreditNote)
admin.site.register(Entity)
admin.site.register(Country)
admin.site.register(EmployeeESI)
admin.site.register(EmployeeProfessionalTax)
admin.site.register(Region)
admin.site.register(EmployeeDeduction)
admin.site.register(EmployeeMaster)
admin.site.register(State)
admin.site.register(Skill)
admin.site.register(EmployeeAddress)
admin.site.register(Cadre)
admin.site.register(EmployeeEarning)
admin.site.register(Designation)
admin.site.register(Servicejob)
admin.site.register(Employeesalesmonthlytarget)
admin.site.register(Employeemonthlyusertarget)
admin.site.register(City)
admin.site.register(Company)
admin.site.register(EmployeeProvidentfund)
admin.site.register(Communication_Address)
admin.site.register(Financial_Calendar_Details)
admin.site.register(Business_Hours)
admin.site.register(FormMaster)
admin.site.register(Voucherdetail)
admin.site.register(Acctgroup)
admin.site.register(EmployeeAnnualCTC)
admin.site.register(Itempreference)
admin.site.register(Partner)
admin.site.register(SalesOrder)
admin.site.register(Customer)
admin.site.register(Territory)
admin.site.register(Services)
admin.site.register(CustomerAddress)
admin.site.register(Servicecategory)
admin.site.register(Configuration)
admin.site.register(AutogenNos)
admin.site.register(Notifications)
admin.site.register(Reminder)
admin.site.register(Templates)
admin.site.register(Workflow)
admin.site.register(Salesorderpreference)
admin.site.register(Invoicepreference)
admin.site.register(Purchasepreference)
admin.site.register(FormPermission)
admin.site.register(ERPUsers)
admin.site.register(ERPUsersActivity)
admin.site.register(Budgetpreference)
admin.site.register(Inventorypreference)
admin.site.register(Transactionapprovalpreference)
admin.site.register(UserLoginStatus)
admin.site.register(UserChangePwd)
admin.site.register(Plan)
admin.site.register(Zone)
admin.site.register(Dropdowntable)
admin.site.register(Subscription)
admin.site.register(Slot)
admin.site.register(Serviceprice)
admin.site.register(Territorysalestarget)
admin.site.register(Servicesalestarget)
admin.site.register(Salesforecast)
admin.site.register(Servicemonthlysalestarget)
admin.site.register(Monthlyusertarget)
admin.site.register(CustomerPaymentmethods)
admin.site.register(Invoice)
admin.site.register(Customerpayments)
admin.site.register(Customerfeedback)
admin.site.register(Customercomplains)
admin.site.register(Customerinvoicedetail)
admin.site.register(Customerticketresolution)
admin.site.register(Servicecancellations)
admin.site.register(Customerrefunds)
admin.site.register(CostCenterBudget)
admin.site.register(Supplierinvoicedetail)
admin.site.register(Accountingcalendar)
admin.site.register(Accountingcalendaritem)
admin.site.register(AccountSegment)
admin.site.register(Invoicedistributionitems)
admin.site.register(Journal)
admin.site.register(Contravoucher)
admin.site.register(MemoDetail)
admin.site.register(Schedules)
admin.site.register(HRcalendar)
admin.site.register(HRCalendarmonths)
admin.site.register(Organisationstructurelevels)
admin.site.register(EmployeeQualification)
admin.site.register(EmployeeExperience)
admin.site.register(Qualification)
admin.site.register(Employeealocation)
admin.site.register(JobAlocationMaster)
admin.site.register(Jobtimecard)
admin.site.register(Leavetype)
admin.site.register(LeaveEmployeeRecord)
admin.site.register(Leave)
admin.site.register(Leavecancellation)
admin.site.register(LeaveExtension)
admin.site.register(TimeSheet)
admin.site.register(TimeManagement)
admin.site.register(AttandanceManagement)
admin.site.register(Payrollsetup)
admin.site.register(Payelements)
admin.site.register(Employeepayelement)
admin.site.register(Paycalculationformula)
admin.site.register(PayCalcElements)
admin.site.register(PayslipHeader)
admin.site.register(ServiceJobDetails)
admin.site.register(Supplierpricedetails)
admin.site.register(Buyermaster)
admin.site.register(Catalogue)
admin.site.register(Inventoryconfiguration)
admin.site.register(MaterialGroup)
admin.site.register(StoreItems)
admin.site.register(OrganizationStructure)
admin.site.register(Budget)
admin.site.register(Distributionbudget)
admin.site.register(Budgethead)
admin.site.register(Approval)
admin.site.register(Documentapproval)
admin.site.register(Payrollconfiguration)
admin.site.register(ExpenseBudget)
admin.site.register(SalesuserTarget)
admin.site.register(SubscriberTarget)
admin.site.register(WorkflowName)
admin.site.register(Assignworkflow)
admin.site.register(WorkflowRule)
admin.site.register(Transactionalseries)
admin.site.register(Assets)
admin.site.register(Equity)
admin.site.register(Liability)
admin.site.register(BusinessUnit)
admin.site.register(Leaveopeningbalance)
admin.site.register(Paycalendar)
admin.site.register(Holidaylist)
admin.site.register(Attandancemaster)
admin.site.register(RoasterMaster)
admin.site.register(Bank)
admin.site.register(Bankbookentry)
admin.site.register(Updaterecords)
admin.site.register(Emailattachment)
admin.site.register(Car)
admin.site.register(ItemCategory)
admin.site.register(subscriptioncustomer)
admin.site.register(ConsolidatedBudget)
admin.site.register(CentralBudget)
admin.site.register(Ticket)
admin.site.register(AssigningTicket)
admin.site.register(TicketResponse)
admin.site.register(Contacts)
admin.site.register(Employeecheckin)
admin.site.register(Employeeclaims)
admin.site.register(Lead)
admin.site.register(Deal)
