
from django.contrib.contenttypes.models import ContentType
from rest_framework import serializers

from .models import (Accountingcalendar, Accountingcalendaritem,
                     AccountSegment, Acctgroup, Approval, Assets, AssigningTicket,
                     Assignworkflow, AttandanceManagement, Attandancemaster,
                     AutogenNos, Bank, Bankbookentry, Batchtable, Budget, Budgethead, Budgetpreference,
                     Business_Hours, BusinessUnit, Buyermaster, Cadre, Car,
                     Catalogue, CentralBudget, Chartofaccount, City, Communication_Address,
                     Company, Configuration, ConsolidatedBudget, Contacts, Contravoucher, Costcenter,
                     CostCenterBudget, Country, Customer, CustomerAddress,
                     Customercomplains, Customerfeedback,
                     Customerinvoicedetail, CustomerPaymentmethods,
                     Customerpayments, Customerrefunds,
                     Customerticketresolution, Deal, Department, Designation,
                     Distributionbudget, Documentapproval, Dropdowntable, Emailattachment,
                     EmployeeAddress, Employeealocation, EmployeeAnnualCTC,
                     EmployeeDeduction, EmployeeEarning, EmployeeESI,
                     EmployeeExperience, EmployeeMaster, Employeecheckin, Employeeclaims,
                     Employeemonthlyusertarget, Employeepayelement,
                     EmployeeProfessionalTax, EmployeeProvidentfund,
                     EmployeeQualification, Employeesalesmonthlytarget, Entity,
                     Equity, ERPForms, ERPModules, ERPUsers, ERPUsersActivity,
                     ErrorCodes, ExpenseBudget, Financial_Calendar_Details,
                     FormMaster, FormPermission, Goodsinwardetail,
                     GoodsTrfOutDetail, GoodsTrfReceiptDetails, Group,
                     Groupcostcenter_or_Profitcenter, Holidaylist, HRcalendar,
                     HRCalendarmonths, Inventoryconfiguration,
                     Inventorypreference, Invoice, Invoicedistributionitems,
                     Invoicepreference, ItemCategory, ItemGroup, ItemMaster, Itempreference,
                     JobAlocationMaster, Jobtimecard, Journal, Lead, Leave,
                     Leavecancellation, LeaveEmployeeRecord, LeaveExtension,
                     Leaveopeningbalance, Leavetype, Liability,
                     MailConfiguration, MailMaster, MaterialGroup,
                     MaterialIssueDetail, MaterialRequestDetail,
                     Materialreturndetails, MaterialTransferDetails,
                     MemoDetail, Monthlyusertarget, Notifications,
                     Organisationstructurelevels, Organization, Partner,
                     PartnerMaster, PayCalcElements, Paycalculationformula,
                     Paycalendar, Payelements, Payrollconfiguration,
                     Payrollsetup, PayslipHeader, Permission, Pincodetable,
                     Plan, POAmendDetails, POCancelDetails, PODetails,
                     PRDetails, Purchasepreference, Qualification,
                     QuotationItem, QuoteDetails, Region, RFQdetails, Reminder,
                     RoasterMaster, Role, RolePermission, SalesOrder, Salesforecast,
                     Salesorderpreference, SalesuserTarget, Schedules,
                     Servicecancellations, Servicecategory, Servicejob,
                     ServiceJobDetails, Servicemonthlysalestarget,
                     Serviceprice, Services, Servicesalestarget, Skill, Slot,
                     State, StoreAdjustment, StoreItems, Storemaster,
                     StoresCreditNote, SubscriberTarget, Subscription,
                     Supplier, Supplieraddress, Supplierinvoicedetail,
                     Supplierpricedetails, Taxsetup, Templates, Territory,
                     Territorysalestarget, Ticket, TicketResponse, TimeManagement, TimeSheet,
                     Transactionalseries, Transactionapprovalpreference, Updaterecords, User,
                     UserChangePwd, UserLoginStatus, Voucherdetail, Workflow,
                     WorkflowName, Workfloworigin, WorkflowRule, Zone, subscriptioncustomer)


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model=User
        fields='__all__'


class ConfigurationSerializer(serializers.ModelSerializer):

    class Meta:
        model=Configuration
        fields='__all__'


class ConfigurationSerializers(serializers.ModelSerializer):

    class Meta:
        model=Configuration
        fields='__all__'
        depth = 1



class OrganizationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organization
        fields = '__all__'


class OrganizationSerializers(serializers.ModelSerializer):
    class Meta:
        model = Organization
        fields = '__all__'  
        depth = 1

class PartnerSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Partner
        fields = '__all__'

class PartnerSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Partner
        fields = '__all__'
        depth = 1    

class Communication_AddressSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Communication_Address
        fields = '__all__'

 
class Business_HoursSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Business_Hours
        fields = '__all__'

class Business_HoursSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Business_Hours
        fields = '__all__'
        depth = 1


class ERPModulesSerialzer(serializers.ModelSerializer):
    class Meta:
        model = ERPModules
        fields = '__all__'


class ERPModulesSerialzers(serializers.ModelSerializer):
    class Meta:
        model = ERPModules
        fields = '__all__'
        depth = 1


class ERPFormsSerialzer(serializers.ModelSerializer):
    class Meta:
        model = ERPForms
        fields = '__all__'


class ERPFormsSerialzers(serializers.ModelSerializer):
    class Meta:
        model = ERPForms
        fields = '__all__'
        depth = 1


class AutogenNosSerialzer(serializers.ModelSerializer):
    class Meta:
        model = AutogenNos
        fields = '__all__'


class ErrorCodesSerialzer(serializers.ModelSerializer):
    class Meta:
        model = ErrorCodes
        fields = '__all__'



class NotificationsSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Notifications
        fields = '__all__'


class NotificationsSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Notifications
        fields = '__all__'
        depth = 1


class ReminderSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Reminder
        fields = '__all__'

class ReminderSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Reminder
        fields = '__all__'
        depth = 1


class Financial_Calendar_DetailsSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Financial_Calendar_Details
        fields = '__all__'


class Financial_Calendar_DetailsSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Financial_Calendar_Details
        fields = '__all__'
        depth = 1


class TerritorySerialzer(serializers.ModelSerializer):
    class Meta:
        model = Territory
        fields = '__all__'


class TerritorySerialzers(serializers.ModelSerializer):
    class Meta:
        model = Territory
        fields = '__all__'
        depth = 1



class TemplatesSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Templates
        fields = '__all__'


class WorkflowRuleSerialzer(serializers.ModelSerializer):
    class Meta:
        model = WorkflowRule
        fields = '__all__'


class WorkflowRuleSerialzers(serializers.ModelSerializer):
    class Meta:
        model = WorkflowRule
        fields = '__all__'
        depth = 1


class WorkflowNameSerialzer(serializers.ModelSerializer):
    class Meta:
        model = WorkflowName
        fields = '__all__'


class WorkflowNameSerialzers(serializers.ModelSerializer):
    class Meta:
        model = WorkflowName
        fields = '__all__'
        depth = 1



class AssignworkflowSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Assignworkflow
        fields = '__all__'


class AssignworkflowSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Assignworkflow
        fields = '__all__'
        depth = 1

class WorkflowSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Workflow
        fields = '__all__'


class WorkflowSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Workflow
        fields = '__all__'
        depth = 1


class WorkfloworiginSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Workfloworigin
        fields = '__all__'


class WorkfloworiginSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Workfloworigin
        fields = '__all__'
        depth = 1


class RoleSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Role
        fields = '__all__'


class RoleSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Role
        fields = '__all__'
        depth = 1


class RolePermissionSerialzer(serializers.ModelSerializer):
    class Meta:
        model = RolePermission
        fields = '__all__'


class RolePermissionSerialzers(serializers.ModelSerializer):
    class Meta:
        model = RolePermission
        fields = '__all__'
        depth = 1


class ERPUsersSerialzer(serializers.ModelSerializer):
    class Meta:
        model = ERPUsers
        fields = '__all__'


class ERPUsersSerialzers(serializers.ModelSerializer):
    class Meta:
        model = ERPUsers
        fields = '__all__'
        depth = 1


class ERPUsersActivitySerialzer(serializers.ModelSerializer):
    class Meta:
        model = ERPUsersActivity
        fields = '__all__'



class UserLoginStatusSerialzer(serializers.ModelSerializer):
    class Meta:
        model = UserLoginStatus
        fields = '__all__'



class UserChangePwdSerialzer(serializers.ModelSerializer):
    class Meta:
        model = UserChangePwd
        fields = '__all__'

class QuotationItemSerialzer(serializers.ModelSerializer):
    class Meta:
        model = QuotationItem
        fields = '__all__'


class ItemGroupSerialzer(serializers.ModelSerializer):
    class Meta:
        model = ItemGroup
        fields = '__all__'
        
class ItemGroupSerialzers(serializers.ModelSerializer):
    class Meta:
        model = ItemGroup
        fields = '__all__'
        depth = 1


class ItemCategorySerialzer(serializers.ModelSerializer):
    class Meta:
        model = ItemCategory
        fields = '__all__'
        
class ItemCategorySerialzers(serializers.ModelSerializer):
    class Meta:
        model = ItemCategory
        fields = '__all__'
        depth = 1


class ItemMasterSerialzer(serializers.ModelSerializer):
    class Meta:
        model = ItemMaster
        fields = '__all__'
        


class ItemMasterSerialzers(serializers.ModelSerializer):
    class Meta:
        model = ItemMaster
        fields = '__all__'
        depth = 1


class PartnerMasterSerialzer(serializers.ModelSerializer):
    class Meta:
        model = PartnerMaster
        fields = '__all__'


class PartnerMasterSerialzers(serializers.ModelSerializer):
    class Meta:
        model = PartnerMaster
        fields = '__all__'
        depth = 1


class BusinessUnitSerialzer(serializers.ModelSerializer):
    class Meta:
        model = BusinessUnit
        fields = '__all__'


class BusinessUnitSerialzers(serializers.ModelSerializer):
    class Meta:
        model = BusinessUnit
        fields = '__all__'
        depth = 1


class ServicecategorySerialzer(serializers.ModelSerializer):
    class Meta:
        model = Servicecategory
        fields = '__all__'

class ServicecategorySerialzers(serializers.ModelSerializer):
    class Meta:
        model = Servicecategory
        fields = '__all__'
        depth = 1


class ServicesSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Services
        fields = '__all__'


class ServicesSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Services
        fields = '__all__'
        depth = 1


class ServicepriceSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Serviceprice
        fields = '__all__'



class ServicepriceSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Serviceprice
        fields = '__all__'
        depth = 1


class TaxsetupSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Taxsetup
        fields = '__all__'


class TaxsetupSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Taxsetup
        fields = '__all__'
        depth = 1


class TerritorysalestargetSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Territorysalestarget
        fields = '__all__'


class TerritorysalestargetSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Territorysalestarget
        fields = '__all__'
        depth = 1


class ServicesalestargetSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Servicesalestarget
        fields = '__all__'


class ServicesalestargetSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Servicesalestarget
        fields = '__all__'
        depth = 1


class ServicemonthlysalestargetSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Servicemonthlysalestarget
        fields = '__all__'


class ServicemonthlysalestargetSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Servicemonthlysalestarget
        fields = '__all__'
        depth = 1


class MonthlyusertargetSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Monthlyusertarget
        fields = '__all__'


class MonthlyusertargetSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Monthlyusertarget
        fields = '__all__'
        depth = 1


class CustomerAddressSerialzer(serializers.ModelSerializer):
    class Meta:
        model = CustomerAddress
        fields = '__all__'


class CustomerSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = '__all__'


class CustomerSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = '__all__'
        depth = 1

class CustomerPaymentmethodsSerialzer(serializers.ModelSerializer):
    class Meta:
        model = CustomerPaymentmethods
        fields = '__all__'


class CustomerPaymentmethodsSerialzers(serializers.ModelSerializer):
    class Meta:
        model = CustomerPaymentmethods
        fields = '__all__'
        depth = 1


class SalesOrderSerialzer(serializers.ModelSerializer):
    class Meta:
        model = SalesOrder
        fields = '__all__'

class SalesOrderSerialzers(serializers.ModelSerializer):
    class Meta:
        model = SalesOrder
        fields = '__all__'
        depth = 1

class InvoiceSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Invoice
        fields = '__all__'


class InvoiceSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Invoice
        fields = '__all__'
        depth = 1


class CustomerpaymentsSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Customerpayments
        fields = '__all__'


class CustomerpaymentsSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Customerpayments
        fields = '__all__'
        depth = 1

class CustomerfeedbackSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Customerfeedback
        fields = '__all__'


class CustomercomplainsSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Customercomplains
        fields = '__all__'


class CustomerticketresolutionSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Customerticketresolution
        fields = '__all__'


class ServicecancellationsSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Servicecancellations
        fields = '__all__'

class ServicecancellationsSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Servicecancellations
        fields = '__all__'
        depth = 1
        

class CustomerrefundsSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Customerrefunds
        fields = '__all__'

class CustomerrefundsSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Customerrefunds
        fields = '__all__'
        depth = 1


class Groupcostcenter_or_ProfitcenterSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Groupcostcenter_or_Profitcenter
        fields = '__all__'


class CostcenterSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Costcenter
        fields = '__all__'


class CostcenterSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Costcenter
        fields = '__all__'
        depth = 1


class CostCenterBudgetSerialzer(serializers.ModelSerializer):
    class Meta:
        model = CostCenterBudget
        fields = '__all__'


class CostCenterBudgetSerialzers(serializers.ModelSerializer):
    class Meta:
        model = CostCenterBudget
        fields = '__all__'
        depth = 1


class ServiceJobDetailsSerialzer(serializers.ModelSerializer):
    class Meta:
        model = ServiceJobDetails
        fields = '__all__'


class ServiceJobDetailsSerialzers(serializers.ModelSerializer):
    class Meta:
        model = ServiceJobDetails
        fields = '__all__'
        depth = 1


class AccountingcalendarSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Accountingcalendar
        fields = '__all__'


class AccountingcalendaritemSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Accountingcalendaritem
        fields = '__all__'


class AccountSegmentSerialzer(serializers.ModelSerializer):
    class Meta:
        model = AccountSegment
        fields = '__all__'



class AccountSegmentSerialzers(serializers.ModelSerializer):
    class Meta:
        model = AccountSegment
        fields = '__all__'
        depth = 1


class ChartofaccountSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Chartofaccount
        fields = '__all__'


class ChartofaccountSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Chartofaccount
        fields = '__all__'
        depth = 1


class BankSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Bank
        fields = '__all__'


class BankSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Bank
        fields = '__all__'
        depth = 1


class BankbookentrySerialzer(serializers.ModelSerializer):
    class Meta:
        model = Bankbookentry
        fields = '__all__'


class BankbookentrySerialzers(serializers.ModelSerializer):
    class Meta:
        model = Bankbookentry
        fields = '__all__'
        depth = 1



class AcctgroupSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Acctgroup
        fields = '__all__'




class InvoicedistributionitemsSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Invoicedistributionitems
        fields = '__all__'


class InvoicedistributionitemsSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Invoicedistributionitems
        fields = '__all__'
        depth = 1


class CustomerinvoicedetailSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Customerinvoicedetail
        fields = '__all__'


class CustomerinvoicedetailSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Customerinvoicedetail
        fields = '__all__'
        depth = 1


class SupplierinvoicedetailSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Supplierinvoicedetail
        fields = '__all__'


class SupplierinvoicedetailSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Supplierinvoicedetail
        fields = '__all__'
        depth = 1


class VoucherdetailSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Voucherdetail
        fields = '__all__'


class VoucherdetailSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Voucherdetail
        fields = '__all__'
        depth = 1



class ContravoucherSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Contravoucher
        fields = '__all__'


class ContravoucherSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Contravoucher
        fields = '__all__'
        depth = 1



class JournalSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Journal
        fields = '__all__'


class JournalSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Journal
        fields = '__all__'
        depth = 1

        

class MemoDetailSerialzer(serializers.ModelSerializer):
    class Meta:
        model = MemoDetail
        fields = '__all__'


class MemoDetailSerialzers(serializers.ModelSerializer):
    class Meta:
        model = MemoDetail
        fields = '__all__'
        depth = 1


class SchedulesSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Schedules
        fields = '__all__'


class SchedulesSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Schedules
        fields = '__all__'
        depth = 1


class HRcalendarSerialzer(serializers.ModelSerializer):
    class Meta:
        model = HRcalendar
        fields = '__all__'



class HRCalendarmonthsSerialzer(serializers.ModelSerializer):
    class Meta:
        model = HRCalendarmonths
        fields = '__all__'


class OrganisationstructurelevelsSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Organisationstructurelevels
        fields = '__all__'



class EmployeeAddressSerialzer(serializers.ModelSerializer):
    class Meta:
        model = EmployeeAddress
        fields = '__all__'



class EmployeeMasterSerialzer(serializers.ModelSerializer):
    class Meta:
        model = EmployeeMaster
        fields = '__all__'



class EmployeeMasterSerialzers(serializers.ModelSerializer):
    class Meta:
        model = EmployeeMaster
        fields = '__all__'
        depth = 1


class EmployeeQualificationSerialzer(serializers.ModelSerializer):
    class Meta:
        model = EmployeeQualification
        fields = '__all__'


class EmployeeQualificationSerialzers(serializers.ModelSerializer):
    class Meta:
        model = EmployeeQualification
        fields = '__all__'
        depth = 1


class EmployeeExperienceSerialzer(serializers.ModelSerializer):
    class Meta:
        model = EmployeeExperience
        fields = '__all__'



class EmployeeExperienceSerialzers(serializers.ModelSerializer):
    class Meta:
        model = EmployeeExperience
        fields = '__all__'
        depth = 1



class EmployeesalesmonthlytargetSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Employeesalesmonthlytarget
        fields = '__all__'


class EmployeesalesmonthlytargetSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Employeesalesmonthlytarget
        fields = '__all__'
        depth = 1


class EmployeemonthlyusertargetSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Employeemonthlyusertarget
        fields = '__all__'


class EmployeemonthlyusertargetSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Employeemonthlyusertarget
        fields = '__all__'
        depth = 1


class ServicejobSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Servicejob
        fields = '__all__'



class ServicejobSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Servicejob
        fields = '__all__'
        depth = 1



class DepartmentSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = '__all__'


class DepartmentSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = '__all__'
        depth = 1


class SkillSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Skill
        fields = '__all__'



class SkillSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Skill
        fields = '__all__'
        depth = 1


class CadreSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Cadre
        fields = '__all__'



class CadreSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Cadre
        fields = '__all__'
        depth = 1


class DesignationSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Designation
        fields = '__all__'



class DesignationSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Designation
        fields = '__all__'
        depth = 1


class QualificationSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Qualification
        fields = '__all__'



class QualificationSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Qualification
        fields = '__all__'
        depth = 1


class LeavetypeSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Leavetype
        fields = '__all__'


class LeavetypeSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Leavetype
        fields = '__all__'
        depth = 1


class LeaveEmployeeRecordSerialzer(serializers.ModelSerializer):
    class Meta:
        model = LeaveEmployeeRecord
        fields = '__all__'


class LeaveSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Leave
        fields = '__all__'

class LeaveSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Leave
        fields = '__all__'
        depth = 1




class AttandancemasterSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Attandancemaster
        fields = '__all__'


class AttandancemasterSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Attandancemaster
        fields = '__all__'
        depth = 1



class HolidaylistSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Holidaylist
        fields = '__all__'


class HolidaylistSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Holidaylist
        fields = '__all__'
        depth = 1


class PaycalendarSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Paycalendar
        fields = '__all__'


class PaycalendarSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Paycalendar
        fields = '__all__'
        depth = 1



class LeaveopeningbalanceSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Leaveopeningbalance
        fields = '__all__'


class LeaveopeningbalanceSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Leaveopeningbalance
        fields = '__all__'
        depth = 1



class LeavecancellationSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Leavecancellation
        fields = '__all__'


class LeavecancellationSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Leavecancellation
        fields = '__all__'
        depth = 1


class LeaveExtensionSerialzer(serializers.ModelSerializer):
    class Meta:
        model = LeaveExtension
        fields = '__all__'



class LeaveExtensionSerialzers(serializers.ModelSerializer):
    class Meta:
        model = LeaveExtension
        fields = '__all__'
        depth = 1



class PayrollsetupSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Payrollsetup
        fields = '__all__'


class PayrollsetupSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Payrollsetup
        fields = '__all__'
        depth = 1



class PayelementsSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Payelements
        fields = '__all__'


class EmployeepayelementSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Employeepayelement
        fields = '__all__'


class PaycalculationformulaSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Paycalculationformula
        fields = '__all__'



class PayCalcElementsSerialzer(serializers.ModelSerializer):
    class Meta:
        model = PayCalcElements
        fields = '__all__'



class PayslipHeaderSerialzer(serializers.ModelSerializer):
    class Meta:
        model = PayslipHeader
        fields = '__all__'



class PayslipHeaderSerialzers(serializers.ModelSerializer):
    class Meta:
        model = PayslipHeader
        fields = '__all__'
        depth = 1


class SupplieraddressSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Supplieraddress
        fields = '__all__'



class SupplierSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Supplier
        fields = '__all__'



class SupplierSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Supplier
        fields = '__all__'
        depth = 1



class PRDetailsSerialzer(serializers.ModelSerializer):
    class Meta:
        model = PRDetails
        fields = '__all__'


class PRDetailsSerialzers(serializers.ModelSerializer):
    class Meta:
        model = PRDetails
        fields = '__all__'
        depth = 1


class RFQdetailsSerialzer(serializers.ModelSerializer):
    class Meta:
        model = RFQdetails
        fields = '__all__'



class RFQdetailsSerialzers(serializers.ModelSerializer):
    class Meta:
        model = RFQdetails
        fields = '__all__'
        depth = 1


class QuoteDetailsSerialzer(serializers.ModelSerializer):
    class Meta:
        model = QuoteDetails
        fields = '__all__'
        


class QuoteDetailsSerialzers(serializers.ModelSerializer):
    class Meta:
        model = QuoteDetails
        fields = '__all__'
        depth = 1


class SupplierpricedetailsSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Supplierpricedetails
        fields = '__all__'


class SupplierpricedetailsSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Supplierpricedetails
        fields = '__all__'
        depth = 1


class BuyermasterSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Buyermaster
        fields = '__all__'


class BuyermasterSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Buyermaster
        fields = '__all__'
        depth = 1

class POCancelDetailsSerialzer(serializers.ModelSerializer):
    class Meta:
        model = POCancelDetails
        fields = '__all__'


class POCancelDetailsSerialzers(serializers.ModelSerializer):
    class Meta:
        model = POCancelDetails
        fields = '__all__'
        depth = 1

class POAmendDetailsSerialzer(serializers.ModelSerializer):
    class Meta:
        model = POAmendDetails
        fields = '__all__'


class POAmendDetailsSerialzers(serializers.ModelSerializer):
    class Meta:
        model = POAmendDetails
        fields = '__all__'
        depth = 1

class PODetailsSerialzer(serializers.ModelSerializer):
    class Meta:
        model = PODetails
        fields = '__all__'


class PODetailsSerialzers(serializers.ModelSerializer):
    class Meta:
        model = PODetails
        fields = '__all__'
        depth = 1


class InventoryconfigurationSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Inventoryconfiguration
        fields = '__all__'


class MaterialGroupSerialzer(serializers.ModelSerializer):
    class Meta:
        model = MaterialGroup
        fields = '__all__'



class StoremasterSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Storemaster
        fields = '__all__'


class StoremasterSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Storemaster
        fields = '__all__'
        depth = 1


class StoreItemsSerialzer(serializers.ModelSerializer):
    class Meta:
        model = StoreItems
        fields = '__all__'


class StoreItemsSerialzers(serializers.ModelSerializer):
    class Meta:
        model = StoreItems
        fields = '__all__'
        depth = 1

class StoreAdjustmentSerialzer(serializers.ModelSerializer):
    class Meta:
        model = StoreAdjustment
        fields = '__all__'


class StoreAdjustmentSerialzers(serializers.ModelSerializer):
    class Meta:
        model = StoreAdjustment
        fields = '__all__'
        depth = 1


class GoodsinwardetailSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Goodsinwardetail
        fields = '__all__'


class GoodsinwardetailSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Goodsinwardetail
        fields = '__all__'
        depth = 1


class GoodsTrfOutDetailSerialzer(serializers.ModelSerializer):
    class Meta:
        model = GoodsTrfOutDetail
        fields = '__all__'



class GoodsTrfReceiptDetailsSerialzer(serializers.ModelSerializer):
    class Meta:
        model = GoodsTrfReceiptDetails
        fields = '__all__'


class GoodsTrfReceiptDetailsSerialzers(serializers.ModelSerializer):
    class Meta:
        model = GoodsTrfReceiptDetails
        fields = '__all__'
        depth = 1


class MaterialRequestDetailSerialzer(serializers.ModelSerializer):
    class Meta:
        model = MaterialRequestDetail
        fields = '__all__'



class MaterialRequestDetailSerialzers(serializers.ModelSerializer):
    class Meta:
        model = MaterialRequestDetail
        fields = '__all__'
        depth = 1

class MaterialRequestfilter(serializers.ModelSerializer):
    class Meta:
        model = MaterialRequestDetail
        fields = ['MatReq_Id']


class MaterialIssueDetailSerialzer(serializers.ModelSerializer):
    class Meta:
        model = MaterialIssueDetail
        fields = '__all__'


class MaterialIssueDetailSerialzers(serializers.ModelSerializer):
    class Meta:
        model = MaterialIssueDetail
        fields = '__all__'
        depth = 1



class MaterialreturndetailsSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Materialreturndetails
        fields = '__all__'



class MaterialreturndetailsSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Materialreturndetails
        fields = '__all__'
        depth = 1


class StoresCreditNoteSerialzer(serializers.ModelSerializer):
    class Meta:
        model = StoresCreditNote
        fields = '__all__'
        

class StoresCreditNoteSerialzers(serializers.ModelSerializer):
    class Meta:
        model = StoresCreditNote
        fields = '__all__'
        depth = 1


# New PErmissions
class GroupSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = '__all__'
        

class GroupSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = '__all__'
        depth = 1

class PermissionSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Permission
        fields = '__all__'
        

class PermissionSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Permission
        fields = '__all__'
        depth = 1

class ContentTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContentType
        fields = ['id', 'model']


# Company Serializer

class CompanySerialzer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = '__all__'
class CompanySerialzers(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = '__all__'
        depth = 1


class MaterialTransferDetailsSerialzer(serializers.ModelSerializer):
    class Meta:
        model = MaterialTransferDetails
        fields = '__all__'
class MaterialTransferDetailsSerialzers(serializers.ModelSerializer):
    class Meta:
        model = MaterialTransferDetails
        fields = '__all__'
        depth = 1

class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = '__all__'


#ponkumar
class EntitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Entity
        fields = '__all__'
#ponkumar
class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = '__all__'
#ponkumar
class RegionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Region
        fields = '__all__'
#ponkumar
class StateSerializer(serializers.ModelSerializer):
    class Meta:
        model = State
        fields = '__all__'

class MailMasterSerializer(serializers.ModelSerializer):
    class Meta:
        model = MailMaster
        fields = '__all__'


#ponkumar
class MailConfigurationSerializer(serializers.ModelSerializer):
    class Meta:
        model = MailConfiguration
        fields = '__all__'

class AttandanceManagementSerialzer(serializers.ModelSerializer):
    class Meta:
        model = AttandanceManagement
        fields = '__all__'
        

class AttandanceManagementSerialzers(serializers.ModelSerializer):
    class Meta:
        model = AttandanceManagement
        fields = '__all__'
        depth = 1


class EmployeeAnnualCTCSerialzer(serializers.ModelSerializer):
    class Meta:
        model = EmployeeAnnualCTC
        fields = '__all__'
        

class EmployeeAnnualCTCSerialzers(serializers.ModelSerializer):
    class Meta:
        model = EmployeeAnnualCTC
        fields = '__all__'
        depth = 1


class EmployeeDeductionSerialzer(serializers.ModelSerializer):
    class Meta:
        model = EmployeeDeduction
        fields = '__all__'
        

class EmployeeDeductionSerialzers(serializers.ModelSerializer):
    class Meta:
        model = EmployeeDeduction
        fields = '__all__'
        depth = 1


class EmployeeESISerialzer(serializers.ModelSerializer):
    class Meta:
        model = EmployeeESI
        fields = '__all__'
        

class EmployeeESISerialzers(serializers.ModelSerializer):
    class Meta:
        model = EmployeeESI
        fields = '__all__'
        depth = 1




class EmployeeProfessionalTaxSerialzer(serializers.ModelSerializer):
    class Meta:
        model = EmployeeProfessionalTax
        fields = '__all__'
        

class EmployeeProfessionalTaxSerialzers(serializers.ModelSerializer):
    class Meta:
        model = EmployeeProfessionalTax
        fields = '__all__'
        depth = 1



class EmployeeEarningSerialzer(serializers.ModelSerializer):
    class Meta:
        model = EmployeeEarning
        fields = '__all__'
        

class EmployeeEarningSerialzers(serializers.ModelSerializer):
    class Meta:
        model = EmployeeEarning
        fields = '__all__'
        depth = 1

class CompanyeSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = '__all__'
        

class CompanyeSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = '__all__'
        depth = 1


class TimeManagementSerialzer(serializers.ModelSerializer):
    class Meta:
        model = TimeManagement
        fields = '__all__'
        

class TimeManagementSerialzers(serializers.ModelSerializer):
    class Meta:
        model = TimeManagement
        fields = '__all__'
        depth = 1


class EmployeeProvidentfundSerialzer(serializers.ModelSerializer):
    class Meta:
        model = EmployeeProvidentfund
        fields = '__all__'
        

class EmployeeProvidentfundSerialzers(serializers.ModelSerializer):
    class Meta:
        model = EmployeeProvidentfund
        fields = '__all__'
        depth = 1


class FormPermissionSerialzer(serializers.ModelSerializer):
    class Meta:
        model = FormPermission
        fields = '__all__'
        

class FormPermissionSerialzers(serializers.ModelSerializer):
    class Meta:
        model = FormPermission
        fields = '__all__'
        depth = 1


class CatalogueSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Catalogue
        fields = '__all__'
        

class CatalogueSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Catalogue
        fields = '__all__'
        depth = 1




class ItempreferenceSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Itempreference
        fields = '__all__'
        

class ItempreferenceSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Itempreference
        fields = '__all__'
        depth = 1


class SalesorderpreferenceSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Salesorderpreference
        fields = '__all__'
        

class SalesorderpreferenceSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Salesorderpreference
        fields = '__all__'
        depth = 1

class InvoicepreferenceSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Invoicepreference
        fields = '__all__'

class InvoicepreferenceSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Invoicepreference
        fields = '__all__'
        depth = 1


class PurchasepreferenceSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Purchasepreference
        fields = '__all__'

class PurchasepreferenceSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Purchasepreference
        fields = '__all__'
        depth = 1


class BudgetSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Budget
        fields = '__all__'

class BudgetSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Budget
        fields = '__all__'
        depth = 1


class ConsolidatedBudgetSerialzer(serializers.ModelSerializer):
    class Meta:
        model = ConsolidatedBudget
        fields = '__all__'

class ConsolidatedBudgetSerialzers(serializers.ModelSerializer):
    class Meta:
        model = ConsolidatedBudget
        fields = '__all__'
        depth = 1


class CentralBudgetSerialzer(serializers.ModelSerializer):
    class Meta:
        model = CentralBudget
        fields = '__all__'

class CentralBudgetSerialzers(serializers.ModelSerializer):
    class Meta:
        model = CentralBudget
        fields = '__all__'
        depth = 1

class BatchtableSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Batchtable
        fields = '__all__'

class BatchtableSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Batchtable
        fields = '__all__'
        depth = 1

class BudgetheadSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Budgethead
        fields = '__all__'

class BudgetheadSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Budgethead
        fields = '__all__'
        depth = 1



class DistributionbudgetSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Distributionbudget
        fields = '__all__'

class DistributionbudgetSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Distributionbudget
        fields = '__all__'
        depth = 1




class BudgetpreferenceSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Budgetpreference
        fields = '__all__'

class BudgetpreferenceSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Budgetpreference
        fields = '__all__'
        depth = 1


class InventorypreferenceSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Inventorypreference
        fields = '__all__'

class InventorypreferenceSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Inventorypreference
        fields = '__all__'
        depth = 1

        

class TransactionapprovalpreferenceSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Transactionapprovalpreference
        fields = '__all__'

class TransactionapprovalpreferenceSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Transactionapprovalpreference
        fields = '__all__'
        depth = 1

class PincodetableSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Pincodetable
        fields = '__all__'


class ZoneSerializer(serializers.ModelSerializer):
    class Meta:
        model = Zone
        fields = '__all__'




class FormMasterSerialzer(serializers.ModelSerializer):
    class Meta:
        model = FormMaster
        fields = '__all__'


class FormMasterSerialzers(serializers.ModelSerializer):
    class Meta:
        model = FormMaster
        fields = '__all__'
        depth = 1

class PlanSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Plan
        fields = '__all__'


class PlanSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Plan
        fields = '__all__'
        depth = 1

class SubscriptionSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Subscription
        fields = '__all__'


class SubscriptionSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Subscription
        fields = '__all__'
        depth = 1



class SlotSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Slot
        fields = '__all__'


class SlotSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Slot
        fields = '__all__'
        depth = 1



class EmployeealocationSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Employeealocation
        fields = '__all__'


class EmployeealocationSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Employeealocation
        fields = '__all__'
        depth = 1



class JobAlocationMasterSerialzer(serializers.ModelSerializer):
    class Meta:
        model = JobAlocationMaster
        fields = '__all__'


class JobAlocationMasterSerialzers(serializers.ModelSerializer):
    class Meta:
        model = JobAlocationMaster
        fields = '__all__'
        depth = 1


class RoasterMasterSerialzer(serializers.ModelSerializer):
    class Meta:
        model = RoasterMaster
        fields = '__all__'


class RoasterMasterSerialzers(serializers.ModelSerializer):
    class Meta:
        model = RoasterMaster
        fields = '__all__'
        depth = 1



class JobtimecardSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Jobtimecard
        fields = '__all__'


class JobtimecardSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Jobtimecard
        fields = '__all__'
        depth = 1



class DropdowntableSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Dropdowntable
        fields = '__all__'


class DropdowntableSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Dropdowntable
        fields = '__all__'
        depth = 1



class ApprovalSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Approval
        fields = '__all__'


class ApprovalSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Approval
        fields = '__all__'
        depth = 1



class DocumentapprovalSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Documentapproval
        fields = '__all__'


class DocumentapprovalSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Documentapproval
        fields = '__all__'
        depth = 1




class PayrollconfigurationSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Payrollconfiguration
        fields = '__all__'


class PayrollconfigurationSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Payrollconfiguration
        fields = '__all__'
        depth = 1




class SalesforecastSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Salesforecast
        fields = '__all__'


class SalesforecastSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Salesforecast
        fields = '__all__'
        depth = 1


class ExpenseBudgetSerialzer(serializers.ModelSerializer):
    class Meta:
        model = ExpenseBudget
        fields = '__all__'


class ExpenseBudgetSerialzers(serializers.ModelSerializer):
    class Meta:
        model = ExpenseBudget
        fields = '__all__'
        depth = 1


class SalesuserTargetSerialzer(serializers.ModelSerializer):
    class Meta:
        model = SalesuserTarget
        fields = '__all__'


class SalesuserTargetSerialzers(serializers.ModelSerializer):
    class Meta:
        model = SalesuserTarget
        fields = '__all__'
        depth = 1



class SubscriberTargetSerialzer(serializers.ModelSerializer):
    class Meta:
        model = SubscriberTarget
        fields = '__all__'


class SubscriberTargetSerialzers(serializers.ModelSerializer):
    class Meta:
        model = SubscriberTarget
        fields = '__all__'
        depth = 1



class TransactionalseriesSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Transactionalseries
        fields = '__all__'


class TransactionalseriesSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Transactionalseries
        fields = '__all__'
        depth = 1

class TimeSheetSerialzer(serializers.ModelSerializer):
    class Meta:
        model = TimeSheet
        fields = '__all__'


class TimeSheetSerialzers(serializers.ModelSerializer):
    class Meta:
        model = TimeSheet
        fields = '__all__'
        depth = 1


class AssetsSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Assets
        fields = '__all__'


class EquitySerialzer(serializers.ModelSerializer):
    class Meta:
        model = Equity
        fields = '__all__'


class LiabilitySerialzer(serializers.ModelSerializer):
    class Meta:
        model = Liability
        fields = '__all__'



class EmailattachmentSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Emailattachment
        fields = '__all__'


class EmailattachmentSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Emailattachment
        fields = '__all__'
        depth = 1



class UpdaterecordsSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Updaterecords
        fields = '__all__'

class CarSerialzer(serializers.ModelSerializer):
    class Meta:
        model = Car
        fields = '__all__'


class CarSerialzers(serializers.ModelSerializer):
    class Meta:
        model = Car
        fields = '__all__'
        depth = 1



class subscriptioncustomerSerialzer(serializers.ModelSerializer):
    class Meta:
        model = subscriptioncustomer
        fields = '__all__'


class subscriptioncustomerSerialzers(serializers.ModelSerializer):
    class Meta:
        model = subscriptioncustomer
        fields = '__all__'
        depth = 1


class TicketSerializer(serializers.ModelSerializer):
    class Meta:
        model= Ticket
        fields= "__all__"

class TicketSerializers(serializers.ModelSerializer):
    class Meta:
        model= Ticket
        fields= "__all__"
        depth=1
        
class AssigningTicketSerializer(serializers.ModelSerializer):
    class Meta:
        model= AssigningTicket
        fields= "__all__"

class AssigningTicketSerializerS(serializers.ModelSerializer):
    class Meta:
        model= AssigningTicket
        fields= "__all__"
        depth=1


class TicketResponseSerializer(serializers.ModelSerializer):
    class Meta:
        model= TicketResponse
        fields= "__all__"

class TicketResponseSerializerS(serializers.ModelSerializer):
    class Meta:
        model= TicketResponse
        fields= "__all__"
        depth=1


class ContactsSerializer(serializers.ModelSerializer):
    class Meta:
        model=Contacts
        fields="__all__"

        
class ContactsSerializerS(serializers.ModelSerializer):
    class Meta:
        model=Contacts
        fields="__all__"
        depth=1


class EmployeecheckinSerializer(serializers.ModelSerializer):
    class Meta:
        model=Employeecheckin
        fields='__all__'
        

class EmployeecheckinSerializers(serializers.ModelSerializer):
    class Meta:
        model=Employeecheckin
        fields='__all__'
        depth = 1
        

class EmployeeclaimsSerializer(serializers.ModelSerializer):
    class Meta:
        model=Employeeclaims
        fields='__all__'
        

class EmployeeclaimsSerializers(serializers.ModelSerializer):
    class Meta:
        model=Employeeclaims
        fields='__all__'
        depth = 1


class LeadSerializer(serializers.ModelSerializer):
    class Meta:
        model=Lead
        fields='__all__'
        

class LeadSerializers(serializers.ModelSerializer):
    class Meta:
        model=Lead
        fields='__all__'
        depth = 1


class DealSerializer(serializers.ModelSerializer):
    class Meta:
        model=Deal
        fields='__all__'
        

class DealSerializers(serializers.ModelSerializer):
    class Meta:
        model=Deal
        fields='__all__'
        depth = 1